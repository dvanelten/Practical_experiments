# Practical experiments

This project contains the practical experiments in physics (AP / FP) during the studies.

## Subprojects

- `AP`: practical experiments in Bachelor studies
- `FP`: practical experiments in Master studies

