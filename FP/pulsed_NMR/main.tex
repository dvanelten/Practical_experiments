\documentclass{scrartcl}
\usepackage{stmaryrd}
\usepackage[utf8]{inputenc} %Umlaute
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc} %Worttrennung
\usepackage{lmodern}
\usepackage{a4wide}
\usepackage{fancybox}
\usepackage{relsize}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{rotating}
\usepackage{multicol}
\usepackage{enumitem} 
\usepackage{longtable}
\usepackage{amssymb}
\usepackage{float}
\usepackage{amsfonts} % Für das komplexe C
\usepackage{natbib}
\usepackage{color}
\usepackage{fancyhdr} % Für Kopfzeile
\usepackage{float}
\usepackage{amsmath}
\usepackage{setspace} 
\usepackage[version-1-compatibility]{siunitx}
\usepackage[normalem]{ulem} % \sout{}, \uwave{}, \xout{} 
\usepackage{pdfpages}
\usepackage{hyperref}
\usepackage[left=2cm,right=2cm,top=0.5cm,bottom=0.5cm,includeheadfoot]{geometry}
\usepackage{verbatim}

% Datum
\newcommand{\datum}[1]{\marginpar{#1}}

% Formelnummern mit Kapiteln
\numberwithin{equation}{section}

% Keine Absätze
\setlength{\parindent}{0em}
       
% Abstände
\newcommand{\abs}{\vspace{0.5cm}}

% Neue Farbdefinitionen
\definecolor{text}{rgb}{0, 0.6, 0.8}

\definecolor{1}{rgb}{1, 0.5, 0} %Orange
\definecolor{2}{rgb}{0.3, 0.3, 1} %DBlau
\definecolor{3}{rgb}{0.8, 0.3, 0} %DRot
\definecolor{4}{rgb}{0.6, 0.8, 0.6} %Grün
\definecolor{5}{rgb}{0.7, 0, 1} %Lila
\definecolor{6}{rgb}{0.6, 0.3, 0.3} %Braun


\begin{document}


% Blattgestaltung
\pagestyle{fancy}
\rhead{\textbf{Versuch 49}  \\ Protokoll vom 04.01.2016}
\lhead{\textbf{Dennis van Elten, Christopher Hasenberg}  \\ Fortgeschrittenen-Praktikum Physik M. Sc.}

\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}


\title{Versuch Nr. 49\\ Gepulste NMR}
\author{Christopher Hasenberg\\ \small{\textit{christopher.hasenberg@hotmail.de}} \and Dennis van Elten\\ \small{\textit{dennis.vanelten@tu-dortmund.de}}}
\date{Durchführungsdatum: 04.01.2016}
\setcounter{page}{-0} 
\maketitle
\thispagestyle{empty} 
\tableofcontents

\newpage

\section{Zielsetzung}
Das Ziel des auf den nachfolgenden Seiten beschriebenen Versuchs ist die Untersuchung des Auf- bzw. Abbaus der Probenmagnetisierung sowie die Bestimmung der Diffusionskonstanten mittels gepulster NMR (Kernspinresonanz). Gepulste NMR beruht dabei auf der Orientierung magnetischer Momente bei einem äußerem Feld, so dass eine makroskopische Magnetisierung entsteht, die durch HF-Strahlung verändert werden kann. \\

\noindent Die NMR kann auch dafür verwendet werden die frequenzabhängige Energieaufnahme zu untersuchen. Die dabei entstehenden Resonanzstellen geben Aufschluss über lokale Magnetfelder und somit über die chemische Zusammensetzung. Dies ist jedoch nicht Ziel dieses Versuches.

\section{Grundlagen der NMR}
Im Weiteren soll auf vier zentrale Prinzipien der NMR eingegangen werden. Zunächst wird die Magnetisierung einer sich im thermischen Gleichgewicht befindlichen Probe theoretisch untersucht. Das Ziel ist die Herleitung der temperaturabhängigen Gleichgewichtsmagnetisierung  für Protonen ($I = \frac{1}{2}$). Anschließend wird die Larmor-Präzession beleuchtet, die dadurch entsteht, dass die Magnetisierung durch HF-Strahlung aus der Gleichgewichtslage gebracht wird. Eng damit verknüpft ist der dritte Aspekt der Relaxationserscheinungen. Dabei existieren eine longitudinale und eine transversale Relaxationszeit, auf die in diesem Kapitel eingegangen wird. Zum Schluss werden die Vorgänge bei Einstrahlung der HF-Quanten betrachtet, die nötig sind um die Magnetisierung aus der Gleichgewichtslage zu bringen.

\subsection{Probenmagnetisierung im thermischen Gleichgewicht}
Nach dem Zeeman-Effekt werden die $I$ Kernzustände bei Anlegen eines äußeren Magnetfeldes $B_0$ in $2 I + 1$ äquidistante Unterniveaus aufgespalten. Diese unterscheiden sich nach dem Pauli Prinzip in der Orientierungsquantenzahl $m$, für die die Einschränkung $-I \leq m \leq I$ gilt. Im thermischen Gleichgewicht sind die benachbarten Niveaus gemäß der Stefan-Boltzmann-Verteilung besetzt:
\begin{equation}
\frac{N(m)}{N(m-1)} = \exp \frac{- \Delta E}{k T}.
\end{equation}
Dabei ist $\Delta E = \gamma B_0 \hbar$ die Energiedifferenz dieser Zustände und $k$ die Boltzmann-Konstante. Jedes Niveau besitzt eine bestimmte Spinorientierung und die ungleiche Besetzung der Energieniveaus führt zu einer Kernspinpolarisation. Diese lautet
\begin{equation}
\langle I_z \rangle = \frac{\sum\limits_{m=-I}^I \hbar m \exp (- m \gamma B_0 \hbar / kT)}{\sum\limits_{m=-I}^I \exp (- m \gamma B_0 \hbar / kT)} = - \frac{\hbar^2}{4} \frac{\gamma B_0}{k T} .
\end{equation}
Das zweite Gleichheitszeichen gilt für den Spezialfall von Protonen mit $I = \frac{1}{2}$, bei dem zwei Unterniveaus bei externem Magnetfeld entstehen. Weiterhin wurde eine lineare Näherung für die Exponentialfunktion verwendet, die im Fall $m \gamma B_0 \hbar << k T$, mit $B = O(T)$ gilt. \\
Über die magnetischen Momente $\vec{\mu}_i$ erzeugt die Kernspinpolarisation eine makroskopische Magnetisierung $M_0$ (im Gleichgewicht). Diese bestimmt sich über
\begin{equation}
M_0 = \frac{1}{4} \mu_0 \gamma^2 \frac{\hbar^2}{k} N \frac{B_0}{T},
\end{equation}
wobei $N$ der Anzahl der Momente pro Volumeneinheit entspricht. Zu sehen ist, dass die Gleichgewichtsmagnetisierung temperaturabhängig ist. 

\subsection{Larmorpräzession}
Wie bereits zuvor erwähnt wird HF-Strahlung verwendet, um die Magnetisierung $M$ aus ihrer Gleichgewichtslage zu bringen. Anschließend kann das zeitliche Verhalten von $M$ untersucht werden. Die Probenmagnetisierung wirkt durch das Zusammenwirken von Einzelmomenten, wobei deren Anzahl $N$ etwa $10^{28}\frac{1}{m^3}$ beträgt. Daher ist eine klassische Betrachtung möglich, in der auf die Magnetisierung ein Drehmoment $\vec{D} = \vec{M} \times B_0 \vec{z}_e$, also transversal zum angelegten Magnetfeld $B_0$, wirkt. Dadurch ändert sich der Gesamtdrehimpuls $\vec{I}$ ($\vec{D} = \frac{d \vec{I}}{dt}$) und damit auch das magnetische Moment $\vec{M}$, da die beiden Größen über $\gamma$, das gyromagnetischen Verhältnis, verknüpft sind. Es gilt:
\begin{equation}
\frac{d \vec{M}}{dt} = \gamma \vec{M} \times B_0 \vec{z}_e
\label{Mdt}
\end{equation} 
Durch Zerlegung der Gleichung in seine Komponenten und deren Lösungen 
\begin{equation}
\begin{aligned}
M_Z &= \textrm{const} \\
M_X &= A \cos \left(\gamma B_0 t \right)\\
M_Y &= - A \sin \left( \gamma B_0 t \right)
\end{aligned}
\end{equation}
zeigt sich, dass die Magnetisierung $\vec{M}$ um die $\vec{z}_e$-Achse eine Präzessionsbewegung mit der Kreisfrequenz $\omega_L = \gamma B_0$, auch Larmorfrequenz genannt, durchführt.

\subsection{Relaxationserscheinungen}
Nun ist es so, dass die Magnetisierung nach der Anregung durch das HF-Feldes wieder in die Gleichgewichtslage zurückkehren möchte. Die Relaxation bezeichnet diesen Vorgang. Theoretisch wird dies beschrieben, indem die im Abschnitt zuvor angedeuteten komponentenweisen Gleichungen nun um einen Relaxationsterm ergänzt werden. Daraus folgen die Blochschen Gleichungen
\begin{equation}
\begin{aligned}
\frac{d M_Z}{dt} &= \frac{M_0 - M_z}{T_1}, \\
\frac{d M_X}{dt} &= \gamma B_0 M_Y - \frac{M_X}{T_2}, \\
\frac{d M_Y}{dt} &= \gamma B_0 M_X - \frac{M_Y}{T_2}.
\end{aligned}
\end{equation}
Die darin auftauchenden Größen $T_1$ und $T_2$ sind die longitudinale bzw. transversale Relaxationszeit. Erstere beschreibt neben der zeitlichen Veränderung der parallel zum Magnetfeld stehenden Magnetisierung auch die Zeit, die die Energieumwandlung zwischen Kernspinsystemen und Gitterschwingungen andauert. Daher wird sie auch als Spin-Gitter-Relaxationszeit bezeichnet. $T_2$ hingegen bezieht sich auf die transversale Komponente. Die Abnahme dieser Magnetisierungskomponente wird dominiert durch Spin-Spin-Wechselwirkungen. Daher heißt $T_2$ auch Spin-Spin-Relaxationszeit.

\subsection{Einstrahlung der HF-Quanten}

Die Probe wird in ein HF-Feld gebracht, dessen Magnetfeldvektor senkrecht zu $\vec{z}_e$ steht. Dieses sei
\begin{equation}
\vec{B}_{HF} = 2 \vec{B}_1 \cos \omega t.
\label{B_HF}
\end{equation}
Zerlegt werden kann dieses in zwei zirkular polarisierte Felder mit einer Frequenz von $\pm \omega$. Befindet sich eine dieser Frequenzen in der Nähe der Kreisfrequenz $\omega_L$, so kann der Beitrag der anderen Frequenz vernachlässigt werden. Das zeitabhängige Magnetfeld $\vec{B}_1$ rotiert dann in der x-y-Ebene. Um nun $\vec{M}(t)$ aus den DGL'en zu gewinnen, wird in ein bewegtes Koordinatensystem gewechselt. Die Zeitabhängigkeit von $\vec{B_1}$ wird eliminiert, wenn das System mit $\omega$ um die $\vec{B}_0$-Achse rotiert. Die Einheitsvektoren sind nun zeitabhängig. Dann verändert sich Gleichung \ref{Mdt} in diesem System zu
\begin{equation}
\frac{d \vec{M}}{dt} = \gamma \left\{ \vec{M} \times \left( \vec{B}_{ges} + \frac{\vec{\omega}}{\gamma} \right) \right\}.
\end{equation}

\noindent Die Ableitung von $\vec{M}$ entspricht derjenigen aus Abschnitt 2.2, jedoch mit
\begin{equation}
\frac{d \vec{M}}{dt} = \gamma \left( \vec{M} \times \vec{B}_{eff} \right),
\end{equation}
wobei $\vec{B}_{eff} = \vec{B}_0 + \vec{B}_1 + \frac{\vec{\omega}}{\gamma}$ gilt.

\begin{figure}[h!]
	\centering{\includegraphics[width=7.5cm]{theorie_beff.png}}
	\caption{Darstellung von $B_{eff}$, das durch Anlegen eines HF-Feldes entsteht [1].}
	\label{fig:Abbildung}
\end{figure}

Ein wichtiger Spezialfall ist $\omega_{HF} = \omega_L$. Dann gilt $\vec{B}_{eff} = \vec{B}_1$	und die Magnetisierung präzidiert um die $\vec{B}_1$-Achse. $\vec{M}$ lässt sich nun für eine Einstrahlzeit $\Delta t$ um einen Drehwinkel $\delta(\Delta t)$ aus der $\vec{z}_e$-Achse ablenken. Wichtige Zeiten sind
\begin{equation}
\begin{aligned}
\Delta t_{90} &= \frac{\pi}{2 \gamma B_1} \quad \textrm{und}\\
\Delta t_{180} &= \frac{\pi} {\gamma B_1},
\end{aligned}
\end{equation}
mit denen sich wohldefinierte Nicht-Gleichgewichtszustände erzeugen lassen. Erstere bewirkt eine Drehung von $\vec{M}$ aus der $\vec{z}$- in die $\vec{y}$-Achse. Letztere klappt $\vec{M}$ wieder in die $\vec{z}$-Achse um. Mit diesen lassen sich die Relaxationszeiten $T_1$ und $T_2$ bestimmen.

\newpage
\section{Durchführung/Versuchsaufbauten zur Ermittlung der Relaxationszeiten $T_1$ und $T_2$ sowie der Diffusionskonstanten $D$}

\subsection{Messprinzipien zur Bestimmung der Spin-Spin-Relaxationszeit $T_2$}
Um die transversale Relaxationszeit $T_2$ zu bestimmen existieren verschiedene Messmethoden, die im Folgenden vorgestellt werden und auf deren wichtigste Aspekte eingegangen wird. Neben dem freien Induktionszerfall sind diese die Spin-Echo-Methode sowie weitere Modifikationen derer.

\subsubsection{Freier Induktionszerfall (FID)} 
Das äußere Magnetfeld $\vec{B}_0$ in z-Richtung wird hier durch einen Permanentmagneten erzeugt. Des Weiteren wird um die Probe eine Spule getwickelt. Da durch diese ein HF-Strom fließt entsteht ein Magnetfeld $\vec{B}_{\textrm{HF}}$ gemäß Gleichung \ref{B_HF}. Dessen Frequenz wird auf $\omega_L = \gamma B_0$ festgelegt. 
\begin{figure}[h!]
	\centering{\includegraphics[width=7.5cm]{aufbau_fid.png}}
	\caption{Aufbau der FID-Methode zur Bestimmung von $T_2$ [1].}
	\label{fig:Abbildung}
\end{figure}

\noindent Das Feld $\vec{B}_1$ wird nun für die Dauer eines $90^\circ$ Pulses eingeschaltet. Dadurch wird die Magnetisierung aus ihrer Gleichgewichtslage gebracht. Nach Ausschalten von $\vec{B}_1$ führt $\vec{M}$ Präzessionsbewegungen durch und findet über Relaxationen zu ihrer Gleichgewichtslage zurück. Dies wird als freier Induktionszerfall (FID) bezeichnet, denn während der Kreiselbewegungen werden messbare Induktionsspannungen erzeugt. \\

\noindent Nun soll genauer auf den Zerfall der transversalen Magnetisierung, also die Relaxationszeit $T_2$ eingegangen werden. Es zeigt sich, dass der Zerfall nicht für alle Spins, die die makroskopische Magnetisierung erzeugen, gleich stark ist. Eine mögliche Ursache sind zusätzliche Felder, die auf die Kernspins wirken z.B. durch Dipolfelder. Andererseits ist das zwischen den Polschuhen erzeugte Magnetfeld tatsächlich leicht inhomogen. Die Folge daraus ist eine Verteilungsfunktion von $\omega_L$, woraus eine Dephrasierung der Spins resultiert. Die Präzessionsbewegung führt zu dem gesuchten Zerfall der transversalen Magnetisierungskomponente. Die messbare Relaxationszeit lautet
\begin{equation}
\frac{1}{T_2^*} = \frac{1}{T_2} + \frac{1}{T_{\Delta B}}.
\end{equation}
Die letzte Größe bezeichnet die Zeitkonstante $T_{\Delta B}$, die durch den Einfluss des inhomogenen $\vec{B_0}$-Feldes entsteht. Solange diese sehr groß ist, ist eine Bestimmung von $T_2$ möglich.  

\subsubsection{Spin-Echo-Verfahren}
Der Nachteil der apparativen Inhomogenität der FID-Methode kann beseitigt werden, wenn dieser zeitlich konstant ist. Eine Lösung stellt die Spin-Echo-Methode dar. Dabei wird mit zwei HF-Pulsen gearbeitet. Zunächst wird ein $90^\circ$ Grad Puls verwendet, um die Magnetisierung von der $\vec{z}$-Achse in die $\vec{y}'$-Richtung des rotierenden Systems zu drehen. Anschließend präzedieren die Spins und kommen dabei außer Phase. Daher nimmt die Induktionsspannung ab und verschwindet nach kurzer Zeit. Nach der Zeit $\tau$ wird ein $180^\circ$ Puls auf die Probe gegeben. Dabei klappt die Spinorientierung um und die Spins laufen wieder zueinander. Nach der Zeit $2 \tau$ sind diese kurzzeitig in Phase und ein Signal ist messbar. Dieses wird auch Hahn-Echo genannt. 
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_spin_echo.png}}
	\caption{Zeitlicher Signalverlauf der Spin-Echo-Methode nach Hahn [1].}
	\label{fig:Abbildung}
\end{figure}

\noindent Für $T_2 \rightarrow \infty$ ist das Hahn-Echo so hoch wie das der FID. Jedoch wechselwirken die Spins irreversibel mit ihrer Umgebung, sodass die Signalhöhe abnimmt. Das Signal nimmt mit zunehmendem Pulsabstand $\tau$ ab, da die Anzahl der irreversiblen Prozesse mit $\tau$ zunimmt. Die Höhe des Hahn-Echos berechnet sich gemäß
\begin{equation}
M_Y(t) = M_0 \exp (-t / T_2)
\end{equation}

\subsubsection{Carr-Purcell-Methode}
Die Carr-Purcell-Methode ist eine Verbesserung des zuvor beschriebenen Spin-Echo-Verfahrens, denn sie ist deutlich zeiteffektiver. Während bei letzterem die Relaxation in die Gleichgewichtsmagnetisierung $M_0$ abgewartet werden muss, verwendet man in der Carr-Purcell-Methode nach dem $90^\circ$ Puls mehrere $180^\circ$ Pulse, die alle aufeinander in einem Abstand von 2 $\tau$ abgegeben werden. Dabei können an den Zeitpunkten $2n \tau$  Signale gemessen werden, da die Spins kurzzeitig in Phase sind. Dabei klingt die Höhe $M_Y$ exponentiell mit $T_2$ ab. So kann analog wie zuvor $T_2$ bestimmt werden.
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_carr_purcell.png}}
	\caption{Zeitlicher Signalverlauf der Spin-Echo-Methode nach Carr und Purcell [1].}
	\label{fig:Abbildung}
\end{figure}

\subsubsection{Meiboom-Gill-Methode}
Der Nachteil der Carr-Purcell-Methode liegt darin, dass die $180^\circ$ Pulse zeitlich exakt justiert  sein müssen, ansonsten wird eine verfälschte transversale Relaxationszeit $T_2$ gemessen. Dies liegt daran, dass der Puls den Spin immer in dieselbe Richtung dreht und sich somit die Fehler aufaddieren. Die Lösung liefert die Meiboom-Gill-Methode. Diese nahezu identisch zu der von Carr und Purcell, jedoch werden nun die $180^\circ$ Pulse um 90$^\circ$ in der Phase gegenüber dem anfänglichen $90^\circ$ Puls gedreht. Ausgenutzt wird dabei, dass jeder zweite Puls zur Refokussierung der zuvor aus der $\vec{x}'$-$\vec{y}'$-Ebene laufenden Spins dient. Selbst bei einer Fehljustierung liefern somit alle geradzahligen Echosignale die richtige Magnetisierungskomponente $M_Y$. Außerdem sind die Echosignale nicht mehr alternierend im Gegensatz zu den bisher beschriebenen Spin-Echo-Verfahren.

\subsection{Einfluss der Diffusion auf das Relaxationsverhalten}
Die bisherigen Messverfahren funktionieren nur solange das $B_0$-Feld zeitunabhängig ist. Aufgrund von Inhomogenitäten des Feldes bewegen sich die Moleküle in verschiedene Feldstärkebereiche. Dadurch wird dann $\omega_L(t)$ und die Refokussierung wird erschwert. Um diesen Effekt zu berücksichtigen werden die Blochschen Gleichungen um einen Term ergänzt, der die Diffusionskonstante $D$ enthält. Letztere beschreibt die thermische Bewegung von Molekülen in Nichtfeststoffen. Für Protonen $\left( \textrm{Spin} \frac{1}{2} \right)$ verhält sich der Diffusionsstrom gemäß 
\begin{equation}
\vec{j} = - D \nabla n,
\end{equation}
mit $n$ der Teilchenzahl pro Volumeneinheit, in einem Anteil für antiparallele Spins $j_{\uparrow \downarrow}$ sowie für parallele Spins $j_{\uparrow \uparrow}$. Unter der Annahme, dass $D$ ortsabhängig ist folgt für die zeitliche Magnetisierung
\begin{equation}
\frac{\partial M}{\partial t} = D \Delta M.
\end{equation}
Wird dieses in die Blochschen Gleichungen integriert folgt für die gesamte Magnetisierung
\begin{equation}
\frac{\partial M}{\partial t} = \gamma \left( \vec{M} \times \vec{B} \right) - \frac{M_X \vec{x}_e + M_Y \vec{y}_e}{T_2} - \frac{\left( M_Z - M_0 \vec{z}_e \right)}{T_1} + \left( \vec{x}_e + \vec{y}_e + \vec{z}_e \right) D \Delta M.
\end{equation}
Über einige Rechenschritte kann eine Lösung für die transversale Magnetisierung $M_Y$ ermittelt werden. Unter Berücksichtigung der Diffusion folgt letztlich
\begin{equation}
M_Y(t) = M_0 \exp \left( -t / T_2 \right) \exp \left( - D \gamma^2 G^2 t^3 / 12 \right).
\end{equation}
Dabei ist $G$ der Feldgradient des äußeren Magnetfeldes.  Die Bestimmung von $T_2$ ist auch bei Diffusionsvorgängen mithilfe der Methode nach Carr-Purcell bzw. Meiboom-Gill möglich, solange $T_D = \frac{3}{D \gamma^2 G^2 \tau^2}$ viel größer als $T_2$ ist. Es besteht aber auch die Möglichkeit $D$ über die Spin-Echo-Methode zu bestimmen, wenn der Feldgradient bekannt ist. Dies ist solange möglich wie $T_2^3 >> \frac{12}{D \gamma^2 G^2}$ gilt.

\subsection{Messprinzip zur Bestimmung der Spin-Gitter-Relaxationszeit $T_1$}
Bisher wurde die Bestimmung von $T_2$ bzw. der Diffusionskonstanten $D$ untersucht. Im Gegensatz dazu treten bei der Messung der longitudinalen Relaxationszeit $T_1$ keinerlei Komplikationen auf. Bei dem ersten Puls handelt es sich um einen $180^\circ$ Puls. Anschließend folgt der Relaxationsprozess der Spins. Bevor diese wieder ihre Gleichgewichtslage erreicht haben, nach einer Zeit $\tau$, wird ein $90^\circ$ Puls injiziert. Durch die damit verbundene Präzessionsbewegung  wird eine Spannung induziert. Die Integration der Blochschen Gleichung liefert die Bestimmungsrelation für $T_1$:
\begin{equation}
M_Z(\tau) = M_0 \left( 1 - 2 \exp \{- \tau / T_1  \} \right).
\end{equation}
Durch Variation des Zeitabstandes $\tau$ zwischen dem $180^\circ$ und $90^\circ$ Puls kann $M_z$ bestimmt werden. Dies gibt Auskunft über die Spin-Gitter-Relaxationszeit $T_1$.

\begin{figure}[h!]
	\centering{\includegraphics[width=6cm]{verlauf_magnetisierung_t1.png}}
	\caption{Verlauf der Magnetisierungskomponente in $z$-Richtung in Abhängigkeit vom Pulsabstand $\tau$. Mittels diesem kann die Relaxationszeit $T_1$ ermittelt werden [1].}
	\label{fig:Abbildung}
\end{figure}

\subsection{Messprinzip zur Bestimmung der Viskosität mithilfe eines Ubbelohde-Viskosimeter}

Unter der Bedingung, dass die Temperatur $T$ der Flüssigkeit (hier Wasser) während der Messung konstant ist, wird diese aus einem Vorratsgefäß in eine Messkugel gesaugt bis das Wasser bis oberhalb der Messmarkierung $M_1$ in der Kapillare steht. Eine weitere Markierung $M_2$ befindet sich leicht unterhalb der Kugel. Nun wird die Saugfunktion abgeschaltet und die Zeit gemessen, bis die Flüssigkeit wieder den Messpunkt $M_2$ erreicht.

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{Ubbelohde-Viskosimeter.png}}
	\caption{Messvorgang und Aufbau eines Ubbelohde-Viskosimeters [2].}
	\label{fig:Abbildung}
\end{figure}

\section{Bestimmung der Relaxationszeiten $T_1$ und $T_2$ sowie der Diffusionskonstanten $D$}

Bevor die gesuchten Parameter bestimmt werden können, müssen zunächst die Längen der Pulse ermittelt werden, die die Magnetisierung aus ihrem Gleichgewicht in eine  Nichtgleichgewichtslage auslenken. Sie werden erreicht mittels eines $90^\circ$ und $180^\circ$ Pulses. Dazu wird anstatt der zu untersuchenden Wasserprobe (bidestilliert) zunächst eine mit Kupfersulfat versetzte Wasserprobe genommen, da diese eine geringere Relaxationszeit aufweist.  Dies liegt daran, dass paramagnetische Substanzen aufgrund ungepaarter Elektronen ein permanentes magnetisches Moment besitzen. Bewegen sich diese Teilchen, so führt dies zu einer Störung und die Relaxationszeit verringert sich (Wiederholzeit \SI{0.5}{s} statt \SI{12}{s}). Dabei entprechen die notwendigen Pulslängen für den $90^\circ$  bzw. $180^\circ$ Puls denen für die Wasserprobe, da die Spinzusammensetzung gleich ist. Damit folgt für die Pulslängen:
\begin{equation}
\begin{aligned}
    90^\circ\,\textrm{Puls}: \quad &\SI{4,78}{\mu s} \\
    180^\circ\,\textrm{Puls}: \quad &\SI{9,20}{\mu s}.
\end{aligned}
\end{equation}

Dabei wird eine Frequenz von etwa \SI{21,7}{MHz} verwendet. Damit sind die Vorbereitungen abgeschlossen und die Parameter können mittels der im Kapitel zuvor beschriebenen Messmethoden ermittelt werden. Dieser Schritt ist in Abbildung \ref{fig:scope1} Die Auswertung ist Inhalt der folgenden Abschnitte.
\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{scope_23.png}}
	\caption{}
	\label{fig:scope1}
\end{figure}
\subsection{Bestimmung von $T_2$ mittels der Methode nach Meiboom-Gill}

Zunächst wird die transversale Relaxationszeit bestimmt. Dazu werden die Pulse wie in dem Kapitel über die Methode beschrieben eingesetzt. Insgesamt werden 100 $180^\circ$ Pulse nachgeschaltet. Damit ergibt sich eine Burstsequenz, wie sie in der folgenden Abbildung dargestellt ist.

\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{meiboom_gill.png}}
	\caption{Aufgenommene Burstsequenz mit der Methode nach Meiboom-Gill.}
	\label{fig:Abbildung}
\end{figure}

Deutlich erkennbar ist die einhüllende Exponentialfunktion, sowie das nicht alternierende Vorzeichen des Echos. Zur Ermittlung der transversalen Relaxationszeit wird eine Fit-Kurve erzeugt. 
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{MG.pdf}}
	\caption{Regressionskurve}
\end{figure}
\newpage
Dafür wurde eine Funktion der Form
\begin{equation}
    f(x)=a\cdot \exp (bx) + c 
\end{equation}
an den exponetiellen Verlauf angepasst. Die Fit-Parameter und ihre Fehler lauten:
\begin{equation}
    \begin{aligned}
        a&=(-1.62429070289 \pm 0.14298031)\,\textrm{V} \\
        b&= (0.770329147237 \pm 0.13335375)\,\textrm{1/s} \\
        c&= (-0.0181260490793 \pm 0.15475089)\,\textrm{V} 
    \end{aligned}
\end{equation}
Die Zeit $T_2$ entspricht dem Inversen des Fitparameters $b$:
\begin{equation}
    T_2=1.298\,\textrm{s}
\end{equation}
Die Größe $M_0$ ist dementsprechend der Fitparameter $a$:
\begin{equation}
    M_0=(-1.62429070289 \pm 0.14298031)\,\textrm{V}
\end{equation}
Des Weiteren soll zum Vergleich dieselbe Pulsfolge mittels der Carr-Purcell-Methode aufgenommen werden, um die Aufnahmen vergleichen zu können. Das Resultat ist nachfolgend dargestellt.

\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{CarrP.png}}
	\caption{Aufgenommene Burstsequenz mit der Methode nach Carr-Purcell.}
	\label{fig:Abbildung}
\end{figure}
Hier erkennt man einen deutlichen Unterschied zur vorherigen Aufnahme nach dem Prinzip von Meiboom-Gill. Die Pulsfolge ist alternierend. Allerdings ist die aufgenommene Sequenz nicht ganz korrekt, da die Pulse exponentiell abfallen müssten, jedoch bleibt die Höhe konstant. Der dafür verantwortliche Fehler konnte jedoch nicht ermittelt werden.
\clearpage 
\section{Bestimmung der Relaxationszeit $T_1$}
Die Relaxationszeit $T_1$ wird mithilfe der
\begin{figure}[h!]
	\centering{\includegraphics[width=8cm]{aufgabe_f2.pdf}}
	\caption{Aufgenommenes Signal nach der Spin-Echo Methode}
\end{figure}
Um die Relaxationszeit $T_1$ zu bestimmen wurde die Funktion 
\begin{equation}
    M_Z(\tau) = M_0(1-2 \exp(-\tau / T_1)
\end{equation}
an die Messdaten gefittet.
Obwohl eigentlich der größte vorkommende Wert von $\SI{452.5}{\mV}$ für $M_0$ angenommen wird, wurde jedoch beim FIt zwecks einer besseren Anpassung dieser Parameter 'frei' gelassen.
Das Ergebnis für $T_1$ lautet:
\begin{equation}
    T_1 = (1.9\pm 0.02)\s
\end{equation}
%Bestimmung Diffusionskonstante D
\subsection{Bestimmung der Diffusionskonstanten}
In diesem Abschnitt soll die Diffusionskonstante mithilfe des Spin-Echo Verfahren ermittelt werden. Dabei wird 
$\ln(M(t)/M_0)+t/T_2 $ gegen $t^3$ aufgetragen. Die Fitfunktion hat folgende Gestalt:
\begin{equation}
    f(x)=a\cdot x +b
\end{equation}
Die Ergebnisse der Fit-Parameter lauten aus Abbildung 12:
\begin{equation}
    \begin{aligned}
        a &= (-68789 \pm 2916 )s^{-3}\\
        b &= 0.058 \pm 0.036
    \end{aligned}
\end{equation}
Mit dem Ergebnis dieser linearen Regression lässt sich schliesslich die Diffusionskonstante berechnen:

\begin{equation}
    D=\frac{12 a}{\gamma^2 G^2 }=(6.8\pm 1.1)\cdot 10^{-8}\SI{}{\m^2 \s^{-1}}
\end{equation}
Dabei hat das gyromagnetische Verhältnis von Protonen den Literaturwert $\gamma_P = 2.675\cdot 10^{8}\hspace{0.04in}\rad \hspace{0.04in} \textrm{s}^{-1} \hspace{0.04in} \textrm{T}$ (Stöcker: Taschenbuch der Physik). Der Wert des Feldgradienten ergibt sich in Abschnitt 5.3 dieses Protokolls zu $G=(0.013 \pm 0.001)  \frac{\textrm{T}}{\textrm{m}}$.
\begin{figure}[h!]
	\centering{\includegraphics[width=8cm]{aufgabe_d.pdf}}
	\caption{Aufgenommenes Signal nach der Spin-Echo Methode}
\end{figure}\label{aufgabe_d1}


\newpage
\subsection{Bestimmung der Halbwertsbreite $t_{1/2}$ mittels des Spin-Echo-Verfahrens}

Um die Halbwertsbreite $t_{1/2}$ des Hahn-Echos zu bestimmen wird der Pulsabstand variiert und die jeweilige Breite am Oszilloksop mittels der Cursoreinstellungen abgelesen. Als tatsächlich angenommene Halbwertsbreite  ermittelt sich anschließend daraus durch Bildung des Mittelwertes der aufgenommenen Daten. Dabei wurde der maximale Feldgradient eingestellt. Nachstehend sind die Messwerte aufgetragen.

\begin{table}[h!]
    \centering
    \begin{tabular}{c c}
    Pulsabstand  & Halbwertsbreite Hahn-Echo \\
    $\tau$ / ms & $t_{1/2}$ / ms$\\
    \hline \\
    5,0    &  0.60 \\
    6,0    &  0.70 \\
    7,0    &  0.70 \\
    8,0    &  0.80 \\
    9,0    &  0.75 \\
    10,0   &  0.70 \\
    11,0   &  0.50 \\
    12,0   &  0.45 \\
    12,5   &  0.55 \\
    13,0   &  0.40 \\
    13,5   &  0.45 \\
    14,0   &  0.40
    \end{tabular}
    \caption{Aufgenommene Halbwertsbreiten in Abhängigkeit des Pulsabstandes.}
\end{table}

Damit folgt für die mittlere Halbwertsbreite und den statistischen Fehler darauf:
\begin{equation}
    \bar{t}_{1/2} = (0,5733 \pm 0,0322)\,\textrm{ms}
\end{equation}

\subsection{Bestimmung des Feldgradienten $G$ über die Halbwertsbreite $t_{1/2}$ des Hahn-Echos}

Nachfolgend soll der Feldgradient bestimmt werden. Die dafür verwendete Relation lautet
\begin{equation}
    \frac{1}{4}\,d\,\gamma_p\,G\,\bar{t}_{1/2} = 2,2.
    \label{feldgradient_formel}
\end{equation}
Dabei entspricht $d = 4,4 \cdot 10^{-3}\,\textrm{m}$ dem Probendurchmesser und $\bar{t}_{1/2}$ ist die im Abschnitt zuvor ermittelte Halbwertsbreite des Hahn-Echos. Durch Umstellen von Gleichung \ref{feldgradient_formel} und Einsetzen der Werte folgt
\begin{equation}
    G = \frac{4 \cdot 2,2}{d \cdot \gamma_p \cdot \bar{t}_{1/2}} \approx (0.013 \pm 0.001)  \frac{\textrm{T}}{\textrm{m}}.
\end{equation}


\subsection{Bestimmung der Viskosität $\eta$.}

Die Viskosität ermittelt sich gemäß der Relation
\begin{equation}
    \eta (T) = \rho \,\alpha (t - \delta).
\end{equation}

Darin enthalten ist die Dichte der Flüssigkeit $\rho$, ein Korrekturfaktor $\alpha = 1,024 \cdot 10^{-9} \SI{}{\frac{m^2}{s^2}}$ sowie die weitere Größe $\delta$, einen zeitabhängigen Korrekturfaktor. Gemessen wurde eine Flusszeit von $t = \SI{1084}{s}$. $\delta$ kann mithilfe der nachfolgenden Tabelle bestimmt werden. 

\begin{table}[h!]
    \centering
    \begin{tabular}{l l}
    t / s & $\delta$ / s \\
    \hline
        350 & 3,4 \\
        400 & 2,6 \\
        450 & 2,1\\
        500 & 1,7\\
        600 & 1,2\\
        700 & 0,9\\
        800 & 0,7\\
        900 & 0,5\\
        1000 & 0,4
    \end{tabular}
    \caption{$\delta$ in Abhängigkeit der Flusszeit $t$.}
\end{table}
Da die gemessene Zeit außerhalb derjenigen in der Tabelle liegt wird $\delta$ mittels linearer Extrapolation ermittelt. Läge die gemessene Zeit zwischen gesicherten Werten, so müsste lineare Interpolation angewandt werden. Mittels des Extrapolation folgt $\delta = \SI{0,316}{s}$. Des Weiteren lautet die Dichte für Wasser $\rho_{H_20} = \SI{1000}{\frac{kg}{m^3}}$. Damit sind alle notwendigen Werte zusammengetragen und es folgt für die Viskosität:
\begin{equation}
\begin{aligned}
   \eta (T) &= \rho \,\alpha (t - \delta) \\
    &= \SI{1000} {\frac{kg}{m^3}} \cdot 1,024 \cdot 10^{-9} \SI{}{\frac{m^2}{s^2}} \cdot (\SI{1084}{s} - \SI{0,316}{s}) \\
    &\approx 1109,69 \cdot 10^{-6} \SI{}{\frac{kg}{m \,s}}.
    \end{aligned}
\end{equation}




\subsection{Bestimmung des Molekülradius $r$ von Wasser}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Bestimmung $r$ mit Stokescher Formel hier einsetzen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Im Weiteren wird zum Vergleich der Molekülradius gemäß zwei anderen Verfahren ermittelt. Zum einen geschieht diesüber die molare Masse und die Dichte von Wasser. Dabei wird die Annahme getroffen, dass die $H_20$-Moleküle eine hexagonal dichteste Kugelpackung (hcp-Struktur) einnehmen. \\
Eine andere Möglichkeit der Bestimmung des Radius erfolgt über den kritischen Druck und die Wassertemperatur. Als erstes wird die erste Methode bestimmt. Dazu wird zunächst die molare Masse $M$ von Wasser betrachtet. Diese beträgt näherungsweise:
\begin{equation}
\begin{aligned}
    M_{H_2O} &= 2 M_H + M_0 = 2 \cdot 1 \frac{g}{mol} + 16 \frac{g}{mol} = 18 \frac{g}{mol}.
    \end{aligned}
\end{equation}

Damit kann mithilfe der Avogadro-Konstante $N_A$ die Masse $m$ bestimmt werden:
\begin{equation}
\begin{aligned}
    m = \frac{M}{N_A}&\approx 2,989 \cdot 10^{-23}\,g \\
    &\approx 2,989 \cdot 10^{-26}\,kg.
    \end{aligned}
\end{equation}

Als nächstes wird die (angenommene) Molekülstruktur des Stoffes untersucht. Die hcp-Struktur nimmt folgende Form an:

\begin{figure}[h!]
	\centering{\includegraphics[width=2cm]{hcp_struktur.png}}
	\caption{Hexagonal dichteste Kugelpackung.}
	\label{fig:Abbildung}
\end{figure}

Bei dieser Anordnung sind $74\%$ des Raumes gefüllt. 
Damit bestimmt sich das Volumen der Kugeln innerhalb der Elementarzelle zu
\begin{equation}
V=0.74\cdot V_{Molekül}
\end{equation}
Die Dichte von Wasser ist bekannt und beträgt $\rho_{H_2O} = \SI{1000}{\frac{kg}{m^3}}$. Die beiden beschriebenen Größen sind miteinander verknüpft gemäß 
\begin{equation}
    V = \frac{m}{\rho},
\end{equation}
wobei $V$ den gesuchten Molekülradius enthält. Einsetzen der zuvorgewonnenen Relationen und Größen liefert für den $r$:
\begin{equation}
\begin{aligned}
    \frac{4}{3}\pi r^3 &= 0.74 V_M \\
    \Longleftrightarrow r &= \sqrt[3]{\frac{3\cdot m\cdot 0.74}{4 \pi \rho}} \approx 1,059 \cdot 10^{-10}\,m =\SI{1.75}{\angstrom}.
    \end{aligned}
\end{equation}
Alternativ lässt sich der Molekülradius über die Van-der-Waals-Gleichung berechnen:
\begin{equation}
    k_B T=\left(P+\frac{a}{V^2} \right)(V-b)
\end{equation}
Stellt man diese Gleichung nach $V$ um erhält man:
\begin{equation}
    \begin{aligned}
        \Rightarrow k_B T &= PV-Pb+\frac{a}{V}-\frac{ab}{V^2} \\
        \Leftrightarrow  \underbrace{k_B T-PV}_\text{=0}+Pb&=\frac{a}{V}-\frac{ab}{V^2} \quad \textrm{$PV=k_B T$(Ideales Gas)}\\
        \Leftrightarrow  \frac{a}{V}-\frac{ab}{V^2}-Pb &= 0\\
        \Leftrightarrow Va-ab-PbV^2 &=0\\
        \Leftrightarrow Va-ab-\frac{k_B T}{V}bV^2 &=0\\
        \Leftrightarrow V(a-k_B T)&=ab \\
        \Leftrightarrow V&=\frac{ab}{(a-k_B T)} \\
        \Leftrightarrow V&=\frac{b}{(1-\frac{k_B T}{a})} 
    \end{aligned}
\end{equation}
Die Konstanten $a$ und $b$ können für $H_2O$ in der Literatur nachgeschlagen werden. 
\begin{equation}
    \begin{aligned}
        a &= 557.3\cdot 10^{-3}\frac{\Pa \hspace{0.05in} \m^6}{\mol^2} \\
         b &= 0.031\cdot 10^{-3}\frac{\m^3}{\mol}
    \end{aligned}
\end{equation}
Die weiteren physikalischen Größen, die zur Berechnung des Molekülradius notwendig sind (Stöcker: Taschenbuch der Physik):
\begin{equation}
    \begin{aligned}
        k_B T &= 4.11 \cdot 10^{-21}\hspace{0.02in}\J \quad
        \textrm{, bei 25$^\circ$}\\
        1\,\mol &= 6.022\cdot 10^{23}
    \end{aligned}
\end{equation}
Setzt man diese Werte ein und stellt nach dem Radius in $V=4\pi r^3$ um erhält man folgenden Molekülradius:
\begin{equation}
    r = \SI{1.6}{\angstrom}
\end{equation}
Nun wird  der Molekülradius mithilfe der gemessenen Viskosität und der ermittelten Diffusionskonstanten berechnet:
\begin{equation}
    \begin{aligned}
        D= \frac{kT}{6\pi r \eta }\\
        \Leftrightarrow r =\frac{kT }{D \eta}
    \end{aligned}
\end{equation}
Setzt man nun die numerische Werte ein und beachtet die Fehlerfortpflanzung erhält man:
\begin{equation}
    r = (5.45 \pm 0.88)\cdot 10^{-11}\SI{}{\m} = (0.545 \pm 0.088)\SI{}{\angstrom}
\end{equation}
%gelöst, muss nur noch aufgeschrieben werden%%

\section{Diskussion}
Im Verlauf dieses Versuchs wurden die Relaxationszeiten $T_1$, $T_2$, sowie die Diffusionskonstante $D$ ermittelt. Weiterhin wurden die Viskosität und der Molekülradius von Wasser ermittelt. Letzteres funktionierte relativ problemlos und es wurden sinnvolle Werte gefunden.
\newline
\newline
Die Relaxationszeiten, die aus den Regressionanalysen ermittelt wurden scheinen physikalisch sinnvoll. Dazu zählt auch, dass die Zeit $T_1$ größer ist als $T_2$. Der Wert der Diffusionskonstanten scheint auch von einer akzeptablen Größenordnung zu sein. 
\newline 
\newline
Der aus den gemessenen Werten berechnete Molekülradius beträgt nur ca. ein Drittel von den theoretischen Werten. Gründe dafür sind eine leichte Abweichung der Viskosität von Literaturwerten, sowie eine leichte Abweichung der Diffusionskonstanten.
\section{Literaturverzeichnis}
\begin{enumerate}
	\item[1] TU Dortmund, Physikalisches Fortgeschrittenenpraktikum: Skript zum Versuch Nummer 49, "Gepulste NMR"
	\item[2] Ubbelohde-Viskosimeter, Bild, http://www.chemgapedia.de/vsengine/media/vsc/de/ch/9/mac/ \\
	reaktionstechnik/viskositaet/molmassenbestimmung/kapillarviskosimeter/kapillar3.png
\end{enumerate}

\section{Messdaten}
\subsection{Messdaten zur Bestimmung von $T_1$}
\centering
\begin{tabular}{cc}
 Zeit [$\mu$s] & Spannung [mV] \\
\hline
0.0201 & -452.500 \\
0.0801 & -447.500 \\
0.2411 & -422.500 \\
0.1801 & -435.000 \\
0.2901 & -412.500 \\
0.3401 & -397.500 \\
0.3701 & -380.000 \\
0.4001 & -372.500 \\
0.4501 & -355.000 \\
0.5001 & -332.600 \\
0.5501 & -315.000 \\
0.6001 & -297.500 \\
0.6501 & -277.500 \\
0.7001 & -257.700 \\
0.7501 & -232.500 \\
0.8001 & -217.500 \\ 
0.8501 & -205.000 \\
0.9001 & -185.000 \\
0.9501 & -170.000 \\
1.0001 & -152.500 \\
1.0501 & -137.500 \\
1.1001 & -120.000 \\
1.1501 & -97.5000 \\
1.2001 & -82.5000 \\
1.2501 & -65.0000 \\
1.3001 & -50.0000 \\
1.4001 & -15.0000 \\
2.0001 & 207.5000  \\
1.9501 & 190.0000 \\
1.9001 & 180.0000 \\
1.8501 & 165.0000 \\
1.8001 & 147.0000 \\
1.7501 & 127.5000 \\
1.7001 & 107.5000 \\
1.6501 & 95.00000 \\
1.6001 & 75.00000 \\
2.1001 & 237.5000 \\
2.2001 & 260.0000 \\
2.3001 & 280.0000 \\
2.4001 & 295.0000 \\
2.5001 & 312.5000 \\
2.6501 & 380.0000 \\
2.8001 & 400.0000 \\
3.0001 & 415.0000 
\end{tabular}
\subsection{Messdaten zur Bestimmung der Diffusionskonstanten}
\begin{center}
\begin{tabular}{cc}
Zeit [$\mu$ s] &	Resonanz [mV]\\
\hline
0.0110 &	60.00\\
0.0120	& 46.25\\
0.0125	& 45.00\\
0.0130	& 31.25\\
0.0100	& 68.75\\
0.0090	& 87.50\\
0.0080	& 89.00\\
0.0070	& 100.00\\
0.0060	& 111.25\\
0.0050	& 117.50\\
0.0140	& 30.00\\
0.0135	& 30.25
\end{tabular}
\end{center}
Da bei der Regressionsanalyse$\ln(M(t)/M_0)+t/T_2 $ gegen $t^3$ aufgetragen wird, haben wir die berechneten Werte in der folgenden Tabelle aufgeführt:
\begin{center}
\begin{tabular}{cc}
$\ln(M(t)/M_0)+t/T_2$ & $t^3$ [$s^3$]\\
\hline
-0.663619195090927 & 0.0000013310 \\
-0.92313187732994 & 0.0000017280 \\
-0.950145643505727 & 0.0000019531 \\
-1.31440354908131 & 0.0000021970  \\
-0.528257436791 & 0.0000010000 \\
-0.287865795998765 & 0.0000007290 \\
-0.271638635654847 & 0.0000005120 \\
-0.155875235423549 & 0.0000003430 \\
-0.0500359163899442 & 0.0000002160 \\
0.00385208012326656 & 0.0000001250 \\
-1.35445512757691 & 0.0000027440 \\
-1.34654153277454 & 0.0000024604 
\end{tabular}
\end{center}



\end{document}