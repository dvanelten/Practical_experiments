\contentsline {section}{\numberline {1}Overview}{1}{section.1}
\contentsline {section}{\numberline {2}Introduction}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}The dye laser: predecessor of the diode laser}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Structure of the diode laser}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Diode lasers with diffraction gratings}{2}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Influences on Frequency}{3}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Active medium}{3}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Internal resonantor}{4}{subsubsection.2.3.3}
\contentsline {subsection}{\numberline {2.4}Resonance}{4}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}External resonator}{5}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Interaction of different influences}{5}{subsubsection.2.4.2}
\contentsline {section}{\numberline {3}Procedure and results}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Operation of the diode laser}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Observing rubidium lines}{7}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Simultaneous current and piezo modulation}{9}{subsubsection.3.2.1}
\contentsline {subsection}{\numberline {3.3}Final observation of Rb lines}{9}{subsection.3.3}
\contentsline {section}{\numberline {4}Discussion on the results}{10}{section.4}
\contentsline {section}{\numberline {5}Bibliography}{12}{section.5}
