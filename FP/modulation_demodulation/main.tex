                                                                                                                                                                                                                                                                        \documentclass{scrartcl}
\usepackage{stmaryrd}
\usepackage[utf8]{inputenc} %Umlaute
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc} %Worttrennung
\usepackage{lmodern}
\usepackage{a4wide}
\usepackage{fancybox}
\usepackage{relsize}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{rotating}
\usepackage{multicol}
\usepackage{enumitem} 
\usepackage{longtable}
\usepackage{amssymb}
\usepackage{float}
\usepackage{amsfonts} % Für das komplexe C
\usepackage{natbib}
\usepackage{color}
\usepackage{fancyhdr} % Für Kopfzeile
\usepackage{float}
\usepackage{amsmath}
\usepackage{setspace} 
\usepackage[version-1-compatibility]{siunitx}
\usepackage[normalem]{ulem} % \sout{}, \uwave{}, \xout{} 
\usepackage{pdfpages}
\usepackage{hyperref}
\usepackage[left=2cm,right=2cm,top=0.5cm,bottom=0.5cm,includeheadfoot]{geometry}
\usepackage{verbatim}

% Datum
\newcommand{\datum}[1]{\marginpar{#1}}

% Formelnummern mit Kapiteln
\numberwithin{equation}{section}

% Keine Absätze
\setlength{\parindent}{0em}
       
% Abstände
\newcommand{\abs}{\vspace{0.5cm}}

% Neue Farbdefinitionen
\definecolor{text}{rgb}{0, 0.6, 0.8}

\definecolor{1}{rgb}{1, 0.5, 0} %Orange
\definecolor{2}{rgb}{0.3, 0.3, 1} %DBlau
\definecolor{3}{rgb}{0.8, 0.3, 0} %DRot
\definecolor{4}{rgb}{0.6, 0.8, 0.6} %Grün
\definecolor{5}{rgb}{0.7, 0, 1} %Lila
\definecolor{6}{rgb}{0.6, 0.3, 0.3} %Braun


\begin{document}


% Blattgestaltung
\pagestyle{fancy}
\rhead{\textbf{Versuch 59}  \\ Protokoll vom 22.02.2016}
\lhead{\textbf{Dennis van Elten, Christopher Hasenberg}  \\ Fortgeschrittenen-Praktikum Physik M. Sc.}

\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}


\title{Versuch Nr. 59\\ Modulation und Demodulation elektrischer Schwingungen}
\author{Christopher Hasenberg\\ \small{\textit{christopher.hasenberg@hotmail.de}} \and Dennis van Elten\\ \small{\textit{dennis.vanelten@tu-dortmund.de}}}
\date{Durchführungsdatum: 22.02.2016}
\setcounter{page}{-0} 
\maketitle
\thispagestyle{empty} 
\tableofcontents

\newpage

\section{Einleitung / Zielsetzung}
Um mit elektromagnetischen Wellen Informationen über eine große Distanz zu vermitteln, muss diese zunächst moduliert werden. Dazu wird ein hochfrequentes Trägersignal in Hinsicht auf ein niederfrequenteres Nutzsignal in Hinsicht auf Amplitude, Frequenz oder Phase verändert. Anschließend wird durch ein Demodulationsverfahren die Information aus dem veränderten Trägersignal zurückgewonnen. Es existieren dabei verschiedene Verfahren, die gemäß ihrer Eigenschaften wie Störsicherheit, Wirkungsgrad etc. charakterisiert werden. Grob eingeteilt existieren die Amplituden- sowie die Frequenzmodulation. Diese beiden Klassen werden im Rahmen dieses Protokolls untersucht.

\section{Theoretische Grundlagen}
In diesem Abschnitt werden die Verfahren der Amplituden- und Frequenzmodulation vorgestellt und physikalisch erläutert.

\subsection{Amplitudenmodulation (AM)}

Bei der Methode der AM wird ein hochfrequentes Trägersignal in seiner Amplitude im Rythmus einer niederfrequenten Modulationswelle verändert. Die beiden Schwingungen lassen sich über ihre Kreisfrequenz und Amplitude folgendermaßen beschreiben:
\begin{equation}
\begin{aligned}
U_T(t) = \hat{U}_T \cos (\omega_T t) \quad \quad U_M(t) = \hat{U}_M \cos (\omega_M t)
\end{aligned}
\end{equation}
Dann lautet die Zeitabhängigkeit der amplitudenmodulierten Schwingung:
\begin{equation}
    U_3(t) = \hat{U}_T (1 + m \cos \omega_M t) \cos \omega_T t,
    \label{AM_schwingung}
\end{equation}
wobei $0 < m = \gamma\, \hat{U}_M < 1$ den Modulationsgrad bezeichnet. Nachfolgend sei der typische Verlauf dieser dargestellt.
\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{V59_AM.png}}
	\caption{Zeitabhängigkeit der amplitudenmodulierten Schwingung[1].}
	\label{fig:Abbildung}
\end{figure}
Eine andere Darstellung von Gl.(\ref{AM_schwingung}) liefert weitere Erkenntnisse. Diese lässt sich über trigonometrische Beziehungen gewinnen:
\begin{equation}
    U_3(t) = \hat{U}_T \left[ \cos \omega_T t + \frac{1}{2} m \cos (\omega_T + \omega_M) t + \frac{1}{2} m \cos (\omega_T - \omega_M) t \right].
\end{equation}
In dieser erkennt man ein Frequenzspektrum mit den drei Frequenzlinien  $\omega_T$, $\omega_T + \omega_M$ sowie $\omega_T - \omega_M$. Des Weiteren lässt sich feststellen, dass die mittlere Linie unabhängig von der Modulationsamplitude $U_M$ ist. Daher wird bei praktischen Anwendungen diese Trägerabstrahlung vollständig unterdrückt um unnötigen Energieverbrauch zu vermeiden. Besteht $U_M$ aus einer Vielzahl an Frequenzen, so bilden sich die Linien zu Frequenzbändern aus. Die Informationen, die übermittelt werden sollen, liegen bereits vollständig in einem Seitenband vor, so dass der Frequenzbereich auf eines dieser eingeschränkt werden kann. Dann wird die Modulation als Einseitenbandmodulation bezeichnet. Die AM besitzt allerdings zwei Nachteile: geringe Störsicherheit sowie geringe Verzerrungsfreiheit.


\subsection{Frequenzmodulation (FM)}
Das andere Verfahren zur Modulation ist die FM. Dabei wird im Gegensatz zur AM nicht die Amplitude, sondern die Frequenz des Trägersignals im Rythmus des Modulationssignals angepasst. Hier wird die zeitabhängige Modulationsschwingung beschrieben durch
\begin{equation}
    U(t) = \hat{U} \sin \left( \omega_T t + m \frac{\omega_T}{\omega_M} \cos \omega_M t \right),
    \label{schwingung_FM}
\end{equation}
woraus sich die Momentanfrequenz 
\begin{equation}
    f(t) = \frac{\omega_T}{2 \pi} \left( 1 - m \sin \omega_M t \right)
\end{equation}
ergibt. Darin steckt der Faktor $\frac{m \omega_T}{2 \pi}$, der Frequenzhub genannt wird und die Variationsbreite der Schwingungsfrequenz charakterisiert. Ein typischer Verlauf einer frequenzmodulierten Schwingung ist in Abbildung 2 dargestellt.
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{V59_FM.png}}
	\caption{Zeitabhängigkeit der frequenzmodulierten Schwingung[1].}
	\label{fig:Abbildung}
\end{figure}
Im Weiteren können zwei Fälle unterschieden werden. Zum einen die sogenannte Schmalband-Frequenzmodulation $m \frac{\omega_T}{\omega_M} \ll 1$ sowie die starke Frequenzmodulation $m \omega_T \approx \omega_M$. Auf den zweiten Fall soll nur soweit eingegangen werden, dass die frequenzmodulierte Schwingung Besselfunktionen enthält. Für den anderen Fall kann man Gleichung (\ref{schwingung_FM}) über trigonometrische Relationen umformen und auf eine ähnliche Darstellung wie bei der AM bringen. Die Schwingung lautet dann
\begin{equation}
    U(t) = \hat{U} \left[ \sin \omega_T t + \frac{1}{2} m \frac{\omega_T}{\omega_M} \cos (\omega_T + \omega_M) t + \frac{1}{2} m \frac{\omega_T}{\omega_M} \cos (\omega_T - \omega_M) t \right].
\end{equation}
Man erkennt auch hier drei Frequenzlinien, jedoch sind die Seitenbänder um eine Phase von $\pi/2$ gegenüber der Trägerschwingung verschoben.

\section{Technische Realisierung - Modulationsschaltungen}
Nun werden Schaltungen zur Modulation vorgestellt, die auch in der Auswertung dieser Arbeit benutzt werden. Zum einen wird eine Schaltung für die AM, welche aus einem Ringmodulator aufgebaut ist, vorgestellt und zum anderen die für die FM. Daei handelt es sich um eine Variation des Ringmodulators. 

\subsection{Modulationsschaltung für die Amplitudenmodulation}
Für die AM ist eine Schaltung mit einem Bauelement nicht-linearer Kennlinie geeignet, da dieses ein Produkt zweier Spannungen, wie gefordert, bildet. Jedoch reicht eine Schaltung nur bestehend aus den beiden Spannungsquellen $U_M(t)$ und $U_T(t)$ sowie einer Diode nicht aus, da in diesem Fall bei einer Reihenentwicklung zwar das gewünschte Produkt $U_T U_M$ auftaucht, jedoch auch weitere Störterme. Diese müssten im Weiteren aufwendig eliminiert werden. Stattdessen verwendet man einen Ringmodulator, bei dem die zusätzlichen Terme nicht auftauchen. 
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{V59_Ringmodulator.png}}
	\caption{Prinzipielles Schaltungsbild eines Ringmodulators[1].}
	\label{fig:Abbildung}
\end{figure}

Bei einem Ringmodulator kann die Kennlinie durch die Potenzreihe in niedrigster Ordnung beschrieben werden. Der Ringmodulator besteht auf 4 Dioden, die in einem Ring geschaltet sind. An den Eingängen $L$ und $X$ liegen die Träger- sowie die Modulationsschwingung an. Die Dioden bilden einen Spannungsteiler. Die geteilten Spannungen können an den Punkten $\alpha$ und $\beta$ abgegriffen werden und gelangen über einen HF-Transformator an den Ausgang $R$. Da es sich um elektrisch baugleiche Dioden handelt wird bei Abwesenheit von $U_M(t)$ keine Spannung an diesen Punkten auftreten. Erst durch Hinzufügen der Trägerspannung $U_T(t)$ verändern sich die Teilungsverhältnisse und eine Spannung $U_R(t)$ liegt am Ausgang $R$ an. Diese lautet in Abhängigket der Eingangsspannungen
\begin{equation}
    U_R(t) = \gamma\, U_M(t) U_T(t),
\end{equation}
mit der Proportionalitätskonstanten $\gamma$, [1/V]. Außerdem lässt sich aus der vorherigen Gleichung ablesen, dass hier die Trägerabstrahlung verhindert wird.

\subsection{Modulationsschaltung für die Frequenzmodulation}

Zur Erinnerung sei noch einmal darauf hingewiesen, dass bei der FM zwei Seitenbänder sowie eine Trägerschwingung auftauchen, wobei zwischen den beiden Arten von Bändern eine Phasenverschiebung von $\pi/2$ vorliegt. Daher kann eine Modulationsschaltung aus der für die AM gewonnen werden.
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{V59_Ringmodulator2.png}}
	\caption{Variation des Ringmodulators zur FM [1].}
	\label{fig:Abbildung}
\end{figure}

Die unmodulierte Trägerspannung wird daher um $\pi/2$ verschoben zum Ausgang $R$ hinzuaddiert. Die Phasenverschiebung wird mithilfe eines Laufzeitkabels erreicht und die Aufteilung der Leistung der Trägerspannung gelingt mithilfe von Spannungsteilern bzw. Iso-T's.


\section{Technische Realisierung - Demodulationsschaltungen}
Im Weiteren werden Demodulationsschaltungen thematisiert, denn das zuvor modulierte Signal muss am Empfängerort demoduliert werden um die transportierten Informationen zurückzugewinnen. Auch hier wird jeweils eine Schaltung für die Demodulation nach AM bzw. FM vorgestellt.

\subsection{Demodulationsschaltung nach Amplitudenmodulation}
Auch hier dient wieder ein Ringmodulator, da dieser das Produkt zweier Spannungen bildet. Diese Eigenschaften kann zur Demodulation verwendet werden. 

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{V59_Demodulation_AM.png}}
	\caption{Variation des Ringmodulators zur FM [1].}
	\label{fig:Abbildung}
\end{figure}



Dafür wird auf den Eingang $L$ das modulierte Trägersignal mit dem Frequenzlinien bei $\omega_T$ sowie $\omega_T \pm \omega_M$ und auf den Eingang $R$ die usprüngliche Trägerspannung mit der charakteristischen Frequenz $\omega_T$ gegeben. Dann erhält man am Ausgang $X$ Spannungen mit den Frequenzen $\omega_M$ und auch $2 \omega_T \pm \omega_M$. Letztere sind jedoch weit von der gewünschten Frequenz $\omega_M$ entfernt, so dass diese bequem mit einem Tiefpass herausgefiltert werden können. Übrig bleibt dann nur noch das gewünschte Modulationssignal. Ein Problem ist jedoch, die benötigte Spannung mit $\omega_T$ zu bekommen, wenn diese phasenstarr mit der Trägerfrequenz des Senders gekoppelt ist. Eine Lösung dafür liefern Phasenregelkreise. Verhindert wird Sie durch eine Schaltung gemäß Abb. \ref{phasenbeziehung}.

\begin{figure}[h!]
	\centering{\includegraphics[width=9cm]{V59_phasenbeziehung.png}}
	\caption{Demodulator-Schaltung zur Unterdrückung der Phasenstarrheit [1].}
	\label{fig:Abbildung}
	\label{phasenbeziehung}
\end{figure}

Mithilfe der eingebauten Diode werden die negativen Halbwellen abgeschnitten. Übrig bleiben die geforderte Trägerfrequent $\omega_T$ sowie gradzahlige Vielfache derer. Letztere können mittles Tiefpasse eliminiert werden. Verbessert werden kann die Unterdrückung der höheren Frequenzen durch eine Gegentaktschaltung.

\subsection{Demodulationsschaltung nach Frequenzmodulation}
Zur Demodulation einer frequenzmodulierten Schwingung wird hier der sogenannte Flankenmodulator vorgestellt.

\begin{figure}[h!]
	\centering{\includegraphics[width=14cm]{V59_flankenmodulator.png}}
	\caption{Prinzipelle Schaltung eines Flankenmodulators [1].}
	\label{fig:Abbildung}
\end{figure}

Wie zu sehen, handelt es sich dabei um einen LC-Schwingkreis. Die Resonanzfrequenz dieses Schwingkreises wird so eingestellt, dass die geforderte Frequenz $\omega_T$ auf der ansteigenden Flanke der Resonanzkurve liegt. Mit einer Änderung der modulierten Schwingung durch die FM wird am Ausgang eine HF-Spannung $U_C(\omega)$ erzeugt. Dessen Amplitude schwankt mit der Modulation, d.h. die FM wurde in eine AM überführt. Damit kann man das Problem analog zum vorherigen Kapitel lösen.

\section{Auswertung: Verschiedene Modulationen und Demodulationen von HF-Signalen}

Inhalt der Auswertung sind die Erzeugung von amplituden- bzw. frequenzmodulierten Schwingungen und anschließender Demodulation derer. Dazu wird zum einen das Schirmbild des Oszilloskops untersucht sowie das Frequenzspektrum analysiert.

\subsection{Frequenzspektrum der Träger- sowie Modulationsschwingung}

Bevor mittels des hochfrequeten Trägersignals und des niederfrequenten Nutzsignals Modulationen und Demodulationen durchgeführt werden, wird zunächst deren eigenes Frequenzspektrum untersucht. Dazu wird der Generator der jeweiligen Schwingung mit einem Frequenzanalysator verbunden. 

In der nachfolgenden Abbildung \ref{spektrum_modulation} ist das Spektrum der Modulationsschwingung aufgetragen. Die Frequenz des Modulationssignals beträgt $f_M =  103\pm 3\SI{}{kHz}$. Dargestellt ist das Frequenzspektrum im Intervall $[\SI{70}{kHz},\SI{700}{kHz}]$. Der Marker wurde dabei auf den ersten Peak gelegt. Diese entspricht der eingestellten Frequenz am Generator. Neben dieser lassen sich jedoch auch weitere Peaks erkennen. Dabei handelt es sich um höhere Harmonische. Man sieht hier deutlich, dass die Amplituden mit der Anzahl der Oberschwingungen abklingen. Diese entstehen dadurch, dass schon im Generator keine reine Sinussignale erzeugt werden (Störungen sowie Verzerrungen bei der Erzeugung und Weiterleitung). Bei der Modulation später werden dazu außerdem nichtlineare Bauteile (z.B. Diode) verwendet. Diese entnehmen dazu nicht exakt sinusförmig den Strom/die Spannung und erzeugen weitere Verzerrungen. Höhere Harmonische tauchen dabei bei allen sinusförmigen Schwingungen auf. Diese lassen sich darstellen als Summe vieler (bis hin zu unendlicher) sinusförmiger Signale. Andersherum kann bei einer Synthese nicht mehr das originale Signal rekonstruiert werden, sondern stattdessen tauchen Abweichungen auf. Je weniger periodisch das ursprüngliche Signal war, desto kontinuierlicher wird das Frequenzspektrum, d.h. dieses beinhaltet immer mehr Oberwellen.

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{modulation_spektrum.pdf}}
	\caption{Frequenzspektrum der Modulationsschwingung.}
	\label{fig:Abbildung}
	\label{spektrum_modulation}
\end{figure}

Dasselbe Verhalten lässt sich im Frequenzspektrum des Trägersignals beobachten. Hier ist die Frequenz im Bereich $[\SI{1}{MHz},\SI{20}{MHz}]$ aufgezeichnet und das Maximum beträgt $f_T = 5,092\pm 0.001\SI{}{MHz}$. Dieser Peak visualisiert wieder die am Generator eingestellte Frequenz. Auch hier sieht man Oberwellen bei ungefähr $\SI{10}{MHz}$ sowie $\SI{15}{MHz}$, jedoch sind diese hier schmaler als die der Modulationsschwingung, da hier ein Sweep von $\SI{47,5}{s}$ verwendet wurde. Sweep bezeichnet dabei die Geschwindigkeit der Frequenzänderung.


\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{traeger_spektrum.pdf}}
	\caption{Frequenzspektrum der Trägerschwingung.}
	\label{fig:Abbildung}
	\label{spektrum_traeger}
\end{figure}

\newpage
\subsection{Amplitudenmodulation mittels Ringmodulator (mit Trägerunterdrückung)}

Als erste Modulationsaufgabe soll eine AM durchgeführt werden. Dabei wird die Schwebung mit Trägerunterdrückung auf dem Oszilloskop dargestellt. Der schematische sowie der tatsächliche Aufbau sind nachfolgend abgebildet. 

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_a.png}}
	\caption{Aufbau der Amplitudenmodulation mittels Ringmodulator.}
	\label{fig:Abbildung}
	\label{spektrum_traeger}
\end{figure}

Für die Träger- und Modulationsspannung werden dabei die Einstellungen wie schon zuvor verwendet, das heißt deren Frequenz entspricht der aus dem vorherigen Abschnitt. Die Werte seien an dieser Stelle noch einmal aufgeführt:
\begin{equation}
    \begin{aligned}
        U_T = 0.956 \pm 0.01\SI{}{\V} \\
        f_T = 5,092\pm 0.001\SI{}{MHz}\\
        U_M = 1.025 \pm 0.001\SI{}{\V} \\
        f_M = 103 \pm 3 \SI{}{kHz}
    \end{aligned}
\end{equation}
Wird die Schaltung wie oben zu sehen aufgebaut, so ergibt sich das Speicherbild in Abbildung \ref{fig:Abbildung_run}.

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{AM_unbereinigt.png}}
	\caption{Zeitabhängigkeit der Schwebung im 'Run Mode' des Oszilloskops.}
	\label{fig:Abbildung_run}
	\label{AM_unbereinigt}
\end{figure}

Der breite Kanal, der auf der Abbildung zu sehen ist, ist das Resultat der Trigger-Funktion. Es wird auf die aufsteigende Flanke des Trägersignals getriggert und dieses wird fehlerhaft dubliziert. Dieses Problem lässt sich jedoch vermeiden, indem das Bild nicht im 'Run'-, sondern im 'Single'-Mode aufgenommen wird. Dann erhält man für die Zeitabhängigkeit der Schwebungsfrequenz das in Abb. \ref{AM_bereinigt} dargestellte Signal.

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{AM_bereinigt.png}}
	\caption{Zeitabhängigkeit der Schwebung im 'Single Mode' des Oszilloskops.}
	\label{fig:Abbildung}
	\label{AM_bereinigt}
\end{figure}

Man erkennt hier deutlich die Schwebung und ihr periodisches Verhalten, also ihre Zeitabhängigkeit. Auch konnte die geforderte Unterdrückung der Trägerabstrahlung erreicht werden. Diese kann daran ausgemacht werden, dass an den Nullstellen der Einhüllenden auch die Amplitude des AM-Signals verschwindet. \\
\clearpage
Des Weiteren wird das Frequenzspektrum des modulierten Signals untersucht. Dafür wird derselbe Aufbau wie zuvor verwendet, jedoch wird nun das Oszilloskop durch einen Frequenzanalysator ersetzt. Es wurde während des Versuchs das folgende Spektrum aufgezeichnet:

\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{frequenzspektrum_b.pdf}}
	\caption{Frequenzspektrum des AM-Signals.}
	\label{fig:Abbildung}
	\label{AM_frequenzspektrum}
\end{figure}

Bei der Betrachtung des Spektrums, welches logarithmisch dargestellt ist, fällt sofort auf, dass kein zentraler Peak existiert, sondern stattdessen 2 etwa gleichhohe daneben. Dies ist eine Folge der 'Resolution Bandwith', also der minimal aufzulösbaren Frequenz. Daraus folgt, dass die Trägerfrequenz zwar auflösbar ist, die Modulationsfrequenz sich jedoch deutlich unterhalb dieser Grenze befindet. Die vielen weiteren sichtbaren Peaks stellen höhere Harmonische der Modulationsfrequenz dar, deren Amplitude mit zunehmender Ordnung geringer wird.

\clearpage
\subsection{Amplitudenmodulation mittels Diode (mit Trägerabstrahlung) und Bestimmung des Modulationsgrads}
\begin{comment}
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteilc.png}}
	\caption{Aufbau der Amplitudenmodulation mittels Gleichrichterdiode.}
	\label{fig:Abbildung}
	\label{spektrum_traeger}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{frquenzspektrum_diode_aufgabenteilc.png}}
	\caption{Zeitabhängigkeit des AM-Signals für 3 verschiedene Zeitauflösungen. Die Modulation erfolgt dabei mittels einer Diode.}
	\label{fig:Abbildung}
	\label{AM_zeitabhaengigkeit_aufgabenteilc}
\end{figure}
\clearpage
\end{comment}
In diesem Abschnitt soll der allgemeine Fall der Amplitudenmodulation mit Trägerabstrahlung betrachtet werden. Laut Aufgabenstellung soll hier die Zeitabhängigkeit der modulierten Strahlung, sowie das Frequenzspektrum dargestellt werden. Ersteres konnte leider nicht erreicht werden, so dass der Modulationsgrad nur mithilfe des Frequenzspektrums bestimmt wird. 
\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{frequenzspektrum_daten_aufgabenteilc.png}}
	\caption{Frequenzspektrum des AM-Signals, welches mit einer Diode moduliert wurde.}
	\label{AM_frequenzspektrum_aufgabenteilc_miz_daten}
\end{figure}

Nun soll mithilfe der Daten aus Abbildung \ref{AM_frequenzspektrum_aufgabenteilc_miz_daten} der Modulationsgrad berechnet werden. Dazu werden die ersten drei Werte verwandt. Dabei wurde die Leistung ausgehend von der Zusammenhang
\begin{equation}
    n \dBm = 10 \log_{10}(P/\SI{1}{\mW})
\end{equation}
berechnet. Durch Umstellen erhält man
\begin{equation}
    P = 10^{n/10}\SI{}{\mW}
\end{equation}
Bei mehreren Messdurchgängen fiel auf, dass es zu leichten Schwankungen des Leistungspegels kommt. Wir haben uns dazu entschieden diese Variationen mit $\Delta_{\mathrm{dBm}}=\pm 0.01$ abzuschätzen. Mit Gausscher Fehlerfortpflanzung ergibt sich somit der Fehler nach:
\begin{equation}
    \Delta P = \Delta_{\mathrm{dBm}} \cdot P \frac{\ln 10}{10}
\end{equation}
\begin{table}[H]
\centering
\begin{tabular}{cccccc}
    Frequenz [\MHz] & \Delta_{\textrm{Frequenz}} & Leistungspegel [\SI{}{dBm}] & \Delta_{\mathrm{dBm}} & Leistung [\mW] & Fehler[\mW] \\
    \hline 
     5.048 & 0.001 & -59.61 & 0.01   &	0.00000109 & 0.000000011 \\
    5.098 & 0.001  & -65.4  & 0.01   &	0.00000029 & 0.000000003 \\
    4.998 & 0.001   & -65.84 & 0.01    &	0.00000026 & 0.000000003
\end{tabular}
\end{table}
Der erste Wert stellt die Trägerfrequenz dar, während die beiden anderen den Seitenpeaks entsprechen. Um nun den Modulationsgrad zu berechnen wird nun die Relation
\begin{equation}
    m=\frac{P_M}{P_T}
\end{equation}
genutzt. Mit Gausscher Fehlerfortpflanzung erhält man:
\begin{equation}
    \Delta m = \sqrt{\left(\frac{1}{P_T}\Delta P_M \right)^2 + \left(\frac{P_M}{P_T^2}\Delta P_T \right)^2}
\end{equation}
Damit ergeben sich zwei Modulationsgrade:
\begin{equation}
    \begin{aligned}
        m_1 = 0.266 \pm 0.003\\
        m_2 = 0.238 \pm  0.003
    \end{aligned}
\end{equation}
Daraus ergibt sich folgender Mittelwert:
\begin{equation}
    m=  0.252\pm 0.004.
\end{equation}


\subsection{Frequenzmodulation und Bestimmung des Modulationsgrads}
Nun soll die Zeitabhängigkeit einer frequenzmodulierten Schwingung sichtbar gemacht werden. Insbesondere ist die Verschmierung der Sinus-Kurve in x-Richtung in Abbildung \ref{verschmier} zu erkennen.


\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteild.png}}
	\caption{Aufbau der Frequenzmodulation mit niedrigem Frequenzhub.}
	%\label{verschmier}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{FM_aufgabenteild.png}}
	\caption{Zeitabhängikeit des FM-Signals.}
	\label{verschmier}
\end{figure}
Nun soll aus mithilfe der Phasenverschiebung der Modulationsgrad der Frequenzmodulation ermittelt werden. Dabei wird die Verschiebung $\Delta t$ in Abbildung \ref{verschmier} abgelesen:
\begin{equation}
    \Delta t = (3.16\pm 0.35)\cdot 10^{-7}\SI{}{\s}
\end{equation}
Der Fehler wurde dabei beim Ablesen abgeschätzt.
Mit diesem Wert lässt sich nun der Modulationsgrad über die Phasenverschiebung berechnen:
\begin{equation}
    \begin{aligned}
        m &= \Delta \phi = \Delta t \cdot \nu_T \\
        & = 0.32\pm 0.04
    \end{aligned}
\end{equation}


\begin{comment}
\newpage

\begin{figure}[h!]
	\centering{\includegraphics[width=10cm]{FM_aufgabenteild_verbreiterung.png}}
	\caption{Darstellung des FM-Signals mit Ausmessung der Signalverschmierung.}
	\label{fig:Abbildung}
	\label{frequenzmodulation_langes_spektrum}
\end{figure}
\end{comment}
\begin{comment}
Weiterhin soll aus der Verbreiterung der Sinuskurve der Frequenzhub und der Modulationsgrad ermittelt werden. Vom Oszilloskop wurde dazu die minimale und maximale Periode abgelesen:
\begin{equation}
    \begin{aligned}
        T_{min}= (1050\pm 30) \SI{}{\ns}\\
        T_{min}= (1200\pm 30) \SI{}{\ns}
    \end{aligned}
\end{equation}
Die Trägerfrequenz und Modulationsfrequenz hatten folgende Werte:
\begin{equation}
    \begin{aligned}
        \nu_{T}&=  \SI{1}{\MHz}\\
        \nu_{M}&=  \SI{100}{\kHz}
    \end{aligned}
\end{equation}
Aus den beiden Periodendauern ergibt sich folgender Frequenzhub:
\begin{equation}
    \begin{aligned}
        2\Delta\nu &= \left(\frac{1}{T_{max}}\right)^{-1}-\left(\frac{1}{T_{min}}\right)^{-1}\\
        &=\nu_{max}-\nu_{min}\\
        \rightarrow \Delta \nu &= 119.5  \pm 27.2 \SI{}{kHz}
    \end{aligned}
\end{equation}
Der Fehler des Frequenzhubs berechnet sich nach:
\begin{equation}
    \sigma_{\Delta \nu}= \sqrt{\sigma_{\nu_{max}}^2+\sigma_{\nu_{min}}^2}
\end{equation}
Mi der eingestellten Trägerfrequenz ergibt sich
 somit der Modulationsgrad
\begin{equation}
    m=(0.119 \pm 0.03)
\end{equation}.
\end{comment}



\subsection{Analyse eines phasenempfindlichen Gleichrichters}
In diesem Aufgabenteil soll die Abhängigkeit der Amplitude vom Kosinus der Phasenverschiebung dargestellt werden. 
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteile.png}}
	\caption{Aufbau des phasenempfindlichen Gleichrichters aus einem Ringmodulator.}
	\label{fig:Abbildung}
	\label{spektrum_traeger}
\end{figure}


Dabei handelt es sich im Wesentlichen um einen Verlauf, der linear angenähert wird. Um aus der Zeitverzögerung $\Delta t$ die Phasenverschiebung zu berechnen wird folgende Formel benötigt:
\begin{equation}
    \Delta \phi = \pm  2\pi\Delta t f
\end{equation}
Die Frequenz beträgt dabei $f=\SI{1}{\MHz}$. Berechnet man nun entsprechenden Kosinus für jede Phasenverschiebung und sie gegen die Amplitude auf, erhält man folgendes Diagramm:
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufgabe_e.pdf}}
	\caption{Regression: Phasenverschiebung und Amplitude}
\end{figure}

Dabei erhält man durch Regressionsanalyse folgenden linearen Zusammenhang:
\begin{equation}
    U(\cos \phi)=[-(1417 \pm 80)\cos \phi + (1504 \pm 78)]\SI{}{\mV}
\end{equation}


Nachfolgend sind beispielhaft die Laufzeitbilder für \SI{4}{ns} und \SI{32}{ns} dargestellt, mit denen die Phasenverschiebung generiert wird.

\begin{figure}[h!]
	\centering{\includegraphics[width=7.5cm]{beispiele_aufgabenteile.png}}
	\caption{Beispielhafte Speicherbilder für eine Lauzzeitverzögerung von \SI{4}{ns} (oben) bzw. \SI{32}{ns} (unten).}
	\label{fig:Abbildung}
\end{figure}
Dabei fällt auf, dass die Amplitude mit der Phasenverschiebung ansteigt. Das ist ein Hinweis darauf, dass die Verschiebung hier offenbar zu konstruktiver Interferenz führt.

%%%%%%%%%%%%%% Aufgabenteil f %%%%%%%%%%%%%%%%

\newpage
\subsection{Demodulation eines AM-Signals mittels Ringmodulator.}

Ziel dieses Aufgabenteils ist die Demodulation eines zuvor amplitudenmodulierten Signals mit einem Ringmodulator. Dafür muss zunächst die Modulationsschwingung mit einem Trägersignal moduliert werden. Da hierfür ebenfalls ein Ringmodulator benötigt wird werden insgesamt zwei benötigt. Nachfolgend ist sowohl der schematische als auch der realisierte Aufba dargestellt. Darüber hinaus zeigt Abb. \ref{am_demod} das zurückgewonnene Modulationssignal und zum Vergleich auch das vom Generator erzeugte.

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteilf.png}}
	\caption{Aufbau der Amplitudendemodulation mittels Ringmodulator.}
	\label{fig:Abbildung}
\end{figure}


\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{scope_124.png}}
	\caption{Speicherbild des Oszilloskops mit dem mittels Modulation und Demodulation zurückgewonnenen Modulationssignal sowie der ursprünglichen Schwingung.}
	\label{fig:Abbildung}
	\label{am_demod}
\end{figure}

Oben ist das zurückgewonnene Signal abgebildet. Vergleicht man dieses mit der in grün dargestellten Modulationsschwingung, so lässt sich eine deutliche Ähnlichkeit, jedoch keine vollkommene Übereinstimmung, zwischen den beiden Signalen feststellen. Es fällt zum einen auf, dass beide Signale dieselbe Periodenlänge besitzen. Weiter existieren zwei dominante Unterschiede im Bereich der Phasenbeziehung und Amplitude. Das aus der Modulation gewonnene Signal ist gegeüber dem anderen in seiner Phase verschoben und besitzt darüber hinaus eine geringere Amplitude. Beide Effekte sind jedoch zu erwarten. Während die Phasenverschiebung von $\frac{\pi}{2}$ auf den im Tiefpass verbauten Kondensator zurückzuführen ist, liegt der Grund für die abgeschwächte Amplitude in den Widerständen, die die Bauteile und Kabel besitzen. Unter der Annahme idealisierter Bedingungen konnte somit das Modulationssignal erfolgreich zurückgewonnen werden. 


%%%%%%%%%%%%%%% Aufgabenteil g %%%%%%%%%%%%%%%%%

\subsection{Amplitudendemodulation mittels einer Gleichrichterdiode}

Nachdem zuvor eine Amplitudenmodulation mit einem Ringmodulator durchgeführt wurde, soll nun dasselbe mit einer Gleichrichterdiode erreicht werden. Dabei soll einmal das Signal direkt nach der Diode (Punkt A) und zum anderen das nach Durchlaufen des Tiefpasses gespeichert werden. 
\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteilg.png}}
	\caption{Aufbau der Amplitudendemodulation mittels Gleichrichterdiode.}
	\label{fig:Abbildung}
    \label{diode_kaputt}
\end{figure}

In der Abbildung \ref{diode_kaputt} lässt sich bereits das bei der Durchführung erzeugte Speicherbild nach der Diode erkennen. Dabei ist zu erkennen, dass keinerlei Schwingung zu beobachten ist. Damit ist auch kein Bild nach dem Tiefpass erzeugt worden. Dies lässt die Vermutung aufkommen, dass die Diode, als wesentliches Bauteil, beschädigt bzw. nicht funktionsfähig ist. Deshalb wurde das reine Träger- bzw. Modulationssignal ohne weitere Bauteile auf die Diode weitergeleitet, jedoch konnte auch dann kein Signal gemessen werden. Häufige Fehlerquellen sind Wackelkontakte oder Unterbrechungen in Leiterbahnen. Bei einer korrekt funktionierenden Diode muss diese in Durchlassrichtung einen geringen bzw. in Gegenrichtung einen sehr hohen Widerstand aufweisen oder äquivalent dazu eine Spannung von \SI{0,7}{V} in Durchlassrichtung besitzen.


%%%%%%%%%%%% Aufgabenteil h %%%%%%%%%%%%%%%

\subsection{FM-Modulation mit anschliessender Demodulation}

Zum Abschluss wird wie bereits in Abschnitt 5.4 eine Spannung frequenzmoduliert, allerdings wird diese nun auch demoduliert. Für die Demodulation wird ein Flankenmodulator gemäß Abb. 7 verwendet. Dabei wird bei geeigneter Einstellung des Schwingkreises das FM-Signal in ein AM-Signal umgewandelt. Der Aufbau bis zu diesem Punkt sowie das Bild auf dem Oszilloskop sind im Folgenden dargestellt.


\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteilh.png}}
	\caption{Aufbau der Frequenzdemodulation mittels Flankendemodulator.}
	\label{fig:Abbildung}
	\label{spektrum_traeger}
\end{figure}


\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{scope_126.png}}
	\caption{Speicherbild nach Durchlaufen des LC-Schwingkreises. Dargestellt ist außerdem das ursprüngliche Modulationssignal}
	\label{fig:Abbildung}
	\label{schwingkreis}
\end{figure}

\newpage
Dargestellt ist zum einen das Signal, welches von der Schaltung bis inklusive Schwingkreis erzeugt wird sowie das vom Generator erzeugte sinusförmige Modulationssignal zum Vergleich. Man erkennt daran eindeutig, dass es das Problem der FM-Demodulation in ein amplitudenmoduliertes umgewandelt wurde. Erkennbar ist des Weiteren, dass die Modulationsschwingung und die Einhüllende der AM dieselbe Phasenlänge besitzen. Auch ist eine Phasenverschiebung auszumachen  sowie eine geringere Amplitude. Gründe dafür können die BNC-Kabel sowie andere Bauteile sein. An den Schwingkreis wird nun eine Diode sowie ein Tiefpass geschaltet. Erstere sorgt dafür, dass die negativen Halbwellen eliminiert werden und der Tiefpass filtert die Trägerfrequenz $\omega_T$ sowie höhere Harmonische derer raus, so dass am Ende nur das ursprüngliche Signal übrig bleibt. Der erweiterte Aufbau ist in Abb. \ref{aufbau_h2} dargestellt und Abbildung \ref{fm_diode} das in diesem Versuch erhaltene Speicherbild.


\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{aufbau_aufgabenteilh2.png}}
	\caption{Umwandlung des FM-Signals in ein AM-Signal mit einem LC-Schwingkreis.}
	\label{fig:Abbildung}
	\label{aufbau_h2}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=12cm]{scope_127.png}}
	\caption{Speicherbild nach Durchlaufen der Diode und des Tiefpasses. Das ursprüngliche Signal konnte dabei aufgrund eines fehlerhaften Bauelements nicht erzeugt werden.}
	\label{fig:Abbildung_diode}
	\label{fm_diode}
\end{figure}


Zu sehen ist sofort, dass das Modulationssignal nicht zurückgewonnen werden konnte. Die Demodulation war somit nicht erfolgreich. Da das Signal nach dem Schwingkreis wie erwartet vorlag und aufgrund der Erkenntnisse aus den vorherigen Aufgabenteilen kann auf eine fehlerhafte Konfiguration geschlossen werden. Bei Betrachtung von Abbildung \ref{fig:Abbildung_diode} fällt die geringe Amplitude des modulierten Signals von etwa $\SI{0.12}{\V}$ auf. Da handelsübliche Germaniumdioden eine Durchlassspannung von etwa $\SI{0.3}{\V}$ besitzen, kann es folglich nicht zu einer Demodulation kommen. Die Verzerrungen, die nun im Speicherbild verglichen mit dem vorherigen auftreten, sind durch den Kondensator im Tiefpass entstanden. Dies lässt auf eine korrekte Funktionsweise des Filters schließen. Durch die zusätzlichen Bauteile ist außerdem die Amplitude weiter gesunken im Vergleich zum ursprünglichen Signal. \\
Eigentlich hätte man hier das Modulationssignal mit einigen Verzerrungen, hervorgerufen durch den Kondensator, erwartet sowie eine weitere Phasenverschiebung zwischen den Signalen aufgrund der Laufzeit durch die weiteren elektrischen Bauteile.

\section{Diskussion der Ergebnisse}
Im Verlauf des Versuchs konnten die meisten Aufgabenstellungen erfolgreich gelöst werden. Modulationen und Demodulationen entprechen den erwarteten Ergebnissen. Auch die errechneten Werte (Modulationsgrad / Frequenzhub) erscheinen in ihrer Größenordnung sinnvoll. Leider ist es nicht gelungen die Oberwellen nach Aufgabenteil c) der Versuchsanleitung auf dem Oszilloskop darzustellen. Ebenfalls ist es nicht gelungen eine AM-Demodulation mittels Gleichrichterdiode durchzuführen. Dieser Schritt war aufgrund der zu geringen Amplitude des modulierten Signals nicht möglich. 
\newline
\newline
Die Bestimmung des Modulationsgrads ist schwierig zu bewerten, da der 'wahre' Wert nicht bekannt ist. Jedoch scheint die Größenordnung intuitiv sinnvoll.

\section{Literaturverzeichnis}
\begin{enumerate}
	\item[1] TU Dortmund, Physikalisches Fortgeschrittenenpraktikum: Skript zum Versuch Nummer 59, "Modulation und Demodulation elektrischer Schwingungen"
\end{enumerate}

\end{document}
