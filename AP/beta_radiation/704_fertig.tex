\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 704}  \\ Protokoll vom \date{22.04.14}}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum SS 14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 704 - Absorption von $\gamma$- und $\beta$-Strahlung}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{22.April.2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
In diesem Experiment werden die Wechselwirkungen von $\gamma$- und $\beta$-Strahlung mit Materie untersucht, indem Absorptionskurven aufgenommen und ausgewertet werden. Für die $\gamma$-Strahlung soll das exponentielle Absorptionsgesetz für verschiedene Materialien bestimmt werden und für die $\beta$-Strahlung, die aus schnellen Elektronen besteht und ein hohes Wechselwirkungsverhalten aufweist, ist das Ziel eine Formel zur Bestimmung der Maximalenergie.

\section{Theoretische Grundlagen}
Beim Auftreffen der Strahlung auf ein Material kommt es aufgrund der Wechselwirkungen zwischen dieser und dem Material zu einer Intensitätsabnahme. Ein Maß für die Wahrscheinlichkeiten solcher Wechselwirkungen ist der Wechselwirkungsquerschnitt $\sigma$. Dabei wird eine fiktive Trefferfläche definiert. Man betrachtet einen Absorber mit dem Querschnitt $F$ und der Dicke $D$ auf den $n$ Masseteilchen treffen. Wegen der vorliegenden Überdeckung durch die große Menge der Atome ist es notwenig alle Wirkungsquerschnitte $\sigma$ in infinitesimal dünnen Schichten zu summieren und anschließend zu integrieren. Daraus ergibt sich in Abhängigkeit von der ursprünglichen Anzahl der Teilchen $N_0$ das exponentielle Absorptionsgesetz:

\begin{equation}
N (D) = N_0 e^{-n \sigma D} = N_0 e^{-\mu D}.
\end{equation}

$\mu$ bezeichnet den Absorptionskoeffizienten.
Mithilfe einer Absorptionsmessung und der damit verbundenen Berechnung von $\mu$ ist die Ermittlung des Wirkungsquerschnitts $\sigma$ möglich. Unter Näherung ergiben sich die Formel

\begin{equation}
n = \frac{z N_L \rho}{M}.
\end{equation}

Dabei ist $z$ die Ordnungszahl, $N_L$ die Loschmidt'sche Zahl, $\rho$ die Dichte und $M$ das Molekulargewicht.
Somit ergibt sich für den Wirkungsquerschnitt als grobe Annäherung

\begin{equation}
\sigma = \frac{\mu}{n} = \frac{\mu M}{z N_L \rho}.
\end{equation}


\subsection{$\gamma$-Strahlung}
Beim Übergang eines im angeregten Zustand versetzten Kerns in einen weniger hoch angeregten Zustand emittiert dieser Energie in Form von $\gamma$-Strahlung. Zur Veranschaulichung werden die $\gamma$-Quanten als Teilchen aufgefasst, deren Energie die Differenz der vom Übergang betroffenen Kernzustände sind:

\begin{equation}
E_{\gamma} = E_1 - E_2
\end{equation}

Das einzelne $\gamma$-Quant kann beim Eindringen in das Material auf verschiedene Arten in Wechselwirkung treten, da es sowohl mit den Atomelektronen als auch mit den Kernen sowie mit den elektrischen Feldern interagieren kann. Im Weiteren werden nur die Wechselwirkungsprozesse im üblichen Energieintervall von 10 kEv bis 10 MeV behandelt. Dabei wird zwischen Annihilation, inelastischer Streuung und elastischer Streuung unterschieden. Die wichtigsten Prozesse sind der innere Photo-Effekt, der Compton-Effekt und die Paarbildung, die im Folgenden erläutert werden.

\subsubsection{Der innere Photo-Effekt}
Beim inneren Photoeffekt wechselwirken das $\gamma$-Quant und ein Hüllenelektron. Ist die Energie des Quants größer als die Bindungsenergie des Elektrons, kommt es zum Photo-Effekt und das Elektron wird aus seiner Hülle gelöst und das $\gamma$-Quant wird absorbiert. Häufig tritt dieser Prozess bei Elektronen auf der innersten Schale auf. Die dadurch enstehenden Lücken werden durch Elektronen von höheren Schalen aufgefüllt. Des Weiteren nimmt die Absorptionswahrscheinlichkeit mit zunehmender Quantenenergie ab.

\subsubsection{Der Compton-Effekt}
Beim Compton-Effekt handelt es sich um einen inelastischen Streuungsprozess; Richtungs- und Energieänderungen sind die Folge. Es handelt sich dabei um die Wechselwirkung der gestrahlten Quanten mit freien Elektronen, bei der das Quant nicht verschwindet. Jedoch erfolgt durch die Streuung eine Intensitätsabnahme des $\gamma$-Strahls.
Der Wirkungsquerschnitt $\sigma_{com}$ des Compton-Effekts in Abhängigkeit der Quantenenergie lautet nach Klein und Nishina

\begin{equation}
\sigma_{com} = 2\pi r_e^2\left[\frac{1+\epsilon}{\epsilon^2}\left(\frac{2(1+\epsilon)}{1+2\epsilon}-\frac{1}{\epsilon}ln(1+2\epsilon)\right)+\frac{1}{2\epsilon}ln(1+2\epsilon)-\frac{1+3\epsilon}{(1+2\epsilon)^2}\right]
\end{equation}

wobei $r_e = 2,82 \cdot 10^{-15}$ als der klassische Elektronenradius definiert ist. Dieser läuft gegen einen Grenzwert, der als Thomson'scher Wirkungsquerschnitt bezeichnet wird:

\begin{equation}
\lim\limits_{\epsilon \rightarrow 0}{\sigma_{com}} = \frac{8}{3} \pi r_e^2 := \sigma_{Th}.
\end{equation}

Für den Absorptionskoeffizienten $\mu_{com}$ des Compton-Effekts ergibt sich

\begin{equation}
\mu_{com} = n \sigma_{com} (\epsilon) = \frac{z N_L \rho}{M} \sigma_{com} (\epsilon)
\end{equation}
in Abhängigkeit von $\epsilon = \frac{E_{\gamma}}{m_0 c^2}$, wobei $m_0$ die Ruhemasse des Elektrons ist. 


\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=14cm]{Bilder/Bild1.png}}
\caption{Schematische Darstellung des Compton-Streuprozesses [1]}
\label{fig:Abbildung}
\end{figure}

\newpage
\subsubsection{Die Paarerzeugung}
Bei der Paarerzeugung handelt es sich um einen Annihilationsprozess in Wechselwirkung mit den elektrischen Feldern. Dieser tritt auf, wenn die Energie des Quants größer als die doppelte Ruhemasse des Elektrons mit $1,02 MeV$ ist. Die Annihilation erfolgt durch Bildung eines Elektrons und Positrons.

\subsubsection{Einflussbereiche der Prozesse}
Insgesamt überlagern sich die drei beschriebenen Effekte bei der Wechselwirkung der $\gamma$-Strahlung mit einem Material. Dabei unterscheiden sie sich jedoch in ihrem Einflussbereich. Bei niedrigen Quantenenergien ist der Photoeffekt der dominierende Effekt. Der Compton-Effekt leistet erst bei Energien ab 200 keV einen entscheidenen Beitrag. Die Paarerzeugung hat erst bei höheren Energien einen großen Einfluss auf den Totaleffekt. Ab etwa 100 MeV liegt nur noch Paarerzeugung vor.

\subsection{$\beta$-Strahlung}
$\beta$-Strahlung besteht aus negativen oder postiven Elektronen mit hoher kinetischer Energie. Diese $\beta$-Teilchen entstehen durch Umwandlung eines Nukleons unter Bildung eines Antineutrinos $\overline v_e$ oder eines Neutrinos $v_e$:

\begin{align}
n &\longrightarrow p + \beta^- + \overline v_e \\
p &\longrightarrow n + \beta^+ + v_e.
\end{align}

Das bei dieser Umwandlung emittierte Neutrino bzw. Antineutrino sorgt für die Energie, Impuls und Drehimpulserhaltung. Ebenso verteilt sich die Energie statistisch auf die Teilchen, was das kontinuierliche Spektrum (Abb.2) erklärt.

\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=10cm]{Bilder/Bild2.png}}
\caption{Emissionsspektrum eines $\beta$-Strahlers [1]}
\label{fig:Abbildung}
\end{figure}

Entscheidend für die Wechselwirkung zwischen den $\beta$-Teilchen und dem Material ist die Ladung der Elektronen sowie die im Vergleich zu anderen Elemenatarteilchen geringe Masse. In Folge dieser Eigenschaften weist ein $\beta$-Teilchen deutlich mehr Wechselwirkungen in der Materialschicht auf als ein $\gamma$-Quant auf. Im Wesentlichen exisiteren drei Prozesse, die in den folgenden Abschnitten erläutert werden.

\subsubsection{Elastische Streuung am Atomkern}
Diese Form der Wechselwirkung ist bekannt als Rutherford-Streuung. Die $\beta$-Teilchen werden durch das Coulomb-Feld der Atomkerne stark abgelenkt, so dass zum einen durch die Auffächerung des Strahls die Intensität abnimmt und zum zweiten ist die tatsächlich zurückgelegte Strecke der $\beta$-Teilchen im Absorber größer als ihre (gradlinige) Reichweite (Abb.3). Jedoch ist die Annahme eines Coulomb-Feldes sehr vereinfacht. Ebenso werden relativistische Effekte, die aufgrund der hohen Geschwindigkeiten berücksichtigt werden müssten, sowie das magnetische Moment des Elektrons vernachlässigt. Das Fazit der elastischen Streuung am Atomkern ist eine geringe Energieabnahme und eine starke Richtungsänderung.

\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=8cm]{Bilder/Bild3.png}}
\caption{Veranschaulichung des Unterschieds zwischen Bahnlänge und Reichweite individueller $\beta$-Teilchen in einer Materialschicht [1]}
\label{fig:Abbildung}
\end{figure}

\newpage
\subsubsection{Inelastische Streuungen an Atomkernen}
$\beta$-Teilchen erfahren aufgrund ihrer Ladung im Coulomb-Feld des Kerns eine Beschleunigung. Dabei geben die Teilchen nach der Quantenmechanik Energie in Form von emittierenden Photonen mit höherer Energie. Die dadurch entstehende Strahlung wird als Bremsstrahlung bezeichnet. Ein Maß für die Wahrscheinlichkeit dieses Prozesses ist der Wirkungsquerschnitt $\sigma_{Br}$. Pro Atomkern gilt:

\begin{equation}
\sigma_{Br} = \alpha \cdot r_e^2 \cdot z^2.
\end{equation}

$\alpha$ bezeichnet dabei die Sommerfeld'sche Feinstrukturkonstante.
Des Weiteren ergibt sich im Durschnitt für die durch den Bremsstrahlungseffekt abgegebene Energie pro $\beta$-Teilchen

\begin{equation}
E_{Br} [keV] \approx 7 \cdot 10^{-7} z \cdot E_{\beta}^2
\end{equation}
in Abhängigkeit von der Energie $E_{\beta}$ des einfallenden $\beta$-Teilchens.

\subsubsection{Inelastische Streuung an Elektronen}
Die Atome, die mit den $\beta$-Teilchen wechselwirken, werden ionisiert und angeregt. Wegen der geringen Energie, die für diesen Prozess benötigt wird, kann das Elektron diese Ionisierungs- und Anregungsprozesse öfters wiederholen.

\subsection{Die Absorptionskurve} 
Aufgrund der Komplexität der $\beta$-Strahlung ist es nicht möglich ein allgemeines Gesetz zwischen der Absorberdicke $D$ und der Intensität $n(D)$, wie bei der $\gamma$-Strahlung, zu formulieren. Jedoch gilt das Absorptionsgesetz für geringe Schichtdicken annähernd, wenn die Elektronen ein kontinuierliches Spektrum besitzen. Erst ab der maximalen Reichweite $R_{max}$ treten starke Abweichungen auf. Die nachfolgende konstante Intensität erklärt sich durch die Bremsstrahlung, jedoch nicht mehr durch die $\beta$-Teilchen.

\begin{figure}[htbp]
\centering{\includegraphics[height=10cm,width=14cm]{Bilder/Bild4.png}}
\caption{Absorptionskurve für einen natürlichen $\beta$-Strahler [1]}
\label{fig:Abbildung}
\end{figure}

Der Wert $R_{max}$ wird dabei nur von den energiereichsten Elektronen bestimmt. Daraus lässt sich die beim $\beta$-Zerfall freiwerdende Gesamtenergie $E_{max}$ bestimmen. In dem Versuch gilt die Beziehung

\begin{equation}
E_{max} = 1,92 \sqrt{(R_{max}^2 + 0,22 R_{max})} [MeV].
\end{equation}
$R_{max} = \rho D$ ist hierbei die Massenbelegung.

\newpage
\section{Durchführung}
Die $\beta$- bzw. $\gamma$-Strahlungsquelle wird auf ein Geiger-Müller-Zählrohr ausgerichtet, so dass dieses die Strahlung misst. Des Weiteren wird eine Messung des Nulleffekts durchgeführt.

\section{Auswertung}
\subsection{Bestimmung von $\mu$ und $N_0$ aus den $\gamma$-Absorptionskurven von Fe und Pb}
Die Messwerte zu der Absorption der $\gamma$-Strahlung eines $^{137}$Cs-Strahlers sind in Tabelle sind für Eisen und Blei in den Tabellen 1 und 2 gelistet sowie in den Diagrammen 5 und 6 halblogarithmisch dargestellt.\\
Die Zählung der Untergrundstrahlung ergab 227 Counts in 900 Sekunden also ergibt sich diese zu
\begin{align*}
N_0=\frac{227}{900 \mathrm{s}}=0,252 \frac{1}{\mathrm{s}}
\notag
\end{align*}
\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c}
Dicke D [mm]&N pro 60s [1/60s]&N pro 1s [1/s]&N-$N_0$ [1/s]\\
\hline
5&	1567&	26.117& 25.864\\
10&	1273&	21.217&	20.964\\
15&	1010&	16.833&	16.581\\
20&	783&	13.050&	12.798\\
25&	575&	9.583&	9.331\\
30&	414&	6.900&	6.648\\
35&	353&	5.883&	5.631\\
40&	299&	4.983&	4.731\\
45&	261&	4.350&	4.098\\
50&	214&	3.567&	3.314\\
55&	168&	2.800&	2.548\\
60&	128&	2.133&	1.881\\
\end{tabular}
\caption{Messwerte $\gamma$-Absorption an Eisen}
\end{table}
\begin{table}[htbp]
\centering{\begin{tabular}{c|c|c|c}
Dicke D [mm]&N pro 60s [1/60s]&N pro 1s [1/s]&N-$N_0$ [1/s]\\
\hline
1&	1809&	30.150&	29.898\\
3&	1533&	25.550&	25.298\\
10&	628&	10.467&	10.214\\
13&	488&	8.133&	7.881\\
20&	205&	3.417&	3.164\\
23&	189&	3.150&	2.898\\
30&	83&	1.383&	1.131\\
40&	56&	0.933&	0.681\\
50&	23&	0.383&	0.131\\
60&	20&	0.333&	0.081\\
\end{tabular}}
\caption{Messwerte $\gamma$-Absorption an Blei}
\end{table}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild10.png}}
\caption{Halblogarythmische Auftragung der Strahlung gegen die Dicke des Eisens}
\label{fig:Abbildung}
\end{figure}
\newpage
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild11.png}}
\caption{Halblogarythmische Auftragung der Strahlung gegen die Dicke des Bleis}
\label{fig:Abbildung}
\end{figure}

Die Ausgleichskurven wurden nach Gleichung 2.1 bestimmt. Daraus ergaben sich die Absorptionskoeffizienten zu
\begin{align}
\notag
\mu_{Fe}=0,49\pm0,01\, \mathrm{cm^{-1}}\\
\notag
\mu_{Pb}=1,14\pm0,04\, \mathrm{cm^{-1}}
\notag\end{align}
Des weiteren werden die Werte für die Größe $N(0)$ als
\begin{align}
\notag
N_{Fe}(0)=33,58\pm0,67\\
\notag
N_{Pb}(0)=34,19\pm0,56
\notag
\end{align}
bestimmt.
\subsection{Vergleich zu den theoretischen Werten}
Nach der Formel von Klein und Nishina wird der Wirkungsquerschnitt $\sigma_{com}$ für $^{137}$Cs mit $\epsilon=1,295$ bestimmt:
\begin{align}
\sigma_{com}=2,56\cdot10^{-25}cm^2
\notag
\end{align}

\begin{table}
\centering{\begin{tabular}{c|c|cc|}
&Eisen&Blei\\
\hline
z				&26				&82\\
$\rho [\mathrm{g/cm^3}]$			&7,874			&11,34\\
M		$[\mathrm{g/mol}]$		&55,8			&207,2\\
$N_L$	$[1\mathrm{mol}]$		&\multicolumn{2}{c}{$6,022\cdot10^{23}$}\\
$\sigma_{com}[\mathrm{cm^2}]$	&\multicolumn{2}{c}{$2,56\cdot10^{-25}$}\\
\end{tabular}}
\caption{Werte zur Berechnung der Absorptionskoeffizienten}
\end{table}

Durch Gleichung 2.6 können mit den Werten aus Tabelle 3 die theoretische Absorptionskoeffizienten berechnet werden: 
\begin{align}
\notag
\mu_{Fe}=0,57\frac{1}{\mathrm{cm}}\\
\notag
\mu_{Pb}=0,69\frac{1}{\mathrm{cm}}
\notag
\end{align}
Im Vergleich zum experimentell bestimmten Wert fällt für das Eisen eine Abweichung von ca. 13,6\% auf. Daraus lässt sich folgern, dass die Absorption von $\gamma$-Strahlung am Eisen zum größten Teil durch den Compton-Effekt bedingt wird.\\ Beim Blei hingegen beträgt die Abweichung etwa 64,4\%. Daher ist naheliegend, dass beim Blei neben dem Compton-Effekt auch weitere Absorptionseffekte eine stärkere Rolle spielen, die somit zu einer höheren Absorptionsrate führen. Besonders Der Photo-Effekt ist hierbei relevant, da dieser bei Atomen mit hoher Elektronenbindungsenergie auf der inneren Schalen - wie beim Blei - besonders Ausgeprägt auftritt. 
\subsection{Absorption von $\beta$-Strahlung}
Zunächst wurde wieder die Nullrate bestimmt mit 3874 Zählungen in 900 Sekunden woraus sich eine Untergrundstrahlung von 4,3 Zählungen pro Sekunde ergibt.
Die Messwerte zu der Zählraten sind in Tabelle 4 Aufgelistet und außerdem in Abbildung 7 der Logarithmus der Zählrate gegen die Dicke des Aluminiums aufgetragen.
\begin{table}[htbp]
\centering{
\begin{tabular}{c|c|c|c|c|c}
Dicke D [mm] &$\Delta D$ [mm] &N [1/60s]&Zeit [s]&N pro 1s [1/s]&log(N pro s)\\
\hline
0	&0&	30055&	10&	3005,50&	3,48\\
102	&1&	23784&	120&	198,20&	2,30\\
126	&1&	11306&	120&	94,21&	1,97\\
153	&0,5&	5146&	120&	42,88&	1,63\\
160	&1&	2914&	120&	24,28&	1,39\\
200	&1&	1360&	120&	11,33&	1,05\\
253	&1&	508&	120&	4,23&	0,63\\
302	&1&	386&	120&	3,22&	0,51\\
338	&5&	433&	120&	3,61&	0,56\\
400	&1&	414&	120&	3,45&	0,54\\
444	&2&	496&	120&	4,13&	0,62\\
482	&1&	638&	180&	3,54&	0,55\\
\end{tabular}}
\caption{Messwerte $\beta$-Absorption an Blei}
\end{table}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild12.png}}
\caption{Zählrate aufgetragen gegen die Dicke des Aluminiums}
\label{fig:Abbildung}
\end{figure}
Wie die ersten sechs Messwerte wurden nach betrachten des Graphen verwendet um die Ausgleichsgerade 2 gemäß f(x)=A*x+B zu bestimmen und die weiteren sechs analog für Ausgleichsgerade 1:
\begin{align}
\notag
A_1&=&(-0,00005\pm0.00026) \frac{1}{\mu m}\qquad B_1&=&(0,58327\pm0,1001)\\
\notag
A_2&=&(-0,0124\pm0,0005) \frac{1}{\mu m}\qquad B_2&=&(3,5005\pm0,0707) 
\notag
\end{align}
Mithilfe dieser Werte lässt sich die Maximale Eindringtiefe $D_{max}$ über den Schnittpunkt der beiden Geraden bestimmen:
\begin{align}
D_{max}=\frac{B_2-B_1}{A_1-A_2}=(236,22\pm14,65)\mathrm{\mu m}
\notag
\end{align}
Für $R_{max}$ ergibt sich daraus zu
\begin{align}
R_{max}=D_{max}*\rho_{Al}=(0,063\pm0,004)\frac{g}{cm^2}
\notag
\end{align}
und die Maximalenergie wird aus Gleichung 2.11 wiederum zu
\begin{align}
E_{max}=(0,258\pm0,003)\mathrm{MeV}
\notag
\end{align}
bestimmt.

\section{Diskussion}
Wie bereits im Abschnitt 4.2 erläutert deuten die Messwerte darauf hin, dass beim Blei der Photo-Effekt in deutlich höherem Maße stattfindet als bei Eisen. Im darauffolgenden Abschnitt könnte eine mögliche Fehlerquelle in der ungenauen Bestimmung der beiden Ausgleichsgeraden aufgrund von einer geringen Anzahl von Messwerten bestehen.
\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 704, Absorption von $\gamma$- und $\beta$-Strahlung
\end{enumerate}


\end{document}


