\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{subfigure} 
\usepackage{siunitx}
\usepackage{amssymb}
\pagestyle{fancy}
\rhead{\textbf{Versuch 354}  \\ Protokoll vom \today}
\lhead{\textbf{Konstantin Pfrang, Dennis van Elten}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}
\begin{document}
\title{Versuch 354 - Gedämpfte und erzwungene Schwingungen}
\author{Konstantin Pfrang \and Dennis van Elten}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents
\newpage
\section{Ziel}
Zunächst sollen gedämpfte elektrische Schwingungen im RCL-Kreis näher betrachtet werden, wobei besonders die zeitliche Abnahme der Kondensatorspannung von Interesse ist. Anschließend wird ein Schwingkreis untersucht, der von außen mit einer sinusförmigen Spannung angeregt wird.
\section{Theorie}
Ein RCL-Schwingkreis (Abb.1) besteht im allgemeinen aus einem Ohmschen Widerstand, einem Kondensator und einer Spule. Dabei fungieren sowohl der Kondensator als auch die Spule als Energiespeicher. Die sich im System befindende Energie kann nun ständig zwischen den beiden Elementen pendeln und somit wechselt der Strom $I(t)$ periodisch sein Vorzeichen. Falls ein Widerstand vorhanden ist, so wird diese Schwingung gedämpft, indem die gesamte elektrische Energie zugunsten von Joulescher Wärme am Widerstand verringert wird.\\
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Gedämpfter Schwingkreis [1]}
\label{fig:Abbildung}
\end{figure}
Bei einem angetriebenen Schwingkreis wird Energie in den Kreis eingeführt. Diese Energiezufuhr wird je nach gewählter Frequenz unterschiedlich. Bei einer bestimmten Frequenz, der sogenannten Resonanzfrequenz, besteht ein Maximum der Energiezufuhr.\\
Im Folgenden sollen sowohl für den angeregten sowie für den nicht angeregten Schwingkreis eine Differentialgleichung hergeleitet werden.
\subsection{Aufstellung und Lösung der Differentialgleichungen}
\subsubsection{gedämpfte Schwingung}
Aus den Beziehungen
\begin{eqnarray}
\notag
U_R(t)&=&RI(t)\\
\notag
U_C(t)&=&\frac{Q(t)}{C}\\
\notag
U_L(t)&=&L\frac{dI}{dt}
\notag
\end{eqnarray}
folgt aus dem 2. Kirchhoffschen Gesetz unter Beachtung des Zusammenhangs
\begin{equation}
I=\frac{dQ}{dt}
\notag
\end{equation}
die gesuchte DGL für eine gedämpfte Schwingung in der Form von
\begin{equation}
\frac{d^2I}{dt^2}+\frac{R}{L}\frac{dI}{dt}+\frac{1}{LC}I=0
\end{equation}
Für die Lösung dieser linearen, homogenen Differentialgleichung wird der Ansatz
\begin{equation}
I(t)=Ae^{i\omega t}
\end{equation}
\begin{equation}
\text{($i$=imaginäre Einheit; $A$ und $\omega$ komplexe Zahlen)}
\notag
\end{equation}
verwendet. Mit diesem Ansatz erhält man für die Frequenz $\omega$ die beiden Werte
\begin{equation}
\omega_{1,2}=i\frac{R}{2L}\pm \sqrt{\frac{1}{LC}-\frac{R^2}{4L^2}}
\notag
\end{equation}
Unter Verwendung der Abkürzungen
\begin{equation}
2\pi \mu :=\frac{R}{2L}\quad \text{und}\quad 2\pi \nu:=\sqrt{\frac{1}{LC}-\frac{R^2}{4L^2}}
\notag
\end{equation}
ergibt sich die Lösung
\begin{equation}
I(t)=e^{2\pi\mu t}(A_1 e^{i2\pi\nu t}+A_2 e^{-i2\pi\nu t})
\end{equation}
Die Gestalt der Lösung hängt nur wesentlich von dem Verhältnis von $1/LC$ zu $R^2/4L^2$ ab:\\
\newline
\newline
\textbf{1.Fall}:\quad gedämpfte Schwingung
\begin{center}
$\frac{1}{LC}>\frac{R^2}{4L^2}$
\end{center}
Damit stellt die Lösung eine gedämpfte Schwingung der Form
\begin{equation}
I(t)=A_0 e^{i2\pi\mu t} cos(2\pi\nu t + \phi)
\end{equation}
mit der Periodendauer $T=\frac{1}{\nu}=\frac{2\pi}{\sqrt{1/LC-R^2/4L^2}}$ dar. Diese Lösung ist in Abb. 2 dargestellt, die einhüllende rote Kurve wird durch $\pm e^{-2\pi\mu t}$ beschrieben.
\begin{figure}[htbp]
\centering{\includegraphics[height=5cm]{Bilder/Bild2.png}}
\caption{Darstellung einer gedämpften Schwingung [1]}
\label{fig:Abbildung}
\end{figure}
\newpage
\textbf{2.Fall}:\quad aperiodische Dämpfung
\begin{center}
 $\frac{1}{LC}<\frac{R^2}{4L^2}$
\end{center}
Hierbei handelt es dich um aperiodische Dämpfung. Die Lösung $I(t)$ enthält nun keine oszillatorischen Anteil und kann je nach den Integrationskonstanten $A_1$ und $A_2$ entweder direkt monoton gegen null streben oder davor einen Extremwert erreichen, wie in Abb. 3 in den durchgezogenen Linien dargestellt. Nach einer längeren Zeit verläuft $I(t)$ in etwa proportional zu
\begin{equation}
e^{(-R/2L+\sqrt{R^2/4L^2-1/LC)t}}
\notag
\end{equation}
was bedeutet, dass ein einfaches Relaxationsverhalten vorliegt.
\begin{figure}[htbp]
\centering{\includegraphics[height=5cm]{Bilder/Bild3.png}}
\caption{Möglicher Zeitverlauf des Stromes in einem Schwingkreis mit aperiodischer Dämpfung [1]}
\label{fig:Abbildung}
\end{figure}
Die gestrichene Kurve des Diagramms zeigt den aperiodischen Grenzfall für den gilt $1/LC=R^2/4L^2$ also $\nu=0$. Es gilt dann
\begin{equation}
I(t)=Ae^{-\frac{R}{2L}t}=Ae^{-\frac{t}{\sqrt{LC}}}
\end{equation}
und ist der Zustand, für den I(t) am schnellsten gegen null geht.
\subsubsection{erzwungene Schwingung}
Unter der Anregung $U_A(t)=U_0e^{i\omega t}$ ergibt sich die Differentialgleichung
\begin{equation}
LC\frac{d^2 U_C}{dt^2}+RC\frac{dU_C}{dt}+U_C=U_0e^{i\omega t}
\end{equation}
wobei $U_C$ die Spannung am Kondensator darstellt.
Der Ansatz $U_C(\omega,t)=U_{max}e^{i\omega t}$ liefert für die Amplitude $U_{max}$ der Kondensatorspannung:
\begin{equation}
U_{max}=\frac{U_0(1-LC\omega^2-i\omega RC)}{(1-LC\omega^2)^2+\omega^2R^2C^2}
\end{equation}
mit der Phase
\begin{equation}
\phi(\omega)=\arctan\left(\frac{-\omega RC}{1-LC\omega^2}\right)
\end{equation}
und dem Betrag von $U_{max}$ der außerdem auch dem Maximum der gesuchten Funktion $U_C$ ist:
\begin{equation}
U_{C,max}(\omega)=|U_{max}(\omega)|=\frac{U_0}{\sqrt{(1-LC\omega^2)^2+\omega^2R^2C^2}}
\end{equation}
Anhand dieser Funktion erkennt man, dass die Amplitude am Kondensator an der sogenannten Resonanzfrequenz $\omega_{res}=\sqrt{\frac{1}{LC}-\frac{R^2}{2L^2}}$ ein Maximum besitzt, das größer als $U_0$ sein kann.\\
Bei schwacher Dämpfung $\frac{R^2}{2L^2}<<\frac{1}{LC}$ nähert sich $\omega_{res}$ der Kreisfrequenz $\omega_{0}$ der ungedämpften Schwingung an und für die Amplitude $U_{C,max}$ gilt in Abhängigkeit von $U_0$:
\begin{equation}
U_{C,max}=\frac{U_0}{\omega_0 RC}=\frac{U_0}{R}\sqrt{\frac{L}{C}}
\end{equation}
Wird die Dämpfung $R$ verschwindend klein, so kann die Amplitude $U_{C,max}$ gegen $\infty$ gehen, was als Resonanzkatastrophe bezeichnet wird. Der Faktor $\frac{1}{\omega_0 RC}$ wird als Güte $q$ bezeichnet. Unter Schärfe wird die Breite von der Resonanzkurve $U_{C,max}$ verstanden, die durch die Frequenzen $\omega_+$ und $\omega_-$, bei denen $U_C$ noch $\frac{1}{\sqrt{2}}$ seines Maximums besitzt. Unter Berücksichtigung von $\frac{R^2}{L^2}<<{\omega_0}^2$ ist die Breite der Resonanzkurve
\begin{equation}
\omega_+ - \omega_ - \approx \frac{R}{L}
\end{equation}
Für die Güte gilt somit auch
\begin{equation}
q=\frac{\omega_0}{\omega_+ - \omega_-}
\end{equation}
Für die Phasendifferenz $\phi$ ergibt sich eine Frequenzabhängigkeit: für große Anregefrequenzen geht $\phi$ gegen $\pi$ und für kleine gegen 0. Für $\omega_0=\pm\frac{1}{\sqrt{LC}}$ gilt $\phi=-\frac{\pi}{2}$
\newpage
\section{Durchführung}
\subsection{Zeitabhängigkeit der Amplitude einer gedämpften Schwingung}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild6.png}}
\caption{verwendete Schaltung zur Zeitabhängigkeit einer gedämpften Schwingung [1]}
\label{fig:Abbildung}
\end{figure}
Der Schaltkreis aus Abb. 5 wird durch eine Rechteckspannung mit einer niedriger Frequenz zu einer gedämpften Schwingung angeregt. Dabei sollte die Frequenz der Impulse so gewählt werden, dass die Amplitude um den Faktor 3 bis 8 abklingt, bevor eine Anregung durch den nächstes Puls entsteht. Für die Messung wird ein hochohmiger Tastkopf verwendet, damit dämpfende Einflüsse vernchlässigt werden können.
\subsection{Bestimmung des Dämpfungswiderstandes $R_{ap}$ beim aperiodischen Grenzfall}
Der Aufbau entspricht erneut dem Kreis aus Abb. 5 bis auf den Unterschied, dass der Widerstand durch einen veränderbaren Widerstand ersetzt wird. Es wird mit maximalem Widerstand begonnen und dieser wird verringert, bis sich auf dem Oszilloskop gerade so keine Überschwingung zeigt.
\subsection{Frequenzabhängigkeit der Kondensatorspannung eines Serienresonanzkreises}  
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild7.png}}
\caption{Schaltung zur Aufnahme des Frequenzganges eines RLC-Kreises [1]}
\label{fig:Abbildung}
\end{figure}
Nach dem in Abb. 6 dargestellten Schaltkreis erfolgt die Anregung durch eine Sinusschwingung und die Kondensatorspannung sowie die Frequenz wird am Oszilloskop gemessen. Der Eigenwiderstand des Voltmeters wird durch einen Tastkopf wie in 3.1 unterdrückt. Dieser besitzt keinen eigenen Frequenzgang und ist somit konstant $U_0$.
\newpage
\subsection{Frequenzabhängigkeit der Phasenverschiebung}
Es wird der Aufbau aus Abb. 6 leicht verändert, indem das Voltmeter durch den zweiten Oszilloskopeingang verwendet wird. Es werden für verschiedene Frequenzen die Phasenverschiebungen a und die Wellenlänge b der Schwingungen gemessen und daraus die Phasenverschiebung $\Phi$ der Kondensatorspannung berechnet:
\begin{equation}
\Phi=\frac{a}{b}360\symbol{23}
\end{equation}

\section{Auswertung}
\subsection{Kennwerte des RCL-Schwingkreises}
Verwendet wurde für den Versuch der Schwingkreis mit der Typbezeichnung "Gerät 3". Seine Eigenschaften sind in der folgenden Tabelle dargestellt.
\begin{table}[htbp]
\centering\begin{tabular}{c c c c} 
Physikalische Größe & Wert & absoluter Fehler & Einheit\\
\hline  
L		& 3,5		& 0,01 &	mH 			\\
C		& 5,0		& 0,02 &	nF 			\\
R_1	& 30,3	& 0,10 &	\Omega 	\\
R_2 & 271,6	& 0,20 &	\Omega	\\
\end{tabular}
\caption{Kennwerte des RLC-Kreises "Gerät 3"}
\label{fig:Tab1}
\end{table}

\subsection{Bestimmung des effektiven Dämpfungsfaktors}
Über die Zeitabhängigkeit der Amplitude einer gedämpften Schwingung ist es möglich den effektiven Dämpfungskoeffizienten zu bestimmen. Dazu werden einige Wertepaare, die aus dem Thermodruck gewonnen werden, verwendet. Diese sind in Tabelle 2 aufgeführt. Die Einteilung des Thermodrucks lautet $50 \mu s$ pro Kästchen in der Zeitachse und $1V$ in der Spannungsachse.
\begin{table}[htbp]
\centering\begin{tabular}{c | c | c} 
$\mathrm {t[\mu s]}$ & $\mathrm {U [V]}$ & $\mathrm{ln (U/[V])}$\\
\hline  
12,4		& 1,800	&	 0,588	\\
40,0		& 1,275	&	 0,243	\\
67,3		& 1,002	&  0,002	\\
93,7 		& 0,777	&	-0,252	\\
120,0		& 0,521	&	-0,652	\\
146,2		& 0,434	&	-0,835	\\
172,1		& 0,369	& -0,997	\\
201,3 	& 0,225	&	-1,492	\\
227,2		& 0,200	&	-1,609	\\
251,9		& 0,191	&	-1,655	\\
\end{tabular}
\caption{Aus dem Thermodruck entnommene Wertepaare}
\label{fig:Tab1}
\end{table}

Nach [Gl.(2.3)] berechnet sich der Abfall der Stromstärke I bzw. der Spannung U mittels einer e-Funktion, daher wird der natürliche Logarithmus der Spannung verwendet. Somit lässt sich über die Steigung der Ausgleichsgeraden der Dämpfungswiderstand ermitteln. Die graphische Darstellung ist in Abb.7 zu erkennen.
\begin{figure}[htbp]
\centering{\includegraphics[width=12cm]{Bilder/Bild9.png}}
\caption{Abklingende Amplitude der gedämpften Schwingung}
\label{fig:Abbildung}
\end{figure}
Die dargestellte Augleichsgerade besitzt die Form f(x) = mx + b, mit
\begin{align}
m &= (-9,797 \pm 0,412) \frac{1}{ms} \\
b &= (0,639 \pm 0,063).
\end{align}
Es gilt nun für Gerät 3
\begin{align}
m &= -\frac{R}{2L} \\
R &= 2mL = 68,58 \Omega.
\end{align}
Der Fehler berechnet sich aus einem Spezialfall der Gauß'schen Fehlerfortpflanzung
\begin{equation}
\Delta R = \sqrt{(\frac{\Delta m}{m})^2 + (\frac{\Delta L}{L})^2} \cdot R = 3\Omega.
\end{equation}
Insgesamt ergibt sich für R = (68,58 \pm 3,00) \Omega.
\newline

Die Abklingdauer $T_{ex}$ berechnet sich als der negative Kehrwert der Ausgleichsgeraden. Diese beträgt somit
\begin{equation}
T_{ex} = -\frac{1}{m} = \frac{2L}{R} = (0,102 \pm 0,04) ms.
\end{equation}
Bei dem Vergleich zwischen dem ermittelten Widerstand und dem angegebenen Wert des eingebauten Widerstandes $R_1 = (30,3 \pm 0,1) \Omega$ erkennt man eine große Diskrepanz. Dies liegt an dem vernachlässigten Beiträgen des ohmschen Widerstandes der Spule sowie des Innenwiderstandes der Spannungsquelle.

\newpage
\subsection{Dämpfungswiderstand $R_{ap}$ des aperiodischen Grenzfalls}
Der in dem Experiment ermittelte Wert für den aperiodischen Grenzfall lautet
\begin{equation}
R_{ap,exp} = 2900 \Omega.
\end{equation}
Der theoretische Wert lässt sich mit der Bedingung für den aperiodischen Grenzfall,$\frac{1}{LC} = \frac{R^2}{4L^2}$ ,berechnen. Es ergibt sich
\begin{equation}
R_{ap,theo} = 2 \sqrt{\frac{L}{C}} = (1673,32 \pm 4,11)\Omega.
\end{equation}
Der Fehler lässt sich mittels der Gauß'schen Fehlerfortpflanzung berechnen. In diesem Fall lautet sie zusammengefasst
\begin{equation}
\Delta R_{ap} = \sqrt{\frac{1}{LC} \cdot (\Delta L)^2 + \frac{L}{C^3} \cdot (\Delta C)^2}.
\end{equation}
Die große Diskrepanz von $42,3 \%$ zwischen dem experimentellen und theoretisch ermittelten Wert ist zurückzuführen auf die weiteren vorhandenen Widerstände in den verwendeten Leitungen und besonders in der Spule.

\subsection{Erzwungene Schwingung - Frequenzabhängigkeit}
In Tabelle 3 sind die Messdaten aufgetragen und in Abbildung 8 graphisch dargestellt. Die gemessene Spannung $U_C$ wird dabei durch die des Tastkopfes $U_T$ geteilt, damit dessen Frequenzunabhängigkeit berücksichtigt wird. $U_T$ beträgt konstant 2V.
\begin{table}[htbp]
\centering\begin{tabular}{c | c | c | c} 
$\omega \mathrm{[kHz]}$ & $U_C \mathrm{[V]}$ & $\mathrm{U_C/U_T}$ & $\mathrm{ln(U_C/U_T)}$\\
\hline  
1			& 2,00	&	1,00	& 0,00\\
5			& 2,06	&	1,03	& 0,03\\
10		& 2,16	& 1,08	& 0,08\\
15 		& 2,32	&	1,16	& 0,15\\
20		& 2,82	&	1,41	& 0,34\\
22,5	& 3,16	& 1,58	& 0,46\\
24 		& 3,44	&	1,72	& 0,54\\
24,5	& 3,52	&	1,76	& 0,57\\
25		& 3,74	&	1,87	& 0,63\\
25,5	& 3,84	&	1,92	& 0,65\\
26		& 4,00	&	2,00	& 0,69\\
26,5	& 4,16	& 2,08	& 0,73\\
27 		& 4,24	&	2,12	& 0,75\\
28		& 5,20	&	2,60	& 0,96\\
30		& 6,30	&	3,15	& 1,15\\
32		& 8,00	& 4,00	& 1,39\\
34 		& 11,20	&	5,60	& 1,72\\
36		& 18,00	&	9,00	& 2,20\\
36,5	& 19,80	&	9,90	& 2,29\\
37		& 20,60	&	10,30	& 2,33\\
37,5	& 20,20	&	10,10	& 2,31\\
38		& 18,40	& 9,20	& 2,22\\
39 		& 14,60	&	7,30	& 1,99\\
40		& 11,20	&	5,60	& 1,72\\
42		& 7,40	&	3,70	& 1,31\\
44		& 5,40	& 2,70	& 0,99\\
46 		& 4,40	&	2,20	& 0,79\\
48		& 3,70	&	1,85	& 0,62\\
50		& 3,20	&	1,60	& 0,47\\
\end{tabular}
\caption{$U_C$ in Abhängigkeit von der Frequenz \omega}
\label{fig:Tab1}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild10.png}}
\caption{Logarithmische Darstellung der Frequenzabhängigkeit der Kondensatorspannung zur Speisespannung}
\label{fig:Abbildung}
\end{figure}

\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild11.png}}
\caption{Lineare Darstellung der Frequenzabhängigkeit der Kondensatorspannung zur Speisespannung im Resonanzbereich}
\label{fig:Abbildung}
\end{figure}

Die aus dem Diagramm bestimmte Resonanzfrequenz beträgt ungefähr 37 kHz.
Der experimentell ermittelte Gütewert berechnet sich aus der maximalen Spannung $U_{C, max}$ und der zugehörigen Generatorspannung $U_T$. Es gilt
\begin{equation}
q_{exp} = \frac{U_{C,max}}{U_T} = \frac{20,6 V}{2 V} = 10,3.
\end{equation}
Ebenso kann die Güte/ Resonanzerhöhung über die Beziehung zwischen Gesamtdämpfungswiderstand $R$, Kondensator $C$ und Spule $L$ theoretisch bestimmt werden:
\begin{equation}
q_{theo} = \frac{1}{R} \sqrt{\frac{L}{C}} = 12,2.
\end{equation}
Die Abweichung zwischen dem experimentellen und theoretischen Wert beträgt in etwa 15,7 \%.
Anschließend wird in der Abb.9 der Resonanzbereich genauer betrachtet. Die Breite der Resonanzkurve kann definiert werden durch die beiden Frequenzen, bei denen $U_C$ nur noch $\frac{1}{\sqrt{2}}U_{max}$ beträgt. Diese Linie ist in der Abbildung durch die grüne Linie dargestellt. Die Frequenzen an den Schnittpunkten betragen:
\begin{align}
\omega_+ &= 39,1 \mathrm{kHz}\\
\omega_- &= 35,0 \mathrm{kHz}.
\end{align}
Daraus ergibt sich für die Breite der Resonanzkurve 
\begin{equation}
b_{exp} = \omega_+ - \omega_- = 4,1 \mathrm{kHz}.
\end{equation} 
Theoretisch lässt sich die Breite mithilfe der Gleichung [Gl.(2.11)] berechnen, allerdings mit der Korrektur $\frac{1}{2 \pi}$, da in der Näherung mit Kreisfrequenzen gerechnet wird:
\begin{equation}
b_{theo} = \frac{1}{2 \pi}\frac{R}{L} = 12,35 \mathrm{kHz}.
\end{equation}
Der theoretische Wert ist 3mal so groß wie der experimentell ermittelte.
\begin{equation}
q = \frac{\omega_0}{b_{exp}} = 9,15.
\end{equation}

\subsection{Frequenzabhängigkeit der Phasenverschiebung}
Nun wird die Phasenverschiebung zwischen der Kondensatorspannung und der Speisespannung betrachtet. Diese lässt sich bestimmen über den zeitlichen Abstand a der Spannungsgraphen und der Schwingungsdauer b. Die Werte sind in der folgenden Tabelle aufgelistet. Dabei berechnet sich die Phase über die Beziehung nach Gleichung [Gl.(3.1)].

\begin{table}[htbp]
\centering\begin{tabular}{c | c | c | c} 
$\omega \mathrm{[kHz]}$ & $\mathrm{a[\mu s]}$ & $\mathrm{b[\mu s]}$ & $\mathrm{\phi [Grad]}$\\
\hline  
1			& 0,00	&	1000	& 0,00\\
5			& 0,00	&	200		& 0,00\\
10		& 1,40	& 100		& 5,04\\
15 		& 1,84	&	66,67	& 9,94\\
20		& 1,96	&	50		& 14,11\\
25		& 2,40	& 40		& 21,60\\
30 		& 3,44	&	33,33	& 37,16\\
35		& 5,52	&	28,57	& 69,56\\
35,5	& 5,80	&	28,17	& 74,12\\
36		& 6,12	&	27,78	& 79,31\\
36,5	& 6,36	&	27,40	& 83,56\\
37		& 6,60	& 27,03	& 87,90\\
37,5 	& 6,92	&	26,67	& 93,41\\
38		& 7,12	&	26,32	& 97,39\\
38,5	& 7,36	&	25,97	& 102,03\\
39		& 7,60	& 25,64	& 106,71\\
39,5 	& 7,76	&	25,32	& 110,33\\
40		& 7,92	&	25		& 114,05\\
45		& 8,31	&	22,22	& 134,64\\
50		& 8,50	&	20		& 153,00\\
\end{tabular}
\caption{Messdaten zur Frequenzabhängigkeit von $U_C$}
\label{fig:Tab1}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild12.png}}
\caption{Frequenzgang der Phase um die Resonanzfrequenz}
\label{fig:Abbildung}
\end{figure}

In Abb. 10 sind die Werte der Phase $\phi$ in Abhängigkeit von der Frequenz aufgetragen. Aus dieser können die Werte für $\omega_0$, $\omega_1$ und  $\omega_2$ entnommen werden. Dabei sind diese so definiert, dass bei $\omega_0$ eine Phase von $\frac{\pi}{2}$, bei $\omega_1$ von $\frac{\pi}{4}$ und bei $\omega_2$ von $\frac{3 \pi}{4}$ vorliegt. Diese sind durch die farblichen Linien in der Abb. gekennzeichnet. Die entnommenen Werte betragen in etwa
\begin{align}
\omega_{0, exp} &= 32,8 kHz \\
\omega_{1, exp} &= 37,3 kHz \\
\omega_{2, exp} &= 45,0 kHz.
\end{align}
Damit ergibt sich 
\begin{equation}
\omega_{2, exp} - \omega_{1, exp} = 12,2 kHz.
\end{equation}
Die theoretischen Werte ergeben sich aus den Gleichungen
\begin{align}
\omega_{1,2,theo} &= \pm \frac{R}{2L} + \sqrt{\frac{R^2}{4L^2} + \frac{1}{LC}} \\
\omega_{1, theo} &= 203,37 kHz \\
\omega_{2, theo} &= 280,97 kHz.
\end{align}
Die theoretischen Werte sind mit der experimentell erhaltenen Breite vereinbar, da die theoretischen Werte Kreisfrequenzen sind und somit um den Faktor $2 \pi}$ größer sind  als die experimentell bestimmten Werte.

\section{Diskussion}
Die aus dem Thermodruck entnommenen Werte ergeben zuverlässige Werte für die Abklingdauer der Amplitude. Im weiteren Verlauf der Auswertung sind immer wieder Unterschiede zu erkennen. Zum Teil liegen diese an dem in die Rechnung einbezogene Widerstände und den tatsächlich vorhandenen. Zum anderen liegen sie an schwer abzulesenden/ermittelten Werte und weiteren Problemen. Der Gütewert ist jedoch relativ ähnlich.

\section{Literaturverzeichnis}

\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 354, Gedämpfte und erzwungene Schwingungen
\newline
http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V354.pdf
\end{enumerate}

\end{document}