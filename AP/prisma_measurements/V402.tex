\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 402}  \\ Protokoll vom \date{06.05.14}}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum SS 14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 402 - Dispersionsmessungen am Glasprisma}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{06.Mai.2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
In diesem Experiment wird das Dispersionsverhalten von Licht in einem Quarzprisma untersucht. Ziel des Versuches ist die Bestimmung des Brechungsindex von Quarz sowie die Untersuchung der Dispersion. 

\section{Theoretische Grundlagen}
\subsection{Dispersion}
In diesem Fall wird Licht als elektromagnetische Welle aufgefasst, die in Materie mit den dortigen Elektronen wechselwirkt. Die Ionenrümpfe und Elektronen werden dort in Schwingungen versetzt und die Ausbreitungsgeschwindigkeit des Lichts wird infolgedessen abgeschwächt. Trifft das Licht nicht orthogonal auf die Grenzfläche erfolgt eine Richtungsänderung, die als Brechung definiert ist. 
Beschrieben wird die Brechung durch den Brechungsindex $n$, der das Verhältnis der Geschwindigkeiten in beiden Medien beschreibt:

\begin{equation}
n := \frac{v_1}{v_2}.
\end{equation}

Dabei spielt die Frequenz eine wichtige Rolle, denn die damit verbundenen unterschiedliche Energien regen die Elektronen und Ionenrümpfe unterschiedlich stark an. Die Abhängigkeit einer Größe von der Frequenz bzw. der Wellenlänge wird als Dispersion bezeichnet

\subsection{Prinzip von Huygens und Snellius-Gesetz}

\begin{figure}[htbp]
\centering{\includegraphics[width=8cm]{Bilder/Bild1.png}}
\caption{Geometrische Herleitung des Snellius-Gesetz mithilfe des Huygens'schen Prinzips [1]}
\label{fig:Abbildung}
\end{figure}

Die Brechung kann erklärt werden mithilfe des Huygens'schen Prinzips. Dieses besagt, dass jeder Punkt einer bestehenden Wellenfläche als Zentrum einer neuen kugelförmigen Elementarwelle aufgefasst werden kann. Ebenfalls sagt es aus, dass sich die neue Lage der Wellenfront aus einer Überlagerung aller Elementarwellen ergibt. Durch geometrische Überlegungen, die in Abb.1 dargestellt sind, folgt das Snellius'sche Brechungsgesetz,dass die Beziehung zwischen Winkel und Ausbreitungsgeschwindigkeiten beschreibt:

\begin{equation}
n = \frac{v_1}{v_2} = \frac{sin (\alpha)}{(\beta)}.
\end{equation}

\subsection{Die Dispersionsgleichung}
Für die Herleitung der Dispersionsgleichung wird die Annahme gemacht, dass die Absorption durch das Material vernachlässigt werden kann. Die Untersuchung am Quarzprisma im Bereich des sichtbaren Licht erfüllt diese Bedingung. Des Weiteren ergibt sich aus der Voraussetzung, dass das Licht die Form einer ebenen Welle vorweist und den Überlegungen, dass auf die Teilchen im Material eine rücktreibende Kraft proportional zur Auslenkung und eine Dämpfung propotional zur Geschwindigkeit wirkt die Differentialgleichung:

\begin{equation}
m_h \frac{d^2 \vec{x}_h}{dt^2} + f_h \frac{d \vec{x}_h}{dt} + a_h \vec{x}_h = q_h \vec{E}_0 e^{i \omega t}.
\end{equation}
$h$ bezeichnet dabei die Teilchenart. Unter Einbeziehung der Polarisation

\begin{equation}
\vec{P} = \sum\limits_h {N_h q_h \vec{x}_h}
\end{equation}

aus der Elektrodynamik lässt sich die gesamte Polarisation aller Teilchen bestimmen:

\begin{equation}
\vec{P} = \sum\limits_h \vec{P}_h = \sum\limits_h \frac{1}{\omega_h^2-\omega^2+i\frac{f_h}{m_h}\omega} \frac{N_q q_h^2}{m_h} \vec{E}_0 e^{i \omega t}.
\end{equation}

$w_h = \frac{a_h}{m_h}$ bezeichnet dabei die Resonanzfrequenz des Systems.
Über die beiden Beziehungen $\vec{P} = (\epsilon - 1) \epsilon_0 \vec{E}$ und der Maxwell'schen Relation $n^2 = \epsilon$ ist es möglich einen Ausdruck für den komplexen Brechungsindex in Abhängigkeit von der Lichtfrequenz aufzustellen:

\begin{equation}
\tilde{n} = 1 + \sum\limits_h \frac{1}{\omega_h^2-\omega^2+i\frac{f_h}{m_h}\omega}{N_q}{m_h}{q_h^2}{\epsilon_0}.
\end{equation}

Der komplexe Brechungsindex $\tilde{n}$ setzt sich dabei zusammen aus dem reellen Koeffizienten $n$ und der Absorptionskonstanten des Lichts in Materie $k$, so dass $\tilde{n} = n (1-ik)$ gilt. 
Durch Zerlegung des Real- und Imaginärteils der Gleichung und unter der Voraussetzung, dass die Materie im untersuchten Spektralbereich durchsichtig und farblos ist ($n^2k \approx 0$) erhält man die von der Freuqenz abhängige Disperionsgleichung

\begin{equation}
n^2 (\omega) = 1 + \sum\limits_h \frac{N_h}{\epsilon_0} \frac{q_h^2}{m_h} \frac{1}{\omega^2 - \omega_h^2}.
\end{equation}

Da es jedoch einfacher ist die Wellenlänge $\lambda$ als die Frequenz $\omega$ zu messen kann die voranstehende Gleichung abgeändert werden zu 

\begin{equation}
n^2 (\lambda) = 1 + \sum\limits_h \frac{N_h q_h^2}{4 \pi^2 c^2 \epsilon_0 m_h} \frac{\lambda^2 \lambda_h^2}{\lambda^2 - \lambda_h^2}.
\end{equation}

Für den Versuch wird davon ausgegangen, dass das Material nur eine Absorptionsstelle $\lambda_1$ besitzt. Dann ist es möglich die eben hergeleitete Disperionsgleichung [Gl.(2.8)] in Potenzreihen nach $\frac{\lambda_1}{\lambda}$ zu entwickeln:
\newline
$\lambda \gg  \lambda_1$

\begin{equation}
n^2(\lambda) = A_0 + \frac{A_2}{\lambda^2} + \frac{A_4}{\lambda^4} + ... , A > 0
\end{equation}

\begin{flushleft}
$\lambda \ll  \lambda_1$
\end{flushleft}

\begin{equation}
n^2(\lambda) = 1 - A_2'\lambda^2 - A_4'\lambda^4 + ... , A' > 0
\end{equation}

Die Werte für $A$ und $A'$ können gemäß geometrischer Überlegungen des Huygens'schen Prinzips (Abb.1) bestimmt werden. Die Dispersionskurven für [Gl.(2.9)] und [Gl.(2.10)] sind in Abbildung 3 dargestellt.

\begin{figure}[htbp]
\centering{\includegraphics[height=5cm,width=10cm]{Bilder/Bild2.png}}
\caption{Dispersionskurven für - a) [Gl.(2.9)] und - b) [Gl.(2.10)]  [1]}
\label{fig:Abbildung}
\end{figure}

\newpage
\section{Durchführung}
\subsection{Versuchsaufbau}
Als Lichtquelle dient eine Hg-Halogenlampe. Das Licht durchläuft gesammelt als paralleles Strahlenbündel gemäß Abb.3 das Kollimatorrohr und trifft auf das drehbare Quarzprisma. Dort wird das Licht zweimal gebrochen und kann durch ein schwenkbares Fernrohr beobachtet werden. Auf der Drehfläche des Prismas ist eine Winkelskala abgemessen, mit deren Hilfe die erforderliche Winkel bestimmt werden können.

\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=16cm]{Bilder/Bild3.png}}
\caption{Schematische Darstellung eines Prismen-Spektralapparates [1]}
\label{fig:Abbildung}
\end{figure}

\subsection{Messung des brechenden Winkels $\phi$}
 Das Prisma wird hierfür mit seiner brechenden Seite, wie in Abb.4 dargestellt, ausgerichtet. Anschließend werden die Winkel der beiden reflektierten Strahlen gemessen und aus der geometrischen Beziehung

\begin{figure}[htbp]
\centering{\includegraphics[height=7cm,width=9cm]{Bilder/Bild5.png}}
\caption{Messung des brechenden Winkels $\phi$ [1]}
\label{fig:Abbildung}
\end{figure}

\begin{equation}
\phi = \frac{1}{2} (\phi_r - \phi_l)
\end{equation}
der Brechungswinkel berechnet.

\subsection{Messung des ablenkenden Winkels $\eta$}

\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=12cm]{Bilder/Bild4.png}}
\caption{Symmetrischer Strahlengang durch das Prisma [1]}
\label{fig:Abbildung}
\end{figure}

Die Apparatur wird so angeordnet, so dass ein symmetrischer Strahl (Abb.5) vorliegt. Dieser entsteht durch Überlagerung von reflektiertem und gebrochenem Strahl. Diese Bedingung wird durch Drehung des Prismas und des Fernrohrs eingestellt. Nun wird der zugehörige Winkel $\Omega_r$ des Fernrohrs abgelesen, anschließend das Prisma spiegelsymmetrisch angeordnet und der neue Winkel des Fernrrohrs $\Omega_l$ gemessen.  Für $\eta$ gilt

\begin{equation}
\eta = 180 - (\Omega_r - \Omega_l).
\end{equation}

\subsection{Bestimmung des Brechungsindex n über die Winkel $\phi$ und $\eta$}
Ist die Anforderung eines symmetrischen Strahlenganges durch das Prisma erfüllt folgt aus dem Snellius'schen Gesetz:

\begin{equation}
n = \frac{sin \alpha}{sin \beta} = \frac{sin(\frac{\eta + \phi}{2})}{sin(\frac{\phi}{2})}, 
\end{equation}
mit $\beta = \frac{\phi}{2}$, $\alpha = \frac{\eta + \phi}{2}$ und $\eta = 2(\alpha - \beta)$.

\section{Auswertung}
Der Versuch wurde mit einer Hg-Lampe durchgeführt und einem aus Quarzglas bestehendem Prisma durchgeführt.
\subsection{Brechender Winkel $\phi$}
Der Brechende Winkel wird mit Hilfe Gleichung 3.1 berechnet. Die Messwerte $\phi_\mathrm{r}$ sowie $\phi_\mathrm{l}$ sind in Tabelle 1 gelistet. Der Mittelwert des brechenden Winkels ergibt sich daraus zu:
\begin{align}
\phi=(59,99\pm0,03)\symbol{23}
\nonumber
\end{align}
\begin{table}[htbp]
\centering{\begin{tabular}{c|c|c}
$\phi_\mathrm{r} \,[\symbol{23}]$&$\phi_\mathrm{l}\,[\symbol{23}]$&$\phi\,[\symbol{23}]$\\
\hline
13,2&	133,3&	60,05\\
142,5&	262,5&	60,00\\
125,4&	245,5&	60,05\\
133,6&	253,4&	59,90\\
99,6&	219,5&	59,95\\
\end{tabular}}
\caption{Messwerte zur Bestimmung des brechenden Winkels $\phi$}
\end{table}
\subsection{Brechungsindex n}
Zur Bestimmung des Brechindexes werden diezu den jeweiligen Wellenlängen gehörenden, gemessenen Winkel $\Omega_\mathrm{l}$ und $\Omega_\mathrm{r}$ benötigt. Aus diesen lässt dich durch Gleichung 3.2 der ablenkende Winkel $\eta$ bestimmen und schließlich daraus mit Gleichung 3.3 der Brechungsindex berechnen.\\
Bei Anwendung von Gleichung 3.2 wurde der Betrag der Werte angegeben, da nur positive brechende Winkel sinnvoll sind. Alle gemessenen und berechneten Größen sind in Tabelle 2 aufgelistet. Der Fehler $\Delta n$ ergibt sich aus der Gauss'schen Fehlerfortpflanzung aus
\begin{align}
\Delta n =\sqrt{\left(\frac{\sin(\frac{\eta}{2})}{\cos(\phi)-1} \Delta\phi\right)^2}
\end{align}
\begin{table}
\centering{\begin{tabular}{l|c|c|c||c|c|c}
Farbe &$\lambda$ [nm]&	$\Omega_l[\symbol{23}]$&	$\Omega_r[\symbol{23}]$&	$\eta[\symbol{23}]$&	$n$	&$\Delta n$\\
\hline\hline
violett&404,7&	115,5&	328,2&	32,7&	1,447&	0,011\\
blau&435,8&	115,7&	327,3&	31,6&	1,434&	0,011\\
grün&491,6&	116,2&	327,0&	30,8&	1,424&	0,010\\
blaugrün&546,1&	116,1&	327,1&	31,0&	1,427&	0,010\\
gelb&579,1&	116,3&	326,9&	30,6&	1,422&	0,010\\
\end{tabular}
\caption{Bestimmung des Brechungsindexes $n$}
}\end{table}
\subsection{Bestimmung der Dispersionskurve}
Zunächst muss entschieden werden, ob die vorliegende Dispersion besser durch Gleichung 2.9 oder 2.10 beschrieben wird. Hierzu wurden die Quadrate die soeben berechneten Brechungsindizes einmal gegen die Wellenlängen zum Quadrat $\lambda^2$ (s. Abb.6) und einmal gegen deren Kerwerte $\lambda^{-2}$ (s. Abb. 7) aufgetragen.
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild11.png}}
\caption{$n^2$ gegen $\lambda^2$ aufgetragen}
\label{fig:Abbildung}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild10.png}}
\caption{$n^2$ gegen $\lambda^2$ aufgetragen}
\label{fig:Abbildung}
\end{figure}
\\Die Ausgleichsgerade wurden mit Gnuplot erstellt und aus diesen ergeben sich die Werte
\begin{align}
\notag
A_0&=1,96\pm0,02\\
\notag
A_2&=(2,09\pm0,50)\cdot10^4 \quad  \mathrm{nm^2}\\
\notag
A_0'&=2,13\pm0,0313\\
\notag
A_2'&=(3,52\pm1,24)\cdot10^{-7}\quad \mathrm{nm^{-2}}\\
\notag
\end{align}
Zum Abschätzen dieser beiden Geraden wurde jeweils die Summe der Abweichungsquadrate errechnet:
\begin{align}
s_n^2&=\frac{1}{z-2}\sum_{i=1}^z\left(n^2(\lambda_i) - A_0 - \frac{A_2}{\lambda_1^2}\right)^2\\
\notag
&=1,29\cdot10^{-4}
\end{align}
\begin{align}
s_{n'}^2&=\frac{1}{z-2}\sum_{i=1}^z\left( n^2(\lambda_i) - A_0 - A_2\lambda_1^2 \right)^2\\
\notag
&=1,99\cdot10^{-4}
\end{align}
Aus der Betrachtung der Werte folgt, dass die Kurve nach Gleichung 2.9 die gesuchte Dispersionskurve besser beschreibt.
Diese wurde rot bis zur zweiten Ordnung in Abb. 8 eingezeichnet, in dem außerdem die Werte $n^2$ gegen die Wellenlänge $\lambda$, sowie blau die Disperionskurve nach Gleichung 2.10 in zweiter Ordnung als Vergleich eingezeichnet.\\
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild12.png}}
\caption{Dispersionskurve des Prismas}
\label{fig:Abbildung}
\end{figure}
Im weiteren Verlauf der Auswertung wird ausschließlich die Dispersionsgleichung nach Gleichung 2.9 berücksichtigt, da diese die Dispersionskurve besser beschreibt.
\subsection{Bestimmung der Abbeschen Zahl}
Mit der soeben bestimmten Dispersionskurve lässt sich die Abbesche Zahl als ein Maß der Farbzerstreuung des Quarzglases berechnen. Diese berechnet sich durch
\begin{align}
\nu=\frac{n_\mathrm{D}-1}{n_\mathrm{F}-n_\mathrm{C}}
\end{align}
wobei $n_\mathrm{C}$, $n_\mathrm{D}$ und $n_\mathrm{F}$ die Brechungsindizes zu den Wellenlängen der Frauenhoferschen Linien sind.
\begin{align}
\notag
\lambda_\mathrm{C}&=656\,nm\\
\notag
\lambda_\mathrm{D}&=589\,nm\\
\notag
\lambda_\mathrm{F}&=486\,nm\\
\notag
\end{align}
Durch die Dispersionkurve $n=\sqrt{A_0+A_2\lambda^{-2}}$ lassen sich die Brechungsinzes
\begin{align}
\notag
n_\mathrm{C}&=1,416\pm0,019\\
\notag
n_\mathrm{D}&=1,420\pm0,019\\
\notag
n_\mathrm{F}&=1,431\pm0,022\\
\notag
\end{align}
berechnen und die dazugehörigen Fehler durch
\begin{align}
\Delta n =\frac{1}{2n}\sqrt{(\Delta A_0)^2+\left(\frac{\Delta A_2}{\lambda^2}\right)^2}.
\notag
\end{align}
Anschließend lässt sich die Abbesche Zahl unter Benutzung der Fehlerformel
\begin{align}
\Delta \nu =\frac{1}{n_F-n_C}\sqrt{(\nu\Delta n_C)^2+(\Delta n_D)^2+(\nu\Delta n_F)^2}
\notag
\end{align}
bestimmen:
\begin{align}
\nu=29,91\pm29,98
\notag
\end{align}
\subsection{Auflösungsvermögen}
Durch das Auflösungsvermögen $A$ ist es möglich, die minimale wahrnehmbare Wellenlängendifferenz $\Delta \lambda=\lambda/A$ zu berechnen. Mit der Basislänge $b$=3\,cm wird das Auflösungsvermögen für $\lambda_\mathrm{C}$ und $\lambda_\mathrm{F}$ bestimmt. 
\begin{align}
\notag
A=\frac{\lambda}{\Delta\lambda}=b\frac{dn}{d\lambda}=-b\cdot\frac{A_2}{\lambda^3 n}
\notag
\end{align}
Die Ergebnisse sind in Tabelle 3 dargestellt.
\begin{table}[htbp]
\centering{\begin{tabular}{c|c|c|c|c}
$\lambda$& $A(\lambda_x)$&$\Delta\lambda(\lambda_x)\,\mathrm{[nm]}$\\
\hline
$\lambda_\mathrm{C}$& $1568\pm375$&$0,42\pm0,10$\\
$\lambda_\mathrm{F}$& $3822\pm914$&$0,13\pm0,03$
\end{tabular}}
\caption{Auflösungsvermögen}
\end{table}
\subsection{Bestimmung der nächstgelegenen Absorptionsstelle $\lambda_1$}
Aus Gleichung 2.9 folgt, dass für die Koeffizienten $A_0$ und $A_2$ gilt:
\begin{align}
\notag
A_0&=1+\frac{N_1q_1}{4\pi^2c^2\epsilon_0m_1}\\
\notag
A_2&=\frac{N_1q_1}{4\pi^2c^2\epsilon_0m_1}\lambda_1^2
\notag
\end{align}
daraus folgt für $\lambda_1$ der Zusammenhang
\begin{align}
\notag
\lambda_1&=\sqrt{\frac{A_2}{A_0-1}}\\
\notag
&=(147\pm17)\,\mathrm{nm}
\notag
\end{align}
Allerdings sollte dieser Wert nicht als allzu realistisch angesehen werden, da dieser durch einige vorheriger Näherungen sehr ungenau ist. Trotzdem lässt sich anhand des Wertes erkennen, dass die dem sichtbaren Bereich nächste Absorptionsstelle des Prismas im UV-Bereich liegt.


\section{Diskussion}
Weitestgehend sind die Werte wie es aufgrund der beschränkten Messgenauigkeit zu erwarten war. Die hohe Abweichung bei Abbeschen Zahl erklärt sich damit, dass bereits sehr kleine Änderungen der Brechungsindizes große Veränderungen in deren Wert haben. Somit ist auch die Abweichung von 44,1$\%$ zum Literaturwert 67,8 [2] im Rahmen des Versuches akzeptabel.\\Die Ungenauigkeit des Versuches erklärt sich durch die zum Teil recht breit ausgefallenen Spektrallinien sowie die geringe Sichtbarkeit einiger Linien. Deshalb ist nicht von einer optimalen Winkelmessung auszugehen. Darüber hinaus war die weiße Linie während der Bestimmung der $\Omega$'s schräg zum Spektrum was die Messung erschwerte.\\
Die Literaturwerte der Brechungsindizes liegen alle im Bereich von ca. 1,55 [3], also höher als die bestimmten Werte. Somit liegt die Vermutung nahe, dass ein systematischer Fehler während des Versuches vorliegt, der vermutlich auch im Zusammenhang mit der schrägen Linie steht.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 402, Dispersionsmessungen am Glasprisma (8.10.2013)
\item http://www.dorotek.de/cms/upload/pdf/optik/deutsch/1-GLAS.pdf (20.5.2014)
\item http://www.filmetrics.de/refractive-index-database/SiO2/Fused-Silica-Silica-Silicon-\\Dioxide-Thermal-Oxide-ThermalOxide (20.5.2014)
\end{enumerate}

\end{document}


