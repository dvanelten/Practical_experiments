\select@language {ngerman}
\contentsline {section}{\numberline {1}Zielsetzung}{1}{section.1}
\contentsline {section}{\numberline {2}Theoretische Grundlagen}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Ultraschall}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Erzeugung von Ultraschall}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Schallfeld}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Computertomographie}{3}{subsection.2.4}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Schallfeld-Messung}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}CT-Messung}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}Auswertung}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Axiale Feldmessung}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Laterale Feldmessung}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}CT-Scans einer zylinderf\IeC {\"o}rmigen Probe}{8}{subsection.4.3}
\contentsline {section}{\numberline {5}Diskussion}{10}{section.5}
\contentsline {section}{\numberline {6}Literaturverzeichnis}{11}{section.6}
