\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[textwidth=16.5cm]{geometry}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch TU}  \\ Protokoll vom \today}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Fortgeschrittenen-Praktikum SS15}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch TU\\ Tomografie mit Ultraschall}
\author{Konstantin Pfrang\\ \small{\textit{konstantin.pfrang@tu-dortmund.de}} \and Dennis van Elten\\ \small{\textit{dennis.vanelten@tu-dortmund.de}}}
\date{\today}
\setcounter{page}{-0} 
\maketitle
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Zielsetzung}
Ziel des Experiments Tomographie mit Ultraschall ist zum einen das Untersuchen des Schallfeldes einer Ultraschallsonde mithilfe eines Hydrophons und zum anderen die Abhängigkeit der Bildqualität einer Computertomographie von der Geschwindigkeit und der Winkelauflösung zu prüfen.
\section{Theoretische Grundlagen}
\subsection{Ultraschall}
Das menschliche Gehör ist in der Lage, Frequenzen zwischen 16\,Hz und 20\,kHz wahrzunehmen. Oberhalb dieses Bereichs liegt mit einer Frequenz bis zu 1\,GHz der Ultraschall, bei noch höheren Frequenzen wird von Hyperschall geredet. Unterhalb der Hörschwelle liegt der Infraschall.\\
Schall breitet sich durch eine Welle aus, in der sich der Druck periodisch ändert. Sie kann beschrieben werden durch
\begin{align}
p(x,t)=p_0+v_0 Z \cos(\omega t-kx).
\end{align}
Hierbei ist $Z$ die akustische Impedanz. Diese setzt sich aus der Schallgeschwindigkeit $c$ und der Dichte $\rho$ wie folgt zusammen:
\begin{align}
Z=c\cdot \rho
\end{align}
Die Schallgeschwindigkeit ist eine materialspezifische Größe. In Gasen und Flüssigkeiten ist die Ausbreitung der Welle rein longitudinal. Deren Geschwindigkeit ist abhängig von der Kompressibilität $\kappa$ und der Dichte:
\begin{align}
c_{\text{Flüssig/Gas}}=\sqrt{\frac{1}{\kappa \rho}}.
\end{align}
In Festkörpern ist die Geschwindigkeit hingegen neben dem Druck auch vom Elastizitätsmodul $E$ abhängig. Hier breitet sich der Schall sowohl longitudinal, als auch transversal aus. Außerdem ist im allgemeinen die Geschwindigkeit abhängig von der Richtung.
\begin{align}
c_{\text{fest}}=\sqrt{\frac{E}{\rho}}
\end{align}
Der Schall verliert durch Absorption in einer Laufstrecke exponentiell an Intensität, so wie es in Formel \ref{int} gegeben ist.
\begin{align}\label{int}
I(x)=I_0\cdot e^{\alpha x}
\end{align}
Hier bezeichnet $\alpha$ den Absorptionskoeffizienten und $x$ die Strecke. Damit die Verluste in Luft verhindert werden können, wird zwischen einem Material und einem Schallsender oder Schallempfänger ein Kontaktmittel, in diesem Versuch Ultraschallgel, verwendet.\\
Trifft eine Schallwelle auf eine Grenzfläche, so wird diese teilweise reflektiert und teilweise transmittiert. Der reflektierte Anteil $R$ berechnet sich aus den akustischen Impedanzen:
\begin{align}
R=\left(\frac{Z_1-Z_2}{Z_1+Z_2}\right)^2
\end{align}
Hieraus lässt sich über die Beziehung $T=1-R$ der transmittierte Anteil bestimmen.

\subsection{Erzeugung von Ultraschall}
Mithilfe eines Ultraschallwandlers lassen sich sowohl Ultraschallwellen erzeugen als auch detektieren. Dies geschieht durch den piezo-elektrischen Effekt. Hierbei bilden sich mikroskopische Dipole in den Elementarzellen, wenn diese gerichtet verformt werden. So entsteht eine messbare elektrische Spannung. Hierbei versteht man unter der gerichteten Verformung, dass der Druck nicht gleichmäßig von allen Seiten erfolgt. Dieser Effekt funktioniert auch umgekehrt, so dass zur Erzeugung des Ultraschalls der Kristall in ein elektrisches Wechselfeld gebracht und somit zur Schwingung angeregt wird. Durch diese Schwingungen im Kristall werden schließlich Ultraschallwellen ausgesandt. Hierbei spielen auch Resonanzen innerhalb der Kristalls eine Rolle.\\
Werden die piezo-ellektrischen Kristalle als Empfänger genutzt, so macht man sich diesen Effekt umgekehrt zu nutzen: der Kristall wird durch den Schall zu Schwingungen angeregt und anschließend kann die Intensität durch die dabei entstehende Spannung bestimmen.\\
\\
Bei einer Laufzeitmessung wird ein kurzer Schallimpuls ausgesandt und dessen Laufstrecke bestimmt. Hierbei sind zwei Vorgehensweisen zu unterscheiden: Zum einen das Durchschallungs-Verfahren, zum anderen das Impuls-Echo-Verfahren.\\
Für das Durchschallungs-Verfahren wird dem Sender gegenüber ein Empfänger aufgestellt, mit dem der am Ende der Probe ankommende Schall registriert werden kann.\\
Beim Impuls-Echo-Verfahren ist ein Kristall Sender und Empfänger zugleich. Der gesendete Impuls wird an einer Grenzfläche reflektiert und anschließend wieder am Kristall aufgenommen. Hierdurch lassen sich Fehlstellen $s$ in der Probe bei bekannter Schallgeschwindigkeit $c$ und Laufzeit $t$ bestimmen durch $s=\frac{1}{2}ct$\\
Zum Vergleich der Methoden sind diese beiden schematisch in Abbildung \ref{lauf} dargestellt\\
\\
Es existieren drei Darstellungsarten für die Laufzeitdiagramme:
\begin{enumerate}
\item A-Scan: Der Amplituden-Scan ist ein eindimensionales Verfahren und wird zur Abtastung von Strukturen verwendet. Hierbei werden die Echoamplituden als Funktion der Laufzeit abgebildet.
\item B-Scan: Beim Brightness-Scan werden mehrere eindimensionale A-Scans zu einem mehrdimensionalen Bild zusammengesetzt. Die Echoamplituden sind durch die Helligkeitsabstufungen dargestellt.
\item TM-Scan: Der Time-Motion-Scan ermöglicht es durch schnelle Abtastfolge eine zeitliche Bildfolge aufzunehmen.
\end{enumerate}
\begin{figure}[h!]
\centering{\includegraphics[height=5.5cm,width=11cm]{Bilder/Bild1.png}}
\caption{Schematische Abbildung der Verfahren zur Laufzeitmessung [1]}
\label{lauf}
\end{figure}
\subsection{Schallfeld}
Unter Schallfeld wird der Bereich des Mediums bezeichnet, in dem sich die Schallwellen ausbreiten. Das axiale Schallfeld wird unterteilt durch ein Maximum der Feldintensität. Vor dem Maximum spricht man von dem Nahfeld, dahinter entsprechend vom Fernfeld. Da die Ultraschallsonde keine punktförmige Quelle ist, entstehen viele nichtparallele Wellen gleichzeitig. Daraus resultieren im Nahfeld Interferenzerscheinungen, wodurch es zu dem bereits erwähnten Maximum in einer bestimmten Entfernung kommt. Die Nahfeldlänge ergibt sich durch
\begin{align}
N=\frac{D^2-\lambda^2}{4\lambda}
\end{align}  
wobei D den Durchmesser der Kontaktfläche und $\lambda$ die Wellenlänge beschreibt.
\



\subsection{Computertomographie}
Die Computertomographie wird zur Untersuchung der räumlichen Struktur eines Körpers durch Querschnittsbilder verwendet. Im Gewebe können die Wellen teilweise absorbiert und gestreut werden. Dies kann als Schwingung des Mediums aufgefasst werden, das schließlich selbst eine Welle abstrahlt. In homogenen Medien folgt hieraus eine exponentielle Intensitätsabnahme in der Form
\begin{align}
I(x)=I_0e^{-\mu x}.
\end{align}
In einem inhomogenen Medium folgt aus der Überlagerung von Elementarwellen eine Schallabstrahlung in alle Richtungen (Streuung). Wenn die Schwingung des Mediums hierbei nicht in Phase mit der Welle ist, so wird durch Überlagerung der Elementarwellen und der gestreuten Wellen der Ultraschall abgeschwächt. Dadurch wird der Schall bei einer Phasenverschiebung um $\frac{\pi}{2}$ maximal absorbiert.\\
Durch die gemessenen Intensitäten kann auf das Material zurückgeschossen werden. Indem dieses Verfahren aus verschiedenen Winkeln durchführt wird, können die aufgenommenen Daten durch den Computer zusammengefügt und somit die räumliche Struktur untersucht werden.


\newpage
\section{Durchführung}
\subsection{Schallfeld-Messung}
\begin{figure}[h!]
\centering
\includegraphics{Bilder/Bild2.png}
\caption{Aufbau der Schallfeld-Messung [2]}
\label{auf1}
\end{figure}
Der Versuchsaufbau zur Schallfeldmessung ist in Abbildung \ref{auf1} dargestellt. Sie besteht aus einer mit Wasser gefüllten Messwanne, über der sich eine Rahmenkonstruktion mit einer Führungsschiene mit Motor befindet. An diesem Motor ist eine Halterung mit einem Hydrophon befestigt. Dieses ermöglicht es, den Wasserschall in elektrische Ladung umzuwandeln. Es wird auch als Unterwassermikrophon bezeichnet. Es lässt sich durch den Motor entlang der Schiene bewegen und wird vor dem Sender, der an der Außenwand der Wanne angebracht ist, positioniert. Zur Vermeidung von Intensitätsverlusten in der Luft wird auf die Oberfläche der Ultraschallsonde Ultraschallgel aufgetragen, bevor diese an die Wanne angeschlossen wird. Der Motor, der Empfänger sowie der Sender sind mit der Steuerungseinrichtung verbunden, mit der sich die Verstärkung, die Geschwindigkeit etc. steuern lassen.\\
Bei der Messung des Schallfeldes wird eine axiale und eine laterale Messung durchgeführt. Bei der axialen Messung wird die Sonde an der kurzen Seite angebracht und das Unterwassermikrofon für eine Strecke von 20\,cm hin und wieder zurück bewegt. Bei der lateralen Messung wird die Sonde an der breiten Seite montiert und für fünf verschiedene Abstände zwischen Sonde und Hydrophon werden Messwerte senkrecht zur Seitenrichtung aufgenommen.



\subsection{CT-Messung}
\begin{figure}[h!]
\centering{\includegraphics{Bilder/Bild3.png}}
\caption{Aufbau der CT-Messung [2]}
\label{auf2}
\end{figure}
Der Aufbau der CT-Messung ist der Schallfeld-Messung sehr ähnlich. Anstelle des Hydrophons befindet sich nun der Probekörper, und es sind zwei Ultraschallsonden an die breiten Seiten gegenüber angeschlossen. Eine davon dient als Sender, eine als Empfänger. Es werden insgesamt vier Schnittbilder des Körpers für verschiedene Winkel von 0 bis $2\pi$ und für verschiedene Geschwindigkeiten durchgeführt. Anschließend werden die aufgenommenen Bilder vom Computer zu einem Gesamtbild zusammengefügt. Dabei werden alle Kombinationen aus geringer / hoher Winkelauflösung und langsamer / schneller Geschwindigkeit untersucht.

\section{Auswertung}
\subsection{Axiale Feldmessung}
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild4.png}
\end{minipage}
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild5.png}
\end{minipage}
\caption{Messwerte der axialen Feldmessung für Hin- und Rückweg}
\label{auf3}
\end{figure}
In der vorangegangenen Abbildung 4 sind die Messwerte für die axiale Feldmessung aufgetragen. Dabei wurde der Hin- sowie der Rückweg des Hydrophons vermessen. Erkennbar ist das Nahfeld, das den Bereich bis zum Maxima bezeichnet, und das anschließende Fernfeld. Die jeweiligen Maxima der einzelnen Wege wurden aus den, von dem Computer erstellten, Datenlisten entnommen. Bei der Bestimmung des Maximums des Hydrophons auf dem Hinweg wurde der ausreißende Datenpunkt bei einer Entfernung von ungefähr 40\,mm außer Acht gelassen und stattdessen der nächsthöhere Punkt verwendet. Somit ergeben sich für die experimentellen Nahfeldlängen:
\begin{align*}
l_{\text{hin}} &= \text{24,9\,mm}  \quad \text{(bei einer Amplitude A von 0,846 V)}\\
l_{\text{rueck}} &= \text{24,0\,mm} \quad \text{(bei einer Amplitude A von 0,815 V)}
\end{align*}

\noindent Die theoretische Nahfeldlänge lässt sich mittels der Gleichung 2.7 ermitteln. Zuvor muss jedoch die Wellenlänge bestimmt werden. Dazu wird für die Schallgeschwindigkeit in Wasser $c$ =\,1497 $\frac{\text{m}}{\text{s}}$ [2] (für eine Temperatur von 25 $^\circ$C) angenommen. Die Frequenz der Sonde beträgt $f=2\,\text{MHz}$. Daraus folgt für die Wellenlänge:
\begin{align}
\lambda = \frac{c}{f} = 7,485 \cdot 10^{-4} \,\text{m}.
\end{align}
Mithilfe der Wellenlänge und einem Sondendurchmesser von 16\,mm lässt sich nun die theoretische Nahfeldlänge bestimmen:
\begin{align}
l_{\text{theo}} = 85,32 \,\text{mm}.
\end{align}

\subsection{Laterale Feldmessung}
Bei der lateralen Feldmessung werden zu fünf verschiedene Abstände jeweils der Hin- und Rückweg gemessen. Bei allen Abtänden in den nachfolgenden Abbildungen ist zu erkennen, dass direkt vor der Sonde ein Peak zu beobachten ist. Die Abstände sind dabei manuell abgelesen worden und besitzen somit einen Ablesefehler.
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild6.png}
\end{minipage}
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild7.png}
\end{minipage}
\caption{Messwerte der lateralen Feldmessung für Hin- und Rückweg bei einem Abstand von 2 cm}
\label{auf4}
\end{figure}

\newpage
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild8.png}
\end{minipage}
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild9.png}
\end{minipage}
\caption{Messwerte der lateralen Feldmessung für Hin- und Rückweg bei einem Abstand von 4 cm}
\label{auf5}
\end{figure}

\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild10.png}
\end{minipage}
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild11.png}
\end{minipage}
\caption{Messwerte der lateralen Feldmessung für Hin- und Rückweg bei einem Abstand von 5 cm}
\label{auf6}
\end{figure}

\newpage
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild12.png}
\end{minipage}
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild13.png}
\end{minipage}
\caption{Messwerte der lateralen Feldmessung für Hin- und Rückweg bei einem Abstand von 6 cm}
\label{auf7}
\end{figure}

\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild14.png}
\end{minipage}
\begin{minipage}[hbt]{0.49\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild15.png}
\end{minipage}
\caption{Messwerte der lateralen Feldmessung für Hin- und Rückweg bei einem Abstand von 8 cm}
\label{auf8}
\end{figure}

\subsection{CT-Scans einer zylinderförmigen Probe}
Im Folgenden wurden verschiedene CT-Scans von der Probe angefertigt, wobei die Einstellungen variiert wurden. Für eine niedrige Winkelauflösung wurden 40$^\circ$ angenommen, während für eine hohe Winkelauflösung 10$^\circ$ verwendet wurden. Eine Geschwindigkeit von 10\,\% wurde als langsam interpretiert, wohingegen 60\,\% als Einstellung für eine hohe Geschwinigkeit verwendet wurden. In den Abbildungen 10-13 zeigt das erste Bild  (links) die Aufnahme mit einer niedrigen Winkelauflösung und einer niedrigen Geschwindigkeit. Im darauffolgenden Bild wurden sowohl die Winkelauflösung als auch die Geschwindigkeit erhöht. Die dritte Aufnahme zeigt eine niedrige Winkelauflösung und eine hohe Aufnahmegeschwindigkeit während beim letzten Bild die bestmöglichen Einstellungen angewandt wurden: hohe Winkelauflösung und niedrige Geschwindigkeit.
Die ganzen Aufnahmen unterscheiden sich in ihrer jeweiligen Funktion und durch verschiedene Filter, die die Helligkeits- und Kontrasteinstellungen zur Bildbearbeitung steuern. 
\newline
\newline
Die nachstehenden Aufnahmen dienen der Darstellung der Schallabschwächung und sind ohne einen Filter versehen. Je intensiver eine Stelle ist (Intensität schwach: blau; Intensität hoch: rot), desto höher ist die Dämpfung und damit verbunden die Absorption. In der Medizinphysik/-technik spricht man hierbei von Attenuation-Aufnahmen. Diese Aufnahmen besitzen den höchsten Informationsgehalt und ermöglichen Aussagen über die Struktur der untersuchten Probe / des untersuchten Körpers zu tätigen. Zwischen den verschiedenen Einstellungen der Winkelauflösung und Aufnahmegeschwindigkeit besteht nur ein geringer Unterschied.
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild16.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild17.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild18.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild19.png}
\end{minipage}
\caption{CT-Dämpfungsaufnahmen für verschiedene Einstellungen}
\label{auf9}
\end{figure}

In Abbildung 11 sind die CT-Dämpfungsaufnahmen mit einem Filter bearbeitet worden. Aufgrund der Kontrastveränderung werden dadurch Unterschiede zwischen den verschiedenen Einstellungen sichtbar. Hierbei ähneln sich die Scans mit derselben Winkelauflösung. Bei niedriger Winkelauflösung lässt sich ein Streifenmuster erkennen, welches auch in einigen nachfolgenden Aufnahmen auftritt. Dieses lässt sich durch Aufnahmefehler erklären.
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild20.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild21.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild22.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild23.png}
\end{minipage}
\caption{CT-Dämpfungsaufnahmen mit Filter für verschiedene Einstellungen}
\label{auf10}
\end{figure}

Die nächsten beiden Abbildungen beschäftigen sich mit Schallgeschwindigkeitsaufnahmen (time-of-flight-Aufnahmen) und sind wiederum ohne eingeschalteten Filter. Bei dieser Methode wird die Zeit gemessen, die ein Signal bis zum Empfang benötigt. Wie bei den CT-Dämpfungsaufnahmen mit Filter lässt sich auch hier ein deutlicher Unterschied zwischen allen Scans feststellen. Im Gegensatz zu diesen ist der Informationsgehalt bei den CT-Schallgeschwindigkeitsaufnahmen deutlich geringer.
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild24.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild25.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild26.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild27.png}
\end{minipage}
\caption{CT-Schallgeschwindigkeitsaufnahmen für verschiedene Einstellungen}
\label{auf11}
\end{figure}

Anschließend wird ein Shepp-and-Logan-Filter dazugeschaltet. Dieser besitzt einen Tiefpasscharakter. Das bedeutet, dass hohe Raumfrequenzen, die in derselben Größenordnung wie das Rauschen liegen, abgeschwächt werden. Dabei bezeichnet das Rauschen allgemein eine Störstelle mit unbekannten Frequenzspektrum. Gleichzeitig werden große Abmessungen stärker betont. Insgesamt bewirkt das Verfahren also eine schlechtere räumliche Auflösung, dafür wird jedoch das Rauschen stärker gedämpft. Es lässt sich jedoch kein allzu großer Informationsgehalt aus den Scans rausfiltern.
\begin{figure}[h!]
\centering
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild28.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild29.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild30.png}
\end{minipage}
\begin{minipage}[hbt]{0.24\textwidth}
\includegraphics[width=\textwidth]{Bilder/Bild31.png}
\end{minipage}
\caption{CT-Schallgeschwindigkeitsaufnahmen mit Filter für verschiedene Einstellungen}
\label{auf12}
\end{figure}

\section{Diskussion}
Zur axialen Schallfeldmessung lässt sich sagen, dass die experimentell ermittelten Werte sehr stark von dem theoretisch bestimmten Wert abweichen, so dass von Ungenauigkeiten in der Durchführung ausgegangen werden müssen. Die Hauptursache kann dafür sein, dass der piezo-elektrische Kristall um einige Winkelgrade verschoben war und somit nicht die Hauptmaxima gemessen wurden. Diese Verschiebung kann dadurch zustande gekommen sein, dass das Kabel des Hydrophons angeklebt war und so Auswirkungen auf die Bewegung der Führungsschiene bzw. dessen Ausrichtung gehabt haben könnte. Des Weiteren folgt auf das Maximum kein reiner expoentieller Abfall, sondern eine Dämpfung der Amplitude mit einem Wendepunkt. Dies deutet darauf hin, dass einige Verstärkungsregler angeschaltet waren, die den Verlauf zusätzlich beeinflussten. Bis auf das verschobene Maximum stimmen die Kurven jedoch mit dem zu erwarteten Verlauf überein und es lässt sich eine Aufteilung in Nah- und Fernfeld erkennen.
\newline
Bei der lateralen Schallfeldmessung ist bei allen Abständen bei den jeweiligen Hin- und Rückwegen ein Peak nahe der Mittelposition bezüglich  der Sonde zu beobachten. Jedoch stimmen Hin- und Rückweg nicht genau überein, so dass eine Asymmetrie erkennbar ist. Die Asymmetrie kann dadurch verursacht worden sein, dass das Hydrophon das Wasser in Bewegung versetzt und nicht gewartet wurde, bis das Wasser sich wieder in Ruhe befand. Insgesamt lässt sich jedoch sagen, dass die zu erwarteten Verläufe aufgezeichnet wurden.
\newline
Bei Betrachtung der CT-Scans lässt sich sagen, dass Unterschiede zwischen den verschiedenen Einstellungen existieren. Diese sind allerdings nicht so groß, dass eine bestimmte Einstellung einen deutlichen Vorteil gegenüber den anderen bewirkt. Dies hat für die praktischen Anwendungen zur Folge, dass schnelle und kostengünstige Aufnahmen gemacht werden können. Bei Betrachtung der Scans wirkt es so, als seien in dem zylinderförmigen Probekörper drei Freiräume, die aus verschiedenen Materialien bestehen. Des Weiteren lässt sich die Aussage treffen, dass der verwendete Shepp-Logan-Filter keinen Vorteil gegenüber der filterfreien Messung beinhaltet.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund Physik Anfängerpraktikum, Versuchsanleitung zu US1 Grundlagen der Ultraschalltechnik
\item Ultraschall in Physik, Medizin und Technik, http://www.gampt.de/content/cms/downloads/pdf/GAMPT-Katalog.pdf
\item Grundlagen der Computer-Tomographie, http://www.physik.uni-kl.de/uploads/media/Grundla-
genCT.pdf
\item http://www.gampt.de/content/cms/downloads/pdf/GAMPT-Katalog.pdf
\end{enumerate}
\end{document}

