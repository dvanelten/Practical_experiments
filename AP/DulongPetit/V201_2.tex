\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}

\begin{document}

\title{Versuch 201	-	Das Dulong-Petitsche Gesetz}
\author{Konstantin Pfrang \and Dennis van Elten}
\maketitle
\newpage

\tableofcontents

\newpage


\section{Ziel}
Nach dem aus der klassischen Mechanik hergeleiteten Dulon-Petitschen Gesetz besitzt die Molwärme eines Festkörpers universell den Wert $3R = 25 \mathrm{\frac{J}{mol\cdot K}}$, wobei $R$  die allgemeine Gaskonstante beschreibt. Dieses Gesetz erfolgt unter der Annahme, dass jedes Atom als ein drei-dimensionaler Oszillator agiert.
Im folgenden wird das Gesetz auf seine Allgemeingültigkeit sowie seinen Geltungsbereich geprüft.

\section{Theoretische Grundlagen}

\subsection{Spezifische Wärmekapazität und Molwärme}
Von einer Wärmemenge $\Delta Q$ wird gesprochen, wenn sich die Temperatur eines Körpers ohne äußere Arbeit um einen Temperaturbeitrag erhöht. Sie ist definiert als 
\begin{align}
\Delta Q = mc \Delta T
\end{align}
Dabei ist $c$ die Energiemenge, die benötigt wird um $1$ kg eines Stoffes um eine Temperatur von $1$ K zu erhöhen ([c] = J/ kg K) und wird im Bezug auf eine Masseneinheit als spezifische Wärmekapazität definiert.
\newline
Diejenige Wärmemenge $\Delta Q$, die erforderlich ist $1$ Mol eines Stoffes zu erwärmen, wird als Molwärme $C$ bezeichnet. Dabei differenziert man zwischen der Molwärme bei konstantem Druck $C_p$ und konstantem Volumen $C_v$.
Aus dem ersten Hauptsatz der Thermodyamik ergibt sich ein Zusammenhang zwischen der inneren Energie eines Mols $U$, der Wärmemenge $Q$ und der mechanischen Arbeit $A$:
\begin{align}
dU = dQ + dA.
\end{align}
Es lässt sich folgern, dass die Änderung der Wärmemenge äquivalent zu der inneren Energie ist. Somit gilt	
\begin{align}
C_v = \frac{dQ}{dT} = \frac{dU}{dT}.
\end{align}


\subsection{Aussage von Dulong-Petite und klassische Herleitung}

Eine zentrale Größe zur Bestimmung der Molwärme $C_v$ ist die gemittelte Energie eines Atoms im Festkörper über einen größeren Zeitraum \tau

\begin{align}
\langle u \rangle = \frac{1}{\tau} \int_0^\tau u(t) dt.
\end{align}
Es handelt sich dabei um einen allgemein gültigen Wert, bestehend aus der gemittelten potentiellen und kinetischen Energie
\begin{align}
\langle u \rangle = \langle E_{kin} \rangle + \langle E_{pot} \rangle.
\end{align}
Im Folgenden wird ein Atom eines Feststoffes betrachtet. Diesem ist es aufgrund der Kristallstruktur nicht möglich Translationen oder Rotationen durchzuführen, was seine Bewegungsfreiheitsgrade einschränkt.
Dessen Verhalten kann durch ein harmonisch schwingfähiges System, einem Oszillator, angenähert werden. Mithilfe der rücktreibenden Kraft $F_R$ (Hooke'sches Gesetz) und der Trägheitskraft $F_T$ ergibt sich die Bewegungsgleichung dieses Systems mit der Lösung 
\begin{align}
x(t) = A cos (\frac{2 \pi t}{\tau})
\end{align}
Dabei bezeichnet A die Amplitude.
Es lässt sich weiter ermitteln, dass die potentielle Energie mit $E_{pot}$ = $\frac{1}{2} D x^2$ und die kinetische Energie mit ${$E_{kin}$ = $\frac{m}{2}$ \.{x}^2}$. für ein schwingendes Atom denselben Wert 
\begin{align}
E_{pot} = E_{kin} = \frac{1}{4} DA^2
\end{align}
besitzen. D bezeichnet hierbei die Federhärte. Somit ergibt sich für die gemittelte innere Energie $\langle u \rangle = 2E_{kin}$.
Desweiteren besagt das Äquipartitionstheorem, dass ein Atom im thermischen Gleichgewicht pro Freiheitsgrad eine mittlere Energie
\begin{align}
E_{kin} = \frac{1}{2} kT
\end{align}
vorzuweisen hat. k ist in der Gleichung die Boltzmann-Konstante.
\newline
Insgesamt ergibt sich nun für die mittlere Energie pro Mol in einem Festkörper mit 3 Freiheitsgraden
\begin{align}
\langle U \rangle = 3 N_A kT = 3RT,
\end{align}
mit der Avogadro-Konstante $N_A$.
$C_v$ beträgt nun
\begin{align}
C_v = \frac{dU}{dT} = 3R.
\end{align}

\subsection{Abweichung vom Dulong-Petite Gesetz}

Für hohe Temperaturen besitzt die Atomwärme $C_v$ für nahezu alle Elemente den Wert $3R$. Bei tiefen Temperaturen hingegen werden diese jedoch beliebig klein und nähern sich für $0$ K dem Nullwert $C_v = 0$ $\frac{J}{mol \cdot K}$. Die Annahme, dass die Atomwärme $C_v$ einen universellen Wert besitzt, steht somit im Widerspruch zur Quantentheorie. Diese sagt aus, dass die Atome im Festkörper ihre Gesamtenergien nur in bestimmen Beträgen
\begin{align}
\Delta u = n \hbar w, n=(1,2,...)
\end{align}
ändern können. Dabei bezeichnet $\hbar$ das Planck'sche Wirkungsquantum und $w$ die Frequenz.
Zusätzlich hängt die Gesamtenergie nicht mehr proportional von der Temperatur $T$ ab. Die Zahl der Moleküle/Atome, die eine Energie von $E$ bis E+$\Delta E$ besitzen, kann mit der Gleichung
\begin{align}
W(E)dE = \frac{e{^\frac{-E}{kT}}}{kT}dE
\end{align}
ermittelt werden. Dabei ist W durch die Boltzmann-Verteilung festgelegt.
Für die mittlere Innere Energie eines Mols ergibt sich nun
\begin{align}
U_{qu} = \frac{3 N_A \hbar w }{e{\frac{\hbar w}{kT}}-1}.
\end{align}
Für große Temperaturen lässt sich nach der Taylor-Entwicklung der Exponentialfunktion erkennen, dass der Wert für $U_{qu}$ sich dem klassischen Wert $3RT$ annähert. Für kleinere Temperaturen hingegen liegt dieses unter dem aus der klassischen Mechanik ermittelten Wert.

\section{Durchführung}

Da es schwierig ist die Messungen bei konstantem Volumen durchzuführen, wird stattdessen mit konstantem Druck gearbeitet. Dabei besteht zwischen den beiden Größen der Zusammenhang 
\begin{align}
C_p - C_v = 9 \alpha^2 \kappa V_0 T
\end{align}
mit dem linearen Ausdehnungskoeffizienten $\alpha$, dem Kompressionsmodul $\kappa$ = $V \frac{\delta p}{\delta V}$ und dem Molvolumen $V_0$.
\newline
Um die spezifische Wärmekapazität $C_K$ eines Festkörpers bei konstantem Druck zu ermitteln wird die in Abbildung 1 dargestellte Apparatur, ein Mischungskalorimeter, verwendet.

\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Schematische Darstellung der Messapparatur}
\label{fig:Abbildung}
\end{figure}

Ein rohrenförmiger Körper wird in Wasser auf eine Temperatur $T_K$ erhitzt und anschließend in ein Dewar-Gefäß mit geringerer Wassertemperatur $T_W$ eingetaucht. Aufgrund der unterschiedlichen Temperaturen stellt sich nach einiger Zeit ein thermisches Gleichgewicht mit der Mischtemperatur $T_M$ ein, bei der die abgegebene Wärmemenge des Körpers
\begin{align}
Q_1 = c_k m_k (T_k - T_m)
\end{align}
und die von der Wand des Kalorimeters und Wasser aufgenommene Wärmemenge
\begin{align}
Q_2 = (c_w m_w + c_g m_g) (T_m - T_w)
\end{align}
denselben Wert besitzen. $c_w$ ist hierbei die spezifische Wärmekapazität des Wassers, die bei etwa 40 Grad den Wert $4,18 \frac{J}{gK}$ besitzt, und $c_g$$m_g$ die Wärmekapazität des Kalorimeters. Die Größe der spezifischen Wärmekapazität des Körpers $c_k$ kann durch die Gleichsetzung der Wärmemengen $Q_1 = Q_2$ ermittelt werden:
\begin{align}
c_k = \frac{(c_w m_w + c_g m_g) (T_m - T_w)}{m_k (T_k - T_m)}.
\end{align}
$c_g$$m_g$ berechnet sich durch die Vermischung zweier Wassermengen $m_x$ und $m_y$ mit den beiden Temperaturen $T_x$ und $T_y$, bei der sich eine Mischungstemperatur $T_m$' einstellt. Es ergibt sich die Gleichung
\begin{align}
c_g m_g = \frac{c_w m_y (T_y - T_m') - c_w m_x (T_m' - T_x)}{(T_m' - T_x)}.
\end{align}
\newline
Um die Temperaturen zu messen wird ein Thermoelement benutzt (Abbildung 2). Dieses besteht aus zwei Metallen mit unterschiedlichen Austrittsarbeiten der Elektronen. Durch die Temperaturdifferenz entsteht eine Thermospannung $U_{th}$, welche an den beiden Enden der A-Drahte abgegriffen werden kann. Dabei wird eine Kontakstelle in Eiswasser (0°) getaucht um als Referenz zu dienen. Mithilfe der Interpolationsformel
\begin{align}
T = 25.157 U_{th} - 0.19 (U_{th})^2  
\end{align}
lässt sich in dem Gültigkeitsbereich von 0<T<100°C aus der Thermospannung die Temperatur bestimmen. Dabei wird mit $U_{th}$ in mV gerechnet.

\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild2.png}}
\caption{Prinzipieller Aufbau eines Thermoelementes}
\label{fig:Abbildung}
\end{figure}

Die zu überprüfenden Körper bestehen dabei aus Zinn, Kupfer und Blei, wobei letzteres 3mal gemessen wird.
Die nachstehende Abbildung 3 stellt Daten zu den verwendeten Materialien dar.
\newpage

\section{Auswertung}

\subsection{Fehlerrechnung}
Der Mittelwert aus einer Reihe von n Messwerte $x_i$ berechnet sich durch die Formel
\begin{align}
\bar{x}=\frac{1}{n} \sum_{i=1}^n x_i
\end{align}
und der Fehler dieses Mittelwertes durch
\begin{align}
\Delta\bar{x}=\sqrt{\frac{1}{n(n-1)}\sum_{i=1}^n (x_i-\bar{x})}
\end{align}
Der wahrscheinlichste Fehler $\Delta y$ einer Größe $y$, die sich aus den fehlerbehafteten Größen $x_i$ berechnet, lässt sich bestimmen durch die Gauß'schen Fehlerfortpflanzung:
\begin{align}
\Delta y=\sqrt{\left(\sum_{i=1}^n \frac{\partial y}{\partial x_i} \Delta x_i\right)^2}
\end{align}

\subsection{Bestimmung der Wärmekapazität des Mischungskalorimeters}

Für die Bestimmung der Wärmekapazität des Mischungskalorimeters werden die in Tabelle 1 gelisteten Messwerte von $m_{x,y}$ und $U_{x,y}$ benötigt.

\begin{table}[htbp]
\centering\begin{tabular}{l||lcr}
 & $m $[g] & $U $[mV] & $T$ [\symbol{23}C]
\\ \hline\hline
1. Messung
\\ \hline
x & 269,31 & 0,76 & 19,01\\
y & 265,27 & 2,86 & 70,39 \\
Misch & 534,58 & 1,76 & 43,69 \\ 
\hline
2. Messung
\\ \hline
x & 275,00 & 0,80 & 20,00\\
y & 276,50 & 2,83 & 69,67\\
Misch & 551,50 & 1,95 & 48,33\\
\hline
3. Messung
\\ \hline
x & 278,83 & 0,82 & 20,50\\
y & 277,17 & 3,03 & 74,48\\
Misch & 556,00 & 1,83 & 45,43\\
\end{tabular}
\caption{Messwerte für die Bestimmung von ${c_g}{m_g}$}
\label{tab:Tabelle}
\end{table}
Der für die weiteren Rechnungen benötigte Faktor ${c_g}{m_g}$ berechnet sich nach Gleichung 18 mit der Näherung $c_w\approx 4,18 \mathrm{\frac{J}{g*K}}$ bei $\mathrm{40\symbol{23}C}$:

\begin{table}[htbp]
\centering \begin{tabular}{l||r}
& $c_gm_g \mathrm{[\frac{J}{K}]}$
\\ \hline\hline
1. Messung & $73,87$
\\ \hline
2. Messung & $-278,90$
\\ \hline
3. Messung & $184,53$
\\ \hline\hline
Mittelwert & $129,20\pm 55,33$
\end{tabular}
\end{table}

Da wegen des negativen Ergebnisses bei der 2. Messung scheinbar ein Fehler unterlaufen ist, wurde dieser aus der Berechnung des Mittelwertes herausgenommen.

\subsection{Bestimmung der spezifischen Wärmekapazität $c_k$ von Zinn, Kupfer und Blei}

Zur Bestimmung der Wärmekapazität sind alle benötigten Messwerte und die durch Gleichung (19) berechneten Temperaturen gelistet.
\begin{table}[htbp]
\centering\begin{tabular}{l||lcr}
 & $m ${[g]} & $U ${[mV]} & $T$ {[\symbol{23}C]}
\\ \hline\hline
Zinn
\\ \hline
Wasser & 562,90 & 0,92 & 22,98\\
Zinn & 231,14 & 3,18 & 78,08 \\
Misch & 794,04 & 0,96 & 23,98 \\
 \hline
Kupfer
\\ \hline
Wasser & 562,90 & 0,95 & 23,73\\
Kupfer & 237,91 & 3,47 & 85,01\\
Misch & 800,81 & 1,03 & 25,71\\
 \hline
Blei
\\ \hline
Wasser & 587,60 & 0,83 & 20,75\\
Blei & 587,03 & 3,12 & 76,64\\
Misch & 1174,63 & 0,92 & 22,98\\
 \hline
Wasser & 587,60 & 0,90 & 22,49\\
Blei & 277,17 & 3,34 & 82,90\\
Misch & 556,00 & 0,99 & 24,72\\
 \hline
Wasser & 586,92 & 0,84 & 21,00\\
Blei & 587,03 & 3,29 & 80,71\\
Misch & 1173,95 & 0,92 & 22,98\\
\end{tabular}
\caption{Messwerte zur Bestimmung von der Wärmekapazität $c_K$ verschiedener Stoffe}
\label{tab:Tabelle}
\end{table}\\
Die Spezifische Wärmekapazität $c_k$ berechnet sich nach Gleichung (17) sowie die Molwärme bei konstanten Druck $C_P$ durch Multiplikation der Wärmekapazität mit der molaren Masse $M$ des verwendeten Stoffes.
\begin{align}
C_P=c_K\cdot M
\end{align}
Die Molwärme bei konstantem Volumen $C_V$ wird durch den Zusammenhang in mit $C_P$ in Gleichung (14) berechnet. Alle dafür benötigten Theoriewerte sind in Abbildung 3 angegeben und für die Temperatur wurde die jeweiligen Mischtemperaturen verwendet.


\begin{table}[htbp]
\centering\begin{tabular}{l||ccccc}
 & Zinn & Kupfer & Blei 1 & Blei 2 & Blei 3
\\ \hline\hline
$c_K \mathrm{[\frac{J}{g\cdot K}]}$ &  $0,20\pm0,004$ & $0,35\pm0,008$ & $0,18\pm0,004$ & $0,17\pm0,004$ & $0,15\pm0,003$\\
$C_P \mathrm{[\frac{J}{mol\cdot K}]}$ & $23,56\pm0,47$ & $22,12\pm0,51$ & $37,92\pm0,83$ & $34,98\pm0,83 $& $31,26\pm0,62$\\
$C_V \mathrm{[\frac{J}{mol\cdot K}]}$ & $23,42\pm0,42 $& $22,06\pm0,51 $& $37,79\pm0,83 $& $34,83\pm0,83 $& $31,13\pm0,62$\\
\end{tabular}
\caption{Berechnete Werte für $c_K$, $C_P$ und $C_V$}
\label{tab:Tabelle}
\end{table}

Für Blei ergeben sich im Mittelwert für $C_V=34,58\pm2,76 \mathrm{\frac{J}{mol\cdot K}}$


\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild3.png}}
\caption{Physikalische Eigenschaften der verwendeten Probematerialien [1]}
\label{fig:Abbildung}
\end{figure}


\subsection{Vergleich zum Dulong-Petit'schen Gesetz}
Nach der Theorie sagt das Dulong-Petit'sche Gesetz für die Werte $C_V$ unabhängig vom Stoff einen Wert von $C_V=3R=3kN_L=24,92 \mathrm{\frac{J}{mol\cdot K}}$ voraus, wobei $k$ die Boltzmann'sche Konstante und $N_L$ die Loschmidtsche Zahl ist.\\
Vergleicht man diesen Theoriewert mit den errechneten Werten aus Tabelle 3, so stellt man bei Blei eine Abweichung von $38,8\%$ nach oben fest. Bei Kupfer weicht der Wert von $C_V$ noch um $11,5\%$ von der Theorie ab und letzlich war der Wert für Zinn mit einer Abweichung von $6\%$ am nächsten an dem vom Dulong-Petit'schen Gesetz vorhergesagten Wert für $C_V$.\\

\section{Diskussion}
Nach der Theorie sollte sich der Wert der Molwärem bei konstantem Volumen $C_V$ mit hoher Temperatur sowie mit steigendem Atomgewicht immer weiter an den Wert $C_V=24,92 \mathrm{\frac{J}{mol\cdot K}}$, der sich aus dem Dulong-Petit'schen Gesetz herleitet, annähern. Jedoch war bei den Messungen von Blei, das schwerste Atom im Versuch, die Abweichung am größten, obwohl die Temperaturen in allen Messungen annähernd gleich war. Bei den weiteren Messungen von Zinn und Kupfer ist die erwartet Tendenz jedoch erkennbar.

Es ist ein Fehler bei den Messungen, besonders bei den Messungen mit Blei, anzunehmen, der aufgrund mehrerer Ursachen zustande kommen kann. Zu nennen wäre dabei zum einen, dass ein Energieaustausch während des Versuches mit der Umwelt nicht vollständig verhindert werden konnte, zum anderen auch, dass nicht mit Sicherheit gesagt werden kann, dass sich die Energie gleichmäßig in dem System bzw. im Wasser verteilt hat, wodurch die Messung der Mischtemperatur nicht exakt sein muss.

Im Bereich der Messgenauigkeit in dem Versuch lässt sich in den Messungen für Zinn und Kupfer jedoch trotzdem das Dulong-Petit'sche Gesetz für höhere Atommassen bestätigen, und auch die quantenmechanische Theorie, die vorhersagt, dass sich die für hohe Temperaturen und hohe Atommassen der $C_V$ Wert immer weiter an das Dulong-Petit'sche Gesetz annähert, kann durch diese Messungen als bestätigt angesehen werden.

\begin{table}[htbp]
\centering\begin{tabular}{c||ccc}
&Zinn&Kupfer&Blei\\
\hline \hline
$C_V \mathrm{\frac{J}{mol \cdot K}}$& 27,06& 24,45 &26,72
\end{tabular}
\caption{Litaraturwerte für $C_V$ \quad [2]}
\label{tab:Tabelle}
\end{table}
Vergleicht man die gemessenen Werte für $C_V$ mit den in Tabelle 4 angegebenen Literaturwerten, erkennt man für Blei eine sehr hohe Abweichung von $29,4\%$. Jedoch sind die anderen Messungen mit einer Abweichung von $9,8\%$ für Kupfer und $13.5\%$ für Zinn scheinbar im Rahmen der Messungenauigkeit repräsentativ.

\section{Literaturverzeichnis}
[1] Physikalisches Anfängerpraktikum TU Dortmund, Skript zum Versuch Nummer 201
[2] www.periodensystem.info/elemente

\end{document}

