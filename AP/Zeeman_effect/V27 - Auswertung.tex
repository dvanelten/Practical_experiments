\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 27}  \\ Protokoll vom \today}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Fortgeschrittenen-Praktikum SS15}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch Nr. 27\\ Der Zeeman-Effekt}
\author{Konstantin Pfrang\\ \small{\textit{konstantin.pfrang@tu-dortmund.de}} \and Dennis van Elten\\ \small{\textit{dennis.vanelten@tu-dortmund.de}}}
\date{\today}
\setcounter{page}{-0} 
\maketitle
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Zielsetzung}
In diesem Experiment wird der Zeeman-Effekt, die Aufspaltung und Polarisation von Spektrallinien unter Einfluss eines äußeren Magnetfeldes $\vec{B}$, am Beispiel der roten und blauen Spektrallinie einer Cd-Quelle untersucht.

\section{Theoretische Grundlagen}

Ausgehend von der Quantentheorie lässt sich durch mehrere Überlegungen der Zeeman-Effekt physikalisch herleiten und erklären. Die Quantentheorie besagt, dass die Elektronen in Atomen nur diskrete Energiewerte annehmen können und Lichtemissionen aus Übergängen dieser Niveaus folgen. Außerdem lassen sich mithilfe der Quantentheorie Spin, magnetisches Moment, die Auswahlregeln und die Richtungsquantelung erklären. Diese physikalischen Größen bzw. Eigenschaften sind Voraussetzungen zum Verständnis des Zeeman-Effekts. \\
\subsection{Herleitung der magnetischen Momente}
Elektronen besitzen neben dem Bahndrehimpuls $\vec{l}$ auch einen Eigendrehimpuls/Spin $\vec{s}$. Für diese beiden Größen gelten die Beziehungen

\begin{align}
|\vec{l}| &= \sqrt{l(l+1)} \hbar, \quad l=0,1,2,...,n-1 \\
|\vec{s}| &= \sqrt{s(s+1)} \hbar, \quad s=\frac{1}{2}.
\end{align}
Dabei stellt n die Hauptquantenzahl. Des Weiteren existieren magnetische Momente, die über die Ladung des Elektrons mit den Drehimpulsen verknüpft sind. Die beiden magnetischen Momenten folgen der Beziehung

\begin{align}
\vec{\mu}_l &= - \mu_B \sqrt{l(l+1)}  \vec{l}_e \\
\vec{\mu}_s &= - g_s \mu_B \sqrt{l(l+1)}  \vec{s}_e 
\end{align}
mit dem Bohrschen Magneton
\begin{equation}
\mu_B =  - \frac{1}{2} e_0 \frac{\hbar}{m_0}.
\end{equation}
Der in Gl.(2.4) auftretende Faktor $g_s$ ist der Landé-Faktor des Elektrons und besitzt nach Dirac ungefähr den Wert 2. Physikalisch bedeutet das, dass das magnetische Moment des Elektrons bezogen auf den Spin doppelt so groß ist wie das bezogen auf den Bahndrehimpuls. Dieses Phänomern trägt die Bezeichung der magnetomechanischen Anomalie des Elektrons.
\newpage
\subsection{Wechselwirkungen mehrerer Drehimpulse}
Im Wesentlichen ist die Berechnung der Wechselwirkungen zwischen Mehrelektronensystemen nur schwer zu handhaben, jedoch kann diesem Problem mit zwei Grenzfällen Abhilfe geschafft werden. Zunächst wird der Fall betrachtet, bei dem Atome mit geringer Kernladungszahl vorliegen. Hierbei ist die Wechselwirkung zwischen den einzelnen Spins und Bahndrehimpulsen wesentlich größer als die der Komponenten untereinander, so dass die Drehimpulse vektoriell addiert werden können:
\begin{align}
\vec{L} &= \sum_i{\vec{l}_i} \\
\vec{S} &= \sum_i{\vec{s}_i}.
\end{align}
Es sei zu erwähnen, dass deren Eigenwerte analog sind zu denen der einzelnen Komponenten. Zu beiden Drehimpulsen gehört ein magnetisches Moment gemäß
\begin{align}
|\mu_l| &= \mu_B \sqrt{L(L+1)} \\
|\mu_s| &= g_s \mu_B \sqrt{S(S+1)}.
\end{align} 
Die bereits erwähnte sehr starke Wechselwirkung unter den einzelnen Drehimpulsen ist ausschlaggebend für die LS-/Russell-Saunders-Kopplung. Die aufsummierten Drehimpulse $\vec{L}$ und $\vec{S}$ addieren zum Gesamtdrehimpuls $\vec{J} = \vec{L} + \vec{S}$. Dieser dient in diesem Experiment zur Beschreibung der Aufspaltung des Zeeman-Effekts und besitzt eine von $S$ und $L$ abhängige (gerade bzw. ungerade) Drehimpulsquantenzahl $J$.
\newline
Der zweite Fall tritt ein für Atome mit einer hohen Kernladungszahl. Hier tritt die j-j-Kopplung auf, die daraus resultiert, dass nun die Wechselwirkung zwischen Spin und Bahndrehimpuls des einzelnen Elektrons größer ist.
\subsection{Spektroskopische Notation}
Die spektroskopische Notation folgt der Darstellung:
\begin{equation}
^ML_J.
\end{equation}
Dabei bezeichnet $M=2S+1$ die Multiplizität, aus der der Spin entnommen werden kann. Der Spin kann dabei die Werte $\frac{N}{2},...,\frac{1}{2}$ annehmen. Diese Einschränkung erfolgt durch die Anzahl $N$ der Elektronen auf nicht abgeschlossenen Bahnen. Für den Wert von $L$ wird der dem Drehimpuls zugeteilte Buchstabe S,P,D,F,... verwendet. $J$ bezeichnet den Gesamtdrehimpuls.
\subsection{Aufspaltung der Energieniveaus im homogenen Magnetfeld}
Um die Aufspaltung der Niveaus zu erklären muss zunächst das magnetische Moment $\vec{\mu}=\vec{\mu_L} + \vec{\mu_S}$, welches zum Gesamtdrehimpuls $\vec{J}$ gehört, bestimmt werden. Dabei sei wichtig zu erwähnen, dass $\vec{\mu}$ und $\vec{J}$ nicht gleichgerichtet sind. Stattdessen präzidiert das magnetische Moment um $\vec{J}$. Daraus folgt die Notwendigkeit das magnetische Moment in eine parallele und eine senkrechte Komponente zu zerlegen, wobei im zeitlichen Mittel die senkrechte Komponente $\vec{\mu_S}$ verschwindet. Für den Betrag des zu $\vec{J}$ gehörenden magnetischen Moment folgt:
\begin{equation}
|\vec{\mu}_J| = \mu_B g_J \sqrt{J(J+1)}
\end{equation}
mit dem allgemeinen Landé-Faktor
\begin{equation}
g_J := \frac{3J(J+1) + S(S+1) - L(L+1)}{2J(J+1)}.
\end{equation}
Ein weiterer wichtiger Betrachtungsaspekt ist die Richtungsquantelung. Diese besagt, dass bei einem von außen angelegten Magnetfeld zwischen $\vec{B}$ und dem magnetischen Moment $\vec{\mu}$ nur Winkel erlaubt sind, die der Relation
\begin{equation}
\mu_{J_z} = -m g_J \mu_B
\end{equation}
folgen. Der Faktor $m$ in Gl.(2.13) bezeichnet die Orientierungsquantenzahl, die insgesamt genau $2J+1$-Werte zwischen $-J$ und $J$ annehmen kann (siehe Abb.1). Mithilfe dieser Vorüberlegungen kann nun die vom $\vec{B}$-Feld auf das magnetische Moment $\vec{\mu}$ übertragende Energie bestimmt werden:
\begin{equation}\label{wichtig}
E_{mag} = m g_J \mu_B B.
\end{equation}
Da $m$ nur ganzzahlige Werte annimmt, folgt aus dieser Formel, dass die Energieaufspaltung äquidistant ist.
\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=12cm]{Bilder/Bild1.png}}
\caption{Aufspaltung der Energieniveaus für $J=2$ [1]}
\label{fig:Abbildung}
\end{figure}

\subsection{Auswahlregeln für Übergänge zwischen aufgespalteten Energieniveaus}
Um die Bildung und Anzahl der Spektrallinien zu verstehen ist eine Betrachtung ausgehend von der zeitabhängigen Schrödinger-Gleichung 
\begin{equation}
- \frac{\hbar^2}{2m} \Delta \psi (\vec{r},t) + U \psi (\vec{r},t) + \frac{\hbar}{i} \frac{\partial \psi (\vec{r},t)}{\partial t} = 0
\end{equation}
notwendig, da es sich um zeitliche Übergänge handelt. Diese besitzt die Lösung
\begin{equation}
\psi_\alpha (\vec{r},t) = \psi_\alpha (\vec{r},t) exp \left( - \frac{i}{\hbar} E_\alpha t \right).
\end{equation}
Ausgehend von dieser Lösung eines Zustandes $\alpha$ und der analogen Lösung eines weiteren Zustandes $\beta$ werden nun die Auswahlregeln ermittelt. Über eine Linearkombination der beiden Lösungen wird auf die zeitabhängige Dichteverteilung geschlossen. Diese beschreibt die Schwingungen des Elektrons. Die Intensität der emittierten Strahlung kann über das bei der Schwingung des Elektrons erzeugte elektrische Dipolmoment bestimmt werden. In den Formeln für die Dipolmomente tauchen Matrixelemente auf, die wichtig für die Berechnung der Strahlungsemission sind und z.B aus Fermis Goldener Regel ermittelt werden können. Nach einigen Rechenschritten folgt die Tatsache, dass nur Spektrallinien emittiert werden können, für die die Differenz der Orientierungsquantenzahlen $\Delta m$ zwischen zwei Zuständen $0$ oder $\pm 1$ beträgt. Des Weiteren ist der Wert von $\Delta m$ entscheidend für die Polarisation der emittierten Spektrallinien. Für $\Delta m = 0$, die $\pi$-Komponente, ergibt sich eine lineare Polarisation in Feldrichtung, während für $\Delta m = \pm 1$, die $\sigma$-Komponente, eine zirkulare Polarisation relativ zur vorliegenden Feldrichtung vorliegt. Dabei ändert sich hier je nach Vorzeichen von $\Delta m$ die Polarisationsrichtung und die Elektronen werden gemäß der Lorentzkraft abgelenkt. Aufgrund der linearen Polarisation der $\pi$-Komponente ist diese nur mit einer zu den Feldlinien transversal orientierten Blickrichtung wahrzunehmen, während die $\sigma$-Komponente wegen ihrer zirkularen Polarisation sowohl longitudinal als auch bei transversal zum Feld beobachtet werden kann.
\subsection{Normaler Zeeman-Effekt}
Unter dem normalen Zeeman-Effekt versteht man die Aufspaltung der Spektrallinien für den Fall, dass der Gesamtspin $S=0$ ist. Damit verbunden ist die Eigenschaft, dass für alle Gesamtdrehimpulse $J$ der Landé-Faktor $g_j=1$ lautet. Die Verschiebung der Energieniveaus entspricht damit 
\begin{equation}
\Delta E = m \mu_B B \quad -J \le m \le J.
\end{equation}
 Es folgt immer eine Aufspaltung in drei Spektrallinien, was als Zeeman-Triplett bezeichnet wird.
\subsection{Anomaler Zeeman-Effekt}
Diseser Effekt tritt auf, wenn der Gesamtspin nicht verschwindet. Es folgen für diesen Fall dieselben Auswahlregeln, allerdings müssen diese aus der spin- und zeitabhängigen Schrödinger-Gleichung bewiesen werden. Ein wesentlicher Unterschied gegenüber dem normalen Zeeman-Effekt sind die von $1$ verschiedenen Landé-Faktoren und der damit verbundene Unterschied der Energiedifferenz zwischen zwei Energieniveaus, der nun der Formel
\begin{equation}
\Delta E = \left(m_1 g(L_1, S_1, J_1) - m_2 g (L_2, S_2, J_2)\right) \mu_B B
\end{equation}
folgt. Die Aufspaltung wird zuätzlich linienreicher, da $g$ von den Drehimpulsen abhängig ist.

\section{Versuchsdurchführung}
\subsection{Experimenteller Aufbau}
Ziel der Versuchsdurchführung ist es, anhand der blauen und roten Cd-Linie den anomalen bzw. normalen Zeeman-Effekt zu beobachten und deren Wellenlängenveränderungen bei der Aufspaltung zu messen. Dazu wird der in der nachstehenden Abbildung dargestellte Aufbau verwendet, bei der sich die Cd-Lampe zwischen den Polschuhen des Elektromagneten befinden. Die Emissionslinien des Cadmiums werden über ein Linsensystem auf ein Gradsichtprisma gelenkt. Dabei kann mittels eines Spaltes die gewünschte Spektrallinie untersucht werden. Anschließend können mittels eines Polarisationsfilter und eines weiteren Spalts die Linien auf eine Lummer-Gehrcke-Platte gelenkt werden. Diese erzeugt ein Interferenzmuster,  welches mit einer Digitalkamera aufgenommen wird.

\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=15cm]{Bilder/Bild2.png}}
\caption{Versuchsaufbau der Messapparatur [1]}
\label{fig:Abbildung}
\end{figure}

\subsection{Lummer-Gehrcke-Platte}
Die Lummer-Gehrcke-Platte nutzt die Interferenz an planparallelen Platten um ein Auflösungsvermögen von $10^5$ zu erreichen. Bei jeder Reflexion inerhalb der Platte tritt ein kleiner Teil aus und kann anschließend konstruktiv mit anderen Austrittsstrahlen konstruktiv interferieren. Dabei gilt für die konstruktive Interferenz die Bedingung
\begin{equation}
2 d \cos(\theta) = n \lambda
\end{equation}
mit der Dicke $d$ der Platte. 
\begin{figure}[htbp]
\centering{\includegraphics[height=4cm,width=10cm]{Bilder/Bild3.png}}
\caption{Interferenz in der Lummer-Gehrcke-Platte [1]}
\label{fig:Abbildung}
\end{figure}
Wird nun das Magnetfeld $\vec{B}$ eingeschaltet verschieben sich die Wellenlängen um $\partial \lambda$ und damit die Interferenzstreifen $\partial s$. Des Weiteren dürfen sich die Wellenlängen nicht überlagern, was durch das Dispersionsgebiet gewährleistet wird, welches von der einfallenden Wellenlänge und dem Brechungsindex abhängt:
\begin{equation}
\Delta \lambda_D = \frac{\lambda^2}{2 d} \cdot \sqrt{\frac{1}{n^2-1}}.
\end{equation}
Das Auflösungsvermögen der Platte berechnet sich nach
\begin{equation}\label{Aufl}
A = \frac{\lambda}{\Delta \lambda_D} = \frac{L}{\lambda} (n^2-1).
\end{equation}



\section{Auswertung}






\subsection{Versuchsvorbereitung}

Zunächst werden Tabellen erstellt, welche die Landee-Faktoren für die Übergänge, die zum einen zu der roten ($^1$P$_1\leftrightarrow ^1$D$_2$) und zum anderen zu der blaue ($^3$S$_1\leftrightarrow ^3$D$_1$) Linie des Cd-Lampe führen. Diese Landee-Faktoren berechnen sich durch 
\begin{align}
g_{ij}=m_ig_i-m_jg_j.
\end{align}
Daraus ergibt sich die Energie zu
\begin{align}
\Delta E=g_{ij}\mu_B B
\end{align}
Die Werte der Landee-Faktoren der Zustände sind in Tabelle \ref{land} gegeben und die der Übergänge in Tabelle \ref{landrot} und \ref{landblau}.


Außerdem wird das Auflösungsvermögen der verwendeten Lummer-Ghercke-Platte ($d=4$\,mm, $L=120$\,mm ) nach Gleichung \ref{Aufl} bestimmt. Die berechneten Werte sind in Tabelle \ref{Auf} angegeben.

\begin{table}[htbp]
\centering
\begin{tabular}{c||ccc|c}
Zustand & 	$l$	&	$s$	&	$j  $ & $g_{i}$ \\
\hline\hline
$^1$P$_1$ & 1 & 0 & 1 & 1\\
$^1$D$_2$ & 2 & 0 & 2 & 1\\
$^3$S$_1$ & 0 & 1 & 1 & 2\\
$^3$P$_1$ & 1 & 1 & 1 & $\frac{3}{2}$
\end{tabular}
\caption{LANDÉ-Faktoren}
\label{land}
\end{table}

\begin{table}[htbp]
\centering
\begin{tabular}{c||cccc| c}
rot & 	$m_i$	&	$g_i$	&	$m_j  $ & $g_j$ & $g_{ij}$  \\
    \hline\hline
$\sigma^-$ & 2 & 1 & 1 & 1 &1\\
& 1 & 1 & 0 & 1 & 1\\
& 0 & 1 & -1 & 1 & 1\\
\hline
$\pi$ & 1 & 1 & 1 & 1 & 0\\
& 0 & 1 & 0 & 1 & 0\\
& -1 & 1 & -1 & 1 & 0\\
\hline
$\sigma^+$ & 0 & 1 & 1 & 1 & -1\\
& -1 & 1 & 0 & 1 & -1 \\
& -2 & 1 & -1 & 1 & -1\\
\end{tabular}
\caption{Landee-Faktoren $g_{ij}$ für rot}
\label{landrot}
\end{table}


\begin{table}[htbp]
\centering{
\begin{tabular}{c||cccc|c}
blau & 	$m_i$	&	$g_i$	&	$m_j  $ & $g_j$ & $g_{ij}$  \\
\hline \hline
$\sigma^-$ & 1 & $\frac{3}{2}$ & 0 & 2 & $\frac{3}{2}$\\ 
& 0 & $\frac{3}{2}$ & -1 & 2& 2\\
\hline
$\pi$ &1 & $\frac{3}{2}$ & 1 & 2 &$-\frac{1}{2}$\\
& 0 & $\frac{3}{2}$ & 0 & 2 & 0\\
& -1 & $\frac{3}{2}$ & -1 & 2 & $\frac{1}{2}$\\
\hline
$\sigma^+$ & 0 & $\frac{3}{2}$ & 1 & 2 & -2\\
& -1 & $\frac{3}{2}$ & 0 & 2 & -$\frac{3}{2}$
\end{tabular}}
\caption{Landee-Faktoren $g_{ij}$ für rot}
\label{landblau}
\end{table}


\begin{table}[htbp]
\centering
\begin{tabular}{c|ccc|c}
& $\lambda$ [nm] & $n$ & $\Delta \lambda_D$ [pm] & A  \\
\hline\hline
rot & 643,8 & 1,4567 & 48,9 &209129 \\
blau & 480,0 & 1,4635 & 27,0 & 285458
\end{tabular}
\caption{Berechnung der Auflösungsvermögens}
\label{Auf}
\end{table}
\newpage

\subsection{Hysteresekurve}
Zur Kalibrierung des im Versuch verwendeten Elektromagneten wird die Flussdichte $B$ bei steigendem und bei sinkendem Stromfluss $I$ gemessen. Diese Messwerte sind in Tabelle \ref{fig:Hyst1} gelistet und in Abbildung \ref{fig:Hyst2} in der sogenannten Hysteresekurve dargestellt. Zusätzlich wurde jeweils eine Lineare Regression mit gnuplot durchgeführt, die folgende Werte ergab.\\
 Aufsteigend:
 \begin{align}
 B\, \text{[mT]} =  (62.70\pm 1.49)\, \text{mT}\cdot I\,\text{[A]} + (21.76\pm 13.93 )\,\text{mT}
\end{align}
Absteigend:
\begin{align}
 B\, \text{[mT]} =  (63.85\pm 0.85)\, \text{mT}\cdot I\,\text{[A]} + (16.02\pm 7.49 )\, \text{mT}
\end{align}

\begin{table}[htbp]
\begin{center}\begin{tabular}{c|c|c}
$B_{Aufsteigend}$\,[mT]& $I$\,[A] & $B_{Absteigend}$\,[mT]\\
\hline
5	&0	&7\\
71	&1	&75\\
140	&2	&134\\
191	&3	&203\\
250	&4	&261\\
323	&5	&331\\
392	&6	&403\\
493	&7	&509\\
580	&8	&540\\
653	&9	&603\\
680	&10&	658\\
716	&11&	708\\
764	&12&	791\\
817	&13&	834\\
886	&14&	903\\
947	&15&	958\\
989	&16&	989\\
\end{tabular}\end{center}
\caption{Messerwerte zur Erstellung der Hysteresekurve}
\label{fig:Hyst1}
\end{table}
\begin{figure}[htbp]
\centering{\includegraphics[height=9cm,width=15cm]{Bilder/Hysterie1_1.png}}
\caption{Hysteresekurve des Magneten}
\label{fig:Hyst2}
\end{figure}
\newpage
\subsection{Interferenzaufnahmen zur Bestimmung der Wellenlängenänderung}
Die Wellenlängenänderung lässt sich durch Formel \ref{änderung}
berechnen.
\begin{align}\label{änderung}
\delta\lambda=\frac{1}{2}\frac{\delta s}{\Delta S}\Delta \lambda_d
\end{align}
Da diese Formel nur abhängig ist von dem Verhältnis der Abstände der Maxima $\Delta S$ ohne Magnetfeld und der Aufspaltung $\delta s$ mit Magnetfeld, ist es ausreichend, beide in Einheiten von Pixeln anzugeben. Dies wurde mithilfe von \textit{Paint} durchgeführt. Zusätzlich wurden die Bilder mithilfe von \textit{GIMP 2} vorverarbeitet, so dass die Maxima deutlich auf den aufgenommenen Bildern zu erkennen sind. Außerdem wurde zur Verdeutlichung der Messung jeweils die Bilder so geschnitten, dass oben die Aufnahmen ohne Magnetfeld sind und unten diejenigen mit eingeschaltetem Magnetfeld.


\subsubsection{$\sigma$-Übergang der roten Linie}\label{rot}

\begin{figure}[htbp]
\centering{\includegraphics[height=6cm,width=13cm]{Bilder/Bildrot.jpg}}
\caption{Bearbeitete Digitalaufnahme des roten Interferenzmusters}
\label{fig:rot1}
\end{figure}
\begin{table}[htbp]\centering{
\begin{tabular}{c||ccc}
Ordnung&$\Delta s$\,[px]&$\delta s$\,[px]&$\delta \lambda$\,[pm]\\
\hline\hline
1&	254&	118&	11,359\\
2&	240&	112&	11,410\\
3&	228&	104&	11,152\\
4&	214&	102&	11,657\\
5&	204&	96&	11,506\\
6&	200&	86&	10,516\\
7&	189&	88&	11,384\\
8&	182&	88&	11,822\\
9&	182&	82&	11,016\\
10&	172&	78&	11,088\\
11&	168&	78&	11,352\\
12&	166&	74&	10,899\\
			\hline
	&&Mittelwert&		11,263\\
	&&Abweichung&		0,102
\end{tabular}}
\caption{Messwerte rotes Interferenzmuster und berechnete Wellenlängenänderung $\delta \lambda$}
\label{fig:rot2}
\end{table}

Die aus Grafik \ref{fig:rot1} abgelesenen Daten sind Tabelle \ref{fig:rot2} aufgelistet. In dieser ist auch der Mittelwert der Wellenlängenänderung sowie deren Abweichung angegeben.




\newpage 
\subsubsection{$\sigma$-Übergang der blauen Linie} 
\begin{figure}[htbp]
\centering{\includegraphics[height=7cm,width=13cm]{Bilder/Bildblau1.jpg}}
\caption{Bearbeitete Digitalaufnahme des blauen Interferenzmusters des $\sigma$-Übergangs}
\label{fig:blau1}
\end{figure}
\begin{table}[htbp]\centering{
\begin{tabular}{c||ccc}
Ordnung&$\Delta s$\,[px]&$\delta s$\,[px]&$\delta \lambda$\,[pm]\\
\hline\hline
1&	392&	180	&6,199\\
2&	344&	192	&7,535\\
3&	336&	193	&7,754\\
4&	292&	184	&8,507\\
5&	302&	144	&6,437\\
6&	274&	128	&6,307\\
7&	272&	116	&5,757\\
8&	252&	116	&6,214\\
9&	248&	120	&6,532\\
10&	224&	106	&6,388\\
			\hline
&&Mittelwert&			6,763\\
&&Abweichung&			0,274
\end{tabular}}
\caption{Messwerte blauen $\sigma$-Interferenzmuster und berechnete Wellenlängenänderung $\delta \lambda$}
\label{fig:blau2}
\end{table}

Entsprechend Abschnitt \ref{rot} sind die aus Bild \ref{fig:blau1} abgelesenen Werte in Tabelle \ref{fig:blau2} mit den Mittelwerten und den Wellenlängenänderungen gelistet. eine Unterscheidung des $\sigma^+$- und des $\sigma^-$-Übergangs ist in dieser Messung nicht möglich.



\newpage
\subsubsection{$\pi$-Übergang der blauen Linie}
\begin{figure}[htbp]
\centering{\includegraphics[height=7cm,width=13cm]{Bilder/Bildblau2.jpg}}
\caption{Bearbeitete Digitalaufnahme des blauen Interferenzmusters des $\pi$-Übergangs}
\label{fig:blau3}
\end{figure}
\begin{table}[htbp]\centering{
\begin{tabular}{c||ccc}
Ordnung&$\Delta s$\,[px]&$\delta s$\,[px]&$\delta \lambda$\,[pm]\\
\hline\hline
1	&312	&120&	5,192\\
2	&284	&116&	5,514\\
3	&274	&112&	5,518\\
4	&256	&102&	5,379\\
5	&238	&98	&5,559\\
6	&238	&90	&5,105\\
7	&220	&90	&5,523\\
8	&218	&88	&5,450\\
9	&202	&86	&5,748\\
10	&205	&82	&5,400\\
11	&198	&76	&5,182\\
\hline			
		&&Mittelwert&	5,415\\
		&&Abweichung& 0,058\\
\end{tabular}}
\caption{Messwerte blauen $\pi$-Interferenzmuster und berechnete Wellenlängenänderung $\delta \lambda$}
\label{fig:blau4}
\end{table}
Ebenfalls sind - wie in den vorhergegangenen Kapiteln - die Werte aus Bild \ref{fig:blau3} in Tabelle \ref{fig:blau4} dargestellt und ausgewertet.



\subsection{Bestimmung der Landé-Faktoren}
Die Landé-Faktoren $g_{ij}$ lassen sich aus den Digitalaufnahmen bestimmen. Für die Energiedifferenz gilt
\begin{align}
\Delta E &= E(\lambda+\delta \lambda)-E(\lambda)=\left(E(\lambda)+\frac{\delta E}{\delta \lambda}\cdot \delta \lambda\right)-E(\lambda)\notag\\&=\frac{\delta E}{\delta \lambda}\cdot\delta \lambda = \frac{hc}{\lambda^2}\cdot \delta \lambda
\end{align}
Hierbei ist $E(\lambda)$ gegeben durch $E(\lambda)=\frac{hc}{\lambda}$.
Durch Umformen von Gleichung \ref{wichtig} erhält man den Landee-Faktor
\begin{align}
|g_{ij}|=\frac{\Delta E}{\mu_B B}=\frac{1}{B\mu_B}\frac{hc}{\lambda^2}\cdot\delta\lambda
\end{align}
In der Rechnung wurden folgende Werte verwendet: 
\begin{align}
h&=6,626\cdot 10^{-34}\,\text{Js}\qquad [2]\notag\\
\mu_B&=9,274\cdot 10^{-24}\,\frac{\text{J}}{\text{T}}\qquad [2]\notag\\
c&=299,8\cdot\, 10^6\frac{\text{m}}{\text{s}}\qquad [2]\notag
\end{align}
Die benötigten Messwerte sind in Tabelle \ref{fig:Land} zusammen mit den sich ergebenden Landé-Faktoren dargestellt. Da bei den blauen Messungen die beiden Landé-Faktoren der $\sigma$-Übergänge nur gemeinsam gemessen werden können, wurde hierbei der Mittelwert der beiden Theoriewerte ($g_{ij}=1,75$) gemessen.


\begin{table}[htbp]\centering{
\begin{tabular}{c|cccccccc}

	&$\lambda$\,[nm]&	$I$\,[A]&	$B$\,[mT]&	$\Delta$ E\,[J]&	$g_{ij}$&	$\Delta g_{ij}$&	$g_{ij,theo}$&Abweichung\\
	\hline
rot&	643,8&	9,7&	629,95&	$5,39\cdot 10^{-24}$	&0,92&	0,01&	1&8,23\%\\
blau $\sigma$&	480&	5,1&	341,53&	$5,83\cdot 10^{-24}$&	1,84&	0,07&	1,75&4,94\%\\
blau $\pi$&	480&	16&	1024,96&	$4,67\cdot 10^{-24}$&	0,49&	0,01&	0,5&1,80\%
\end{tabular}}
\caption{Berechnung der Landee-Faktoren}
\label{fig:Land}
\end{table}


\section{Diskussion}
Wie in Tabelle \ref{fig:Land} zu erkennen, liegen die Abweichungen der experimentell bestimmten Landee-Faktoren zu den Theoriewerten zwischen $1,80\%$ und $8,23\%$. Diese sind auf eine ungenaue, da nicht perfekt scharfe, Digitalaufnahme zurückzuführen. Darüber hinaus ist davon auszugehen, dass beim Ablesen der Werte aus den Aufnahmen Abweichungen  zustande kamen.\\
Außerdem zeigte sich bei der Messung des Magnetfeldes eine große Schwankung, je nachdem wie die Hallsonde in das Magnetfeld gehalten wurde. Dies legt die Annahme nah, dass die Werte des Magnetfeldes am Ort der Lampe nicht exakt den Messwerten entspricht.


\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Fortgeschrittenenpraktikum: Skript zum Versuch Nummer 27, "Der Zeeman-Effekt"
\item http://www.springer.com/cda/content/document/cda_downloaddocument/9783540254218-c5.pdf?SGWID=0-0-45-479098-p63371798
\end{enumerate}
\end{document}

