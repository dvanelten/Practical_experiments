\select@language {ngerman}
\contentsline {section}{\numberline {1}Zielsetzung}{1}{section.1}
\contentsline {section}{\numberline {2}Theoretische Grundlagen}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Herleitung der magnetischen Momente}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Wechselwirkungen mehrerer Drehimpulse}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Spektroskopische Notation}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Aufspaltung der Energieniveaus im homogenen Magnetfeld}{2}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Auswahlregeln f\IeC {\"u}r \IeC {\"U}berg\IeC {\"a}nge zwischen aufgespalteten Energieniveaus}{3}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Normaler Zeeman-Effekt}{4}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Anomaler Zeeman-Effekt}{4}{subsection.2.7}
\contentsline {section}{\numberline {3}Versuchsdurchf\IeC {\"u}hrung}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Experimenteller Aufbau}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Lummer-Gehrcke-Platte}{5}{subsection.3.2}
\contentsline {section}{\numberline {4}Auswertung}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Versuchsvorbereitung}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Hysteresekurve}{8}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Interferenzaufnahmen zur Bestimmung der Wellenl\IeC {\"a}ngen\IeC {\"a}nderung}{9}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}$\sigma $-\IeC {\"U}bergang der roten Linie}{9}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}$\sigma $-\IeC {\"U}bergang der blauen Linie}{11}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}$\pi $-\IeC {\"U}bergang der blauen Linie}{12}{subsubsection.4.3.3}
\contentsline {subsection}{\numberline {4.4}Bestimmung der Land\IeC {\'e}-Faktoren}{13}{subsection.4.4}
\contentsline {section}{\numberline {5}Diskussion}{13}{section.5}
\contentsline {section}{\numberline {6}Literaturverzeichnis}{14}{section.6}
