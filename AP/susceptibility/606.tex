\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 606}  \\ Protokoll vom 20.05.14}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum SS 14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 606 - Messung der Suszeptibilität paramagnetischer Substanzen}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{20.Mai.2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
Das Ziel des Versuches ist die Untersuchung magnetischer Eigenschaften von stark paramagnetischen Ionen Seltener Erde. Genauer wird die Größe der Suszeptibilität experimentell bestimmt und mit den theoretischen Grundlagen verglichen.

\section{Theoretische Grundlagen}
\subsection{Berechnung der Suszeptibilität $\chi$ paramagnetischer Substanzen}
Im Vakuum existiert ein Zusammenhang zwischen der magnetischen Flussdichte $\vec{B}$ und der magnetischen Feldstärke ${\vec{H}$. Bei Anwesenheit von Materie liegt zusätzlich eine Magnetisierung ${\vec{M}$ vor, so dass gilt

\begin{equation}
\vec{B} = \mu_0 \vec{H}+\vec{M}.
\end{equation}

Dabei steht die Magnetisierung mit der mag. Feldstärke ${\vec{H}$ in folgender Beziehung

\begin{equation}
\vec{M} = \mu_0 \chi \vec{H},
\end{equation}

wobei $\chi$ die zu untersuchende Suszeptibilität ist. Diese ist neben der Temperatur $T$ auch von $\vec{H}$ abhängig.
Je nach Wert der Suszeptibiliät wird zwischen verschiedenen Formen der Magnetisierung unterschieden. Grundsätzlich liegt bei allen Atomen der Diamagnetismus ($\chi < 0$) vor, der durch Induktion magnetischer Momente mit dem äußeren Magnetfeld hervorgerufen wird. Dieses induziert ein entgegengesetz gerichtetes Feld. Des Weiteren liegt bei Atomen, Ionen oder Molekülen, die einen Drehimpuls ungleich Null besitzen,  der Paramagnetismus vor. Auslöser für diese Form der Magnetisierung ist die mit dem Drehimpuls gekoppelte Orientierung der magnetischen Momente relativ zu einem äußeren Feld. Der Paramagnetismus verstärkt das Magnetfeld um einen kleinen Faktor und ist temperaturabhängig.

\newline
Im Weiteren wird das Verhältnis zwischen Drehimpuls und magnetischem Moment im Atom betrachtet. Der Gesamtdrehimpuls $J$ setzt sich aus einem Bahndrehimpuls der Elektronenhülle $L$, dem Eigendrehimpuls der Elektronen (Spin) $S$ und dem Kerndrehimpuls, dessen Wirkung auf den Paramagnetismus vernachlässigt werden kann, zusammen. Für ein nicht zu starkes äußeres Magnetfeld gilt für den Gesamtdrehimpuls  der Elektronenhülle die LS-Kopplung

\begin{equation}
\vec{J} = \vec{L} + \vec{S}.
\end{equation}

Dabei sind $\vec{L}$ und $\vec{S}$ die Vektorsummen der Einzeldrehimpulse. Die zu den beiden Drehimpulsen, die zur Bestimmung von $\vec{J}$ notwendig sind, gehörenden magnetischen Momente lauten

\begin{align}
\vec{\mu}_L &= - \frac{\mu_B}{\hbar} \vec{L} \\
\vec{\mu}_L &= - g_S \frac{\mu_B}{\hbar} \vec{S}.
\end{align}

Dabei bezeichnet $\mu_B = \frac{1}{2}\frac{e_0}{m_0} \hbar$ das Bohrsche Magneton und die Größe $g_S$ das gyromagnetische Verhältnis des freien Elektrons.
Nun ist es möglich die Beträge der drei Größen $|\vec{L}|$, $|\vec{S}|$ und $|\vec{J}|$ über die jeweiligen Quantenzahlen auszudrücken

\begin{align}
|\vec{L}| &= \sqrt{L(L+1)} \hbar\\
|\vec{S}| &= \sqrt{S(S+1)} \hbar\\
|\vec{J}| &= \sqrt{J(J+1)} \hbar
\end{align}

Um weiter eine Gleichung für die gesuchte Suszeptibilität $\chi$ zu erhalten, sind weitere quantenmechanische Faktoren zu berücksichtigen. Unter Berücksichtigung der Richtungsquantelung, der Aufspaltung von Spektrallinien durch ein Magnetfeld (Zeeman-Effekt) ergibt sich das Curie'sche Gesetz des Paramagnetismus. Dieses besagt, dass die Suszeptibilität $\chi$ und eine hinreichend hohe Temperatur $T$ folgendermaßen zusammenhängen:

\begin{equation}
\chi \propto \frac{1}{T}.
\end{equation}

Das Gesetz folgt aus der Gleichung

\begin{equation}
\chi = \frac{\mu_0 \mu_B^2 g_J^2 N J(J+1)}{3 k T}
\end{equation} 

mit dem Landé-Faktor $g_J := \frac{3J(J+1) + {S(S+1)-L(L+1)}}{2J(J+1)}$, der das Verhältnis zwischen dem gemessenem magnetischen Moment und dem klassisch zu erwartenden magnetischen Moment angibt.

\subsection{Berechnung der Suszeptibilität $\chi$ Seltener-Erd-Verbindungen}
Die großen Drehimpulse der Seltener-Erd-Atome werden nur von den inneren Elektronen erzeugt. Des Weiteren entshet der Paramagnetismus durch 4$f$-Elektronen, die erst ab einer Ordnungszahl von z=56 existieren. Da diese weit innerhalb der 6s-Schale liegen, sind auch die Ionen der Seltenern Erden stark paramagnetisch. Der vorliegende Gesamtdrehimpuls $\vec{J}$ wird durch die Hund'schen Regeln bestimmt:

\begin{flushleft}
1. Die Spins $\vec{s}_i$ kombinieren zum maximalen Gesamtspin $\vec{S}= \sum{\vec{s}_i}$ der nach dem Pauli-Prinzip möglich ist.
\end{flushleft}
\newline
\begin{flushleft}
2. Die Bahndrehimpulse $\vec{l}_i$ setzen sich so zusammen, dass der maximale Drehimpuls $\vec{L}= \sum{\vec{l}_i}$, der mit dem Pauli-Prinzip und der Regel 1 verträglich ist, entsteht.
\end{flushleft}
\newline
\begin{flushleft}
3. Der Gesamtdrehimpuls ist $\vec{J} = \vec{L} - \vec{S}$, wenn die Schale weniger als halb und $\vec{J} = \vec{L} + \vec{S}$, wenn die Schale mehr als halb gefüllt ist.
\end{flushleft}

\section{Durchführung}
\subsection{Apparatur zur Messung der Suszeptibilität $\chi$}
Die Suszeptibilität $\chi$ kann mittels einer Induktivitätsmessung bestimmt werden. Dabei gilt für die Induktivität einer Spule

\begin{equation}
L = \mu_0 \frac{n^2}{l}F,
\end{equation}

wobei $n$ die Windungszahl, $l$ die Länge  und $F$ die Querschnittsfläche der Spule bezeichnen. Bei Füllung der Spule erweitert sich die Gleichung um den Faktor $\mu$. Jedoch füllen die beim Versuch verwendeten Proberöhrchen die Spule nicht vollständig aus, so dass für die Induktivität der zu untersuchenden Materie, in Abhängigkeit von der Suszeptibilität $\chi$ und des Probenquerschnitts $Q$, gilt

\begin{equation}
L_M = \mu_0 \frac{n^2 F}{l} + \chi \mu_0 \frac{n^2 Q}{l}.
\end{equation}

Für die Differenz von [Gl.(3.1)] und [Gl.(3.2)] ergibt sich

\begin{equation}
\Delta L = \mu_0 \chi Q \frac{n^2}{l}.
\end{equation}

Der Unterschied ist jedoch sehr gering und eine hohe Auflösung der L-Messung ist notwendig. Diese Voraussetzung kann mit der in Abb.1 dargestellten Brückenschaltung, bei der zwei möglichst gleiche Spulen verwendet werden, realisiert werden.
Es existieren nun zwei Möglichkeiten die Suszeptibilität $\chi$ zu messen.

Zum einen kann über eine Änderung der Abgleichelemente die Suszeptibilität ermittelt werden. Dazu lässt sich über die Kirchhoff'schen Regeln die Abgleichbedingung

\begin{equation}
(R_M + i \omega L_M) R_4' = (R + i \omega L ) R_3'
\end{equation}

bestimmen. Mithilfe der bekannten Beziehungen für die beiden Induktivitäten lautet die Formel zur Bestimmung von $\chi$:

\begin{equation}
\chi = 2 \frac{\Delta R}{R_3} \frac{F}{Q}.
\end{equation}

\newline
Bei der zweiten Methode wird die Änderung der Brückenspannung $U_{Br}$ gemessen, die entsteht, wenn die zuvor abgeglichene Brücke mit dem Proberöhrchen gefüllt wird. Mit den Kenntnissen aus [Gl.(3.1)] und [Gl.(3.3)] lautet der Zusammenhang zwischen der Brückenspannung $U_{Br}$ und der Suszeptibilität $\chi$

\begin{equation}
U_{Br} = \frac{\omega \mu_0 \chi n^2 Q}{4l} \frac{1}{\sqrt{R^2 + \omega^2 \left(\mu_0 \frac{n^2}{l}F\right)^2}}U_{Sp}
\end{equation}

Unter der Annahme, dass hohe Frequenzen mit der Bedingung ($\omega^2 L^2 \gg R^2$) gemessen werden vereinfacht sich die voranstehende Formel zu 

\begin{equation}
\chi (\omega \rightarrow \infty) = 4 \frac{F}{Q} \frac{U_{Br}}{U_{Sp}}.
\end{equation}

\begin{figure}[htbp]
\centering{\includegraphics[height=14cm,width=12cm]{Bilder/Bild1.png}}
\caption{Brückenschaltung für eine Suszeptibilitätsmessung [1]}
\label{fig:Abbildung}
\end{figure}

\subsection{Unterdrückung von Störspannungen bei Messung kleiner Signalspannungen}
Bei der vorliegenden Messung treten an den Ausgangsklemmen der Brückenschaltung Störspannungen auf, die im Verhältnis zu $U_{Br}$ sehr groß sind. da es sich bei $U_Br$ jedoch im Gegensatz zu den Störspannungen um eine monofrequente Spannung handelt, können die Störungen mithilfe eines Selektivverstärkers zum großen Teil rausgefiltert werden. Dabei entspricht der Selektivverstärker einem Bandpass, dessen Filterkurve die Gestalt einer Glockenkurve besitzt (Abb.2).

\begin{figure}[htbp]
\centering{\includegraphics[height=10cm,width=12cm]{Bilder/Bild2.png}}
\caption{Filterkurve eines Selektivverstärkers [1]}
\label{fig:Abbildung}
\end{figure}

Ein Maß für die Unterdrückung der Störspannungen durch den Selektivverstärker ist die Güte $Q$. Diese wird über die Formel 

\begin{equation}
Q = \frac{v_0}{v_+ - v_-}
\end{equation}

bestimmt. Zu Beachten ist die Tatsache, dass Frequenzen nache $v_0$, wenn auch abgeschwächt, den Filter passieren können.


Die Messung wird mit einer durch einen Sinusgenerator erzeugten Wechselspannung mit einer Spannung von $\approx$ 1V und einer Frequenz zwischen 20 und 40 kHz durchgeführt.

\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=14cm]{Bilder/Bild3.png}}
\caption{Blockschaltbild der verwendeten Apparatur [1]}
\label{fig:Abbildung}
\end{figure}
\newpage
\section{Auswertung}
Da die zur Berechnung benötigten Werte für Praseodymium(III)oxalate (C$_6$O$_{12}$Pr$_2$) nicht angegeben waren und auch nach längerer Recherche nicht auffindbar waren, wurden die Messwerte nur der Vollständigkeit halber angegeben jedoch war eine Auswertung unmöglich.
\subsection{Filterkurve des Selektiv-Verstärkers}
In Tabelle 1 sind die Messwerte aufgelistet und in Abbildung Graphisch dargestellt. 
\begin{table}[htbp]
\centering\begin{tabular}{c|c}
Frequenz $\nu$ [kHz]&Spannung $U_{sp}$ [mV]\\
\hline
30,00&	2,90\\
31,00&	3,68\\
32,00&	5,15\\
33,00&	7,61\\
33,50&	9,80\\
34,00&	13,60\\
34,25&	17,40\\
34,50&	24,20\\
34,70&	32,00\\
34,80&	38,70\\
34,90&	48,80\\
35,00&	63,50\\
35,10&	86,00\\
35,20&	99,90\\
35,30&	89,00\\
35,40&	66,90\\
35,50&	50,20\\
35,75&	30,10\\
36,00&	21,00\\
36,50&	12,90\\
37,00&	9,54\\
38,00&	6,11\\
39,00&	4,50\\
40,00&	3,50\\
\end{tabular}
\caption{Messwerte der Filterkurve}
\end{table}Die mit Gnuplot erzeugte Ausgleichsgerade folgt der Gleichung
\begin{equation}
U_{sig}=\frac{U_{max}}{\sqrt{1+(\nu_{0}-\eta)^2\cdot \tau}}
\end{equation}
wobei sich für $U_{max}$, $\nu_{0}$ und $\tau$ die Werte
\begin{align}
U_{max}&=(100,77\pm0,2969)\,\mathrm{V}\\
\notag
\nu_{0}&=35,21\,\mathrm{kHz}\\
\notag
\tau&=(34,62\pm0,40)\,\mathrm{s^2}\\
\end{align}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild10.png}}
\caption{$U_{sig}$ gegen $\nu_0$ aufgetragen}
\label{fig:Abbildung}
\end{figure}
Aus der Ausgleichsfunktion lässt sich $\nu_-=35,04\,\mathrm{kHZ}$ und $\nu_+=35,38\,\mathrm{kHZ}$ bestimmen und daraus mit Gleichung 3.8
\begin{align}
Q=103,57.
\end{align}
Da die Signalspannung $U_{sp}$ nicht näher bestimmt wurde, wird in der weiteren Auswertung die Spannung $U_{sp}=100,77\,\mathrm{mV}$ benutzt.

\subsection{Bestimmung der Suszeptibilität $\chi$}
\subsubsection{Theoretische Berechnung}
Zur Berechnung der Suszeptibilität $\chi$ wird der Landé-Faktor benötigt. Anschließend lässt sich $\chi$ durch Gleichung 2.10 bestimmen, wobei folgende Konstanten verwendet wurden:
\begin{align}
\notag
\mu_0&=1,257\cdot 10^{-6}\,\frac{\mathrm{N}}{\mathrm{A^2}}\\
\notag
T&=293\,\mathrm{K}\\
\notag
\mu_b&=9,274\cdot 10^{-24}\,\frac{\mathrm{J}}{\mathrm{T}}\\
\notag
k_B&=1,381\cdot 10^{-23}\,\frac{\mathrm{J}}{\mathrm{K}}\\
\notag
N_A &=6,022\cdot10^{23}\,\mathrm{mol^{-1}}\\
N&=2N_A\,\frac{\rho_w}{M}
\end{align}
Die zur Berechnung benutzen Werte sind in Tabelle 2  gelistet
\begin{table}[htbp]
\centering\begin{tabular}{|l|c|c|c|}
\hline
&Dy$_2$O$_3$&Gd$_2$0$_3$&Nd$_2$O$_3$\\
\hline
Spin $S$&	2,5&	3,5&	1,5\\
Bahndrehimpuls $L$&	5&	0&	6\\
Gesamtdrehimpuls $J$&	7,5&	3,5&	4,5\\
Landré-Faktor$g_j$&	1,33&	2,00&	0,73\\
Dichte $\rho$\,[g/cm^3] &	7,80&	7,40&	7,24\\
Molmasse $M$\,[g/mol]&	373&	362,5&	336,5\\
$N$\,[10^{28}/m^3]&2,51&2,46&2,59\\
\hline
$\chi_{theo}$ [10^{-3}]&25,42&13,79&3,02\\
\hline
\end{tabular}
\caption{Theoretische Berechnung der Suszeptibilität}
\end{table}
\newpage
\subsection{Experimentelle Bestimmung}
Da die Dichte der vorliegenden staubförmigen Proben geringer ist, als die eines Kristalls, muss zunächst die Querschnittfläche Q der Probe durch den realen Querschnitt ersetzt werden:
\begin{align}
Q_{real}=\frac{m_p}{\rho l}
\end{align}
Die für die Proben berechneten realen Querschnitte sind in Tabelle 3 gegeben. Da die Abweichungen der Längen sehr gering sind (im Bereich von $<0,04\%$) sind diese Vernachlässigbar.
\begin{table}[htbp]
\centering\begin{tabular}{l|c|c|c}
&Dy$_2$O$_3$&Gd$_2$0$_3$&Nd$_2$O$_3$\\
\hline
Masse $m$\,[g]&16,60&14,08&9,00\\
Länge $l$\,[cm]&18,09&17,7&18,00\\
Dichte $\rho_w$\,[g/cm^2]&7,80&7,40&7,24\\
\hline
$Q_real$ [mm$^2$]&11,76&10,75&6,90\\
\end{tabular}
\caption{Berechnung des realen Querschnittes}
\end{table}
In Tabelle 4 sind die Messwerte zur Berechnung der Suszeptibilitäten nach den Gleichungen 3.5 und 3.7 angegeben. Die sich daraus ergebenden Werte der Suszeptibilitäten sind in Tabelle 5 angegeben.
\begin{table}[htbp]
\centering\begin{tabular}{l|ccc|ccc}
&	$U_{Br}$\,[mV]&$U_{Br}$'\,[mV]&$\Delta U_{Br}$\,[mV]&	$R$\,[\Omega]&	$R$'\,[\Omega]&	$\Delta R$\,[\Omega]\\
\hline\hline
Dy_2O_3&0,198&0,225&	0,027&	2,664&	1,083&	1,581\\
\hline
Gd_2O_3&0,216&0,198&	0,018&	2,673&	1,957&	0,716\\
\hline
Nd_2O_3&0,192&0,191&	0,001&	2,657&	2,525&	0,132\\
\hline
&0,192&0,195&0,003&2,681&2,754&0,073\\
C_6O_{12}Pr_2&0,193&0,194&0,001&2,706&2,637&0,069\\
&0,191&0,201&0,010&2,595&2,713&0,118\\

\end{tabular}
\caption{Messwerte zur experimentellen Bestimmung der Suszeptibilität}
\end{table}
\begin{table}[htbp]
\begin{tabular}{l||cc|c||cc|c}
&	$\chi_U [10^{-3}]$&$\Delta\chi_U [10^{-3}]$&Abweichung [\%]&	$\chi_R [10^{-3}]$&$\Delta\chi_R [10^{-3}]$&Abweichung[\%]\\
\hline
Dy_2O_3&7,89&0,02&67,0&	23,28&0,01&8,4\\
Gd_2O_3&5,76&0,02&58,3&	11,53&0,00&16,2\\
Nd_2O_3&0,50&0,00&83,5&	3,31&0,00&9,6\\
\end{tabular}
\caption{Ergebnisse der Suszeptibilitäten}
\end{table}
\newpage
\section{Diskussion}
Wie in Tabelle 5 zu erkennen ist, stimmen die über die Widerstände bestimmten Werte $\chi_R$ im Rahmen der Messgenauigkeit mit den theoretischen Werten überein.\\
Anders ist es bei $\chi_U$ mit Abweichungen von 58,3\% bis 83,4\%. Dafür lassen sich verschiedene mögliche Gründe nennen. Zum einen waren die Abweichungen der abgeglichenen Brückenspannung sehr gering und zusätzlich wurde eine genaue Messung durch Schwanken des Zeigers erschwert. Des weiteren ist durch die nicht sehr genaue Bestimmung der Signalspannung $U_{Sp}$ ein zusätzliche Abweichung möglich.\\
Nichtsdestotrotz lassen sich die Theoriewerte auf Grundlage der über die Widerstände berechneten Werte bestätigen und zumindest die absteigende Größe der Suszeptibilitäten der Reihe nach von Dy$_2$O$_3$ über Gd$_2$O$_3$ bis zu Nd$_2$O$_3$ lässt sich auch nach der Brückenspannungsmethode erkennen.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 606, Messung der Suszeptibilität paramagnetischer Substanzen
\end{enumerate}
\end{document}



