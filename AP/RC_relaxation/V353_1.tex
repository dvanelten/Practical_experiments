\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\usepackage{scrpage2} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 204}  \\ Protokoll vom \today}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 353 - Das Relaxationsverhalten eines RC-Kreises}
\author{Konstantin Pfrang \and Dennis van Elten}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
Untersucht werden RC-Kreise (Schaltungen, die hauptsächlich aus einem Kondensator und einem Widerstand bestehen) auf ihr Relaxationsverhalten. Dazu wird der zeitliche Verlauf der Relaxation, die Rückkehr des Systems nach einer Anregung/Störung in seinen Grundzustand, sowie das Verhalten bei einer periodischen Anregung des Systems untersucht.

\section{Theoretische Grundlagen}

\subsection{Allgemeine Relaxationsgleichung}
Wird eine physikalische Größe $A$ betrachtet, so ist deren Änderungsgeschwindigkeit zu einem Zeitpunkt $t$ meistens proportional zur Abweichung von $A$ vom (asymptotischen) Endzustand $A (\infty)$
\begin{equation}
\frac{dA}{dt} = c [A(t)-A(\infty)].
\end{equation}
Nach Integration der Gleichung [Gl(2.1)] über das Intervall 0 bis $t$ ergibt sich für die Größe $A(t)$
\begin{equation}
A(t) = A(\infty) + [A(0)-A(\infty)]e^{ct}
\end{equation}
mit c<0, damit $A$ gegen eine Grenze konvergiert.

\subsection{Entladevorgang}

\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Entladung (Stellung 1) und Aufladung (Stellung 2) eines Kondensators über einen Widerstand [1]}
\label{fig:Abbildung}
\end{figure}

Die Spannung $U_C$, die zwischen den beiden Platten eines Kondensators $C$ mit einer Ladung $Q$ liegt, berechnet sich mit
\begin{equation}
U_C = \frac{Q}{C}.
\end{equation}
Weiter kann nach dem Ohmschen Gesetz der Strom $I$ durch einen Widerstand $R$ ausgedrückt werden als
\begin{equation}
I = \frac{U_C}{R}.
\end{equation}
In einem Zeitintervall $dt$ ändert sich die Ladung $dQ$ = $-Idt$. Mithilfe der obigen Gleichungen folgt die Differentialgleichung
\begin{equation}
\frac{dQ}{dt} = -\frac{1}{RC}Q(t).
\end{equation}
Nach Integration und der Randbedingung $Q(\infty)=0$ lässt sich die Ladung ausdrücken durch
\begin{equation}
Q(t) = Q(0) e^{\frac{-t}{RC}}.
\end{equation}

\subsection{Aufladevorgang}

Ist die Kapazität $C$ über einen Widerstand $R$ an eine Spannungsquelle mit Spannung $U_0$ geschlossen, wird der Aufladevorgang durch die Gleichung
\begin{equation}
Q(t) = CU_0 (1-e^{\frac{-t}{RC}})
\end{equation}
beschrieben. Dabei gelten die Randbedingungen $Q(0)=0$ und $Q(\infty)=CU_0$. Das Produkt $RC$ ist die Zeitkonstante des Relaxationsvorganges. Sie beschreibt, wie schnell das System sich seinem Endzustand $Q(\infty)$ annähert. Die Ladungsänderung des Kondensators in dem Zeitintervall $\Delta T=RC$ lautet
\begin{equation}
\frac{Q(t=RC)}{Q(0)} = \frac{1}{e} \approx 0,368.
\end{equation}
Nach $\Delta T = 2,3RC$ sind nur noch 10\%, nach $\Delta T=4,6RC$ nur noch 1\% der Ausgangsladung vorhanden.

\subsection{Relaxationsphänomene eines RC-Kreises bei anliegender Sinusspannung}
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild2.png}}
\caption{Relaxationsphänomene eines RC-Kreises mit Wechselspannung [1]}
\label{fig:Abbildung}
\end{figure}
Die in Abb.2 dargestellte Schaltung wird mit einer Wechselspannung
\begin{equation}
U(t) = U_0 \cdot cos(\omega t)
\end{equation}
betrieben. Ist die Kreisfrequenz $\omega$ << $\frac{1}{RC}$ ist die Kondensatorspannung $U_C(t)$ $\approx U(t)$, der angelegten Wechselspannung. Mit zunehmender Frequenz stellt sich jedoch eine Phasenverschiebung $\phi$ ein, da die Auf- und Entladung des Kondensators über den Widerstand $R$ immer weiter hinter dem zeitlichen Verlauf der Einspeisespannung zurückbleibt. Die Amplitude $A$ der Kondensatorspannung nimmt dabei ab. Über den Ansatz
\begin{equation}
U_C(t) = A(\omega) \cdot cos(\omega t + \phi(\omega)) \cdot cos(\omega t)
\end{equation}
und nach dem zweiten Kirchhoffschen Gesetz gilt
\begin{equation}
U_0 \cdot cos (\omega t) = I(t)R + A(\omega) \cdot cos(\omega t + \phi)^2.
\end{equation}
Weiter lässt sich die Beziehung unter der Voraussetzung
\begin{equation}
I(t) = C \frac{dU_C}{dt}
\end{equation}
zu
\begin{equation}
U_0 \cdot cos (\omega t) = -A \omega RC \cdot sin(\omega t+\phi) + A (\omega) \cdot cos (\omega t+\phi)
\end{equation}
weiterentwickeln. Gleichung [Gl.(2.13)] muss für jeden Zeitraum $t$ gelten. Für $wt = \frac{\pi}{2}$ gilt
\begin{equation}
0 = -\omega RC \cdot sin(\frac{\pi}{2}+\phi) + cos(\frac{\pi}{2}+\phi)
\end{equation}
und damit für die Frequenzabhängigkeit der Phase
\begin{equation}
\phi(\omega) = arctan (-\omega RC).
\end{equation}
Für sehr kleine Frequenzen strebt die Phasenverschiebung gegen Null, wohingegen diese sich für große Frequenzen dem Wert $\frac{\pi}{2}$ annähert. Die Frequenzabhängigkeit der Amplitude $A$ folgt aus Gleichung [Gl.(2.13)] mit $\omega t + \phi = \frac{\pi}{2}$:
\begin{equation}
A(\omega) = - \frac{sin \phi}{\omega RC} \cdot U_0.
\end{equation}
Mithilfe der trigonometrischen Beziehung $sin^2 \phi+cos^2 \phi = 1$ lässt sich aus der Frequenzabhängigkeit der Phase [Gl.(2.15)] die Formel
\begin{equation}
sin \phi = \frac{\omega RC}{\sqrt{1+ \omega^2R^2C^2}}
\end{equation}
herleiten. Somit ergibt sich für die Amplitude
\begin{equation}
A(\omega) = \frac{U_0}{\sqrt{1+ \omega^2R^2C^2}}.
\end{equation}
Verläuft die Frequenz gegen Null, so nähert sich die Amplitude $A$ der Einspeisespannung $U_0$ an. Für hohe Frequenzen verschwindet die Amplitude. Für $A(1/RC)$ gilt $U_0/\sqrt{2}$. Häufig werden daher RC-Glieder als Tiefpässe verbaut, nachteilig ist jedoch die Abnahme der Amplitude gegen Null nur mit $1/\omega$.

\subsection{RC-Kreis als Integrator}
Der RC-Kreis wird nach Abb.2 verbaut. Ist die Frequenz $\omega >> 1/RC$, kann die zeitabhängige Spannung 
integriert werden, da $U_C$ proportional zur Integration $\int{U(t)}dt$ ist. Mit dem zweiten Kirchhoffschen Gesetz und der Beziehung für $I$ aus Gleichung [Gl.(2.12)] ergibt sich
\begin{equation}
U(t) = RC \frac{dU_C}{dt} + U_C(t).
\end{equation}
Aufgrund der Bedingung für die Frequenz $\omega$ gilt $|U_C| << |U_R|$ und $|U_C| << |U|$. Daher ist anschließend
\begin{equation}
U_C(t) = \frac{1}{RC} \int{U(t')dt'}.
\end{equation}

\section{Durchführung}

\subsection{Bestimmung der Zeitkonstanten durch Beobachtung des Auf- und Entladevorganges}
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild3.png}}
\caption{Messschaltung zur Bestimmung der Zeitkonstanten [1]}
\label{fig:Abbildung}
\end{figure}
Mit der obrigen Schaltung (Abb.3) wird die Spannung $U_C$ in Abhängigkeit von der Zeit mit einem Oszilloskops betrachtet. Der Rechteckgenerator sorgt für abwechselnde Auf- und Enladungen des Kondensators. Der Aufladevorgang beginnt, wenn die Ausgangsspannung von 0 auf ihren Maximalwert springt und endet,wenn sie anschließend wieder auf 0 abfällt. Solange die Rechteckspannung auf Null verharrt entlädt sich der Kondensator. Die erwartete Asymptotik ist nur im Ansatz zu registrieren. Eine geeignete Darstellung der Entladekurve ist in Abb. 4 dargestellt. Sobald eine geeigente Kurve zu beobachten ist wird ein Thermodruck angefertigt.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild4.png}}
\caption{Geeignete Darstellung der Entladekurve [1]}
\label{fig:Abbildung}
\end{figure}

\subsection{Messung der Amplitude $A$ in Abhängigkeit von der Frequenz}
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild5.png}}
\caption{Messschaltung zur Bestimmung  der Frequenzabhängigkeit der Amplitude [1]}
\label{fig:Abbildung}
\end{figure}
Es wird die Schaltung aus Abb.5 genutzt. Mit einem geeigneten Millivoltmeter wird die Amplitude $A$ in Abhängigkeit von der Frequenz über etwa 3 Zehnerpotenzen gemessen. Der Widerstand $R$ wird über ein digitales Ohmmeter bestimmt. Zusätzlich wird die Frequenzabhängigkeit der Generatorspannungsamplitude $U_0$ überprüft.

\subsection{Bestimmung der Phasenverschiebung $\phi$ in Abhängigkeit von der Frequenz}
\begin{figure}[htbp]
\centering{\includegraphics[width=14cm]{Bilder/Bild6.png}}
\caption{Messschaltung zur Bestimmung der Phasenverschiebung [1]}
\label{fig:Abbildung}
\end{figure}
Die Phasenverschiebung $\phi$ zwischen der Kondensator- und Generatorspannung wird mit der Schaltung aus Abb.6 bestimmt. Die beiden Spannungsverläufe werden mit einem Zweistrahl-Oszilloskop wiedergegeben. Dabei ist das Schirmbild für $\phi$>0 ähnlich wie Abb.7.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild7.png}}
\caption{Darstellung der Phasenverschiebung  [1]}
\label{fig:Abbildung}
\end{figure}
Die Phasenverschiebung $\phi$ lässt sich über die Beziehung
\begin{equation}
\phi = \frac{a}{b} \cdot 2 \pi
\end{equation}
berechnen. Dabei bezeichnet $a$ den zeitlichen Abstand der beiden Nulldurchgänge der Schwingungen und $b$ die Schwingungsdauer.

\subsection{Integration mit dem RC-Kreis}
Verwendet wird die Schaltung von Abb.6. Das RC-Glied wird der Reihe nach mit einer Rechteck-, Sinus- und Dreiecksspannung gespeist. Auf dem Oszilloskop wird sowohl die integrierte als auch die zu integrierende Spannung dargestellt und Thermodrucke angefertigt.

\section{Auswertung}
\subsection{Entladung des RC-Gliedes}
Um die Zeitkonstante des RC-Gliedes zu bestimmen, wurde ein Entladevorgang untersucht. Die gemessenen Werte sind in Abbildung 8 halblogarithmisch dargestellt zusammen mit einer Ausgleichsfunktion die nach Formel 2.6 bestimmt wurde. Zur Berechnung wurden, in der weiteren Auswertung ebenfalls, Gnuplot verwendet sowie, damit die Werte möglichst genau sind, nur die Werte von $t_1=-0,42ms$ bis $t_2=0,166ms$.
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild11.png}}
\caption{Entladevorgang des RC-Gliedes}
\label{fig:Abbildung}
\end{figure}
Die Gerade in der Abbildung folgt der Formel
\begin{equation}
U_c(t)=(102\pm0,05)\,\, exp((-1263\pm 2)t)\, \mathrm{V}
\notag
\end{equation}
Daraus ergibt sich die Zeitkonstante des Gliedes als
\begin{equation}
RC=(0,792\pm 0,001)\mathrm{ms}
\notag
\end{equation}
\subsection{Amplitude der Kondensatorspannung}
Die gemessenen Amplituden sind mit den jeweils zugehörigen Frequenzen in Tabelle 1 aufgeführt. Aus diesen Werten wird mittels einer nichtlinearen Ausgleichsrechnung die Zeitkonstante nach Gleichung 2.18 bestimmt:
\begin{eqnarray}
2\pi\cdot RC&=&(4,81\pm0.016)\mathrm{ms}\\
\notag
RC&=&(0,766\pm0,003)\mathrm{ms}
\notag
\end{eqnarray}
Dabei ist der Wert $U_0=80,89 \mathrm{V}$.
\begin{table}
\centering\begin{tabular}{ccc||ccc}
$f$\,[Hz]&$U_C$\,[V]&$U_C/U_0$&$f$\,[Hz]&$U_C$\,[V]&$U_C/U_0$\\
\hline\hline
50&	78,40&	0,97	&1300&	12,80&0,16		\\
70	&76,80&	0,95	&1500&	11,20&0,14		\\
90	&74,40&	0,92	&2000&	8,60&0,11		\\
110	&71,20&	0,88		&2500&	6,60&0,08	\\
130	&68,80&	0,85		&3000&	5,50&0,07	\\
150	&65,60&	0,81		&4000&	4,20&0,05	\\
200	&58,40&	0,72		&5000&	3,40&0,04	\\
250	&52,00&	0,64		&10000&	1,70&0,02	\\
300	&46,40&	0,57		&15000	&1,10&0,01	\\
350	&40,80&	0,50		&20000	&0,84&0,01	\\
400	&37,20&	0,45		&25000	&0,69&0,01	\\
450	&33,60&	0,42		&30000	&0,58&0,01	\\
500	&30,80&	0,38		&35000	&0,50&0,01	\\
700	&23,20&	0,29		&40000	&0,43&0,01	\\
900	&18,40&	0,23		&45000	&0,39&0,00	\\
1100	&15,20&	0,19	&50000&	0,36&0,00	\\
\end{tabular}
\caption{Frequenzen und dazugehörige Amplituden}
\end{table}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild22.png}}
\caption{Spannungsamplitude in Abhängigkeit der Frequenz}
\label{fig:Abbildung}
\end{figure}
\newpage

\subsection{Phasenverschiebung am RC-Glied}
Die Gemessene Phasenverschiebungen sind in Tabelle 2 gelistet. Dabei berechnen sich die Werte für die Phasenverschiebung nach der Formel 
\begin{equation}
\phi=\frac{a}{b}\cdot2\pi
\notag
\end{equation}
\begin{table}
\centering\begin{tabular}{c|c|c|c}
$f$[1/s]&$\Delta t$[ms]&$\Delta \phi$ [rad]&$A/U_0$ \\
\hline\hline
50&	0,7800&	0,245&	0,970\\
70&	0,7400&	0,325&	0,948\\
90&	0,7400&	0,418&	0,914\\
110&	0,7200&	0,498&	0,879\\
130&	0,6800&	0,555&	0,850\\
150&	0,6700&	0,631&	0,807\\
200&	0,6100&	0,767&	0,720\\
250&	0,5600&	0,880&	0,637\\
300&	0,5100&	0,961&	0,572\\
350&	0,4700&	1,034&	0,512\\
400&	0,4300&	1,081&	0,471\\
450&	0,4000&	1,131&	0,426\\
500&	0,3720&	1,169&	0,391\\
700&	0,2920&	1,284&	0,283\\
900&	0,2360&	1,335&	0,234\\
1100&	0,2000&	1,382&	0,187\\
1300&	0,1700&	1,389&	0,181\\
1500&	0,1500&	1,414&	0,156\\
2000&	0,1160&	1,458&	0,113\\
2500&	0,0930&	1,461&	0,110\\
3000&	0,0780&	1,470&	0,100\\
4000&	0,0590&	1,483&	0,088\\
5000&	0,0480&	1,508&	0,063\\
10000&	0,0232&	1,458&	0,113\\
15000&	0,0158&	1,489&	0,082\\
20000&	0,0118&	1,483&	0,088\\
\end{tabular}
\caption{Gemessene Werte der Phasenverschiebung}
\end{table}
Durch den Zusammenhang 2.15 wurde mit Gnuplot eine Ausgleichsfuntkion erstellt die zusammen mit den Werten in Abb. 10 dargestellt sind.
Aus der Funktion folgt für RC der Zusammengang
\begin{eqnarray}
\notag 
2\pi\cdot RC&=&(4,77\pm0,095)\mathrm{ms}\\
\notag
RC&=&(0,759\pm0,015)\mathrm{ms}
\end{eqnarray}
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild33.png}}
\caption{Phasenverschiebung der Spannung in Abhängigkeit von der Frequenz}
\label{fig:Abbildung}
\end{figure}
Mithilfe dieses ermittelten RC-Wertes ergibt sich nach Gleichung 2.18 die Formel für die Amplitudenabhängigkeit $A/U_0$ von der Phasenverschiebung $\phi$
\begin{equation}
\frac{A(\omega)}{U_0}=\frac{1}{\sqrt{1+\omega^2R^2C^2}}=\frac{1}{\sqrt{1+tan^2(\phi)}}=cos(\phi)
\notag
\end{equation}
Mit $U_0=80,89\mathrm{V}$ folgt das in Abb. 11 dargestellte Schaubild, in dem zusätzlich die gemessenen Paare von $A/U_0$ und $\phi$ aufgetragen sind.
\begin{figure}[htbp]
\centering{\includegraphics[width=\textwidth]{Bilder/Bild44.png}}
\caption{Amplitudenverhältnis $A/U_0$ in Abhängigkeit der Phasenverschiebung}
\label{fig:Abbildung}
\end{figure}

\newpage
\subsection{Das RC-Glied als Integrator}
Wie im Theorieteil hergeleitet, kann das RC-Glied als Integrator fungieren, falls die Frequenz ausreichend hoch ist.
Im praktischen Versuch wurde wie die Abbildungen 12 bis 14 zeigen beobachtet, dass eine Rechteckspannung zu einer Dreieckspannung, eine Dreieckspannung zu einer Spannung, die sich durch 2 Parabeln beschreiben lässt und eine Sinusspannung zu einer Kosinusspannung integriert wurde. 
\begin{figure}[htbp]
\centering{\includegraphics[height=6cm]{Bilder/Bild66.jpg}}
\caption{Integration der Rechteckspannung}
\label{fig:Abbildung}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[height=6cm]{Bilder/Bild55.jpg}}
\caption{Integration der Dreieckspannung}
\label{fig:Abbildung}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[height=6cm]{Bilder/Bild77.jpg}}
\caption{Integration der Sinusspannung}
\label{fig:Abbildung}
\end{figure}
\newpage

\section{Diskussion}
Die für $RC$ bestimmten Messwerte liegen mit $RC=(0,792\pm 0,001)\,\mathrm{ms}$, $RC=(0,766\pm0.003)\,\mathrm{ms}$ bzw. $RC=(0,759\pm0,015)\mathrm\,{ms}$ im Rahmen der Messgenauigkeit in der selben Größenordnung. Allerdings ist der über den Entladevorgang bestimmte Wert etwas höher, weshalb hierbei auf einen systematischen Fehler geschlossen werden kann. Dieser kommt dadurch zustande dass der Innenwiderstand des Generators nicht berücksichtigt wurde, wodurch der tatsächliche Wert etwas kleiner wäre, also näher an den anderen Messungen.
\\
Die in Abb. 11 eingezeichneten Messwerte sind der eingezeichneten Theoriekurve sehr nahe, womit sich der auf die anderen Methoden bestimmte Wert bestätigen lässt.\\
Bei der Integration der Spannungen ließ sich die Dreieckspannung und die Sinusspannung nicht wie erwünscht vom Sinusgenerator erzeugen, wodurch die Bilder etwas ungewöhnlich erscheinen. Nichtsdestotrotz lassen sich die erwarteten Effekte der Integration erkennen.

\section{Literaturverzeichnis}
[1] Physikalisches Anfängerpraktikum, Skript zum Versuch Nummer 353\\
http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V353.pdf

\end{document}
\end{document}


