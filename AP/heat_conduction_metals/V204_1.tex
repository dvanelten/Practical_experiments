\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\usepackage{scrpage2} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 204}  \\ Protokoll vom \today}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 204 - Wärmeleitung von Metallen}
\author{Konstantin Pfrang \and Dennis van Elten}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Ziel}
Untersuchung der Wärmeleitung von Aluminium, Messing und Edelstahl

\section{Theoretische Grundlagen}
Aufgrund eines Temperaturungleichgewichts kommt es zu der Übertragung von thermischer Energie, die als Wärmeübertragung bezeichnet werden. Dieser Transport erfolgt in Richtung des Temperaturgefälles. Dabei kann der Transport über 3 verschiedene Arten passieren:
\newline
\newline
Thermische Konvektion ist mit dem Transport von Teilchen verknüpft, welche thermische Energie führen. In Nichtfeststoffen ist aufgrund der hohen Beweglichkeit der Teilchen Konvektion unvermeidbar.
\newline
\newline
Eine zweite Möglichkeit ist die Wärmestrahlung. Jeder Körper sendet temperaturabhängige, elektromagnetische Strahlung aus. Die Intensität der Strahlung hängt neben der Temperatur von der Wellenlänge und der Material- und Oberflächenbeschaffenheit ab.
\newline
\newline
Desweiteren ist die Wärmeübertragung mittels der Wärmeleitung, auf die in diesem Versuch eingegangen wird, eine weitere Option. Dieser Vorgang findet im atomaren bzw. molekularen Bereich statt und erfolgt über frei bewegliche Elektronen und Phononen. Der Gitterbeitrag wird im Folgenden vernachlässigt.
\newline
\newline
Liegt ein Metallstab mit einer Länge $L$, der Dichte $\rho$ und der spezifischen Wärme $c$ vor, so lässt sich seine Wärmemenge $dQ$, die in der Zeit $dt$ fließt,  mit der Gleichung
\begin{align}
dQ = - \kappa A \frac{\delta T}{\delta x} dt
\end{align}
bestimmen. Dabei gibt $\kappa$ die materialabhängige Wärmeleitfähigkeit und $A$ die Querschnittsfläche wieder. Der Wärmestrom fließt in Richtung des Temperaturgefälles.
Mithilfe der Wärmestromdichte $j_w$ 
\begin{align}
j_w = - \kappa \frac{\delta T}{\delta x}
\end{align}
und der Kontinuitätsgleichung leitet man die eindimensionale Wärmeleitungsgleichung
\begin{align}
\frac{\delta T}{\delta t} = \frac{\kappa}{\rho c} \frac{\delta^2 T}{\delta x^2}
\end{align}
ab. Diese beschreibt sowohl die räumliche als auch die zeitliche Temperaturverteilung. Der Faktor $\frac{\kappa}{\rho c}$ beschreibt die Geschwindigkeit des Temperaturausgleichs bei einer Temperaturdifferenz und wird als Temperaturleitfähifkeit definiert.
\newline
\newline
Durch eine periodische Erwärmung bzw. Abkühlung eines langen Stabes ergibt sich mit einer Periode T die Temperaturwelle
\begin{align}
T(x,t) = T_{max} e^{-{\sqrt{\frac{w \rho c}{2 \kappa}}x}} cos (wt - \sqrt{\frac{w \rho c}{2 \kappa}}x)
\end{align}
mit der Phasengeschwindigkeit $v$
\begin{align}
v = \frac{w}{k} = \frac{w}{\sqrt{\frac{w \rho c}{2 \kappa}}} = \sqrt{\frac{2 \kappa w}{\rho c}}.
\end{align}
Desweiteren herrscht eine Dämpfung vor. Diese lässt sich aus dem Verhältnis der Amplituden $A_{nah}$ und $A_{fern}$ an den zwei Stellen $x_{nah}$ und $x_{fern}$ ermitteln.
Um die Wärmeleitfähigkeit $\kappa$ zu bestimmen, nutzt man die Kreisfrequenz $w = \frac{2 \pi}{T}$ sowie die Phasenbeziehung $\phi = 2 \pi \frac{\Delta t}{T}$. Mit $\Delta x$, dem Abstand der Messpunkte, und $\Delta t$, der Phasendifferenz der Welle, ergibt sich:
\begin{align}
\kappa = \frac{\rho c (\Delta x)^2}{2 \Delta t \cdot ln (\frac{A_{nah}}{A_{fern}})}.
\end{align}


\section{Durchführung}
\subsection{Versuchsaufbau}
Auf einer Grundplatte sind vier rechteckige Probestäbe. Darunter befindet sich ein Aluminium-, ein Edelstahl- sowie zwei unterschiedlich breite Messingstäbe.


\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Messungen, Dichten und spez. Wärmen der Metalle}
\label{fig:Abbildung}
\end{figure}


Mithilfe eines Peltier-Elements (TEC) können diese gleichlaufend erwärmt bzw. gekühlt werden. Bei Stromfluss erzeugt dieses eine Temperaturdifferenz bzw umgekehrt. Es werden zwei Drähte aus verschiedenen Materialien (genauer: Energieniveaus) benötigt, die verbunden werden. Anschließend wird eine Spannung angelegt. Der entstehende Strom transportiert die Wärme, so dass die beiden Verbindungsstellen unterschiedliche Temperaturen besitzen.
Die Temperaturen werden an jedem Stab an zwei Stellen mit einem Thermoelement abgemessen. Es werden immer alle 8 Thermoelemente simultan mithilfe des "Xplorer GMX" gemessen und graphisch umgesetzt. Nach jeder Messung werden die Isolierungen entfernt und die Stäbe gekühlt.

\subsection{Statische Methode}
Bei einer Betriebsspannung von $U_p$ = 5V wird die Temperatur als Funktion der Zeit gemessen. Die Wärmeleitfähigkeit der Metalle wird dabei über den zeitlichen Temperaturverlauf der beiden Messstellen bestimmt. Mit einer Abtastrate $\Delta t_{GLX}$ = 5s wird solange gemessen, bis das Thermoelement T7 eine Temperatur von 45 Grad aufweist. Nach dem Abkühlen werden in zwei Graphiken T1 und T4 bzw. T5 und T8 aufgetragen und verglichen. Die Wärmeleitung der einzelnen Stäbe wird über die Temperaturen an den Thermoelementen T1, T4, T5 und T8 nach einer Zeit von T = 700s ermittelt.
Ebenfalls wird der Wärmestrom $\frac{\Delta Q}{\Delta t}$ für fünf Messzeiten berechnet und in zwei Graphiken die Temperaturdifferenz $\Delta T_{St}=T7-T8$ sowie $\Delta T_{St}=T2-T1$ aufgetragen und verglichen.

\subsection{Dynamische Methode}
Bei der Angström-Methode wird der Probenstab periodisch erhitzt. Aus der Ausbreitungsgeschwindigkeit $v$  der Temperaturwelle kann die Wärmeleitfähigkeit $\kappa$ ermittelt werden. Diese Methode wird mit einer Betriebsspannung von $U_p$ = 8V ausgeführt. Nachdem die Abtastrate auf $t_{GLX}$ = 2s und die Probestäbe eine Temperatur von unter 30 Grad abgekühlt sind, werden die Probestäbe mit mindestens 10 Periode von jeweils T = 80s geheizt. Nach dem Kühlen werden die Temperaturverläufe für T1 und T2  (Messing) aufgetragen und daraus die Temperaturwelle, die Amplituden und die Phasendifferenz ermittelt. Aus den Daten kann im Weiteren die Wärmeleitfähigkeit $\kappa$ ermittelt werden. Für Aluminium erfolgt die analoge Durchführung.
Für Edelstahl wird die Messung mit einer Periode T = 200s durchgeführt, bis auf einem Thermoelement 80 Grad erreicht sind. Nach dem Abkühlen erfolgt die vorherige Vorgehensweise zur Ermittlung der Wärmeleitfähigkeit $\kappa$.

\section{Auswertung}
\subsection{Statische Methode}

Die aus der Statischen Methode gewonnenen Messwerte für die beiden Messingstäbe, Aluminium und Edelstahl sind in den Abbildungen 2 und 4 sowie in Abbildungen 3 und 5 graphisch dargestellt. Sie wurden wie alle weiteren Diagramme auch mit dem XplorerGLX erstellt.
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=95mm]{Bilder/Bild1.jpg}}
\caption{Messwerte für Messing Breit und Dünn}
\label{fig:Abb1}
\end{figure}
In diesen Diagrammen kann man bei allen Messkurven einen ähnlichen Verlauf erkennen: Zunächst bleibt die Temperatur an den Thermoelementen annähernd konstant und steigt anschließend deutlich an, bis diese einen Wendepunkt erreicht, an dem die Steigung wieder abnimmt und sich die Temperatur an einen Grenzwert annähert. Diese Form lässt sich damit erklären, dass zu Beginn die Temperaturdifferenz zwischen dem Peltier-Element und den Stäben sehr groß ist, da sich die Stäbe langsamer erwärmen. Dadurch wird der Temperaturanstieg bis zu dem Wendepunkt größer, danach ist die Temperaturdifferenz geringer und der Anstieg wird ebenfalls geringer. Letztlich wird eine Sättigungspunkt in der Temperatur erreicht, so dass sich auf lange Zeit die Temperatur an einen Grenzwert anschmiegt.
\\
Werden nun die beiden Kurven für die Messingstäbe betrachtet, lässt sich gut der Einfluss des Querschnittes auf den Verlauf der Temperatur erkennen. So steigt die Temperatur des dünneren Messingstabes zunächst schneller an, jedoch wird der Temperaturanstieg auch früher wieder geringer. Diese Beobachtung lässt sich dadurch erklären, dass am Peltier-Element beide Stäbe die gleiche Wärmemenge $Q$ erhalten. Da allerdings die Querschnittfläche $A$ des dünneren Stabes kleiner ist, sonst jedoch alle Materialkonstanten identisch sind, erkennt man aus Gleichung 1, dass die Wärme schneller durch den Stab geleitet werten muss. Allerdings kann der Dünnere der beiden Stäbe auf lange Zeit weniger Wärme aufnehmen, da die aufnehmbare Energie proportional zu der Masse ist, die bei dem dünneren Messingstab logischer weiße geringer ist.
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=95mm]{Bilder/Bild2.jpg}}
\caption{Messwerte für Aluminium und Edelstahl}
\label{fig:Abb1}
\end{figure}

Die Grenztemperatur, gegen die die Messwerten nach langer Zeit streben, hängt neben dem Querschnitt außerdem auch von der Wärmeleitfähigkeit ab. Weil der dickere Messingstab, der Aluminiumstab und der Edelstahlstab alle die selben Maße besitzen, kann aufgrund der in Tab. 1 gelisteten Endtemperaturen nach ca. 700\,s sofort eine Aussage über die Wärmeleitfähigkeit gemacht werde.\\Es kann über das Aluminium die Aussage getroffen werden, dass dieses die größte Wärmeleitfähigkeit besitzt, da dieses die höchste Endtemperatur erreicht. Edelstahl hat demnach mit der geringsten Endtemperatur die mit Abstand kleinste Wärmeleitfähigkeit. In der Mitte gelegen ist die Wärmeleitfähigkeit des Messings.
\begin{table}[htbp]
\centering\begin{tabular}{c||cccc}
Zeit [s]&Messing breit & Messing dünn & Aluminium & Edelstahl\\
\hline\hline
700&43,07 & 42,90 & 44,88 & 35,6\\
\end{tabular}
\caption{Temperatur nach 700\,s}
\end{table}
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=95mm]{Bilder/Bild1.jpg}}
\caption{Messwerte für Messing Breit und Dünn}
\label{fig:Abb1}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=95mm]{Bilder/Bild2.jpg}}
\caption{Messwerte für Aluminium und Edelstahl}
\label{fig:Abb1}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=95mm]{Bilder/Bild3.jpg}}
\caption{Temperaturdifferenz von Thermoelement 2 zu Thermoelement 1 (Messing breit)}
\label{fig:Abb1}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=95mm]{Bilder/Bild4.jpg}}
\caption{Temperaturdifferenz von Thermoelement 7 zu Thermoelement 8 (Edelstahl breit)}
\label{fig:Abb1}
\end{figure}



\newpage
In den Abbildungen 6 und 7 sind die Temperaturdifferenzen der näheren und der ferneren Thermoelemente für Messing und Edelstahl aufgetragen.
Da die Wärme sich nicht sofort gleichmäßig in dem Stab verteilt, sondern durch den Stab wandert, erhitzt sich der Stab am näheren Thermoelement schneller als am fernen Thermoelement, wodurch zunächst ein starker Anstieg in den der Differenz zustande kommt. Nachdem die nähere Temperatur bereits den Wendepunkt überschritten hat, steigt die fernere Temperatur weiter stark an. Somit sinkt die Temperaturdifferenz nach erreichen eines Hochpunktes wieder ab bis zum erreichen eines Grenzwertes.\\
Aus den in Abb. 6  und Abb. 7 dargestellten Temperaturdifferenzen, lassen sich die in Tab. 2 gelisteten Messwerte ablesen, die für die Berechnung des Wärmestromes durch den breiten Messingstab sowie den Edelstahlstab benötigt werden.
Außerdem werden dazu die Physikalischen Größen aus Tab. 3 benötigt. Mithilfe von Gleichung 1 wird mit diesen Angaben der Wärmestrom $\frac{\Delta Q}{\Delta t}$ berechnet und die Ergebnisse für verschiedene Zeiten in Tab. 4 dargestellt.
\begin{table}[htbp]
\centering\begin{tabular}{c||c|c}
Zeit [s]&T2-T1 [\symbol{23}C]&T7-T8 [\symbol{23}C]\\
\hline \hline
50	& 8,06	& 8,10 \\
100	& 10,45	& 10,42\\
200	& 10,12	& 10,15\\
300	& 9,00	& 8,98\\
400	& 8,27	& 8,31\\
500	& 7,68	& 7,63\\
600	& 7,27	& 7,22\\
700	& 6,95	& 6,95\\
\end{tabular}
\caption{Messwerte der Temperaturdifferenz}
\label{fig:Tab1}
\end{table}

\begin{table}[htbp]
\centering\begin{tabular}{c||c|c|c|c}
&Messing (breit)&Messing(schmal)&Aluminium&Edelstahl\\
\hline\hline
$\kappa [\mathrm{\frac{J}{kg\cdot K}}]$ &115&115&150&15	\\
$A \mathrm{[m^2]}$ &$4,8 \cdot 10^{-5}$&$2,8 \cdot 10^{-5}$&$4,8 \cdot 10^{-5}$&$4,8 \cdot 10^{-5}$							\\
$\delta x \mathrm{[cm]}$&3,01&3,01&3,01&3,01
\end{tabular}
\caption{Physikalischen Größen der Probestäbe}
\label{fig:Tab1}
\end{table}

\begin{table}[htbp]
\centering\begin{tabular}{c||c|c|}
&\multicolumn{2}{c}{$\frac{\Delta Q}{\Delta T} \mathrm{[W]$}}\\ 
\hline
Zeit[s]&Messing (breit)& Edelstahl\\
\hline\hline
50	&-1,48&	-0,19\\
100	&-1,92&	-0,25\\
200	&-1,86&	-0,24\\
300	&-1,65&	-0,22\\
400	&-1,52&	-0,20\\
500	&-1,41&	-0,18\\
600	&-1,33&	-0,17\\
700	&-1,27&	-0,17\\
\end{tabular}
\caption{Errechnete Werte für den Wärmestrom $\frac{\Delta Q}{\Delta t}$}
\label{fig:Tab1}
\end{table}

\newpage
\subsection{Dynamische Methode}
Die durch die Angström-Methode erhaltenen Temperaturkurven für den breiten Messingstab, den Aluminiumstab und den Edelstahlstab sind in den Abb. 8 bis 10 dargestellt. Die aus den Abbildungen abgelesenen Messwerte sind in Tab. 5 gelistet. $\Delta t$ ist dabei die Phasen Differenz der beiden Thermoelemente pro Stab, die an den Minima der Temperaturen pro Periode gemessen wurde. $A_{nah}$ und $A_{fern}$ sind die doppelten Temperaturamplituden und wurden jeweils erst bestimmt, nachdem die durchschnittliche Temperatur halbwegs konstant blieb, damit die Messwerte nicht verfälscht werden.


\vspace*{5mm}
\hspace*{-2cm} 
\begin{sidewaystable }
\centering\begin{tabular}{c||ccc||ccc||ccc|}
&\multicolumn{3}{c}{Messing}&\multicolumn{3}{c}{Aluminium}&\multicolumn{3}{c}{Edelstahl}\\
\hline
&$\Delta t\mathrm{[s]}$ &	$A_{nah}\mathrm{[\symbol{23}C]}$&	$A_{fern}\mathrm{[\symbol{23}C]}$&	$\Delta t\mathrm{[s]}$&	$A_{nah}\mathrm{[\symbol{23}C]}$&	$A_{fern}\mathrm{[\symbol{23}C]}$&	$\Delta t\mathrm{[s]}$&	$A_{nah}\mathrm{[\symbol{23}C]}$&	$A_{fern}\mathrm{[\symbol{23}C]}$\\
\hline \hline
&25,26&			5,84&		1,38&		5,79&		7,10&		2,96&		48,51&		&		\\
&27,89&			6,12&		1,74&		8,42&		6,90&		3,32&		43,89&		&		\\
&27,37&			6,10&		1,66&		6,32&		6,80&		3,60&		43,89
&		&		\\
Mess-&26,32&			5,98&		1,68&		6,84&		7,00&		3,44&		48,51&		&		\\
werte&28,95&			6,28&		1,84&		7,89&		7,70&		3,36&		48,51&		11,18&	1,08\\
&27,89&			6,08&		1,86&		5,26&		7,36&		3,30&		46,20&		11,42&	1,15\\
&26,84&			5,92&		1,74&		5,79&		7,78&		3,18&		47,36&		11,32&	1,32\\
&30,53&			6,32&		1,86&		6,32&		7,42&		3,50&		48,51&		11,35&	1,25\\
&27,37&			5,94&		1,78&		5,79&		7,44&		3,34&		46,20&		11,25&	1,37\\
\hline
\\
\hline							
Mittel&27,60&			6,07&		1,73&		6,49&		7,28&		3,33&		46,84&		11,30&	1,24\\
$\sigma_m$&0,51&			0,05&		0,05&		0,35&		0,12&		0,06&		0,64&		0,04&	0,05\\
\hline
\end{tabular}
\caption{Für die Berechnung der Wärmeleitfähigkeit benötigten Messwerte und deren Mittelwerte}
\label{fig:Tab1}
\end{sidewaystable }
Tab. 5: Für die Berechnung der Wärmeleitfähigkeit benötigten Messwerte und deren\\	\hspace*{13mm}	Mittelwerte}

\vspace*{5mm}
Die Frequenz der Temperaturschwingung berechnet sich durch die Formel $f=\frac{1}{T}$. Für die Wellenlänge gilt $\lambda=\frac{c}{f}=\frac{\Delta x}{\Delta t} \cdot T$ mit $\Delta x$ als Entfernung der beiden Thermoelemente.\\
Damit ergeben sich für diese beiden Größen die in Tab. 6 dargestellte Werte.

\setcounter{table}{5} 
\begin{table}[htbp]
\centering\begin{tabular}{c||ccc}
&Messing (breit) & Aluminium & Edelstahl\\
\hline\hline
$f\mathrm{\frac{1}{s}]}$&0,013&0,013&0,005\\
$\lambda\mathrm{[cm]}$&8,72\pm 0,16&37,10\pm 2,01&12,85\pm 0,18\\
\end{tabular}
\caption{Errechnete Werte für die Frequenz $f$ und die Wellenlänge $\lambda$}
\label{fig:Tab1}
\end{table}



Die Berechnung der Wärmeleitfähigkeit $\kappa$ erfolgt mit Gleichung 6. Alle hierzu benötigten Messwerte finden sich in Tab. 5, die benötigten Literaturangaben sind in Abb. 1 gelistet.\\
Damit ergibt sich für die Wärmeleitfähigkeit für Messing, Aluminium und Edelstahl die in Tab. 8 zusammengefassten Werte.



\begin{table}
\centering\begin{tabular}{c||ccc}
&Messing&Aluminium	&Edelstahl\\
\hline \hline
$\kappa \mathrm{[\frac{W}{m\cdot K}]}$&42,91\pm 1,31 &207,70\pm 5,07&13,98\pm 0,60\\
\end{tabular}
\caption{Errechnet Werte für die Wärmeleitfähigkeit $\kappa$}
\label{fig:Abb1}
\end{table}




\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=90mm]{Bilder/Bild5.jpg}}
\caption{Temperaturkurven für den Breiten Messingstab (hell - näheres Thermoelement, dunkel - entfernteres Thermoelement)}
\label{fig:Abb1}
\end{figure}
\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=90mm]{Bilder/Bild6.jpg}}
\caption{Temperaturkurven für den Aluminiumstab (hell - näheres Thermoelement, dunkel - entfernteres Thermoelement)}
\label{fig:Abb1}
\end{figure}\begin{figure}[htbp]
\centering{\includegraphics[width=1\textwidth,height=90mm]{Bilder/Bild7.jpg}}
\caption{Temperaturkurven für den Edelstahlstab (dunkel - näheres Thermoelement, hell - entfernteres Thermoelement)}
\label{fig:Abb1}
\end{figure}

\newpage
\section{Diskussion}
Die Literaturwerte für die Wärmeleitfähigkeit des Messings, Aluminiums und Edelstahls sind bereits in Tab. 3 gelistet. Durch Vergleich der gemessenen Werte mit diesen Literaturwerten ergibt sich für Edelstahl eine geringe Abweichung von $6,9\%$. Bei den Messungen am Aluminium liegt mit einem Fehler von $38,47\%$ ein starker Unterschiede zum Literaturwert vor. Allerdings schwanken die Literaturwerte für das Aluminium recht stark, so dass auch von einem Literaturwert von $200\mathrm{\frac{W}{m\cdot K}}$ [2] ausgegangen werden kann, wobei dann nur noch eine Abweichung von $3,85\%$ vorliegt.\\
Ähnliches gilt für Messing, dessen Wärmeleitfähigkeit mit einer Abweichung von $62,69\%$ am stärksten vom Literaturwert variiert. Je nach Legierung sind jedoch die Literaturwerte entsprechend unterschiedlich, und verglichen mit einem Literaturwert von $81\mathrm{\frac{W}{m\cdot K}}$ [2] weicht der gemessene Wert um $47,02\%$ ab.
Diese Recht hohe Abweichung vom Literaturwert lässt sich damit erklären, dass während des gesamten Versuches ein Energieaustausch mit de Umwelt nicht vollkommen zu verhindern ist.


\section{Literaturverzeichnis}
[1] Physikalisches Anfängerpraktikum, Skript zum Versuch Nummer 204\\
[2] http://www.schweizer-fn.de/stoff/wleit_metall/wleit_metall.php

\end{document}
\end{document}


