\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{float}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 605}  \\ Protokoll vom 1. Juli 2014}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 605 - Die Spektren der Alkali-Atome}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{1. Juli 2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage

\section{Einleitung}
Aus der von angeregten Atomen emittierte Strahlung lassen sich Informationen über den Schalenaufbau ihrer Elektronenhüllen gewinnen. Während zur Untersuchung der inneren Elektronenhülle hauptsächlich das emittierte Röntgen-Spektrum verwendet wird, wird mit dem sichtbaren oder ultravioletten Spektrum wie hier im Versuch beabsichtigt der äußere Hüllenaufbau untersucht. Im wesentlichen werden die Energien der angeregten Atomzustände bestimmt, da wegen des Pauli-Verbots die äußeren Elektronen nur in höhere, meist unbesetze Zustände übergehen können. Aus diesen Energien wird weiter die Stärke des Kernfelds am Ort des äußeren Elektrons bestimmt. Es ist anzunehmen, dass das Coulomb-Feld des Kernes durch die inneren Elektronen nach außen abgeschirmt wird. Deshalb wird die sogenannte Abschirmungszahl $\sigma$ eingeführt, die die Stärke dieses Effektes auszudrücken.
\section{Theoretische Grundlagen}
Im allgemeinen ist es eine sehr aufwendige Rechnung, die zu den Emissionslinien die entsprechenden Energien zu berechnen. Da bei dem Versuch jedoch Alkali-Atome verwendet werden, so kann diese Rechnung durch die Ein-Elektron-Näherung deutlich vereinfacht werden. Alkali-Atome besitzen über einer oder mehreren abgeschlossenen Elektronenschalen noch ein äußeres Elektron - das Leuchtelektron, welches das einzige ist, das für die Anregungszustände in Frage kommt. Unter dieser Näherung ist die Energie nur abhängig von der Hauptquantenzahl $n$. Allerdings ist diese nicht in der Lage, alle experimentelle Werte zu erklären. Hierfür wird zusätzlich die Bahndrehimpulsquantenzahl $l$ sowie der Spin $s$ als Eigendrehimpuls benötigt. Die Wechselwirkung zwischen diesen beiden Drehimpulsen wird als Spin-Bahn-Kopplung bezeichnet. Als gesamter Drehimpuls wird die Größe $j=l+s$ aufgefasst.\\
Die hier nun ausgelassene längere Rechnung führt auf die Sommerfeldsche Feinstrukturformel:
\begin{align}
E_{n,j}=-R_{\infty}\left[\frac{(z-\sigma)^2}{n}+\alpha^2\frac{(z-\sigma)^4}{n^3}\left(\frac{1}{(j+\frac{1}{2}}-\frac{3}{4n}\right) \right]
\end{align}
Hierbei ist die Rydberg-Energie
\begin{align}
R_\infty=\frac{e_0^4m_e}{8\epsilon_0^2h^2}\approx13,6056981\,\mathrm{eV}
\end{align}
und die Sommerfeldsche Feinstrukturkonstante
\begin{align}
\alpha=\frac{e_0^2}{2hc\epsilon_0}.
\end{align}
Für die durch die Quantenzahlen $n$, $l$ und $j$ festgelegen Energieniveaus werden Kurzbezeichnungen eingeführt: Die Bahndrehimpulsquantenzahl $l$ werden durch Großbuchstaben ersetzt ($0:=S\,$;$\,1:=P\,$;$\,2:=D\,$;$\,3:=F\,$), die Hauptquantenzahl $n$ wird vor die Buchstaben geschrieben,. Letztlich wird der Wert von $j$ in den Index hinter die Buchstaben geschrieben.
\subsection{Auswahlregeln}
Aus der Auswertung der Quantenübergänge verschiedener Energieniveaus konnten einige Regeln zur Übergangswahrscheinlichkeit zwischen diesen Niveaus festgestellt werden. So kommen Übergänge bei denen sich der Bahndrehimpuls nicht ändert ($\Delta l =0$) oder bei denen er sich um mindestens zwei ändert ($\Delta l \geq 2$) praktisch nicht vor, sind also "verboten". Daraus folgt die Auswahlregel
\begin{align}
\Delta l\pm 1.
\end{align}
Darüber hinaus ist auch ein Übergang von $\Delta j =0$ sehr unwahrscheinlich. In Abbildung 1 sind Beispielhaft einige dieser erlaubten Übergänge dargestellt.
\begin{figure}[H]
\centering{\includegraphics[height=5cm]{Bilder/Bild1.png}}
\caption{Einige mögliche Übergänge in Einelektronen-Spektren [1]}
\label{fig:Abbildung}
\end{figure}
Im Gegensatz zu $j$ und $l$ gibt es für die Änderung der Hauptquantenzahl $n$ keine Einschränkungen. Allerdings zeigen sich Übergänge mit großem $\Delta n$ mit deutlich geringerer Wahrscheinlichkeit.
Weil Energieniveaus mit dem gleichen $l$ aber unterscheidlichem $j$ deutlich näher beieinander liegen als die mit unterschiedlichen $l$, beobachtet man in dem Spektrum jeweils zwei sehr nahe Linien, weshalb von der sogenannten Dublett-Struktur der Alkalispektren spricht.
\subsection{Abschirmungszahlen}
Bei genauerer Betrachtung der Sommerfeldschen Feinstrukturformel (2.1), ist es notwendig, anstatt einer sogar zwei Abschirmungszahlen einzuführen. 
\begin{align}
E_{n,j}=-R_{\infty}\left[\frac{(z-\sigma_1)^2}{n}+\alpha^2\frac{(z-\sigma_2)^4}{n^3}\left(\frac{1}{(j+\frac{1}{2}}-\frac{3}{4n}\right) \right]
\end{align}
Dabei stellt die Abschirmungszahl $\sigma_1$ die Konstante der vollständigen Abschirmung dar, weil alle Rumpfelektronen an der Abschirmung beteiligt sind. Anders bei der zweite Abschirmungszahl $\sigma_2$, die stets kleiner ist als $\sigma_1$ und die Abschirmung der Elektronen beschreibt, die innerhalb der Schale des Leuchtelektrons liegen. Daher nennt man $\sigma_2$ auch die Konstante der inneren Abschirmung. Zur experimentellen Bestimmung von $\sigma_2$ ist es daher nötig, den Abstand $\Delta E_D$ zwischen zwei Linien eines Dubletts zu bestimmen. Wie in Abbildung 1 erkennbar, hat sich dazwischen nur die Quantenzahl $j$ um eins geändert, während alle anderen gleich bleiben. Daher gilt die Beziehung
\begin{align}
\Delta E_D:=E_{n,j}-E_{n,j+1}=\frac{R_\infty\alpha^2}{n^3}(z-\sigma_2)^4\frac{1}{l/l+1)}.
\end{align}
Weiterhin gilt für die Energie $E=\frac{hc}{\lambda}$ wodurch sich Gleichung (2.6) ergibt zu:
\begin{align}
\Delta E_D=hc\left(\frac{1}{\lambda}-\frac{1}{\lambda'}\right)\approx hc\frac{\Delta \lambda}{\lambda^2}
\end{align}
Somit kann die Energiedifferenz des Dubletts durch Bestimmung der Wellenlänge mit einem Spektralapperat hinreichender Auflösung messen.
\section{Durchführung}
Um die Abstände der Dublett Linien zu messen, wird ein Gitterspektralapparat (Abb.2) verwendet, dessen wesentlicher Bestandteil ein Beugungsgitter darstellt. Je größer die Gitterkonstante $g$ ist, desto schärfer sind die sich ergebenden Interferenzmuster. Die Hauptmaxima lassen sich mithilfe der Gleichung
\begin{align}
\sin\phi=\frac{k\lambda}{g}
\end{align}
bestimmen, wobei $\phi$ der Beugungswinkel und $k$ die Ordnungszahl des Maximums ist. Während des Versuches wird immer das Maximum erster Ordnung verwendet.\\
\begin{figure}[H]
\centering{\includegraphics{Bilder/Bild2.png}}
\caption{Perspektivische Darstellung des verwendeten Gerätes [1]}
\label{fig:Abbildung}
\end{figure}
Der Spektralapparat besitzt einen Spaltblende (Abb.2: 1.1), durch welches das Licht eintritt und das mit einer Linse parallelisiert wird. Durch das Kollimatorrohr (1) fällt das Licht auf das Gitter und das Beugungsbild kann durch das schwenkbare Fernrohr (3) beobachtet werden. Der Beugungswinkel kann nach einstellen der Fernrohrposition an der Skala (4) abgelesen werden. Das Licht wird sohwol nach rechts als auch nach links gebeugt daher ist es sinnvoll, wie in Abb. 3 skizziert, die Werte $\phi_l$ und $\phi_r$ zu messen und daraus durch die Formel
\begin{align}
\phi=\frac{1}{2}(\phi_l-\phi_r) 
\end{align}
den Beugungswinkel zu bestimmen.
\begin{figure}[H]
\centering{\includegraphics[height=5cm]{Bilder/Bild3.png}}
\caption{Skizze zur Bestimmung des Beugungswinkels $\phi$ [1]}
\label{fig:Abbildung}
\end{figure}
\subsection{Auflösungsvermögen des Gitterspektralapparates}
Als Auflösungsvermögen $A$ wird der Quotient aus der Differenz $\Delta \lambda$ zweier vom Gerät gerade noch trennbarer Wellenlängen und der aus diesen beiden Wellenlängen gemittelte Wert $\lambda'$ verstanden. Zwei Spektrallinien mit den Wellenlängen $\lambda$ und $\lambda+\Delta\lambda$ lassen sich gerade dann noch trennen, wenn das Hauptmaximum der einen Linie auf ein Minimum der zweiten fällt. Damit sich zwei Linien noch trennen lassen, muss die folgende Beziehung gelten:
\begin{align}
\frac{k}{g}(\lambda+\Delta\lambda)=\frac{kp+1}{p}\frac{\lambda}{g}
\end{align}
Hierbei ist p die Anzahl der Gitteröffnungen. Für das Auflösungsvermögen folgt schließlich
\begin{align}
A=\frac{\lambda}{\Delta\lambda}=kp
\end{align}
\subsection{Ausmessung von Dublett-Linien}
Die Winkelauflösung des Goniometers reicht mit ca. $0,1^\circ$ nicht aus, um zwei Linien eines Dubletts zu bestimmen. Dazu ist eine Okularmikrometerschraube an dem sich im Okular befindlichen Fadenkreuz angebracht. Mithilfe dieser Schraube ist es möglich den Abstand $\Delta s$ (s. Abb. 4) zu messen. Da die Messung von $\Delta s$ nur in Teilstrichen möglich ist, ist zusätzlich eine weitere Eichmessung notwendig.\\
\begin{figure}[H]
\centering{\includegraphics[height=4cm]{Bilder/Bild4.png}}
\caption{Skizze zur Eichung des Okularmikrometers [1]}
\label{fig:Abbildung}
\end{figure}
Hierfür wird der Abstand $\Delta t$ zweier benachbarter Spektrallinien mit bekannten Wellenlängen $\lambda_1$ und $\lambda_2$ gemessen. Es ergibt sich der Zusammenhang
\begin{align}
\Delta \lambda=\frac{\cos\overline{\phi}}{\cos\overline{\phi}_{1,2}}\frac{\Delta s}{\Delta t}(\lambda_1-\lambda_2).
\end{align}
Dabei ist $\overline{\phi}$ der Winkel unter dem das gemessene Dublett beobachtet wird und $\overline{\phi}_{1,2}$ der gemittelte Winkel zwischen den zu $\lambda_1$ und $\lambda_2$ gehörigen Beugungswinkel (s. Abb. 5).
\begin{figure}[H]
\centering{\includegraphics[height=8cm]{Bilder/Bild5.png}}
\caption{Ausmessung kleiner Winkelunterschiede $\Delta\phi$ mit dem Okularmikrometer [1]}
\label{fig:Abbildung}
\end{figure}

Dieser Versuch handelt von dem Spektren der Alkali-Atome. Um Berechnungen durchführen zu können, müssen die Energien der Emissionslinien betrachtet werden. Zur Berechnung und Herleitung wird als Näherung die sogenannte Ein-Elektronen-Näherung verwendet, da aufgrund chemischer Zusammensetzung die Alkali-Atome auf ihrer äußeren Schale lediglich ein Elektron, das sogenannte Leuchtelektron, besitzen. Zudem ist zu beachten, dass bei der Energiebestimmung ein abgeschwächtes Coulomb-Feld des Kerns vorliegt, bei welcher es sich um die Abschirmungszahl $\sigma$ handelt. 
Mit Hilfe der Schrödingergleichung lässt sich für den nicht-relativistischen Fall mit Hilfe des Hamilton- und Drehimpulsoperators, sowie diversen Umformungen eine Gleichung für die Energie herleiten, welche lediglich von der Hauptquantenzahl n und nicht von der Drehimpulsquantenzahl l abhängig ist, denn $n = k+1+l$ und da gilt $k_{min}=0$ lässt sich die Drehimpulsquantenzahl l mit $n-1$ in der Energiegleichung ersetzen. Andererseits ist es nötig, die Schrödingergleichung mit Hilfe des relativistischen Hamiltonoperators lösen. Dabei entsteht ein Zusatzterm, welcher als Störung bzw. Störungsoperator bezeichnet wird. Es ergibt sich wieder ein Term für die relativistische Energie, die jedoch sowohl von der Drehimpulsquantenzahl l als auch von der Hauptquantenzahl n abhängt. Da auch diese Gleichung noch nicht die experimentellen Spektren exakt beschreibt, ist es notwendig zusätzlich den Eigendrehimpuls bzw. Spin des Elektrons $s=\frac{1}{2}$, welcher durch das Stern-Gerlach-Experiment abgeleitet wird, mit in die Lösung eingehen zu lassen. Da zwei unterschiedliche Drehimpulse betrachtet werden und beide ein eigenes magnetisches Moment besitzen, welche miteinander wechselwirken, folgt aus der Spin-Bahn-Kopplung, dass der Gesamtdrehimpuls $J = L + S$ oder auch $J= L \pm \frac{1}{2}$, aufgrund der parallelen oder antiparallelen Stellung, ergeben muss. Wird nun der Ansatz verfolgt, dass die Schrödingergleichung unter Verwendung des Hamilton-Operators unter Einfluss der Spin-Bahn-Wechselwirkung gelöst wird, so ergibt sich ein Term für die Energie, welcher Abhängigkeiten von der Drehimpulsquantenzahl l, der Hauptquantenzahl n und des Gesamtdrehimpulses j aufweist, wobei $j=l\pm\frac{1}{2}$ ersetzt wird. Dieser ist bekannt unter der Sommerfeldschen Feinstukturformel:

\begin{equation}
E_{n,j} = -R_{ \infty} \left\{\frac{(Z-\sigma_{1})^2}{n^2}+\alpha^2\frac{(Z-\sigma_{2})}{n^3}\left(\frac{1}{j+\frac{1}{2}}-\frac{3}{4n}\right)\right\}
\end{equation}

Zu beachten sind hierbei die beiden unterschiedlichen Abschirmungszahlen $\sigma_{1}$ und $\sigma_{2}$. Das, in diesem Versuch zu bestimmende, $\sigma_{2}$ ist eine Konstante, die die innere Abschirmung beschreibt. Wohingegen sich $\sigma_{1}$ auf die vollständige Abschirmung bezieht. Um nach $\sigma_{2}$ umstellen zu können, wird Gebrauch der folgenden zwei Formeln gemacht:

\begin{equation}
\Delta E_{D}=E_{n,j}-E_{n,j+1}=\frac{R_{ \infty} \alpha^2}{n^3}(Z-\sigma_{2})^4 \frac{1}{l(l+1)}
\end{equation}


\begin{equation}
\Delta E_{D} \approx hc \frac{\Delta \lambda}{\lambda^2}
\label{Energie}
\end{equation}

Aus der Abschätzung mit l = 1 folgt die Abschirmungszahl durch Umformungen:

\begin{equation}
\sigma_{2} = Z - \sqrt[4]{2n^3\frac{\Delta E_{D}}{R_{ \infty}\alpha^2}}
\label{Abschirm}
\end{equation}

wobei $Z$ die Kernladungszahl, $n$ die Hauptquantenzah, $l$ die  Drehimpulsquantenzahl, $R_{\infty}$ die Rydbergkonstante und 
$\alpha$ die Sommerfeldsche Feinstrukturkonstante ist, mit

\begin{equation}
 \alpha = \frac{e_{0}}{2hc\epsilon_{0}}
\end{equation}

Bezüglich der Quantenzahlen lässt sich noch feststellen, dass es einige Auswahlregeln der möglichen Quantenübergänge gibt. Sobald für die Drehimpulsquantenzahl gilt $\Delta l = \pm 1$ ist ein Übergang möglich. Zu den nicht erlaubten, bzw unwahrscheinlichen Übergangen gelten somit: $\Delta l = 0$, $\Delta l \ge 2$ sowie $\Delta j = 0$ und Übergänge mit großem $\Delta n$. Bezüglich der Alkali-Atome lässt sich sagen, dass bei dessen Spektren zwei dicht aneinander liegende Linien sichtbar werden, die sogenannten Dublett-Linien. Für diese Linien gilt, dass die Drehimpulsquantenzahl gleich ist und lediglich der Gesamtdrehimpuls sich ändert.

Um letztendlich die in Gleichung (\ref{Energie}) auftretenden Wellenlängen zu bestimmen, wird der Versuch mit Hilfe eines Gitterspektralapparats, welcher ein Beugungsgitter enthält, durchgeführt. An diesem Gitter werden die einfallenden Lichtstrahlen abgelenkt und gebeugt. Durch die, dort entstehende Richtungsänderung, wird ein Winkel aufgenommen mit Hilfe dessen die Wellenlänge bestimmt werden kann. Es gilt folgender Zusammenhang:

\begin{equation}
\sin(\phi) = k \frac{\lambda}{g} \Leftrightarrow g = k \frac{\lambda}{\sin(\phi)}
\label{Gitterk}
\end{equation}

Das Auflösevermögen des Beugungsgitters lässt sich schließlich wie folgt bestimmt:

\begin{equation}
A = \frac{d}{g}
\end{equation}
 wobei d der Spaltabstand ist.


\section{Auswertung}

\subsection{Fehlerrechnung}
Um den Fehler der Messwerte zu bestimmen, wird zuerst der Mittelwert gebildet. 
\begin{equation}
\overline{x} = \frac{1}{N} \sum\limits^{N}_{i=1} x_{i}
\label{Mittelwert}
\end{equation}
Mit Hilfe folgender Formel lässt sich der Fehler des Mittelwertes berechnen:
\begin{equation}
\sigma_{\overline{x}} = \sqrt{\frac{1}{N(N-1)}\sum\limits_{i=1}^{N} (x_{i}-\overline{x})^2}
\label{SAW}
\end{equation}
Den Fehler der Größe y, der aus fehlerbelasteten Größen $x_{i}$ berechnet wird, ermittelt sich mit Hilfe der Gaußschen Fehlerfortpflanzung:
\begin{equation}
\sigma_{y} = \sqrt{\sum\limits_{i=1}^{N} \left(\frac{\delta y}{\delta x_{i}}\right)^{2} \sigma_{x_{i}^2}}
\label{GFF}
\end{equation}

\subsection{Gitterkonstante des Beugungsgitters}

Mit Hilfe der bekannten Spektrallinien des Quecksilbers wird die Gitterkonstante $g$ des verwendeten Beugungsgitters bestimmt. Die in Tabelle \ref{Tabelle1} aufgeführten Messdaten werden linear approximiert. Hierbei wird $sin(\phi)$ gegen $\lambda$ in Abbildung \ref{Abbildunga} aufgetragen. Das Hauptmaximum befindet sich bei einem Winkel von $\phi(x) = 223.7^{\circ}$

\begin{table}[H]
\begin{center}
\begin{tabular}[H]{|l|l|l|l|l||l|l|}
\multicolumn{7}{}{Tabelle1} \\ \hline
$\phi_{1}$ [$^\circ$]	&	$\phi_{2}$ [$^\circ$]	&	Farbe	&	$\lambda$ [nm]	&	Intensität	&	$\Delta \phi_{rel}$ [$^\circ$]	&	$sin(\Delta \phi_{rel})$	\\ \hline
274.4	&	345.2	&	gelb	&	579.1	&	stark	&	35.40	&	0.5793	\\
274.6	&	345.0	&	gelb	&	577.0	&	stark	&	35.20	&	0.5764	\\
276.8	&	343.0	&	grün	&	546.1	&	stark	&	33.10	&	0.5461	\\
280.5	&	339.4	&	blaugrün	&	491.6	&	schwach	&	29.45	&	0.4917	\\
284.1	&	335.7	&	violett	&	435.8	&	schwach	&	25.80	&	0.4352	\\
286.0	&	333.7	&	violett	&	434.7	&	mittel	&	23.85	&	0.4043	\\
\hline
\end{tabular}
\caption{Spektrallinien des Quecksilbers}
\label{Tabelle1}
\end{center}
\end{table}

\begin{figure}[H] 
\centering 
\includegraphics[width=350pt]{Bilder/Bild6.png}
\caption{Lineare Regression zur Ermittlung der Gitterkonstante}
\label{Abbildunga}
\end{figure}


Wir erhalten aus der linearen Regression eine Steigung der Geraden $a = (0.00111 \pm 0.00008) \frac{1}{nm}$. Mit dieser und Formel (\ref{Gitterk}) ergibt sich folgende Gitterkonstante:

\begin{equation}
g = (990.90 \pm 64.93) \mathrm{nm}
\end{equation}

Der Fehler ergibt sich mit Hilfe der Gaußschen Fehlerfortpflanzung (\ref{GFF}) mit folgender Formel:

\begin{equation}
\sigma_{g} = \sqrt{(-\frac{k}{a^2})^2 \sigma_{a}^2}
\end{equation}






\subsection{Eichung des Okkularmikrometers}

Unter Verwendung der gelben Hg-Spektrallinie wird der Zusammenhang zur Eichung des Mikrookularmeters mit folgender Formel ermittelt:

\begin{equation}
\gamma = \frac{\lambda_{1} - \lambda_{2}}{\Delta s \cdot \cos(\phi_{1,2})}
\label{Eichung}
\end{equation}

Mit der Distanz $\Delta s = 10.2$ $skal$ für die gelbe Linie folgt die gesuchte Eichgröße

\begin{equation}
\gamma = 0.2065 \mathrm{\frac{nm}{skal}}
\end{equation}

wobei $skal$ der Skaleneinheit der Einstellschraube des Okkularmikrometers entspricht.





\subsection{Dublettlinien der Alkalimetalle}

Desweiteren wird der Abstand der Dublettlinien verschiedener Alkalimetalle (Na, Ka, Rb), wie in Tabelle \ref{Tabelle2} festgehalten, ermittelt. 

\begin{table}[H]
\begin{center}
\begin{tabular}[H]{|l|l|l|l|l|l|l|l|}
\multicolumn{8}{}{Tabelle2} \\ \hline
Stoff	&	Farbe	&	$s_{1}$ [skal]	&	$s_{2}$ [skal]  &	$\Delta s$ [skal]	&	$\phi_{links} [^{\circ}]$	&	$\phi_{rechts} [^{\circ}]$	&	$\phi [^{\circ}]$	\\ \hline
Rubidium	&	rot     	&	10.1	&	-15.0	&	25.1	&	274.0	&	346.0	&	36.00	\\
Kalium	    &	gelb    	&	36.8	&	9.9		&   26.9	&	274.3	&	310.3	&	18.00	\\
Natrium	    &	rot 	    &	33.5	&	1.2		&   32.3	&	271.9	&	347.8	&	37.95	\\
	        &	gelb    	&	29.3	&	-1.0    &	30.3	&	273.8	&	345.9	&	36.05	\\
	        &	grüngelb	&	10.5	&	-15.2	&	25.7	&	275.2	&	344.5	&	34.65	\\
\hline
\end{tabular}
\caption{Dublettlinien einiger Alkalimetalle}
\label{Tabelle2}
\end{center}
\end{table}

Hierraus wird auf die Energiedifferenz $E_{D}$ mit Formel (\ref{Energie}) und somit auf die Abschirmungszahl $\sigma_{2}$ mit Formel (\ref{Abschirm}) geschlossen. Dafür wird die mittlere Wellenlänge über den Zusammenhang in Formel (\ref{Gitterk}) und dem Wellenlängenunterschied mit Formel (\ref{Eichung}) berechnet. In Tabelle \ref{Tabelle4} sind die zu verwendenen Werte angegeben. Die errechneten Werte sind Tabelle \ref{Tabelle3} zu entnehmen. 


\begin{table}[H]
\begin{center}
\begin{tabular}[H]{|l|l|l|l|l|l|l|}
\multicolumn{7}{}{Tabelle3} \\ \hline
Stoff	&	Farbe	&	$\lambda = sin(\phi) \cdot g$ [nm]	&	$\sigma_{\lambda}$ [nm]	&	$\delta \lambda = \gamma \cdot cos(\phi)$ [nm]	&	$E_{D}$ [meV]	&	$\sigma_{2}$	\\ \hline
Rubidium	&	rot	&	587.67	&	8.70	&	4.194	&	15.06	&	32.92	\\
Kalium	&	gelb	&	336.53	&	28.16	&	5.283	&	57.85	&	14.17	\\
Natrium	&	rot	&	612.17	&	6.51	&	5.260	&	17.40	&	8.12	\\
	&	gelb	&	588.31	&	8.64	&	5.059	&	18.13	&	8.09	\\
	&	grüngelb	&	570.36	&	10.21	&	4.366	&	16.64	&	8.15	\\
\hline
\end{tabular}
\caption{Dublettlinien einiger Alkalimetalle}
\label{Tabelle3}
\end{center}
\end{table}


\begin{table}[H]
\begin{center}
\begin{tabular}[H]{|l|l|l|l|}
\multicolumn{4}{}{Tabelle4} \\ \hline
Element	&	Hauptquantenzahl n	&	Nebenquantenzahl l	&	Kernladungszahl Z	\\ \hline
Kalium	&	4	&	1	&	19	\\
Natrium	&	3	&	1	&	11	\\
Rubidium	&	5	&	1	&	37	\\ 
\hline
\end{tabular}
\caption{Materialeigenschaften der zu unterschenden Elemente}
\label{Tabelle4}
\end{center}
\end{table}

Mit Hilfe des Mittelwertes (\ref{Mittelwert}) und der Standardabeichung (\ref{SAW}) werden die folgenden Zahlen ermittelt: 

\begin{equation}
\begin{aligned}
\sigma_{Ka} &= 14.17 \\
\sigma_{Na} &= (8.12 \pm 0.02) \\
\sigma_{Rb} &= 32.92
\end{aligned}
\end{equation}


\subsection{Bestimmung des Auflösungsvermögens}

Das Auflösungsvermögen des verwendeten Gitterspektralaparates soll am gelben Natrium-Dublett untersucht werden. Dazu wird der Spalt, durch den das Licht der Natrium-Dampf-Lampe fällt solange verkleinert, bis die beiden Linien des Dubletts ineinander übergehen. Mittels einer Mikrometerschraube wird die Spaltbreite d gemessen. Mit der errechneten Gitterkostente g ergibt sich das Auflösungsvermögen von

\begin{equation}
A = \frac{d}{g} = (3048 \pm 312).
\end{equation}

Für das gelbe Natrium-Dublett erhält man

\begin{equation}
\frac{\lambda}{\Delta \lambda} = 195.93.
\end{equation}

Man erkennt das das Auflösungsvermögen, das für die Auflösung der Natriumdublettlinien nötig ist, sogar bei weitem übersteigt. Die optische Apparatur ist somit absolut ausreichend für diesen Versuch.


\section{Diskussion}

Der durchgeführte Versuch dient der Bestimmung der Abschirmungszahl $\sigma_{2}$ verschiedener Elemente. Durch Bestimmung der Gitterkostante, Eichung des Okkularmikrometers und ermitteln der Dublettlinien der Alkalimetalle wird auf die Abschirmungszahl von Natrium, Kalium und Rubidium geschlossen. Hierbei finden Abweichungen in Höhe von 8.50 \% für Kalium ($\sigma_{Ka}$ = 13.06 [2]), 8.85 \% für Natrium ($\sigma_{Na}$ = 7.46 [2]) und 36 \% für Rubidium ($\sigma_{Rb}$ = 22.15 [2]) statt.

Zu den Ungenauigkeiten der Messwerte kommt es überwiegend durch eine starke Ableseungenauigkeiten bei der Winkelbestimmung, da die Unterschiede zum Teil im Millimeterbereich liegen.

\section{Literatur}
\begin{thebibliography}{---}
\bibitem[1]{Anleitung} Versuchsanleitung V605: Spektren der Alkali-Atome,

http://129.217.224.2/HOMEPAGE/MEDPHYS/BACHELOR/AP/SKRIPT/V605.pdf

vom 19.05.2014

\bibitem[2]{Werte} Bergmann/Schäfer - Experimentalphysik Band IV




\end{thebibliography}
\end{document}


