\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{float}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 501/502}  \\ Protokoll vom 10.06.14}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum SS 14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch 501/502 - Ablenkung eines Elektronenstrahls im elektrischen Feld und transversalen Magnetfeld}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{10.Juni 2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
In dem vorliegenden Experiment wird das Verhalten eines Elektronenstrahls im elektrischen und magnetischen Feld untersucht. Für die Erzeugung eines unbeeinflussten Elektronenstrahls wird eine evakuierte Kathodenstrahlröhre verwendet. Ein besonderes Augenmerk liegt dabei auf den Parametern, die die Ablenkung des Strahls beschreiben.

\section{Theoretische Grundlagen}
\subsection{Aufbau einer Kathodenstrahlröhre}

\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=13cm]{Bilder/Bild1.png}}
\caption{Querschnitt durch eine Kathodenstrahlröhre und die Beschaltung ihrer Elektronenkanone [1]}
\label{fig:Abbildung}
\end{figure}

Eine Kathodenstrahlröhre, gemäß Abb.1, besteht aus 3 entscheidenen Elementen: Elektronenkanone, Ablenk- und Nachweissystem. Ersteres ist das Hauptelement, welches die Elektronen erzeugt, beschleunigt und anschließend zu einem Strahl bündelt. Das Nachweissystem macht den durch das Ablenksystem passierten Elektronenstrahl auf einem Schirm sichtbar. Die im Wehnelt-Zylinder eingeschlossene Kathode, die über eine geringe Austrittsarbeit $W_A$ verfügt, erzeugt durch Glühemission freie Elektronen. In Strahlrichtung verfügt der Wehnelt-Zylinder über eine Bohrung und ermöglicht aufgrund seiner negativen Ladung eine Variation der Elektronenstrahlintensität. Des Weiteren folgt in der Kathodenstrahlröhre eine Elektrode mit positivem Potential $U_B$, die alle Elektronen mit hinreichend großer Energie beschleunigt. Es gilt die energetische Beziehung

\begin{equation}
\mathrm{\frac{m_0 v_z^2}{2} = e_0 U_B}.
\end{equation}

Inhomogene Felder erzeugen eine elektronische Linse hinter der Beschleunigungselektrode, welche die einfallenden Elektronen fokussiert. Die Brechkraft ist abhängig von der variablen Spannung $U_C$. Um den Elektronenstrahl, bzw. seinen Auftreffpunkt, nun sichtbar zu machen wird der Leuchtschirm benötigt. Die Elektronen regen die Aktivatorzentren zur Emission von Lichtquanten an. Um eine Aufladung des Schirms zu verhindern ist dieser mit der Beschleunigungselektrode verbunden.
Das Ablenksystem, das der Elektronenstrahl passiert, besteht aus zwei Plattenpaare, dessen Platten jeweils parallel angeordnet sind. Das zwischen den Platten entstehende elektrische Feld wirkt sich auch auf die Elektronen aus und lenkt diese ab.

\subsection{Berechnung der Ablenkung eines Elektronenstrahls im elektrischen Feld}
In der nachstehenden Abbildung (Abb.2) ist die Ablenkung des Elektronenstrahls geometrisch dargestellt.

\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=17cm]{Bilder/Bild2.png}}
\caption{Elektronenstrahl im Ablenksystem [1]}
\label{fig:Abbildung}
\end{figure}

Es wird ersichtlich, dass die Verschiebung $D$ von der angelegten Ablenkspannung $U_d$ abhängig ist. Für die elektrische Feldstärke $E$ des homogenen Feldes zwischen den Kondensatorplatten (sonst $\approx 0$) gilt für $d \ll p$

\begin{equation}
E = \frac{U_d}{d}.
\end{equation}


In der Zeit $\Delta t$, in der sich das Elektron zwischen den Platten aufhält, wirkt durch die elektrische Feldstärke auf das Elektron eine Kraft $\vec{F} = e_0 \vec{E}$. Durch diese konstante Kraft wird das Elektron in die y-Richtung abgelenkt und die Geschwindigkeitskomponente verändert sich zu
\begin{equation}
v_y = \frac{e_0}{m_0} \frac{U_d}{d} \Delta t \quad , mit \quad \Delta t = \frac{p}{v_z}.
\end{equation}

Es ist nun möglich über den Winkel $\theta$ der Richtungsänderung die Verschiebung $D$ zu bestimmen. Der Winkel berechnet sich dabei aus dem Quotient der beiden Geschwindigkeitskomponenten. Mithilfe der Beziehung aus Gl.(2.1), mit der $v_z$ eliminiert werden kann, lautet die Verschiebung

\begin{equation}
D = \frac{p}{2d} L \frac{U_d}{U_B}.
\end{equation}

Um eine hohe Ablenkempfindlichkeit zu erlangen müssen die dementsprechenden Parameter angepasst werden. 

\subsection{Berechnung der Elektronenbahn im homogenen Magnetfeld}
Ein Gegensatz zur elektrostatischen Kraftwirkung ist die Tatsache, dass magnetostatische Felder nur auf sich relativ zum Feld bewegende Ladungen Kräfte ausüben. Eine Ladung erfährt im homogenen $\vec{B}$-Feld die Lorentz-Kraft

\begin{equation}
\vec{F}_L = q \vec{v} \times \vec{B}.
\end{equation}

Bewegt sich ein Elektron mit einer konstanten Geschwindigkeit und dringt anschließend senkrecht in ein homogenes Magnetfeld ein, so erfährt es dort eine Kraft

\begin{equation}
F = e_0 v_0 B. 
\end{equation}

Dies hat zur Folge, dass sich das Elektron auf einer Kreisbahn bewegt, bei der die potentielle Energie erhalten bleibt. Für die kinetische Energie gilt

\begin{equation}
E_{kin} = \frac{1}{2} m_0 v^2,
\end{equation}

mit der weiteren Folge, dass $|\vec{v}| = v_0 = const$.
Über das Gleichgewicht der Lorentzkraft und der Zentripetalkraft lässt sich der Krümmungsradius des Elektrons definieren:

\begin{equation}
r = \frac{m_0 \nu_0}{e_0 B}.
\end{equation}

\subsection{Prinzip des Kathodenstrahl-Oszillographen}
Ein Kathodenstrahl-Oszilloskop lässt sich leicht realisieren. Dazu wird an die Platten der x-Ablenkung eine Sägezahnspannung angelegt. An dem anderen Plattenpaar wird die zu untersuchende Wechselspannung angelegt. Liegen deren Frequenzen in dem geeigneten Verhältnis

\begin{equation}
n \cdot \nu_{S"a} = m \cdot \nu_{We} \quad  n,m = {1,2,3,...}
\end{equation}

zeichnet der Elektrodenstrahl den zeitlichen Verlauf der Wechselspannung auf. Konkreter: durch ein ganzzahliges Verhältnis zwischen den beiden periodischen Spannungen entstehen stehende Wellen. Jedoch lassen sich nur niederfrequente Wechselspannungen untersuchen, da $\Delta t$ klein gegen die Periodendauer von $U_d$ sein muss.

\subsection{Experimentelle Bestimmung der spezifischen Elektronenladung}
Mit der bekannten Kathodenstrahlröhre und aus der Beziehung für den Krümmungsradius ist es möglich die spezifische Ladung $\frac{e_0}{m_0}$ des Elektrons zu bestimmen. Mit dem Beschleunigungspotential $U_B$ gilt

\begin{equation}
v_0 = \sqrt{2 U_B \frac{e_0}{m_0}}.
\end{equation}

Das sich im feldfreien Raum gradlinig bewegende Elektron erfährt im Wirkungsbereich des Magnetfeldes eine Ablenkung. Die Auslenkung $D$ lässt sich besser messen als der Krümmungsradius $r$. Aus geometrischen Überlegungen sowie Gleichung (2.8) folgt die Bestimmungsgleichung für die spezifische Ladung der Elektronen

\begin{equation}
\frac{D}{L^2+D^2} = \frac{1}{\sqrt{8 U_B}} \sqrt{\frac{e_0}{m_0}} B.
\end{equation} 


\begin{figure}[H]
\centering{\includegraphics[width=13cm,height=10cm]{Bilder/Bild3.png}}
\caption{Skizze zur Ableitung einer Beziehung zwischen $L$, $D$ und $r$ [1]}
\label{fig:Abbildung}
\end{figure}


\section{Durchführung}
\subsection{Ablenkung im elektrischen Feld}
Es wird die Propotionalität zwischen der Verschiebung $D$ und der Ablenkspannung $U_d$ für 5 Spannungen $U_B$ im Bereich von 180-500 V untersucht. Dazu wird die Schaltung aus Abb.4 verwendet.

\begin{figure}[H]
\centering{\includegraphics[height=6cm,width=14cm]{Bilder/Bild4.png}}
\caption{Schaltung zur Messung der Verschiebung in Abhängigkeit von der Ablenkspannung [1]}
\label{fig:Abbildung}
\end{figure}

Des Weiteren wird eine Sinusspannung mithilfe des in Abb.5 dargestellte Kathodenstrahl-Oszilloskops auf deren Frequenz sowie deren Amplitude untersucht, indem die die Frequenzen der Sägezahnspannung gemessen werden, bei denen sich ein stehendes Bild ergibt.

\begin{figure}[H]
\centering{\includegraphics[height=6cm,width=14cm]{Bilder/Bild5.png}}
\caption{Prinzipschaltbild eines Kathodenstrahl-Oszillographen [1]}
\label{fig:Abbildung}
\end{figure}

\subsection{Ablenkung im magnetischen Feld}
Es soll die spezifische Ladung des Elektrons experimentell bestimmt werden. Dazu wird mithilfe einer Helmholtzspule ein homogenes Magnetfeld erzeugt mit der Flussdichte $B$

\begin{equation}
\mathrm{B = \mu_0 \frac{8}{\sqrt{125}} \frac{NI}{R}}.
\end{equation}
Dabei bezeichnet $N$ die Windungszahl, $I$ den Spulenstrom und $R$ den Spulenradius. Nachdem die Achse der Kathodenstrahlröhre in Richtung der horizontalen Erdmagnetfeldkomponente ausgerichtet wurde, wird die Verschiebung $D$ in Abhängigkeit von der Flussdichte $B$ gemessen. Dabei liegt eine konstante Spannung $U_B$ von 250 bzw. 430 V an.
\newline
\newline
Ebenfalls soll die Intensität des lokalen Erdmagnetfeldes bestimmt werden. Dazu wird die Achse der Kathodenstrahlröhre in Nord-Süd-Richtung eingestellt, der Leuchtfleck notiert und anschließend in Ost-West-Richtung verlagert. Die durch die Drehung auftretende Kraft verschiebt den Lichtfleck. Mithilfe der Helmholtzspule wird die Verschiebung kompensiert und die Totalintensität $B_{total}$ kann über den Inklinationswinkel $\phi$ berechnet werden.

\begin{figure}[H]
\centering{\includegraphics[height=18cm,width=16cm]{Bilder/Bild6.png}}
\caption{Abmessungen der verwendeten Kathodenstrahlröhre [1]}
\label{fig:Abbildung}
\end{figure}

\section{Auswertung}
\subsection{Ablenkung des Elektronenstrahls}
In Tabelle 1 sind die Messwerte der Verschiebung angegeben, die in Abbildung 7 dargestellt sind. Die linearen Ausgleichsrechnungen werden dabei mithilfe von Gnuplot erzeugt.
\begin{table}[H]
\centering{\begin{tabular}{|c|c|c|c|c|c|}
\hline
	&$U_B=250$\,[V]&	$U_B=300$\,[V]&	$U_B=350$\,[V]&	$U_B=400$\,[V]&	$U_B=450$\,[V]\\
$D\,$[m]&	$U_d$\,[V]&	$U_d$\,[V]&	$U_d$\,[V]&	$U_d$\,[V]&	$U_d$\,[V]\\
\hline
-0,025&-26,0&	-30,8&	-35,5&-&-		\\
-0,019&-21,4&	-25,0&	-29,3&	-33,5&-	\\
-0,013&-16,4&	-19,7&	-22,9&	-26,6&	-30,2\\
-0,006&-12,3&	-14,8&	-17,4&	-19,8&	-21,7\\
0,000&-8,1&	-9,7&	-11,3&	-13,1&	-14,1\\
0,006&-4,1&	-4,6&	-5,4&	-6,8	&-6,6\\
0,013&0,4&	0,4&	1,0&	0,2&	1,1\\
0,019&4,9&	6,0	&7,3&	7,4&	9,2\\
0,025&9,7&	11,5&	14,0&	15,0&	17,4\\
\hline
\end{tabular}}
\caption{Messwerte zur Verschiebung des Elektronenstahls für verschiedene Ablenkspannungen}
\end{table}

\begin{figure}[H]
\hspace{-0,8cm}
\includegraphics[width=1.1\textwidth,height=9.35cm]{Bilder/Bild10.png}
\caption{Plot der Messwerte für die Ablenkung im E-Feld sowie deren Ausgleichsgeraden}
\label{fig:Abbildung}
\end{figure}
Für die Steigungen der fünf Ausgleichsgeraden gilt die Beziehung $m=\frac{D}{U_D}$. Diese Werte sind in Tabelle 2 gelistet.
\begin{table}[H]
\centering{\begin{tabular}{c|c|c}
$U_B\,[\mathrm{V}]$&$\frac{D}{U_D}\,[\frac{\mathrm{V}}{\mathrm{m}}]$&$\Delta\frac{D}{U_D}\,[\frac{\mathrm{V}}{\mathrm{m}}]$\\
\hline\hline
250&0,00143&0,00001\\
300&0,00121&0,00001\\
350&0,00103&0,00001\\
400&0,00092&0,00001\\
450&0,00081&0,00001\\
\end{tabular}}
\caption{Proportionalitätsfaktoren $D/U_D$ der Ausgleichsgeraden}
\end{table}
Diese werden wiederum in Abbildung 8 zusammen mit einer Ausgleichsgeraden dargestellt. Die Steigung dieser Ausgleichsgerade genügt gerade dem Wert $\frac{D\,U_B}{U_D}$, der außerdem dem Wert für $\frac{pL}{2d}$ entspricht.
\begin{align}
\notag
\frac{D\,U_B}{U_D}=\frac{pL}{2d}=(0,351\pm0,007)\,\mathrm{m}
\notag
\end{align}
\begin{figure}[H]
\includegraphics[width=\textwidth]{Bilder/Bild11.png}
\caption{Steigung $D/U_D$ gegen die inverse Beschleunigungsspannung geplottet}
\label{fig:Abbildung}
\end{figure}
Zum Vergleich wird nun der theoretische Wert $\frac{pL}{2d}$ berechnet. Die benötigten Werte werden dabei aus Abbildung 6 abgelesen: $p$ als Länge der Ablenkplatte $p=0,019\,\mathrm{m}$, $L$ ist die Strecke zwischen dem Ablenkkondensator und dem Leuchtschirm $L=0,143\,\mathrm{m}$ und $d=0,0038\,\mathrm{m}$, den Abstand der Kondensatorplatten. Der theoretische Wert ergibt sich also zu
\begin{align}
\notag
\frac{D\,U_B}{U_D}=\frac{pL}{2d}=0,3575\,\mathrm{m}.
\notag
\end{align}
Die Abweichung vom experimentellen zum theoretischen Wert beträgt $1,8\%$.
\subsection{Analysieren der Sinusspannung}
\begin{table}[H]
\centering{\begin{tabular}{|c|c|c|}
n&$\nu_{sae}$[Hz]&$\nu_{sin}$[Hz]\\
\hline
4&19,89&79,56\\
2&39,93&79,86\\
1&79,48&79,48\\
0,5&159,71&79,86\\
\end{tabular}
}\caption{Messwerte der abgeglichenen Sägezahnfrequenzen}\end{table}
In Tabelle 3 sind die Messwerte der abgeglichenen Sägezahnfrequenzen gelistet. Dabei ergibt sich die Frequenz des Sinusgenerators $\nu_{sin}$ aus den Sägezahnspannungen durch $\nu_{sin}=\nu_{sae}\cdot n$. Der Mittelwert wird schließlich bestimmt zu
\begin{align}
\notag
\overline{\nu}_{sin}=(79,69\pm0,09)\,\mathrm{Hz}.
\notag
\end{align}
Die Amplitude an dem Bildschirm betrug $D=1,9''=0,012\,\mathrm{m}$. Mithilfe von Gleichung (2.4) ergibt sich daraus mit den im Kapitel 4.1 bestimmten Faktor $\frac{pL}{2d}$ sowie der Beschleunigungsspannung $U_B$ die Amplitude der Spannung. Allerdings wurde die Beschleunigungsspannung nicht bestimmt, weshalb nun in der Rechnung die letzte eingestellte Spannung $U_B=450\mathrm{V}$ benutzt wird. Damit ergibt sich für die Amplitude der Sinusspannung
\begin{align}
\notag
U_0=\frac{D\cdot U_B}{\frac{pL}{2D}}=0,034\cdot U_B=(15,47\pm0,31)\,\mathrm{V}
\end{align}
\subsection{Berechnung der spezifischen Ladung eines Elektrons}
Die Werte für die Auslenkungen mit den dazugehörigen Stormstärken und dem daraus an der Helmholtzspule mit 20 Windungen und einem Radius von 0,282\,m resultierenden Magnetfeld sind in Tabelle 4 aufgelistet. Dabei sind ergibt sich die Linearisierung $\Phi$ aus der Beziehung
\begin{align}
\notag
\Phi=\frac{D}{L^2+D^2}
\notag
\end{align}
\begin{table}[H]
\centering{\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
&&&		$U_B=$250V&&		$U_B=$430V&\\	
$D\,['']$&	$D\,$[mm]&	$\Phi\,$[1/m]&	$I\,$[A]&	$B\;$[mT]&	$I\,$[A]&	$B\,$[mT]\\
\hline\hline
0,00&	0,00&	0,00&	0,00&	0,00&	0,00&	0,00\\
1,00&	6,35&	0,21&	0,36&	0,02&	0,36&	0,02\\
2,00&	12,70&	0,41&	0,70&	0,04&	0,82&	0,05\\
3,00&	19,05&	0,61&	1,00&	0,06&	1,24&	0,08\\
4,00&	25,40&	0,81&	1,33&	0,08&	1,67&	0,11\\
5,00&	41,75&	1,00&	1,68&	0,11&	2,09&	0,13\\
6,00&	38,10&	1,19&	2,00&	0,13&	2,52&	0,16\\
7,00&	44,45&	1,36&	2,35&	0,15&	2,95&	0,19\\
8,00&	50,80&	1,53&	2,69&	0,17&	-	&	-	\\
\hline
\end{tabular}
}\caption{Messwerte zur Bestimmung der spezifischen Ladung vom Elektron}\end{table}
Außerdem sind die Werte der Linearisierung in Abbildung 9 gegen die Magnetfelder für $U_B=250\,$V und $U_B=430\,$V geplottet.
\begin{figure}[H]
\includegraphics[width=\textwidth]{Bilder/Bild12_2.png}
\caption{Ablenkung der Elektronen im Magnetfeld}
\label{fig:Abbildung}
\end{figure}
Die Steigung $m=\frac{\Phi}{B}$ der Ausgleichsgeraden wird dabei benötigt, um die spezifische Ladung des Elektrons zu bestimmen. Diese besitzen folgende Werte:
\begin{align}
\notag
m_{250\mathrm{V}}=(9,03\pm0,15)\,\frac{1}{\mathrm{mT\cdot m}}\\
\notag
m_{430\mathrm{V}}=(7,22\pm0,11)\,\frac{1}{\mathrm{mT\cdot m}}
\notag
\end{align}
Aus Gleichung (2.11) folgt nun mit der soeben bestimmten Steigung für die spezifische Ladung der Zusammenhang
\begin{align}
\notag
\frac{e_0}{m_0}=m^2\cdot8U_B.
\notag
\end{align}
Für $U_B=250\,$V ergibt sich somit die spezifische Ladung des Elektrons zu
\begin{align}
\notag
\frac{e_0}{m_0}=(1,63\pm0,03)\,10^{11}\,\frac{\mathrm{C}}{\mathrm{kg}}
\notag
\end{align}
und für $U_B=430\,$V zu
\begin{align}
\notag
\frac{e_0}{m_0}=(1,79\pm0,03)\,10^{11}\,\frac{\mathrm{C}}{\mathrm{kg}}.
\notag
\end{align}
Verglichen mit dem Theoriewert von $\frac{e_0}{m_0}=1,76\,10^{11}\,\frac{\mathrm{C}}{\mathrm{kg}}$ [2] beträgt die Abweichung $7,3\%$ bzw. $1,8\%$. 
\subsection{Bestimmung des Erdmagnetfeldes}
Die Messung bei einer Beschleunigungsspannung von $U_B=200\,$V ergibt für den Strom einen Wert von $I=0,419\,$A und der Neigungswinkel $\phi$ wird bestimmt zu $\phi=64\symbol{23}$.
Die Horizontale Komponente des Magnetfeldes wird durch Gleichung (3.1) bestimmt zu
\begin{align}
\notag
B_{hor}=\mu_0\frac{8}{\sqrt{125}}\cdot \frac{nI}{r}=26,7\,\mathrm{\mu T}
\notag
\end{align}

Unter Berücksichtigung des Winkels ergibt sich für das Erdmagnetfeld der Zusammenhang
\begin{align}
\notag
B_{Erde}&=\frac{B_{hor}}{\cos\phi}\\
\notag
&=31,5\,\mathrm{\mu T}.
\notag
\end{align}

\newpage
\section{Diskussion}
Bei der Bestimmung der Amplitude der Sinusspannung fehlte der Wert der Beschleunigungsspannung, so dass [3] dieser nur in Abhängigkeit der Beschleunigungsspannung angegeben werden konnte und zusätzlich mit der Annahme von $U_B=450\,$V berechnet wurde.\\ Auch bei dem bestimmten Erdmagnetfeld ist die Abweichung zum Literaturwert von ca. $48\,\mathrm{\mu T}$ [4] mit $34,4\%$ recht groß, was mit der ungenaue Bestimmung des Winkels $\phi$ zu erklären ist. \\ Weiterhin beträgt die Abweichung des experimentell bestimmten Werte von $\frac{pL}{2d}$ geringe $1,8\%$ zum theoretischen Wert und die bestimmte spezifische Ladung des Elektrons wurde bis auf eine Abweichung von $7,3\%$ bzw. $1,8\%$ zum Literaturwert bestimmt. Diese Abweichungen lassen sich auf Ablesefehler zurückführen und täuschen nicht über die sehr sehr geilen Messwerte hinweg.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 501/502, Ablenkung eines Elektronenstrahls im E- und B-Feld (21.11.2013)
\item National Institute for Standards and Technology NIST\\ http://physics.nist.gov/cuu/Constants/index.html: (16.06.2014)
\item http://www.duden.de/rechtschreibung/sodass (24.06.2014)
\item http://www.iifeh.de/magnetfeld-der-erde.php (24.06.2014)
\end{enumerate}
\end{document}




