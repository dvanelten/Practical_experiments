\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{subfigure} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 302}  \\ Protokoll vom \today}
\lhead{\textbf{Konstantin Pfrang, Dennis van Elten}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}
\begin{document}
\title{Versuch 302 - Elektrische Brückenschaltung}
\author{Konstantin Pfrang \and Dennis van Elten}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Ziel}
Die Eigenschaften elektrischer Bauelemente sollen durch Verwendung von verschiedenen Brückenschaltungen bestimmt werden. Dabei werden Widerstände, Kapazitäten und Induktivitäten durch unterschiedliche Schaltungen gemessen. Darüber hinaus soll die Frequenzabhängigkeit der Brückenspannung einer Wien-Robinson-Brücke untersucht werden und mit dieser Brücke außerdem der Klirrfaktor gemessen werden

\section{Theorie}
Die in der Messtechnik weite Verbreitung der Brückenschaltungen begründet sich darauf, dass sie eine relativ genaue Messung verschiedener Eigenschaften erlauben. Vorallem ist diese genaue Messung möglich, wenn mit einer Nullmethode arbeitet, die mit der abgeglichenen Brücke ermöglicht wird.\\ Durch die Brückenschaltungen lässt sich generell jede physikalische Größe messen, solange sie sich als Widerstand interpretieren lässt. Dabei spielt es keine Rolle, ob dieser real- oder komplexwertig ist.
\subsection{Allgemeine elektrische Brückenschaltung}
In Brückenschaltungen wird die Potentialdifferenz zweier Punkte auf zwei getrennten, stromdurchflossenen Leitern abhängig von ihren Widerstandsverhältnissen gemessen. Daher ist die allgemeine Brückenschaltung stets die in Abb. 1 dargestellte Form.

\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=9cm]{Bilder/Bild1.png}}
\caption{Darstellung einer Allgemeinen Brückenschaltung [1]}
\label{fig:Abbildung}
\end{figure}

Die Spannung zwischen den beiden Punkten A und B wird als Brückenspannung $U$ bezeichnet. Zur Berechnung dieser Spannung sind die Kirchhoffschen Gesetze von Nöten:
\begin{enumerate}
\item Knotenregel:\\Die Summe über alle Ströme an einem Knotenpunkt ist gleich null.\\
\begin{equation}
\sum_k{I_k} = 0
\end{equation}
\item Machenregel:\\Innerhalb jeder Masche eines Leiternetzwerkes ist die Summe über alle Spannungen gleich null.
\begin{equation}
\sum_k{U_k}=0
\end{equation}
\end{enumerate}

Unter Beachtung dieser beiden Gesetze ergibt sich die Brückenspannung $U$ zu
\begin{equation}
U=\frac{R_2R_3-R-1R_4}{(R_3+R_4)(R_1+R_2)}U_S
\notag
\end{equation}
Diese Brückenspannung $U$ verschwindet unabhängig von der Speisespannung $U_S$ genau dann, wenn gilt
\begin{equation}
R_1R_4=R_2R_3
\end{equation}
Ist diese Bedingung erfüllt, so wird die Brückenschaltung als abgeglichene Brücke bezeichnet.\\
Es lässt sich erkennen, dass die Bedingung für die abgeglichene Brücke einzig von den Verhältnissen der Widerstände abhängt. Dies ermöglicht es, einen beliebigen Widerstand zu messen, wenn die anderen drei Widerstände bekannt sind. Die Genauigkeit hängt von der Toleranz der drei festen Widerstände ab und wie genau man die Brückenspannung nullen kann.
\subsection{Brückenschaltung mit komplexen Widerständen}
Enthält eine Brückenschaltung Widerstände, Kapazitäten oder Induktivitäten, so ist es erforderlich, die Berecnung mit allgemeinen komplexen Widerständen $\Re$ (2.3) durchzuführen.
\begin{equation}
\Re = X + iY
\end{equation}
\begin{equation}
\mbox{i = imaginäre einheit}
\notag
\end{equation}
$X$ wird in dabei als Wirkwiderstand und $Y$ als Blindwiderstand bezeichnet. Für die gängigsten, idealen Bauteil, ohmsche Widerstände ($R$), Kondensatoren ($C$) und Induktivitäten ($L$), gilt in komplexer Schreibweise:
\begin{eqnarray} 
\Re_R&=&R \\ 
\Re_C&=&-\frac{i}{\omega C}\\
\Re_L&=&i\omega L
\end{eqnarray}
Die Abgleichbedingung (2.3) ergibt sich nun mit komplexen Widerständen zu
\begin{equation}
\Re_1 \Re_4 = \Re_2 \Re_3
\notag
\end{equation}
Da dabei gelten muss, dass sowohl Real- als auch Imaginärteil übereinstimmen muss, ergeben sich die beiden Abgleichbedingungen zu
\begin{equation}
X_1X_4-Y_1Y_4=X_2X_3-Y_2Y_3
\end{equation}
und
\begin{equation}
X_1Y_4-X_4Y_1=X_2Y_3-X_3Y_2
\end{equation}
Bei abgeglichenen Wechselstrombrücken müssen also zwei Bedingungen gleichzeitig erfüllt sein. Die Brückenspannung muss vom Betrag und Phase verschwinden. Dazu werden zwei unabhängige Stellglieder benötigt.

\subsection{Spezielle Brückenschaltungen}
\subsubsection{Die Wheatstonesche Brücke}
\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=9cm]{Bilder/Bild2.png}}
\caption{Darstellung der Wheatstonesche Brücke [1]}
\label{fig:Abbildung}
\end{figure}
Die Brücke besitzt die in Abb.2 gegeben Form. Mit ihrer Hilfe lässt sich der unbekannte Widerstand $R_x$ bestimmen. Aus der Abgleichbedingung (2.3) folgt die Bestimmungsgleichung für $R_x$:
\begin{equation}
R_x=R_2\frac{R_3}{R_4}
\end{equation}
Hierbei ist nur das Verhältnis von $R_3$ und $R_4$ entscheidend, weshalb diese Widerstände als Potentiometer ausgebildet sind.


\subsubsection{Die Kapazitäts- und Induktivitätsmessbrücke}
Die Funktionsweisen der Kapazitäts- und Induktivitätsmessbrücke sind analog zueinander, weshalb sie in diesem Abschnitt parallel vorgestellt werden.\\
Sie sind ähnlich zu der Wheatstoneschen Brücke aufgebaut. Es wird davon ausgegangen, dass es keine idealen Kondensatoren bzw. Spulen gibt. Das bedeutet, dass ein Teil der elektrischen Energie aufgrund von dielektrischen Verlusten beim Kondensator und dem Innenwiderstand bei der Spule in Wärme umgewandelt wird. Um dies zu berücksichtigen, wird in der Schaltung vor diese Komponenten ein (fiktiver) Widerstand geschaltet.
Somit ergeben sich die realen komplexen Widerstände für diese beiden Bauelemente als
\begin{eqnarray}
\Re_C=R-\frac{i}{\omega C}\\
\Re_L=R+i\omega L
\end{eqnarray}
Der Aufbau der Kapazitätsmessbrücke ist in Abb. 3 dargestellt, wobei für die Induktivitätsmessbrücke lediglich die Kapazitäten ($C_x,C_2$) durch Induktivitäten ($L_x,L_2$) ersetzt werden.

\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=9cm]{Bilder/Bild3.png}}
\caption{Darstellung der Kapazitätsmessbrücke [1]}
\label{fig:Abbildung}
\end{figure}
Da nun komplexe Widerstände betrachtet werden, werden nun jeweils zwei Stellenglieder benötigt, die durch den variablen Widerstand $R_2$ und das Potenziometer ($R_3,R_4$) gegeben sind. Aus den Gleichungen (2.8) und (2.9) folgt:
\begin{eqnarray}
R_x=R_2\frac{R_3}{R_4}\\
C_x=C_2\frac{R_4}{R_3}\\
L_x=L_2\frac{R_3}{R_4}
\end{eqnarray}
In der Regel ist der Bau der Kondensatoren so, dass die dielektrischen Verluste sehr klein sind. Also gilt $R_X\ll \frac{1}{\omega C}$ selbst für Frequenzen in der Größenordnung von $10^4 \mathrm{Hz}$ und somit $R_2\approx 0$ wodruch sich der einbau dieses Stellgliedes erübrigt.\\
Bei der Induktivitätsmessbrücke sollten die Verluste an der Spule $L_2$ möglichst gering sein. Besonders bei niedrigen Frequenzen ist eine Umsetzung sehr kompliziert, weshalb zur Induktivitätsmessung häufig die Maxwell-Brücke verwendet wird.

\subsubsection{Die Maxwell-Brücke}
Die Schaltung der Maxwell-Brücke ist in Abb. 4 dargestellt. Die Abgleichelemente sind nun die veränderlichen Widerstände $R_3$ und $R_4$. Außerdem sollte $C_4$ eine Kapazität mit möglichst geringen Verlusten sein und $R_2$ ein ohmscher Widerstand bekannter Größe mit niedriger Toleranz.
\begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=11cm]{Bilder/Bild4.png}}
\caption{Darstellung der Maxwell-Brücke [1]}
\label{fig:Abbildung}
\end{figure}
Die Berechnung dieser Brückenschaltung liefert nun die Gleichungen
\begin{eqnarray}
\Re_1&=&R_x + i \omega L_x\\
\frac{1}{\Re_4}&=&\frac{1}{R_4} + i\omega C_4
\end{eqnarray}
Für die gesuchte Induktivität ergibt sich durch einsetzen in die Abgleichbedingungen (2.8) und (2.9) 
die Formeln
\begin{eqnarray}
R_x&=&R_2\frac{R_3}{R_4}\\
L_x&=&R_2R_3C_4
\end{eqnarray}


\subsubsection{Die Wien-Robinson-Brücke}
Bei Wien-Robinson-Brücke ist im gegensatz zu den zuvor beschriebenen Brückenschaltungen kein Abgleichelement verbaut, wie in Abb. 5 zu sehen ist. Der Abgleich erfolgt nur über die Frequenz. Die Bauteile $C$, $R$ und $R'$ sollen möglichst geringe Toleranzen aufweisen und außerdem $C$ nur geringe Verluste besitzen.
 \begin{figure}[htbp]
\centering{\includegraphics[height=8cm,width=12cm]{Bilder/Bild5.png}}
\caption{Darstellung der Wien-Robinson-Brücke [1]}
\label{fig:Abbildung}
\end{figure}
 
Mit den Definitionen
\begin{eqnarray}
\notag
\omega_0&=&\frac{1}{RC}\\
\notag
\Omega&=&\frac{\omega}{\omega_0}
\notag
\end{eqnarray}
lässt sich für die Brückenspannung $U_{Br}$, die Speisespannung $U_S$ und das Frequenzverhältnis $\Omega$ der Zusammenhang
\begin{equation}
\left|{\frac{U_{Br}}{U_S}}\right|^2=\frac{1}{9}\frac{(\Omega^2-1)^2}{(1-\Omega^2)^2+9\Omega^2}
\end{equation} 
An Gleichung (2.20) lässt sich die Funtion der Wien-Robinson-Brücke als Filter erkennen, da sie alle Frequenzen für die $\Omega\approx1$ gilt stark abschwächt und für $\omega=\omega_0$  sogar vollständig herausfiltert\\
Mithilfe dieser Brücke wird im Laufe des Versuches eine Klirrfaktor-Messung durchgeführt. Als Klirrfaktor $k$ wird das Amplitudenverhältnis nicht erwünschter Oberwellen zu der Grundwelle einer vom Generator erzeugten Sinusspannung bezeichnet. Sie berechnet sich nach der Formel
\begin{equation}
k=\frac{\sqrt{U_2^2+U_3^2+...}}{U_1}
\end{equation}
wobei $U_1$ die Amplitude der Grundwelle ist und $U_n$ die Amplitude der $n$-ten Oberwelle der Frequenz $n_{v_0}$
\section{Durchführung}
Im laufe des Versuches werden verschiedene elektrischen Bauelemente unter Verwendung von geeigneten Brückenschaltungen vermessen.
\begin{enumerate}
\item Zunächst wird die in Abb. 2 dargestellte Wheatstonesche Brücke verwendet, um wie in der Theorie beschrieben zwei unbekannte Widerstände zu messen.
\item Anschließend benutzt man die Kapazitätsmessbrücke aus Abb. 3, um die Kapazität zweier Kondensatoren zu bestimmen bei denen die Verluste vernachlässigt werden (wodurch bei der Messung der Widerstand $R_2$ ausgelassen werden kann). Außerdem wird eine RC-Kombination vermessen, also ein verlustbehafteter Kondensator. Hierbei muss der veränderbare Widerstand $R_2$ in die Brücke eingebaut werden.
\item Eine Induktivität soll zusammen mit einem Verlustwiderstand vermessen werden. Dazu verwendet man die Induktivitätsmessbrücke die analog zu Abb. 3 ist. Anschließend wird die Spule erneut unter Verwendung der Maxwell-Brücke (Abb. 4) vermessen.
\item Schließlich wird die Frequenzabhängigkeit der Wien-Robinson-Brücke aus Abb. 5 untersucht, indem man $U_{Br}(v)$ und $U_S(v)$ misst und den Quotienten halblogarithmisch gegen $\Omega=\frac{v}{v_0}$ aufträgt.\\ Mit der Wien-Robinson-Brücke wird auch die Klirrfaktor-Messung durchgeführt. Der Klirrfaktor lässt sich dabei aus den zuvor gemessenen Daten errechnen.
\end{enumerate}

\newpage
\section{Auswertung}

\subsection{Fehlerrechnung}
In diesem Abschnitt werden kurz die notwendigen Fehlerrechnungen benannt. Das arithmetische Mittel wird mithilfe der Gleichung
\begin{equation}
\overline x = \frac{1}{N} \sum_{i=1}^N x_i
\end{equation}
berechnet.
Um den Fehler einer Größe $y$, die aus einer Menge $N$ mehrerer fehlerbehafteten Größen $X_i$ zusammengesetzt ist, zu bestimmen, kann im Falle einer Multiplikation bzw. Division ein Spezialfall der Gaußschen Fehlerfortpflanzung genutzt werden. Dabei berechnet sich der Fehler aus den absoluten Fehlern der Größen $x_i$ wie folgt:
\begin{equation}
\Delta y = \sqrt{\sum_{i=1}^N (\frac{\Delta x_i}{x_i})^2}
\end{equation}
Um den Mittelwert der absoluten Fehler $\Delta \overline y$, bestehend aus gleichen Anteilen der Größen $\Delta  y$, zu ermitteln, muss dieser ebenfalls mit Gauß berechnet werden. Dabei wird die Formel für den Spezialfall der Summen bzw. Differenzen inklusive eines Vorfaktors verwendet. Es ergibt sich
\begin{equation}
\Delta \overline y = \frac{1}{N}\sqrt{\sum_{i=1}^N (\Delta \overline y_i)^2}
\end{equation}
für die Berechnung des mittleren aboluten Fehler.

\newpage
\subsection{Bestimmung zweier Widerstände mit der Wheatstoneschen Brückenschaltung}
Nach Theorie [2.3.1] werden zwei unbekannte Widerstände mithilfe der Brückenschaltung nach Wheatstone gemessen. Die unbekannten Widerstände berechnen sich mit der Beziehung
\begin{equation}
R_x = R_2 \cdot \frac{R_3}{R_4}.
\end{equation}
Der ohmsche Widerstand $R_2$ ist mit einem Fehler von $0,2$\%, die Linearität von $R_3/R_4$ mit 0,5\% behaftet.
Der absolute Fehler von $R_x$ ergibt sich mit
\begin{equation}
\Delta R_x = \sqrt{(\frac{\Delta R_2}{R_2})^2 + (\frac{\Delta (R_3/R_4)}{R_3/R_4})^2} \cdot R_x.
\end{equation}
Daraus folgt für den gemittelten absoluten Fehler des unbekannten Widerstandes $R_x$
\begin{equation}
\Delta R_x = \frac{1}{3} \sqrt{{\Delta (R_{x_1})^2 + \Delta (R_{x_2})^2 + \Delta (R_{x_3})^2}}.
\end{equation}
Die Messdaten und errechneten Werte für den Widerstand des Wertes 10 sind in der folgenden Tabelle 1 aufgeführt.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c||c|c} 
$R_2$[$\Omega$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $R_x$[$\Omega$] & $\Delta R_x$[$\Omega$] \\
\hline \hline
332		& 421,7	& 578,3 & 242,1  & 1,304\\
664		& 266,3	& 733,7 & 233,74 & 1,259\\
1000	& 193,1	& 806,9 & 239,31 & 1,289\\
\end{tabular}
\caption{Messwerte zur Widerstandsmessung von Wert 10}
\label{fig:Tab1}
\end{table}


Der Fehler des aboluten mittleren Fehlers wird mit Gleichung [Gl.(4.6)] berechnet und der Widerstand $R_x$ mit dem arithmetischen Mittel bestimmt. Somit ergibt sich für den Widerstand mit dem Wert 10
\begin{equation}
R_{10} = (238,38 \pm 0,74) \Omega.
\end{equation}

Analog wird der Widerstand mit dem Wert 13 bestimmt. Die Daten sind in Tabelle 2 aufgelistet.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c||c|c} 
$R_2$[$\Omega$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $R_x$[$\Omega$] & $\Delta R_x$[$\Omega$] \\
\hline \hline
332		& 493		& 507	  & 322,83 & 1,738\\
664		& 327		& 673	  & 322,63 & 1,737\\
1000	& 244,9	& 755,1 & 324,33 & 1,747\\
\end{tabular}
\caption{Messwerte zur Widerstandsbestimmung von Wert 13}
\label{fig:Tab1}
\end{table}

Die errechnete Größe des ohmschen Widerstandes vom Wert 13 beträgt
\begin{equation}
R_{13} = (323,26 \pm 1,00) \Omega.
\end{equation}

\newpage
\subsection{Bestimmung zweier Kapazitäten und Bestimmung der Daten einer RC-Kombination mit der Kapazitätsmessbrücke}
Die Messung erfolgt mithilfe der Kapazitätsmessbrücke. Dabei wird bei der Bestimmung der Kapazitäten von annähernd idealen Kondensatoren ausgegangen, sodass dort kein ohmscher Widerstand berücksichtigt werden muss. Die Kapazität lässt sich mit 
\begin{equation}
C_x = C_2 \frac{R_4}{R_3}
\end{equation}
ermitteln. In diesem Fall besitzt sowohl die schon zuvor erwähnte Linearität $R_3/R_4$ als auch der Kondensator $C_2$ eine Eichgenauigkeit. Die Toleranz von $C_2$ beträgt $0,2$\%.
Der absolute Fehler von $C_x$ ergibt sich mit
\begin{equation}
\Delta C_x = \sqrt{(\frac{\Delta C_2}{C_2})^2 + ({\frac{\Delta \frac{1}{R_3/R_4}}{\frac{1}{R_3/R_4}}})^2} \cdot C_x.
\end{equation}
Der gemittelte absolute Fehler wird wie zuvor berechnet:
\begin{equation}
\Delta C_x = \frac{1}{3} \sqrt{{\Delta (C_{x_1})^2 + \Delta (C_{x_2})^2 + \Delta (C_{x_3})^2}}.
\end{equation}
Die Daten für die Kapazität mit dem Wert 1 sind in der nachstehenden Tabelle 3 dargestellt.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c||c|c} 
$C_2$[$nF$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $C_x$[$nF$] & $\Delta C_x$[$nF$] \\
\hline \hline
399	& 378,9	& 621,1	& 654,15 & 3,523\\
450	& 407,3	& 592,7	& 654,84 & 3,526\\
750	& 532 	& 468   & 659,77 & 3,553\\
\end{tabular}
\caption{Messwerte zur Kapazitätsbestimmung von Wert 1}
\label{fig:Tab1}
\end{table}

Es ergibt sich nach analoger Rechnung die Kapazität
\begin{equation}
C_{1} = (656,25 \pm 2,04) nF.
\end{equation}

Für die zweite Kapazität mit dem Wert 4 sind die Mess- und errechneten Werte in Tabelle 4 aufgelistet.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c||c|c} 
$C_2$[$nF$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $C_x$[$nF$] & $\Delta C_x$[$nF$] \\
\hline \hline
399	& 284 	& 716	  & 1005,93 & 5,417\\
450	& 307,3	& 692,7	& 1014,37 & 5,463\\
750	& 422 	& 716   & 1027,25  & 5,532\\
\end{tabular}
\caption{Messwerte zur Kapazitätsbestimmung von Wert 4}
\label{fig:Tab1}
\end{table}

Für die Kapazität des Kondensators mit dem Wert 4 ergibt sich
\begin{equation}
C_{4} = (1015,85 \pm 3,16) nF.
\end{equation} 

Bei der Untersuchung der RC-Kombination des Wertes 8 wird von einem realen Kondensator ausgegangen. Dies hat zur Folge, dass der Widerstand $R_2$ nicht vernachlässigt werden kann. Dieser lässt sich mit der Beziehung
\begin{equation}
R_x = R_2 \frac{R_3}{R_4}
\end{equation}
ausdrücken. Zusätzlich zu den bisherigen Toleranzen besitzt der hier verwendete ohmsche Widerstand $R_2$ eine Eichgenauigkeit von $3$\%.
Dadurch definiert sich dessen absoluter Fehler als
\begin{equation}
\Delta R_x = \sqrt{(\frac{\Delta R_2}{R_2})^2 + ({\frac{\Delta \frac{1}{R_3/R_4}}{\frac{1}{R_3/R_4}}})^2} \cdot R_x.
\end{equation}

Der gemittelte absolute Fehler des Widerstands $R_x$ ist trivial zur Rechnung des Fehlers der Kapazität:
\begin{equation}
\Delta R_x = \frac{1}{3} \sqrt{{\Delta (R_{x_1})^2 + \Delta (R_{x_2})^2 + \Delta (R_{x_3})^2}}.
\end{equation}
Es ergeben die sich in Tabelle 5 aufgetragenen Größen für den ohmschen Widerstand und Kapazitätswert 8.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c||c|c|c|c} 
$C_2$[$nF$] & $R_2$[$\Omega$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $R_x$[$\Omega$] & $C_x$[$nF$] & $\Delta R_x$[$\Omega$] & $\Delta C_x$[$nF$] \\
\hline \hline
399	& 414,5 	& 571	  & 429 & 551,7		& 299,77	& 16,78	& 1,614\\
450	& 367			& 604		& 396 & 559,77	& 295,03	& 17,02	& 1,589\\
597	& 276 	 	& 671   & 329 & 562,91	& 292,72	& 17,12	& 1,576\\
\end{tabular}
\caption{Messwerte zur Kapazitäts- und Widerstandsbestimmung von Wert 8}
\label{fig:Tab1}
\end{table}

Für die Eigenschaften des realen Kondensators ergeben sich nach der Berechnung
\begin{equation}
\begin{align}
C_{8} &= (295,84 \pm 0,92) nF \\
R_{8} &= (558,13 \pm 9,80) \Omega.
\end{align}
\end{equation}

\newpage
\subsection{Bestimmung der Induktivität und des Verlustwiderstandes einer Spule mit der Induktionsmessbrücke}
Der unbekannte Widerstand $R_x$ wird mit einer Induktionsmessbrücke ermittelt. Wie bereits bei der RC-Kombination wird dieser durch die Gleichung
\begin{equation}
R_x = R_2 \frac{R_3}{R_4}
\end{equation}
berechnet. Die Bestimmung des gemittelten absoluten Fehlers erfolgt ebenso nach vorherigen Überlegungen.
Die Induktivität kann über die Beziehung
\begin{equation}
L_x = L_2 \frac{R_3}{R_4}
\end{equation}
berechnet werden.
Da auch hier wie schon bei der Messung des realen Kondensators der Widerstand $R_2$ über ein Potentiometer variiert wird besitzt dieser eine Toleranz von $3$\%. Ebenfalls fehlerbehaftet sind neben den vorherigen Größen die Induktivität der zweiten Spule "$L_2$ mit $0,2$ \%. Der absolute Fehler der Spule berrechnet sich mit
\begin{equation}
\Delta L_x = \sqrt{(\frac{\Delta L_2}{L_2})^2 + (\frac{\Delta (R_3/R_4)}{R_3/R_4})^2} \cdot L_x.
\end{equation}
Der gemittelte absolute Fehler kann mithilfe von Gauß errechnet werden:
\begin{equation}
\Delta L_x = \frac{1}{2} \sqrt{{\Delta (L_{x_2})^2 + \Delta (L_{x_3})^2}}.
\end{equation}

Es wurde die Spule mit dem Wert 19 gemessen. Die gesammelten Daten sind in der nachstehenden Tabelle 6 notiert.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c||c|c|c|c} 
$L_2$[$mH$] & $R_2$[$\Omega$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $R_x$[$\Omega$] & $L_x$[$mH$] & $\Delta R_x$[$\Omega$] & $\Delta L_x$[$mH$] \\
\hline \hline
14,6	& 60 	& 548,7	& 451,3 & 72,95		& 17,75	& 2,22	& 0,1\\
20,1	& 76	& 573		& 427 	& 101,99	& 26,97	& 3,1		& 0,15\\
27,5	& 113 & 497   & 503 	& 111,65	& 27,17	& 3,4		& 0,15\\
\end{tabular}
\caption{Messwerte zur Induktions- und  Verlustwiderstandsbestimmung von Wert 19}
\label{fig:Tab1}
\end{table}

Die erste Messreihe wird aufgrund der hohen Abweichung für die weitere Rechnung nicht berücksichtigt. Dadurch sind die experimentell und rechnerisch ermittelten Eigenschaften der Spule vom Wert 19
\begin{equation}
\begin{align}
L_{19} &= (27,07 \pm 0,11) mH \\
R_{19} &= (106,82 \pm 2,30) \Omega.
\end{align}
\end{equation}

\newpage
\subsection{Bestimmung der Induktivität und des Verlustwiderstandes einer Spule mit der Maxwell-Brücke}
Die bei der Induktionsmessbücke verwendete Spule des Wertes 19 wird nun ein weiteres Mal mit der Maxwell-Brücke verwendet. Hier besitzen $R_3$ sowie $R_4$ eine Eichgenauigkeit von $3$\%, der Widerstand $R_2$ und $C_4$ von $0,2$\%.
Der Widerstand vom Wert 19 berechnet sich aus
\begin{equation}
R_x = \frac{R_2 R_3}{R_4}.
\end{equation}
Der daraus bestimmte absolute Fehler ist
\begin{equation}
\Delta R_x = \sqrt{(\frac{\Delta R_2}{R_2})^2 + (\frac{\Delta R_3}{R_3})^2} + (\frac{\Delta R_4}{R_4})^2 \cdot R_x.
\end{equation}

Um die Induktivität zu bestimmen ist die folgende Gleichung nötig:
\begin{equation}
L_x = R_2 R_3 C_4.
\end{equation}
Der absolute Fehler der Induktivität wird mit dem Spezialfall der Gaußschen Fehlerfortpflanzung bestimmt:
\begin{equation}
\Delta L_x = \sqrt{(\frac{\Delta R_2}{R_2})^2 + (\frac{\Delta R_3}{R_3})^2 + (\frac{\Delta C_4}{C_4})^2} \cdot L_x.
\end{equation}
Die hierbei ermittelten Ergebnisse für die Spule mit Wert 19 sind dargestellt in Tabelle 7.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c||c|c|c|c} 
$C_4$[$nF$] & $R_2$[$\Omega$] & $R_3$[$\Omega$] & $R_4$[$\Omega$] & $R_x$[$\Omega$] & $L_x$[$mH$] & $\Delta R_x$[$\Omega$] & $\Delta L_x$[$mH$] \\
\hline \hline
399	& 1000 	& 66	& 620,8  & 106,31	& 26,33	& 4,52	& 0,79\\
450	& 664		& 89	& 565 	 & 104,59	& 26,59	& 4,44	& 0,8\\
597	& 1000 	& 44  & 417 	 & 105,52	& 26,27	&	4,48	& 0,79\\
\end{tabular}
\caption{Messwerte zur Induktions- und  Verlustwiderstandsbestimmung von Wert 19}
\label{fig:Tab1}
\end{table}

Mithilfe dieser Daten lässt sich die Induktivität und der Widerstand berechnen. Es ergeben sich für diese Größen:
\begin{equation}
\begin{align}
L_{19} &= (26,4 \pm 0,46) mH \\
R_{19} &= (105,47 \pm 2,59) \Omega.
\end{align}
\end{equation}

\newpage
\subsection{Frequenzabhängigkeit von Brückenschaltungen am Beispiel der Wien-Robinson-Brücke}
In diesem Kapitel wird die Frequenzabhängigkeit einer Wien-Robinson-Brücke untersucht. Dabei wurden folgende Bauteile verwendet: $R = 1000 \Omega$, $R^{'} = 332 \Omega$, $2R^{'} = 664 \Omega$ sowie $C = Wert1$ (656,25 nF). Die angelegte Spannung $U_S$ beträgt 2,125 V.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c|c} 
Frequenz [$Hz$] & normierte Frequenz $\Omega$ & $U_{Br}$[$V$] & $\Delta U$ & $\Delta U$ (Theorie) \\
\hline \hline
20		& 0,083 		& 0,728	& 0,343   & 0,323	\\
30		& 0,124			& 0,697	& 0,328 	& 0,312	\\
40		& 0,166 		& 0,663 & 0,312 	& 0,297	\\
50		& 0,207 		& 0,628	& 0,296   & 0,279	\\
60		& 0,249			& 0,588	& 0,276 	& 0,261	\\
70		& 0,290 		& 0,544 & 0,256 	& 0,241	\\
80		& 0,332 		& 0,503	& 0,237   & 0,222	\\
90		& 0,373			& 0,456	& 0,215 	& 0,203	\\
100		& 0,415 		& 0,416 & 0,196 	& 0,185	\\
110		& 0,456 		& 0,375	& 0,176   & 0,167	\\
120		& 0,498			& 0,341	& 0,160 	& 0,150	\\
130		& 0,539 		& 0,303 & 0,143   & 0,134	\\
140		& 0,581 		& 0,269	& 0,126   & 0,118	\\
150		& 0,622			& 0,233	& 0,110 	& 0,104	\\
160		& 0,664 		& 0,205 & 0,096 	& 0,090	\\
170		& 0,705 		& 0,178	& 0,084   & 0,077	\\
180		& 0,747			& 0,150	& 0,071 	& 0,065	\\
190		& 0,788 		& 0,124 & 0,058 	& 0,053	\\
200		& 0,830 		& 0,100	& 0,047   & 0,041	\\
210		& 0,871			& 0,074	& 0,035 	& 0,031	\\
220		& 0,913 		& 0,055 & 0,026 	& 0,020	\\
230		& 0,954 		& 0,028	& 0,013   & 0,010	\\
240		& 0,996			& 0,004	& 0,002 	& 0,001	\\
250		& 1,037 		& 0,021 & 0,010 	& 0,008	\\
260		& 1,079 		& 0,041	& 0,019   & 0,017	\\
270		& 1,120			& 0,056	& 0,026 	& 0,025	\\
280		& 1,162 		& 0,075 & 0,035   & 0,033	\\
290		& 1,203 		& 0,091	& 0,043   & 0,041	\\
300		& 1,245			& 0,108	& 0,051 	& 0,049	\\
350		& 1,452 		& 0,181 & 0,085 	& 0,082	\\
400		& 1,660 		& 0,242	& 0,114   & 0,111	\\
450		& 1,867			& 0,294	& 0,138 	& 0,135	\\
500		& 2,075 		& 0,338 & 0,159 	& 0,156	\\
750		& 3,112 		& 0,491	& 0,231   & 0,227	\\
1000	& 4,149			& 0,569	& 0,268 	& 0,264	\\
1500	& 6,224 		& 0,641 & 0,301 	& 0,299	\\
2000	& 8,299 		& 0,666	& 0,313   & 0,313	\\
2500	& 10,373		& 0,682	& 0,321 	& 0,320	\\
4000	& 16,598 		& 0,697 & 0,328 	& 0,328	\\
7000	& 29,046 		& 0,700	& 0,329   & 0,332	\\
10000	& 41,494		& 0,707	& 0,332 	& 0,332	\\
20000	& 82,988 		& 0,707 & 0,332 	& 0,333	\\
30000	& 124,481 	& 0,707	& 0,332   & 0,333	\\
\end{tabular}
\caption{Frequenzabhängigkeit der Wien-Robinson-Brücke}
\label{fig:Tab1}
\end{table}

Die in Tabelle 8 aufgetragenen Werte sind in Abb. 6 zum Vergleich graphisch dargestellt. $\Delta U$ in Tabelle 8 bezeichnet den Quotienten $\frac{U_{Br}}{U_{S}}$, der nach Gleichung [Gl.(2.20)] bestimmt wird. $v_0$ ist die Frequenz, bei der die Brückenspannung verschwindet. Laut Experiment beträgt sie
\begin{equation}
v_0 = 240 Hz.
\end{equation}
Der theoretisch ermittelte Wert beträgt
\begin{equation}
v_0 = \frac{w_0}{2 \pi} = \frac{1}{2 \pi R C} = 242,52 Hz.
\end{equation}


\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild6.png}}
\caption{Mess- und Theoriewerte zur Frequenzabhängigkeit der Wien-Robinson-Brücke}
\label{fig:Abbildung}
\end{figure}

\subsection{Bestimmung des Klirrfaktors}
Der Klirrfaktor $k$ ist ein Maß für das Verhältnis von Oberwellen bezüglich der Grundwelle. Er ist definiert als
\begin{equation}
k = \frac{\sqrt{U_2^2+U_3^2+...}}{U_1}.
\end{equation}
Desweiteren gilt
\begin{equation}
U_2 = \frac{U_{Br}}{f(2)} = \frac{U_{Br}(\Omega=1)}{{\sqrt{\frac{1}{9}\frac{(2^2-1)^2}{(1-2^2)^2+9 \cdot 2^2}}}}.
\end{equation}
Mit $U_{Br}$ = 0,004297V und $U_2$ = 0,028825V ergibt sich für den Klirrfaktor
\begin{equation}
k = \frac{U_2}{U_1} = 0,0136 = 1,36 \%.
\end{equation}

\newpage
\section{Diskussion}
Die Messungen konnten mit einer hohen Genauigkeit durchgeführt werden. Bei der Bestimmung der Induktivität und des Widerstandes der Spule sind die Ergebnisse bei der Induktionsmess- und der Maxwell-Brücke nahezu identisch.Die Abweichungen liegen dort bei 2,54 \% bzw. 1,28 \%.
Des Weiteren sind die Frequenzen $v_0$, bei denen die Brückenschaltung der Wien-Robinson-Brücke nahezu 0V beträgt, fast gleich. Dort beträgt die Abweichung 1,05 \%. Auch die experimentellen Werte von $\Delta U$ sind fast gleich den Theoriewerten.
Auch der Klirrfaktor ist mit knapp 1,36 \% gering.
Der Fehler bei einer Messreihe der Induktivität könnte durch ein falsches Ablesen der Potentiometerskala des Widerstandes $R_2$ entstanden sein.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 302, Elektrische Brückenschaltungen
\newline
http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V302.pdf
\end{enumerate}
\end{document}