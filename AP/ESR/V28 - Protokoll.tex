\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{siunitx}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 28}  \\ Protokoll vom \today}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Fortgeschrittenen-Praktikum SS15}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch Nr. 28\\ Elektronenspinresonanz}
\author{Konstantin Pfrang\\ \small{\textit{konstantin.pfrang@tu-dortmund.de}} \and Dennis van Elten\\ \small{\textit{dennis.vanelten@tu-dortmund.de}}}
\date{\today}
\setcounter{page}{-0} 
\maketitle
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Zielsetzung}
\flushleft
Durch den Bahndrehimpuls erzeugen die Hüllenelektronen eines Atoms magnetische Momente. Allerdings zeigen viele Experimente, dass auch Hüllenelektronen mit verschwindendem Bahndrehimpuls und freie Elektronen ein magnetisches Moment besitzen, was das Einführen eines Eigendrehimpulses des Elektrons, dem Spin, zur Folge hat. Im Laufe dieses Versuches soll der Zusammenhang zwischen dem magnetischen Moment und dem Spin untersucht werden, indem Methoden der Hochfrequenz-Spektroskopie verwendet werden.
\section{Theoretische Grundlagen}
\subsection{Zusammenhang zwischen Bahndrehimpuls und Magnetischem Moment}
Für Atome in der Einelektronennäherung lässt sich die Wellenfunktion $\Psi$ in Polarkoordinaten schreiben als
\begin{align}
	\Psi_{\text{n,l,m}}(r,\theta,\phi)=\text {R}_{\text{n,l}}(r)\cdot\Theta_{\text{l,m}}(\theta)\cdot\Phi(\phi),
\end{align}
wobei n die Hauptquantenzahl, l die Bahndrehimpulsquantenzahl und m die Orientierungsquantenzahl darstellt. R ist die Radial-, $\Theta$ die Breitenkreis- und $\Phi$ die Azimutalabhängigkeit der Wellenfunktion. Alle diese Funktionen sind normiert.\\
Da sowohl R als auch $\Theta$ reelle Funktionen sind, bleibt für die Stromdichte
\begin{align}
	\vec{S}=\frac{\hbar}{2 i m_0}(\Psi^*\nabla \Psi-\Psi\nabla \Psi^*)
\end{align}
nur die azimutale Teilchenstromdichte 
\begin{align}\label{strom}
	S_\phi =\frac{\hbar}{m_0}\frac{\text{R}^2\Theta^2}{2\pi}\frac{m}{r\sin\theta}.
\end{align}
Aus Gleichung \ref{strom} erhält man durch Multiplikation der Elementarladung die elektrische Stromdichte
\begin{align}
	j_\phi = - e_0 S_\phi
\end{align}
Hieraus ergibt sich das infinitesimale magnetische Moment als Produkt des Kreisstroms und der umlaufenen Fläche
\begin{align}
	\text{d}\mu_z = F(\theta) \text{d}I_\phi
\end{align}
wobei die umlaufenen Fläche durch $F(\theta)=\pi r^2 \sin^2\theta$ und der Strom durch ein Flächenelement durch $\text{d}I=j_\phi \text{d}f$ gegeben sind.
Hieraus ergibt sich durch Integration über die Querschnittfläche unter Berücksichtigung der Normierungsbedingungen das magnetische Moment
\begin{align}
	\mu_z=:\mu_B m.
\end{align}
In dieser Formel bezeichnet $\mu_B$ das Bohrsche Magneton:
\begin{align}
	\mu_B=-\frac{e_0\hbar}{2m_0} =(9,274915\pm0,000003)\cdot 10^{-24}\,\frac{\text{J}}{\text{T}}
\end{align}
\subsection{Richtungsquantelung}
Zusätzlich zu der Quantelung des Drehimpulsbetrags ist auch dessen Richtung gequantelt. Die Orientierungen eines Drehimpulses sind relativ zu einer festgelegten Raumachse wie zum Beispiel der Magnetfeldrichtung so festgelegt, dass die z-Komponente $l_z$ nur ein ganzzahliges Vielfaches von $\hbar$ sein kann:
\begin{align}
	l_z=m_l\hbar
\end{align}
Eine Komponente eines Vektors kann nicht größer sein, als dessen Betrag, daher gilt für die Orientierungsquantenzahl $ -l\leq m_l\leq+l$. Daraus ergeben sich insgesamt $2l+1$ Orientierungen des Drehimpulses, wie beispielhaft in Abbildung \ref{richtung} für $l=1$ dargestellt ist.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{Bilder/Bild1.png}
	\caption{Richtungsquantelung eines Bahndrehimpulses [1]}
	\label{richtung}
\end{figure}
Wie aus der Elektrodynamik bekannt, folgt aus einem magnetischem Moment in einem äußeren Magnetfeld eine potentielle Energie. Dadurch ergibt sich eine Aufspaltung eines Zustandes entsprechend zu
\begin{align}
	\text{E}_{mag}(\text{m}_l)=\mu_z B =\text{m}_l\mu_B B 
\end{align}
wobei wie zuvor erwähnt gilt $\text{m}_l=-l,\cdots,0,\cdots,l$.
In Abbildung \ref{spaltung} ist diese Aufspaltung dargestellt. Die Elektronen in der Einelektronennäherung besitzen ohne magnetische oder elektrische Feldern eine Energie von $E_0$. Durch das Magnetfeld wird dieses Energieniveau in $2l+1$ äquidistante Niveaus aufgespalten. Dies wird auch als Zeeman-Effekt bezeichnet.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{Bilder/Bild2.png}
	\caption{Energieaufspaltung eines Hüllenelektrons mit $l=2$ [1]}
	\label{spaltung}
\end{figure}
\newpage
\subsection{Aufspaltung der Energieniveaus durch den Elektronenspin}
Wie zuerst durch den Stern-Gerlach-Versuch entdeckt, spalten auch Einelektronenatome ohne resultierendem Bahndrehimpuls in einem inhomogenen  Magnetfeld in zwei Teilstrahle auf. Das inhomogene Feld übt eine Kraft
\begin{align}
	\text{F}_z=\mu_{s,z}\frac{\partial B_z}{\partial z}
\end{align}
aus, woraus bereits erwähnte Aufspaltung folgt. Die Spinquantenzahl berechnet sich somit aus
\begin{align}
	2s+1=2
\end{align}
zu $s=\frac{1}{2}$. Daher kann die Orientierungsquantenzahl nur die Werte \begin{align}
	\text{m}_s=-\frac{1}{2}\qquad\text{und}\qquad \text{m}_s=\frac{1}{2}
\end{align}
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{Bilder/Bild3.png}
	\caption{Richtungsquantelung des Elektronenspins mit $l=1/2$ [1]}
	\label{spaltung}
\end{figure}
annehmen. Nun ist es von Interesse, das magnetische Moment $\mu_s$ zum Elektronenspin zu kennen. Für die z-Komponente schreibt man mit dem Bohrschen Magneton
\begin{align}
	\mu_{s_z}=-g \text{m}_s \mu_B=-\frac{1}{2}g \text{m}_s.
\end{align}
In dieser Formel wurde der Landé-Faktor des Elektrons $g$ eingeführt. Dieser berücksichtigt, dass der Zusammenhang zwischen Drehimpuls und dem magnetischen Moment ein andere ist als beim Bahndrehimpuls. Im folgenden Versuch soll das magnetische Moment mithilfe eines Verfahrens der Hochfrequenz-Spektroskopie, das im folgenden Kapitel beschrieben wird, bestimmt werden.
\newpage
\section{Durchführung}
\subsection{Messprinzip}
Eine Substanz mit freien Elektronen wird in ein homogenes Magnetfeld mit der Flussdichte $B$ gebracht wodurch eine Aufspaltung des Energieniveaus $\text{E}_0$ in zwei Niveaus stattfindet:
\begin{align}
	\Delta E = g\mu_B B
\end{align}
Im thermischen Gleichgewicht ist der obere Zustand gemäß der Maxwell-Boltzmann-Statistik etwas schwächer besetzt:
\begin{align}
	\frac{N(m_s=-\frac{1}{2})}{N(m_s=+\frac{1}{2})}=\exp\left(\frac{-g\mu_B B}{k T}\right)
\end{align}
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{Bilder/Bild4.png}
	\caption{Aufspaltung des Energieniveaus [1]}
	\label{spaltung}
\end{figure}
Durch hinzuführen von Energiequanten die genau der Energiedifferenz der Niveaus entspricht, können die Elektronen in den höheren Zustand übergehen, wobei sie ihren Spin umklappen. Dieser Effekt wird als Elektronenspin-Resonanz bezeichnet. Makroskopisch zeigt sich dieser Effekt durch das Absorbieren von Lichtquanten. Die angeregten Elektronen geben anschließend ihre Energie durch Wechselwirkungsprozesse an ihre Umgebung ab.
\subsection{Versuchsaufbau}
Der Aufbau ist schematisch in Abbildung \ref{aufbau} dargestellt. Der quarzstabilisierte HF-Generator erzeugt eine hochfrequente Wechselstrom. Durch die Brückenspannung fließt dieser Strom in eine Hochfrequenz-Spule. In dieser befindet sich die Probe. Durch die Stellelemente R und C wird diese so abgeglichen, dass bei abgeschaltetem Gleichfeld die Brückenspannung, die an deren Ausgang abgenommen wird, fast null ist. Bei abgestimmter Brücke ist diese so gering, dass es zum Messen notwenig ist, diese sehr stark nachzuverstärken. 

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{Bilder/Bild5.png}
	\caption{Schematische Darstellung der Messapparatur [1]}
	\label{aufbau}
\end{figure}

Da es sich um eine empfindliche Messapparatur handelt, ist es wichtig, die auftretenden Störspannungen wirksam zu unterdrücken. Dies wird durch einen Überlagerungsempfänger (Superheterodyn-Empfänger) realisiert. 
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.4]{Bilder/Bild6.png}
	\caption{Blockschaltbild eines Überlagerungsempfängers [1]}
	\label{aufbau}
\end{figure}
Ein auf die Signalfrequenz $\nu_e$ abgestimmter Vorverstärker unterdrückt spannungen mit anderen Frequenzen, während er das Eingangssignal leicht verstärkt. Durch Überlagerung mit einer zweiten Spannung der Frequenz $\nu_{\text{Osz}}$ entstehen Schwebungserscheinungen mit der Schwebungsfrequenz
\begin{align}
	\nu_{ZV}=\nu_e-\nu_{\text{Osz}} \qquad (\nu_e > \nu_{\text{Osz}})
\end{align}
Bei dem ZF-Verstärker handelt es sich um einen Selektivverstärker. Durch mehrerer Schwingkreise hoher Güte, lässt sich die Frequenz $\nu_{ZF}$ verstärken. Dieser Verstärker ist maßgeblich für die Unterdrückung unerwünschter Frequenzen und die Verstärkung verantwortlich. Die Demodulationsstufe wird für die Gleichrichtung benötigt.\\
Mithilfe des Rampengenerators wird eine zeitproportionale Spannung erzeugt, die eine Variation des durch die Helmholtzspule erzeugten Magnetfeldes ermöglicht. Ist die Resonanzfeldstärke erreicht, ändert sich der komplexe Widerstand der Probe, was dazu führt, dass sich die Abgleichung der Brückenspannung ändert. Diese Brückenspannung wird mithilfe eines XY-Schreibers gegen die Spannung des Rampengenerators aufgetragen.
\subsection{Versuchsdurchführung}
Zunächst wird die Helmholtzspule zusammen mit der HF-Spule und der  zu messenden Probe einmal parallel und einmal antiparallel zum Erdmagnetfeld ausgerichtet. Hiermit können Störfaktoren vermieden werden, die berücksichtigt werden müssen, da mit geringen Magnetfeldern gearbeitet wird. Gemessen wird für fünf verschiedene Eingangsfrequenzen jeweils parallel und antiparallel. Dabei wird folgendermaßen vorgegangen: Die Messfrequenz $\nu_e$ wird zunächst grob in der Nähe der vorgegebenen Oszillatorfrequenzen $\nu_{Osz}$ eingestellt. Zusätzlich muss der entsprechende Frequenzbereich durch Kippschalter am Überlagerungsempfänger und an der Messapparatur eingestellt werden. Die Frequenz wird nun geändert, so dass eine Brückenspannung zu vermerken ist. Diese Eingangsfrequenz ist die Frequenz, die verwendet wird, um die Resonanzstelle der Probe zu finden. Anschließend wird die Brückenspannung abgeglichen, indem man zunächst mit dem Kondensator $C_{\text{grob}}$ verändert, bis eine möglichst geringe Brückenspannung beobachtet wird. Hierzu wird die ZF-Verstärkung nachgeregelt um eine möglichst genaue Messung zu ermöglichen. Ist eine kleine Spannung vorhanden, so wird mit dem Vorverstärker ein maximaler Wert eingestellt, und anschließend durch die Stellglieder $C_{\text{fein}}$ und $R$ die Brücke abschließend abgeglichen. Anschließend wird diese nun wieder leicht verstimmt. Dies dient dazu, dass es möglich ist, anschließend die erwünschte Resonanzkurve zu erhalten. Die Resonanzstelle findet man anschließend mithilfe des Rampengenerators. Am XY-Schreiber lässt sich nun die Resonanzkurve aufnehmen. Um diese Auszuwerten ist zusätzlich auch eine Justierung des Schreibers notwendig.

\section{Auswertung}
\subsection{Bestimmung des Landé-Faktors $g$}

Um den Landé-Faktor zu untersuchen müssen zunächst die Resonanzkurven graphisch bestimmt werden. Als nächstes werden mithilfe der neugewonnenen linearen Gleichungen die Stromstärken an den Resonanzstellen bestimmt, aus denen die Flussdichten ausgerechnet werden können. Aus der graphischen Ausgleichsrechnung der Flusdichte in Abhängigkeit von der Frequenz lässt sich letztendlich der Landé-Faktor errechnen.

\subsubsection{Messung der Stromstärken an den Resonanzstellen für verschiedene Eingangsfrequenzen $f_E$}

Zunächst werden für verschiedene Eingangsfrequenzen die Stromstärken gemessen. Die vom XY-Schreiber aufgezeichneten Messdaten sind in Abb. 1 und Abb.2 dargestellt. Der erste Messpunkt der jeweils 5 Messpaare wurde dabei an Position Null gesetzt.

\begin{figure}[h!]
	\centering
\begin{minipage}[hbt]{0.24\textwidth}
		\begin{tabular}{c c}
			\toprule[1.5pt] 
			Position & Stromstärke  \\
			 $x$ / cm & $I$ / mA \\
			\midrule 
			0,0&	-62\\
			2,4&	+35\\
			4,4&	+124\\
			7,5&	+261\\
			11,5&	+437\\
			13,3&	+521\\
			\bottomrule[1.5pt] 
		\end{tabular}
\end{minipage}
\hspace{1.5 cm}
\begin{minipage}[hbt]{0.24\textwidth}
	\begin{tabular}{c c}
		\toprule[1.5pt] 
		Position & Stromstärke  \\
		$x$ / cm & $I$ / mA \\
		\midrule 
		0,0&	-708\\
		3,0&	-589\\
		4,5&	-528\\
		8,0&	-382\\
		9,9&	-310\\
		12,8&	-195\\
		\bottomrule[1.5pt] 
	\end{tabular}
\end{minipage}
\hspace{1.5 cm}
\begin{minipage}[hbt]{0.24\textwidth}
	\begin{tabular}{c c}
		\toprule[1.5pt] 
		Position & Stromstärke  \\
		$x$ / cm & $I$ / mA \\
		\midrule 
		0,0&	-795\\
		2,9&	-680\\
		5,5&	-579\\
		8,6&	-457\\
		10,9&	-367\\
		12,5&	-299\\
		\bottomrule[1.5pt] 
	\end{tabular}
\end{minipage}
	\caption{Aufgenommene Stromstärken und ihre Positionen bei einer Eingangsfrequenz (von links) $f_E$ = \SI{10,549}{MHz}, \SI{15,935}{MHz} sowie \SI{20,546}{MHz}}
\end{figure}

\begin{figure}[h!]
	\centering
	\begin{minipage}[hbt]{0.24\textwidth}
		\begin{tabular}{c c}
			\toprule[1.5pt] 
			Position & Stromstärke  \\
			$x$ / cm & $I$ / mA \\
			\midrule 
			0,0&	-856\\
			2,0&	-781\\
			3,8&	-710\\
			6,8&	-591\\
			9,5&	-488\\
			12,6&	-362\\
			\bottomrule[1.5pt] 
		\end{tabular}
	\end{minipage}
	\hspace{1.5 cm}
	\begin{minipage}[hbt]{0.24\textwidth}
		\begin{tabular}{c c}
			\toprule[1.5pt] 
			Position & Stromstärke  \\
			$x$ / cm & $I$ / mA \\
			\midrule 
			0,0&	-821\\
			1,8&	-754\\
			3,2&	-697\\
			5,3&	-616\\
			8,1&	-507\\
			12,3&	-343\\
			\bottomrule[1.5pt] 
		\end{tabular}
	\end{minipage}
	\caption{Aufgenommene Stromstärken und ihre Positionen bei einer Eingangsfrequenz von $f_E$ = \SI{25,000}{MHz} (links) und \SI{29,391}{MHz} (rechts)}
\end{figure}

\flushleft Anschließend werden für die aufgenommenen Daten der jeweiligen Eingangsfrequenzen $f_E$ Ausgleichsgeraden der Form f(x) = ax+b mithilfe des Programms Python3 angefertigt, die die Stromstärke in Abhängigkeit von der Position beschreiben. Die Funktionen lauten beginnend mit der kleinsten $f_E$:
\begin{align}
I_{\approx 15 MHz} &= (40,24 \pm 0,24)\; \frac{\text{mA}}{\text{cm}} \cdot x + (-708,18 \pm 1,82)\; \text{mA} \\
I_{\approx 20 MHz} &= (39,49 \pm 0,18)\; \frac{\text{mA}}{\text{cm}} \cdot x + (-795,41 \pm 1,44)\; \text{mA} \\
I_{\approx 25 MHz} &= (39,21 \pm 0,19)\; \frac{\text{mA}}{\text{cm}} \cdot x + (-858,11 \pm 1,41)\; \text{mA} \\
I_{\approx 30 MHz} &= (39,94 \pm 0,12)\; \frac{\text{mA}}{\text{cm}} \cdot x + (-822,24 \pm 0,76)\; \text{mA}. 
\end{align}

Für die späteren Berechnungen wurden aus Konventionsgründen die Vorzeichen der soeben genannten Funktionen geändert. Die Messreihe der Stromstärken für die Eingangsfrequenz $f_E$ = \SI{10,549}{MHz} wurde außer Acht gelassen, da diese eine sehr hohe Abweichung aufweisen. Dadurch wird der in diesem Experiment ermittelte Wert des Landé-Faktors exakter. 


\subsubsection{Bestimmung der Resonanzstromstärken} 

Um aus den zuvor bestimmten Funktionen die Flussdichten an den Resonanzstellen zu bestimmen werden im nächsten Schritt die Stromstärken an den Resonanzpositionen für parallele sowie antiparallele Magnetfeldlinien bestimmt. Der Fehler der Resonanzstromstärken berechnet sich nach folgender Formel:
\begin{align}
\sigma_I = \sqrt{x^2 \sigma^2_a + a^2 \sigma^2_x + \sigma^2_b}.
\end{align}	
Der Fehler $\sigma_x$ beschreibt in der Formel den Ablesefehler der Resonanzpositionen und wird als \SI{0,1}{cm} angenommen. Die Resultate sind in der nachstehenden Tabelle dargestellt. Dabei wurden die Abkürzungen $p$ und $ap$ für parallel und antiparallel verwendet. Die erste Spalte bezeichnet die Eingangsfrequenz, darauf folgend die beiden Resonanzpositionen (parallel und antiparallel). Anschließend sind in der Tabelle die dortigen Stromstärken sowie deren Fehler angegeben.

\begin{table}[htbp]
	\centering
	\begin{tabular}{c c c c c c c}
		\toprule[1.5pt]
		$f_E$ / MHz & $x_p$ / cm & $x_{ap}$ / cm & $I_p$ / mA & $\sigma_{I_{p}}$ / mA & $I_{ap}$ / mA & $\sigma_{I_{ap}}$ / mA \\
		\midrule
		15,935 & 9,0 & 6,5 & 346 & 5 & 447 & 5\\
		20,546 & 5,8 & 8,5 & 566 & 4 & 460 & 4\\
		25,000 & 4,0 & 7,0 & 701 & 4 & 584 & 4\\
		29,391 & 1,0 & 3,2 & 783 & 4 & 698 & 4\\
		\bottomrule[1.5pt] 
	\end{tabular}
	\caption{Die Stromstärken der Resonanzstellen für parallel und antiparallel ausgerichtetes Magnetfeld der Helmholtzspule relativ zum Erdmagnetfeld.}
\end{table}

\subsubsection{Bestimmung der Resonanzflussdichten}

Mit den im vorherigen Abschnitt bestimmten Resonanzstromstärken lassen sich die magnetischen Flussdichten an diesen Positionen bestimmen. Diese bestimmen sich aus der Funktion der Helmholtzspule gemäß
\begin{align}
B = \frac{8}{\sqrt{125}} \mu_0 \frac{n}{r} \cdot I.
\end{align}
Dabei sind die Wicklungszahl $n$ = \SI{156}{} und der Spulenradius $r$ = \SI{10}{cm} laut Versuchsanordnung gegeben.

\begin{table}[htbp]
	\centering
	\begin{tabular}{c c c c c c c c}
		\toprule[1.5pt]
		$f_E$ / MHz & $B_p$ / \SI{}{\mu}T & $\sigma_{B_{p}}$ / \SI{}{\mu}T & $B_{ap}$ / \SI{}{\mu}T & $\sigma_{B_{ap}}$ / \SI{}{\mu}T & $\bar{B}$ / \SI{}{\mu}T & \SI{}{\Delta}B / \SI{}{\mu}T & $\sigma_{\bar{B} / \Delta B}$ / \SI{}{\mu}T \\
		\midrule
		15,935 & 485 & 7 & 627 & 7 & 556 & 71 & 4\\
		20,546 & 794 & 6 & 645 & 6 & 720 & 75 & 4\\
		25,000 & 984 & 6 & 819 & 6 & 902 & 83 & 4\\
		29,391 & 1099 & 6 & 979 & 6 & 1039 & 60 & 4\\
		\bottomrule[1.5pt] 
	\end{tabular}
	\caption{Die magnetischen Flussdichten der Resonanzstellen für parallel und antiparallel ausgerichtetes Magnetfeld sowie das errechnete Erdmagnetfeld.}
\end{table}

Die dabei neugewonnenen Größen $\bar{B}$ und $\Delta B$ berechnen sich nach den folgenden Gleichungen, wobei auf die Größe $\Delta B$ im nächsten Kapitel eingegangen wird.
Es gilt:
\begin{align}
\bar{B} = \frac{B_p + B_{ap}}{2}.
\end{align}
Bei der parallelen Flussdichte wird das Erdmagnetfeld aufaddiert, während es bei der antiparallelen Ausrichtung subtrahiert werden muss. Insgesamt wird das Erdmagnetfeld so substituiert. Es handelt sich bei der Größe $\bar{B}$ somit um das mittlere magnetische Feld der Helmholtzspule. Der Fehler dieser Größe wird bestimmt nach 
\begin{align}
\sigma_B = \frac{1}{2} \sqrt{\sigma^2_{B_p} + \sigma^2_{B_{ap}}}
\end{align}

In Abbildung 9 sind die mittleren magnetischen Feldflussdichten in Abhägigkeit der Eingangsfrequenz $f_E$ graphisch dargestellt.

\begin{figure}[h!]
	\centering{\includegraphics[width=13cm]{Bilder/Ausgleichsgerade.png}}
	\caption{Addierte Magnetflussdichten der Resonanzstellen in Abhängigkeit der Eingangsfrequenz}
	\label{fig:Abbildung}
\end{figure}

Die ermittelte Ausgleichsgerade ist eine Ursprungsgerade f(x) = mx und besitzt daher als zu bestimmenden Parameter nur die Steigung $m$. Diese lautet
\begin{align*}
m = (3,54 \pm 0,01) \cdot 10^{-5}\, \text{Ts}.
\end{align*}

Mittels der Relation $h f_E = g \mu_B B$, die die notwendige Energie der Photonen für die Elektronenspinresonanz  beschreibt, und dem gewonnenen Wert der Steigung $m = \frac{B}{f_E}$ lässt sich der Landé-Faktor bestimmen. Es gilt:
\begin{align}
g = \frac{h}{\mu_B m}
\end{align}

Es folgt daraus unter der Annahme, dass der relative Fehler der Steigung auch dem des Landé-Faktors entspricht, für $g$:
\begin{align*}
g = (2,0161 \pm 0,0054).
\end{align*}

\subsection{Bestimmung des Erdmagnetfeldes}

Für die experimentelle Bestimmung des örtlichen Erdmagnetfeldes werden die Daten aus Tabelle 2 genutzt, genauer die Größe $\Delta B$. Diese berechnet sich nach
\begin{align}
\Delta B = \frac{|B_{ap} - B_{p}|}{2}
\end{align}
und besitzt denselben Fehler wie schon $\bar{B}$. Es folgt gemittelt für das Erdmagnetfeld der Wert
\begin{align*}
	B_{Erde} = (72,0 \pm 2,0)\, \text{$\mu$T}.
\end{align*}

\newpage
\section{Diskussion}

Zur Übersicht sind die Ergebnisse in einer Tabelle zusammengefasst.

\begin{table}[htbp]
	\centering
	\begin{tabular}{c c c c}
		\toprule[1.5pt]
		phys. Größe & exp. Wert & theo. Wert & rel. Abweichung \\
		\midrule
		$g$ & 2,0161 $\pm$ 0,0054 & 2,0023... [2] & 0,7 \% \\
		$B_{Erde}$ & 72,0 $\pm$ 2,0 &  $\sim$ 30 bis 60 \text{$\mu$T} [3] & 20 \% \\
		\bottomrule[1.5pt] 
	\end{tabular}
	\caption{Übersicht der experimentell bestimmten und theoretischen Werte des Landé-Faktors für Elektronen sowie des Erdmagnetfeldes. Ebenfalls sind die relativen Abweichungen berechnet.}
\end{table}

Bei der Messung des Landé-Faktors der Elektronen stimmt der Wert ziemlich genau mit dem theoretischen Modell überein, so dass bei der Bestimmung keine Fehler auszumachen sind. Eine kleine Unsicherheit existiert trotzdem, da das Magnetfeld nicht exakt parallel und antiparallel eingestellt sein kann. Bei der Messung des Erdmagnetfeldes wurde die relative Abweichung zum nächstgelegenden Literaturwert bestimmt, in diesem Fall zu 60 \text{$\mu T$}. Eine mögliche Fehlerquelle für die Verfälschung können die weiteren Laborapparaturen sowie das viele verbaute Pb sein. Jedoch lässt sich aufgrund der korrekten Größenordnung des experimentellen Erdmagnetfeldes sowie dessen gerininger Fehler aussagen, dass die Messung unter den oben genannten Nebeneffekte gut funktioniert hat.


\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Fortgeschrittenenpraktikum: Skript zum Versuch Nummer 505, "Der Elektronspin-Resonanz"
\item Eintrag zum Landé-Faktor im Universal-Lexikon, \url{http://universal_lexikon.deacademic.com/144745/Land%C3%A9-Faktor}
\item Eintrag zum Erdmagnetfeld von www.uni-protokolle.de, \url{http://www.uni-protokolle.de/Lexikon/Dynamotheorie.html}
\end{enumerate}
\end{document}

