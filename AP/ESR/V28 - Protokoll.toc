\select@language {ngerman}
\contentsline {section}{\numberline {1}Zielsetzung}{1}{section.1}
\contentsline {section}{\numberline {2}Theoretische Grundlagen}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Zusammenhang zwischen Bahndrehimpuls und Magnetischem Moment}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Richtungsquantelung}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Aufspaltung der Energieniveaus durch den Elektronenspin}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Messprinzip}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Versuchsaufbau}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Versuchsdurchf\IeC {\"u}hrung}{6}{subsection.3.3}
\contentsline {section}{\numberline {4}Auswertung}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Bestimmung des Land\IeC {\'e}-Faktors $g$}{7}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Messung der Stromst\IeC {\"a}rken an den Resonanzstellen f\IeC {\"u}r verschiedene Eingangsfrequenzen $f_E$}{7}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Bestimmung der Resonanzstromst\IeC {\"a}rken}{8}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Bestimmung der Resonanzflussdichten}{9}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Bestimmung des Erdmagnetfeldes}{10}{subsection.4.2}
\contentsline {section}{\numberline {5}Diskussion}{11}{section.5}
\contentsline {section}{\numberline {6}Literaturverzeichnis}{11}{section.6}
