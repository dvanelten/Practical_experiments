\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}

\begin{document}

\title{Versuch103	-     Biegung elastischer Stäbe}
\author{Konstantin Pfrang \and Dennis van Elten}
\maketitle
\newpage

\tableofcontents

\newpage


\section{Ziel}
Das Ziel des Versuches ist es, das Elastizitätsmodul E verschiedener Metalle zu bestimmen. Das Elastizitätsmodul bezeichnet den Proportionalitätsfaktor zwischen einer senkrecht zur Oberfläche stehenden Kraft und der relativen Längenänderung. Dieser Zusammenhang nennt sich Hook'sches Gesetz und charakterisiert die in der Technik wichtige Materialkonstante, das Elastizitätsmodul.

\section{Theorie}
\subsection{Grundlagen}
Wirkt auf einen Körper eine Kraft, so kann diese an dem Körper Gestalt- und Volumenänderungen verursachen. Die Kraft die pro Fläche auf den Körper einwirkt wird als Spannung bezeichnet. Diese Spannung lässt sich in einen zur Oberfläche parallelen Teil, der Schubspannung, und in einen dazu senkrechten, der Normalspannung $\sigma$, unterteilen. 
Bei ausreichend kleiner Auslenkung besteht ein linearer Zusammenhang  zwischen der Normalspannung und der relativen Längenänderung, der durch das Hook'sche Gesetz beschrieben ist:
\begin{align}
\sigma = E \frac{\Delta{L}}{L}
\end{align}
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Dehnung einer stabförmigen Probe unter dem Einfluss einer Normalspannung}
\label{fig:Abbildung}
\end{figure}

Da eine direkte Messung der Längenänderung $\Delta L$ in der Regel wegen geringer Auslenkung nicht möglich ist, wird eine alternative Messung zur Bestimmung des Elastizitätsmoduls benutzt. Es werden 3 Messreihen durchgeführt aus denen die Elastizität der Probestäbe durch die Biegung von Stäben bestimmt werden kann.

\subsection{Einseitige Fixierung}
Ein Stab wird einseitig fixiert und am freien Ende wie in Abbildung 2 eine Kraft durch eine angehängte Masse $m$ ausgeübt, was zu einer Durchbiegung $D(X)$ des Stabes in Abhängigkeit von dem Abstand $x$ führt.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild2.png}}
\caption{Durchbiegung eines einseitig fixierten Stabes}
\label{fig:Abbildung}
\end{figure}
Dabei entsteht ein Drehmoment $M_F$, das den Stab aus der Ruhelage auslenkt. Bei der Biegung des Stabes wird die untere Seite gestaucht und die obere Seite des Stabes gedehnt. Die dadurch entstehenden Zug- und Druckspannungen sind vom Betrag gleich, jedoch im Vorzeichen entgegengesetzt, wodurch sie ein Drehmoment $M_\sigma$ erzeugen. Der Stab wird dann so weit ausgelenkt, bis sich ein Gleichgewicht zwischen diesen beiden Drehmomenten ergibt: 
\begin{align}
M_F = M_\sigma
\end{align}
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild3.png}}
\caption{Berechnung des Drehmoments $M_\sigma$}
\label{fig:Abbildung}
\end{figure}

$M_F$ ist dabei gegeben durch die Formel
\begin{align}
M_F=F(L-x)
\end{align}
und das Drehmoment im Stab $M_\sigma $ durch
\begin{align}
M_\sigma=\int\limits_Q{y\sigma(y)}dq
\end{align}
In dieser Formel bezeichnet y den Abstand des Flächenelements $dq$ von der neutralen Faser. Als neutrale Faser wird die Fläche bezeichnet, die auch bei Biegung des Stabes ihre ursprüngliche Länge beibehält. In Abbildung 3 ist diese als gestrichelte Linie eingezeichnet.

\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild4.png}}
\caption{Skizze zur Normalspannung $\sigma (y)$ im gebogenen Stab}
\label{fig:Abbildung}
\end{figure}
Aus Abbildung 4 lassen sich folgende Näherungen herleiten:
\begin{align}
\delta X = y \frac{\Delta x}{R}
\end{align}
und
\begin{align}
\frac{1}{R}\approx \frac{d^2D}{dx^2}
\end{align}
Mit Hilfe des Hook'schen Gesetzes (Gl. 1), sowie diesen Näherungen, ergibt sich aus dem Gleichgewicht $M_F=M_\sigma$ die Gleichung für die Durchbiegung bei einseitiger Fixierung:
\begin{align}
D(x)=\frac{F}{2E\textbf{I}}\left(Lx^2-\frac{x^3}{3}\right)
\end{align}
Dabei entspricht $\textbf{I}$ dem Flächenträgheitsmoment des Stabes und berechnet sich durch
\begin{align}
\textbf{I}=\int\limits_Q{y^2}dq(y).
\end{align}

\subsection{Zweiseitige Fixierung}
In dieser Messung wird der Stab wie in Abbildung 5 gezeigt, an beiden Enden aufgelegt und durch eine Masse, die in der Stabmitte angehängt wird, ausgelenkt.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild5.png}}
\caption{Skizze zur Durchbiegung bei beidseitiger Auflage}
\label{fig:Abbildung}
\end{figure}
Nun ergibt sich für das äußere Drehmoment $M_F$ für $0\leq x\leq\frac{L}{2}$:
\begin{align}
M_F=-\frac{F}{2}x
\end{align}
und für $\frac{L}{2}\leq x \leq L$
\begin{align}
M_F=-\frac{F}{2}(F-x)
\end{align}
Entsprechend dem Vorgehen im vorherigen Kapitel ergibt sich die Durchbiegung $D(x)$ des Stabes in Abhängigkeit vom Ort $x$. Dabei unterscheiden sich die Formeln auf der linken und der rechten Seite des Stabes:\\Links vom Mittelpunkt, also für $0\leq x \leq\frac{L}{2}$
\begin{align}
D(x)=\frac{F}{48E\textbf{I}}\left(3L^2x-4x^3\right)
\end{align}
und auf der rechten Seite, für $\frac{L}{2}\leq x \leq L$
\begin{align}
D(x)=\frac{F}{48E\textbf{I}}\left(4x^3-12Lx^2+9L^2x-L^3\right)
\end{align}
\newpage

\section{Durchführung}

Die in Abbildung 6 skizzierte Apparatur wird verwendet, um die Auslenkung $D(x)$ an der Stelle $x$ aus der Ruhelage zu messen.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild6.png}}
\caption{Skizze der Messvorrichtung für elastisch gebogene Stäbe}
\label{fig:Abbildung}
\end{figure}
Dabei wird für die ersten Messungen bei einseitiger Fixierung der Stab mit Hilfe der Spannvorrichtung C eingespannt. Für die zweiseitige Fixierung wird der Stab auf die Auflagepunkte A und B gelegt.

Es werden zwei Messungen zur einseitigen Einspannung durchgeführt, eine mit einem runden Aluminiumstab und eine mit einem rechteckigen Messingstab. Darauf folgend eine Messung des rechteckigen Messingstabes mit beidseitiger Auflage.

Zunächst werden die Stäbe wie bereits beschrieben in der Apparatur befestigt und anschließend ein Gewicht so gewählt, dass die maximale Auslenkung ausreichend groß ist, jedoch weniger als 7 mm beträgt. Dann werden an verschiedenen Stellen $x$ die verschiebbaren Messuhren zunächst auf Null gestellt und anschließend die Auslenkung an dieser Stelle mit angehängtem Gewicht gemessen.

Außerdem werden die Länge $L$, der Durchmesser $d$ und die Masse $m$ des Stabes bestimmt.


\newpage
\section{Auswertung}

\subsection{Fehlerrechnung}

Für die Auswertungen werden einige zentrale Formeln zur Fehlerrechnung benötigt. Zum einen berechnet sich das arithmetische Mittel $\overline{x}$ für $N$ Messdaten $x_i$ mithilfe der Gleichung
\begin{align}
\overline{x} = \frac{1}{N}\sum_{i=1}^N{$x_i$}
\end{align}

Ebenso notwendig ist der Fehler des Mittelwertes $\Delta\overline{x}$, der sich mit
\begin{align}
\Delta\overline{x} = \frac{1}{n(n-1)}\sqrt{\sum_{i=1}^N{(x_i}-{\overline{x})^2}}.
\end{align}
berechnen lässt.
Ist es notwendig den Fehler einer Größe A zu bestimmen, nutzt man die Fehlerfortpflanzung. Im folgenden Protokoll existieren nur Fehler, die durch Division oder Produkte entstehen, so dass der Spezialfall
\begin{align}
\frac{A}{\Delta A} = \pm\sqrt{(\frac{\Delta U}{U})^2 + (\frac{\Delta V}{V})^2}
\end{align}
mit den fehlerbehafteten Größen $U$ und $V$ ausreicht.

\newline

\subsection{Einseitige Einspannung}

Um den Elastizitätsmodul $E$ zu bestimmen benötigt man die Kraft $F$, die Linearisierung $\lambda$, die Durchbiegung $D(x)$ sowie das Flächenträgheitsmoment $I$. Dabei gilt folgender Zusammenhang
\begin{align}
E = \frac{mg}{2aI}.
\end{align}
Der Wert $a$ lässt sich dabei mithilfe der Durchbiegung und der Linearisierung bestimmen. Dazu trägt man diese in einem Diagramm gegeneinander auf und bestimmt die Steigung a. Die Linearisierung $\lambda$ ist dabei gegeben mit 
\begin{align}
\lambda = Lx^2-\frac{x^3}{3}.
\end{align}
Dabei kennzeichnet $x$ den Abstand der Messuhr und $L$ die Stablänge bis zur Einspannung.

\newpage
\subsection{Einseitige Einspannung - Aluminium}

Bei dem verwendeten Aluminiumstab handelt es sich um einen Stab mit runder Querschnittsfläche. Dabei beträgt die Einspannungslänge $L$ = 50cm. Durch Abmessungen ergibt sich für den Durchmesser $d$ = (9.965$\pm 0.004$)mm. Die eingehängte Masse beträgt 521.2g.
Mithilfe des Durchmessers und der Gleichung 
\begin{align}
I_r=\int\limits_Q{y^2}dq = \frac{\pi}{64}d^4
\end{align}
lässt sich das Flächenträgheitsmoment $I$ bestimmen.
Dieses beträgt I = (4.8404\pm$0.0016$)*10{^{-10}} m^{4}.
\begin{flushleft}
Die Ausgleichsgerade in Diagramm 1 wird dabei mit GNUPLOT erzeugt. Die Steigung a beträgt $(69478,1\pm$540.8$)*10{^{-6}}$. Der y-Achsenabschnitt kann dabei mit etwa $112*10^{-6}$m vernachlässigt werden. Somit ergibt sich für den Elastizitätsmodul des Aluminiumstabes $E_{Al}$ = $(76.02\pm0.64)$GNm^{-2}.

Laut Literaturwert besitzt Aluminium einen Modul von etwa 70 GNm^{-2}.
\end{flushleft}


\subsection{Einseitige Einspannung - Messing}

Die Durchführung des Messingstabes ist analog zu der des Aluminiumstabes.
Bei dem verwendeten  Messingstab handelt es sich um einen Stab mit quadratischer Querschnittsfläche. 
Die Einspannungslänge L beträgt auch hier genau 50 cm. Der Durchmesser liegt bei $d$ = (9.976$\pm 0.001$)mm. Für die Messungen wird der Stab mit einer Last von 758.3g belastet.
Die Formel für das Flächenträgheitsmoment wird dabei der quadratischer Querschnittsfläche angepasst. Somit ergibt sich
\begin{align}
I_q=\int\limits_Q{y^2}dq = \frac{1}{12}d^4.
\end{align}
Für den Messingstab beträgt dieser I=(8.2536\pm$0.004$)*10^{-10} m^4.


Die mit GNUPLOT erzeugte Regressionsgerade (Diagramm 2) besitzt die Steigung a = $(49824.8\pm$408.7$) *10^{-6}$. Der y-Achsenabschnitt kann dabei wie bereits zuvor aufgrund seiner geringen Größe unbeachtet bleiben.
Somit ergibt sich für den Elastizitätsmodul des Messingstabes $E_{Al}$ = $(90.44\pm$0.74$) GNm^{-2}$. 
Laut Literaturwert besitzt Messing einen Modul von etwa 78-123 GNm^{-2}.

\newpage
\subsection{Zweiseitige Auflage - Messing}

Für die zweiseitige Auflage wird der Elastizitätsmodul E nach Gleichung (11) bzw. (12). berechnet. Daraus ergibt sich
\begin{align}
E = \frac{mg}{48aI}.
\end{align}
Es wird hierbei derselbe Messingstab mit quadratischem Querschnitt verwendet wie bereits zuvor um die Ergebnisse gut vergleichen zu können. Die angehängte Masse wird nun auf $3522.1$g erhöht und der Stab auf L = 55 cm verlängert. Das Flächenträgheitsmoment $I$ des Messingstabes ist bereits aus der vorherigen Messung bekannt und beträgt I=$(8.2536\pm$0.004$)*10^{-10} m^4$.
Die von der angehängten Masse rechte bzw. linke Stabhälfte besitzen bei der zweiseitigen Auflage des Stabes unterschiedliche Linearisierungen. Für die rechte Seite bleibt
\begin{align}
\lambda = Lx^2-\frac{x^3}{3},
\end{align}
während sich für die linke Hälfte der Faktor verändert. Dieser beträgt nun
\begin{align}
\lambda = 4x^3-12Lx^2+9L^2x-L^3.
\end{align}
Im Anschluss werden die Messdaten mittels der Tabellen und Diagramme 3 und 4 ausgewertet. Mittelt man im nächsten Schritt die beiden Steigungen erhält man als mittlere Steigung $\overline{a}$ = $(2948.36\pm$261.45$) *10^{-6}$.
Bestimmt man daraus nun aus Gleichung (20) den Elastizitätsmodul E des Messingstabes erhält man  E = $(295.81\pm$26.16$) GNm^{-2}$.  

\section{Diskussion}

\textcolor[rgb]{{1,0.41,0.13}{Die Messwerte und Ergebnisse der einseitigen Einspannung beider Stäbe sind sehr gut mit den Literaturwerten vereinbar. Bei der zweiseitigen Auflage hingegen kann der ermittelte Elastizitätsmodul nicht korrekt sein, denn der Messingstab kann keine zwei unterschiedliche Werte der Konstante besitzen. Nach physikalischen Grundlagen müssten die Ergebnisse nahezu identisch sein, abgesehen von systematischen Fehlern. Zurückzuführen sind die Fehler auf eine falsche Bedienung der Apparatur sowie einer fehlerhaften Messuhr. Leichte Erschütterungen könnten ebenso zur Verfälschung beigetragen haben.}
}
\section{Quellenverzeichnis}

[1] Tabelle der verschiedenen Elastizitätsmodule
		http://de.wikipedia.org/wiki/Elastizit%C3%A4tsmodul
		(04.11.13, 21:44 Uhr)
		
[2]	TU Dortmund, Physikalisches Grundpraktikum, Skript V103
		http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V103.pdf
		(04.11.13, 21:45 Uhr)
		
[3]	H.J.Eichler, Das Neue Physikalische Grundpraktikum

\end{document}
