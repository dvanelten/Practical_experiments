\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 408}  \\ Protokoll vom 17. Juni 2014}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{\begin{Large}\textmd{ Versuch 408} \end{Large}\\  Geometrische Optik}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{17. Juni 2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
Im folgenden Versuch wird die Brennweite verschiedener Linsen und Linsensystemen bestimmt. Dies wird auf drei verschiedene Methoden durchgeführt: durch Messung der Gegenstandweite und Bildweite, durch die Methode von Bessel sowie durch die Methode von Abbe. Zusätzlich wird die chromatische Aberration durch die Methode von Bessel für rotes und blaues Licht untersucht.\\
Verifiziert werden dabei das Abbildungsgesetz, aus dem für dünne Linsen die Linsengleichung folgt.

\section{Theoretische Grundlagen}
Linsen bestehen in der Optik meist aus, bezogen auf die Luft, optisch dichteren Materialien. Trifft ein Lichtstrahl auf diese, so wird der Strahl sowohl beim Ein- als auch beim Austritt nach dem Brechungsgesetz gebrochen. Es wird unterschieden zwischen Sammellinsen und Zerstreuungslinsen. Sammellinsen sind mittig dicker als am Rand und bündeln paralleles Licht im Brennpunkt. Außerdem sind die Brennweite $f$ und die Bildweite $b$ bei diesem Typ von Linse positiv und es entsteht ein reelles Bild. Im Gegensatz dazu sind bei einer Zerstreuungslinse die Brennweite $f$ und die Bildweite $b$ negativ und ein virtuelles Bild wird erzeugt.\\In Abbildung 1 sind eine dünne Sammellinse, eine dünne Zerstreuungslinse und eine dicke Sammellinse dargestellt. Für die Konstruktion der Strahlengänge ist es bei der dünnen Linse möglich, die Brechung auf die Mittelebene der Linse zu reduzieren, was bei der dicken Linse nicht möglich ist. Deshalb werden gedanklich zwei brechende Hauptebenen ($H$ und $H'$) eingeführt.\\
\begin{figure}[htbp]
 \includegraphics[scale=0.8]{Bilder/Bild1.jpg}
 \caption{dünne Sammellinse (1), Zerstreuungslinse (2), dicke Sammellinse (3) ${[1]}$}
 \label{fig: Abbildung Nr}
\end{figure}
Für die geometrische Konstruktion des Bildes werde drei Strahlen verwendet: der Parallelstrahl $P$, der Mittelpunktsstrahl $M$ und der Brennpunktstrahl $B$. Der Parallelstrahl läuft parallel zur optischen Achse vom Gegenstand und wird dann an der Mittelebene oder Hauptebene der Linse gebrochen. Anschließend wird dieser zum Brennpunktstrahl. Der Mittelpunktstrahl geht ohne Richtungsänderung durch die Mitte der Linse und ändert dabei seine Richtung nicht. Der Brennpunktstrahl geht durch den Brennpunkt der Linse und wird durch Brechung an der Mittel- oder Hauptebene zum Parallelstrahl.\\
Aus der Bildkonstruktion und den Strahlensätzen folgt das Abbildungsgesetz
\begin{align}
V = \frac{B}{G} = \frac{b}{f}.
\end{align}
Hierbei ist $V$ der Abbildungsmaßstab, $B$ die Bildgröße, $G$ die Gegenstandsgröße, $b$ die Bildweite und $g$ die Gegenstandsweite. Für dünne Linsen folgt aus diesem Gesetz mit der Bildkonstruktion die Linsengleichung
\begin{align}
\frac{1}{f} = \frac{1}{b} + \frac{1}{g}
\end{align}
Für die bei dicken Linsen bereits eingeführten Hauptebenen $H$ und $H'$  werden die Brennweiten, Gegenstandsweiten und Bildweiten zu den jeweiligen Hauptebenen bestimmt, wodurch die Linsengleichung weiterhin gültig ist, was strenggenommen allerdings nur für achsennahe Strahlen gilt. Bei achsenfernen Strahlen liegt der Brennpunkt näher an der Linse als der von achsennahen Strahlen, was als sphärische Aberration bezeichnet wird. Diese sorgt dafür, dass das Bild nicht mehr scharf abgebildet werden kann. Durch Ausblenden der fernen Strahlen mit einer Irisblende kann diese Aberration jedoch vermieden werden. Die chromatische Aberration bezeichnet die unterschiedliche Lage des Brennpunktes für Licht verschiedener Wellenlänge durch Dispersion. So liegt der Brennpunkt von blauem Licht näher an der Linse als der von rotem Licht.\\
Als reziproke Brennweite wird die Brechkraft $D=1/f$ mit der Einheit Dioptrie [$1\mathrm{dpt}=1/\mathrm{m}$] bezeichnet. In einem Linsensystem aus mehreren dünnen Linsen addieren sich diese Brechkräfte:
\begin{align}
D=\sum_i^N D_i.
\end{align}

\section{Durchführung}
Für diese Versuchsreihe wird eine Schiene verwendet, auf der verschiedene Reiter mit der Halogenlampe, dem Gegenstand "Perl L", den Linsen und dem Schirm verschoben werden können. Die Abstände werden auf der Skala der Schiene abgelesen. 
\subsection{Messung der Gegenstandweite und der Bildweite}
Zuerst werden die Reiter mit der Halogenlampe, dem Gegenstand, einer Sammellinse mit bekannter Brennweite ($f=100\,$mm) und der Schirm auf die Schiene gestellt. Für zehn verschiedene Gegenstandsweiten wird der Schirm verschoben, bis ein scharfes Bild auf dem Schirm abgebildet wird. Die entsprechenden Wertepaare für die Gegenstandsweite $g$ und die Bildweite $b$ werden notiert und die daraus errechnete Brennweite wird mit der bekannten Brennweite verglichen. Zusätzlich soll für die Linse das Abbildungsgesetz verifiziert werden, so dass sowohl die Bildgröße $B$ als auch die Gegenstandsgröße $G$ gemessen werden müssen.\\
Danach wird die gleiche Messreihe für eine Linse mit einer unbekannten Brennweite durchgeführt. Die Linse ist flexibel und mit Wasser befüllbar, wodurch die Brennweite durch die eingefüllte Wassermenge variiert werden kann. Diese Linse stellt ein modell für das menschliche Auge dar.
\subsection{Methode von Bessel und chromatische Aberration}
Bei der Methode von Bessel bleibt der Abstand zwischen dem Gegenstand und dem Bild fest. Durch Verschieben der Linse werden zwei Postionen festgehalten, bei denen das Bild am Schirm scharf ist. Da es sich um eine symmetrische Linsenstellung handelt, werden Bildweite und Gegenstandweite vertauscht (siehe Abb.2):
\begin{align}
b_1 = g_2.\\
b_2= g_1.
\end{align}
Für $g > b$ ist das Bild auf dem Schirm verkleinert und für $g<b$ ist es vergrößert. Die Brennweite wird mit dem Abstand zwischen Gegenstand und Bild: $e=g_1 + b_1 = g_2 + b_2$, und dem Abstand zwischen der ersten und zweiten Linsenposition: $d=g_1-b_1 = g_2-b_2$, bestimmt:
\begin{align}
f= \frac{e^2-d^2}{4e}.
\end{align}
Der Abstand $e$ soll mindestens $e=4f$ betragen. Für zehn verschiedene Werte von $e$ werden je zwei Linsenpositionen bestimmt und die nach Formel (3.3) errechnete Brennweite wird mit der bekannten Brennweite der Linse verglichen. \\
Die gleiche Messung wird dann für rotes und blaues Licht zu fünf verschiedenen Werten von $e$ durchgeführt.
\begin{figure}[htbp]
\centering{\includegraphics[width=8cm, height=4.5cm]{Bilder/Bild2.jpg}}
\caption{Abstände nach der Methode von Bressel ${[1]}$}
\label{fig:Abbildung}
\end{figure}
\subsection{Methode von Abbe}
Bei der Methode von Abbe wird eine Zerstreuungslinse mit $f=-100\,\mathrm{mm}$ und eine Sammellinse mit $f=100\,\mathrm{mm}$ auf der Schiene nebeneinander gestellt. Die Linsen werden bei den Messungen dicht beieinander gehalten und ein beliebiger Referenzpunkt $A$ für die Messung von $g'$ und $b'$ ausgewählt. Die Größen werden bei zehn unterschiedlichen Positionen ermittelt. Die Abstände $g'$ und $b'$ werden relativ zum gewählten Referenzpunkt sowie dem Abbildungsmaßstab $V$ gemessen. 
Für die gemessenen Abstände gelten die folgenden Beziehungen (s. Abb. 3):
\begin{align}
g' = g+ h = f \cdot \left( 1 + \frac{1}{V} \right)+ h. \\
b' = b+ h' = f\cdot (1+V) + h'.
\end{align}
Mit $V$, $g'$ und $b'$ können die Brennweiten und die Lage der Hauptebenen bestimmt werden.
\begin{figure}[htbp]
\centering{\includegraphics[width=8cm, height=4.5cm]{Bilder/Bild3.jpg}}
\caption{Abstände nach der Methode von Abbe ${[1]}$}
\label{fig:Abbildung}
\end{figure}
\newpage
\section{Auswertung}
\subsection{Fehlerrechnung}
Für die Berechnung der fehlerbehafteten Messwerte, werden folgende Formeln verwendet.
Der Mittwelwert $\overline{x}$ einer Messreihe mit $n$ Messwerten $x_i$ wird berechnet durch:
\begin{align}
\overline{x} = \frac{1}{n} \sum_{i=1}^n \ x_i.
\end{align}
Mit der Formel
\begin{align}
\Delta \overline{x} = \sqrt{\frac{1}{n(n-1)}\sum_{i=1}^n \cdot (x_i - \overline{x})^2}
\end{align}
wird der Fehler des Mittelwertes $\Delta\overline{x}$ errechnet. Zudem wird noch die Formel 
\begin{align}
r_x= \frac{\delta \overline{x}}{\overline{x}}
\end{align}
verwendet, um den relativen Fehler zu erhalten.
Die Gauß'sche Fehlerfortpflanzung:
\begin{align}
\delta y= \sqrt{\sum_{i=1}^n (\frac{dy}{dx_i})^2 \delta x_i^2}
\end{align}
wird für die Berechnung von einem Fehler y verwendet, der aus fehlerbehafteten Größen besteht.

\newpage
\subsection{Untersuchung einer Linse mit bekannter Brennweite und Verifizierung des Abbildungsgesetzes}
In diesem Abschnitt des Experiments wird über eine Messreihe der Gegenstands- und Bildweiten die Brennweite einer bekannten Linse bestimmt. Die experimentell aufgenommenen Werte sind in der nachstehenden Tabelle aufgelistet.

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
g [m]	&	b [m]	&	f [m]	\\ \hline
0.185	&	0.212	&	0.098790932	\\
0.352	&	0.138	&	0.099134694	\\
0.261	&	0.159	&	0.098807143	\\
0.209	&	0.177	&	0.095836788	\\
0.372	&	0.132	&	0.097428571	\\
0.479	&	0.123	&	0.097868771	\\
0.686	&	0.114	&	0.097755000	\\
0.195	&	0.181	&	0.093869681	\\
0.293	&	0.147	&	0.097888636	\\
0.471	&	0.124	&	0.098157983	\\ \hline
\end{tabular}
\caption{Verifikation der Linsengleichung mit Brennweite $f= 100$ mm}
\label{Tab1}
\end{center}
\end{table}

Die Brennweite wurde über eine Umformung der Linsengleichung bestimmt:
\begin{equation}
f = \frac{b \cdot g}{b + g}.
\end{equation}

Die mittlere Brennweite ergibt sich nach Gl.(4.1), sowie die Standardabweichung nach Gl.(4.2):

\begin{equation}
f = (0.0976 \pm 0.0005) \mathrm{m}.
\end{equation}
Der vom Hersteller angegebene Wert für die Brennweite der Linse lautet $0.1 \, m$. Daraus ergibt sich eine Abweichung von 2,4\% zwischem dem Hersteller- und dem experimentell ermittelten Wert. 

Des Weiteren soll die Messgenauigkeit überprüft werden. Dazu werden die Wertepaare $(g_{i}, b_{i})$ in einem kartesischen Koordinatensystem miteinander verbunden. Es werden $i$ lineare Funktionen gemäß der Form $f(x) = b_i - \frac{b_i}{g_i} \cdot x$ erstellt. Im folgenden Diagramm (Abb.4) sind diese Funktionen dargestellt.
\begin{figure}[htbp]
\centering{\includegraphics[width=13cm, height=8cm]{Bilder/Bild4.png}}
\caption{Genauigkeit der Standardmethode für eine bekannte Linse}
\label{fig:Abbildung}
\end{figure}

Abschließend soll das Abbildungsgesetz überprüft werden: 
\begin{equation}
V = \frac{B}{G} = \frac{b}{g}.
\end{equation}
Dazu wurden zu den ersten 4 Messungen zusätzlich die Bildweiten $B$ sowie einmal die Gegenstandsgröße $G$ gemessen. Zur Verifizierung werden die aufgenommenen Werte in Gl.(2.1) eingesetzt. Die Werte sowie die Ergebnisse sind in der nachstehenden Tabelle erfasst. 

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|c|c|c|c|}
\multicolumn{4}{}{Tab2} \\ \hline
g [m]	& b [m]	&	G [m]  & B [m] & $\mathrm{\frac{B}{G}}$ & $\mathrm{\frac{b}{g}}$ & Abweichung [\%] \\ \hline
0.185	&	0.212	&	0.03 & 0.0322	& 1.067 & 1.146 & 7.40    \\
0.352 & 0.138 & 0.03 & 0.0118	& 0.400 & 0.392 & 2.00		\\
0.261 & 0.159 & 0.03 & 0.0175	& 0.600 & 0.609 & 1.50		\\
0.209 & 0.177 & 0.03 & 0.0250	& 0.833 & 0.847 & 1.68		 \\ \hline
\end{tabular}
\caption{Messwerte zur Überprüfung des Abbildungsgesetzes}
\label{Tab1}
\end{center}
\end{table}

\newpage
\subsection{Untersuchung einer Linse mit unbekannter Brennweite (Wasserlinse)}
In diesem Abschnitt soll die Brennweite einer Linse mit unbekannter Brennweite ermittelt werden. Dazu wurde zuvor eine Linse mit Wasser befüllt. In Tab.3 sind die aufgenommenen Messdaten sowie die dazugehörigen errechneten Brennweiten aufgetragen:

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
g [m]	&	b [m]	&	f [m]	\\ \hline
0.270	&	0.141	&	0.092627737	\\
0.388	&	0.126	&	0.095112840	\\
0.562	&	0.118	&	0.097523529	\\
0.373	&	0.126	&	0.094184369	\\
0.206	&	0.158	&	0.089417582	\\
0.654	&	0.115	&	0.097802341	\\
0.589	&	0.117	&	0.097610482	\\
0.504	&	0.118	&	0.095614148	\\
0.447	&	0.123	&	0.096457895	\\
0.329	&	0.127	&	0.091629386	\\ \hline
\end{tabular}
\caption{Bestimmung der Brennweite einer unbekannten Linse}
\label{Tab1}
\end{center}
\end{table}

Analog wie zuvor bei der Linse mit bekannter Brennweite wird der Mittelwert und die Standardabweichung der errechneten Brennweiten bestimmt. Es ergibt sich:
\begin{equation}
f = (0.0948 \pm 0.0009) \, \mathrm{m}.
\end{equation}

Ebenso wird ein Diagramm zur Überprüfung der Genauigkeit angefertigt.
\begin{figure}[htbp]
\centering{\includegraphics[width=13cm, height=8cm]{Bilder/Bild5.png}}
\caption{Genauigkeit der Standardmethode für eine unbekannte Linse}
\label{fig:Abbildung}
\end{figure}

\newpage
\subsection{Bestimmung der Brennweite nach der Methode von Bessel}

\subsubsection{Linse mit bekannter Brennweite}
In diesem Teil der Experiments soll für die Linse mit bekannter Brennweite, dessen Brennweite bereits nach der Standardmethode bestimmt wurde, diese nun ebenfalls mit der Methode von Bessel ermittelt werden. Die Brennweite der Linse wird über die folgende Beziehung errechnet:
\begin{equation}
f = \frac{e^2 - d^2}{4e}.
\end{equation}

Die gemessenen Werte sowie die jeweils errechneten Brennweiten sind in Tab.4 dargestellt.

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
d [m]	&	e [m]	&	f [m]	\\ \hline
0.371	&	0.620	&	0.099499597	\\
0.264	&	0.529	&	0.099312382	\\
0.426	&	0.670	&	0.099785075	\\
0.514	&	0.750	&	0.099434667	\\
0.153	&	0.450	&	0.099495000	\\
0.078	&	0.410	&	0.098790243	\\
0.313	&	0.570	&	0.099531140	\\
0.395	&	0.640	&	0.099052734	\\
0.460	&	0.699	&	0.099070458	\\
0.481	&	0.720	&	0.099666319	\\ \hline
\end{tabular}
\caption{Bestimmung der Brennweite einer bekannten Linse nach der Methode von Bessel}
\label{Tab1}
\end{center}
\end{table}

Daraus ergibt sich als Wert für die Brennweite der Linse:
\begin{equation}
f = (0.0994 \pm 0.0001) \, \mathrm{m}.
\end{equation}

\subsubsection{Chromatische Aberration}
Neben der Brennweite wurde auch die chromatische Aberration von blauem und rotem Licht untersucht. In den beiden Tabellen 5 und 6 sind die Messdaten und Brennweiten aufgelistet:

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
d [m]	&	e [m]	&	f [m]	\\ \hline
0.215	&	0.490	&	0.098915816	\\
0.299	&	0.560	&	0.100088839	\\
0.346	&	0.600	&	0.100118333	\\
0.287	&	0.550	&	0.100059545	\\
0.391	&	0.640	&	0.100280859	\\ \hline
\end{tabular}
\caption{Chromatische Aberration bei blauem Licht nach Bessel}
\label{Tab1}
\end{center}
\end{table}

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
d [m]	&	e [m]	&	f [m]	\\ \hline
0.350	&	0.604	&	0.100296358	\\
0.299	&	0.560	&	0.100088839	\\
0.208	&	0.490	&	0.100426531	\\
0.414	&	0.660	&	0.100077273	\\
0.150	&	0.450	&	0.100000000	\\ \hline
\end{tabular}
\caption{Chromatische Aberration bei rotem Licht nach Bessel}
\label{Tab1}
\end{center}
\end{table}

Die so ermittelten Brennweiten lauten:
\begin{align}
f_{rot} &= (0.10018 \pm 0.00008) \, \mathrm{m}, \\
f_{blau} &= (0.0999 \pm 0.0002) \, \mathrm{m}.
\end{align}

\subsection{Bestimmung der Brennweite eines Linsensystems nach der Methode von Abbe}
In diesem Teil des Experiments soll aus einem unbekannten Linsensystem die Brennweite sowie die Lage der Hauptachsen bestimmt werden. Um die gesuchten Größen zu ermitteln, wird zum einen $g'$ gegen $(1+\frac{1}{V})$, sowie $b'$ gegen $(1+V)$ in jeweils einem Diagramm aufgetragen. Anschließend wird mithilfe des Programms Gnuplot eine Ausgleichsrechnung durchgeführt. Die Gegenstandsgröße $G$ ist konstant $\mathrm{G = 0.03 \, m}$. In der nachstehenden Tabelle sind die Messdaten und berechneten Werte aufgelistet und in den weiteren Diagrammen graphisch dargestellt:

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
g' [m]	&	b' [m]	&	B [m] & (1+1/V)	& (1+V)\\ \hline
0.723	&	0.297	&	0.008	&	4.75000	&	1.26667\\
0.639	&	0.311	&	0.009	&	4.33333	&	1.30000\\
0.552	&	0.328	&	0.011	&	3.72727	&	1.36667\\
0.490	&	0.330	&	0.013	&	3.30769	&	1.43333\\
0.413	&	0.357	&	0.017	&	2.76471	&	1.56667\\ 
0.292	&	0.408	&	0.027	&	2.11111	&	1.90000\\
0.524	&	0.326	&	0.012	&	3.60870	&	1.38333\\
0.598	&	0.322	&	0.010	&	4.00000	&	1.33333\\
0.325	&	0.405	&	0.024	&	2.27660	&	1.78333\\
0.275	&	0.385	&	0.030	&	2.00000	&	2.00000\\ \hline
\end{tabular}
\caption{Bestimmung der Brennweite eines Linsensystems nach der Methode von Abbe}
\label{Tab1}
\end{center}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=13cm, height=8cm]{Bilder/Bild6.png}}
\caption{Brennweitebestimmung nach Abbe ($g'$)}
\label{fig:Abbildung}
\end{figure}

Die zu der in Abb.6 augetragenen Daten von Gnuplot errechnete Regressionsfunktion lautet:
\begin{equation}
\mathrm{g'(x) = (0.159 \pm 0.003) \, m \cdot (1+\frac{1}{V}) + (0.15 \pm 0.04) \, m}.
\end{equation}

\begin{figure}[htbp]
\centering{\includegraphics[width=13cm, height=8cm]{Bilder/Bild7.png}}
\caption{Brennweitebestimmung nach Abbe ($b'$)}
\label{fig:Abbildung}
\end{figure}

Die von Gnuplot errechnete Funktion der Ausgleichsgeraden zu Abb.7 beträgt:
\begin{equation}
\mathrm{b'(x) = (0.131 \pm 0.023) \, m \cdot (1+V) - (0.04 \pm 0.01) \, m}.
\end{equation}

Jede Auslgeichsfunktion liefert einen Wert für die Brennweite des Systems. Um eine einheitlichen Wert zu erhalten werden diese Werte gemittelt. Der Fehler dieses Mittelwertes berechnet sich nach GL.(4.4) (Gauß'sche Fehlerfortpflanzung). Daraus ergibt für die Brennweite des Systems eine Brennweite von 
\begin{equation}
f = (0.145 \pm 0.0026) \, \mathrm{m}.
\end{equation}

Weiter lauten die resultierenden Werte für die Lage der beiden Hauptebenen:
\begin{align}
h &= -(0.04 \pm 0.01) \mathrm{m}, \\
h' &= (0.15 \pm 0.04) \mathrm{m}.
\end{align}

\section{Diskussion}
Im ersten Abschnitt des Experiments konnte sowohl die Linsengleichung als auch das Abbildungsgesetz verifiziert werden. Bei der Standardmethode wich der experimentell ermittelte Wert nur 2.4 \% vom Herstellerwert ab. Der relative Fehler liegt bei 0.5 \% und steht somit zu einem guten Verhältnis zu der in Abbildung 4 graphisch dargestellten Genauigkeit, die durch den gut zu erkennenden Schnittpunkt der Verbindungslinien gegeben ist. Die Methode nach Bessel liefert zu derselben Linse ähnliches Ergebnis. Die Abweichung beträgt bei dieser Methode 6.4 \% und der relative Fehler 0.11 \%. Dieses Ergebnis weist ebenfalls auf eine recht hohe Messgenauigkeit hin.
\\
\\

Die Brennweite von der Wasserlinse ist unbekannt. Jedoch lässt der relative Fehler von 0.95 \% auf ein gut ermittelten Wert schließen. Diese Annahme wird durch die graphische Darstellung in Abb.5 bekräftigt, denn dort ist wie bei der Linse mit bekannter Brennweite ein Schnittpunkt der Verbindungslinien auszumachen.
\\
\\

Des Weiteren konnte in dem Versuch das Phänomen der chromatischen Aberration nachgewiesen werden. Die Brennweite von blauem Licht verringerte sich wie erwartet, wohingegen sich diese für rotes Licht erhöhte. Für blaues Licht liegt der relative Fehler bei 0.2 \%; bei rotem Licht bei 0.1 \%. Die niedriegen Fehler lassen auf eine hohe Präzession bei der Messung der Werte schließen.
\\
\\

Ungenauer ist jedoch die Methode nach Abbe für Linsensysteme gewesen. Dort beträgt der relative Fehler für die durch das Linsensystem erzeugte Brennweite 17.93 \%. Auch für die beiden Hauptebenen sind die Fehler deutlich höher als im vorherigen Verlauf des Experiments. Für die Hauptebene $h$ ergibt sich ein Fehler von 25 \%; für $h'$ beträgt dieser 26.66 \%. Hier ist eine Fehlerquelle gegeben durch Ablesefehler. Diese treten durch das subjektive Empfinden, wann eine Abbildung scharf bzw. unscharf ist, auf.
\\
\\

In der folgenden Tabelle sind die experimentellen Werte in einer Übersicht dargestellt.

\begin{table}[htbp]
\begin{center}
\begin{tabular}[H]{|c|c|c|c|}
\multicolumn{3}{}{Tab1} \\ \hline
Methode:	&	Standard	&	Bessel & Abbe\\ \hline \hline
f (bekannte Linse) [m]	&	0.0976 \pm \,0.0005	&	0.0936 \pm \,0.0001	& -\\
f (unbekannte Linse) [m]	&	0.0948 \pm \,0.0009	&	-	&	-\\
f_{rot} \mathrm{(chr. Aberration) [m]} & -	&	0.1002 \pm \,0.0001	&	-\\
f_{blau} \mathrm{(chr. Aberration) [m]} & -	&	0.0999 \pm \,0.0002	&	- \\
f (Linsensystem) [m] & - & - & 0.145 \pm \,0.026 \\
Hauptebene h [m] & - & - & -(0.04 \pm 0.01) \\
Hauptebene h' [m] & - & - & 0.15 \pm \,0.04 \\ \hline
\end{tabular}
\caption{Übersicht der experimentellen Ergebnisse}
\label{Tab1}
\end{center}
\end{table}

\newpage
\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 408, Geometrische Optik, \\
http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V408.pdf (22.06.14)
\end{enumerate}
\end{document}


