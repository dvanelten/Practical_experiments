\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{siunitx}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 28}  \\ Protokoll vom \today}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Fortgeschrittenen-Praktikum SS15}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{Versuch Nr. 25\\ Der Stern-Gerlach-Versuch}
\author{Konstantin Pfrang\\ \small{\textit{konstantin.pfrang@tu-dortmund.de}} \and Dennis van Elten\\ \small{\textit{dennis.vanelten@tu-dortmund.de}}}
\date{\today}
\setcounter{page}{-0} 
\maketitle
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Zielsetzung}

Das Ziel des Stern-Gerlach Versuchs ist die Beobachtung der Richtungsquantelung von Elektronen nach Wechselwirkung in einem inhomogenen Magnetfeld. Dieses kann aufgrund seiner Eigenschaften, die in Kapitel 2 erläutert werden, am Beispiel eines Kalium $^{39}K$-Atomstrahls durchgeführt werden.

\section{Theoretische Grundlagen}

Historisch gesehen wurde durch den Stern-Gerlach Versuch der Beweis der Richtungsquantelung von Elektronen nachgewiesen, indem die Aufspaltung eines Silber-Atomstrahls beobachtetet werden konnte. Silberatome besitzen aufgrund des Spins $m_s$\;=\;$\pm\frac{1}{2}$ ihres äußeren Elektrons ein magnetisches Moment. Da sie nur dieses eine äußere Elektron besitzen, verschwindet der Bahndrehmoment. Selbiges gilt für die in dieser Durchführung verwendeten Kaliumatome. Zusätzlich ist Kalium kostengünstiger und die Erzeugung des Atomstrahls erfordert geringere Temperaturen. \newline
Das gesamte magnetische Moment eines Atoms ergibt sich aus dem Bahndrehimpuls $L$, dem Eigendrehimpuls $S$ und dem Kernspin $I$:
\begin{align}
\mu = \mu_L + \mu_s + \mu_I.
\end{align}
Das Magnetische Moment des Kernspins ist hierbei vernachlässigbar klein, von daher ist das magnetische Moment im Grundzustand gegeben durch den Spin des Elektrons:
\begin{align}
\mu_{\vec{S}} = - \frac{e}{2m} g_S \vec{S}.
\end{align}
Hierbei ist $g_s \sim$ 2 der Landé-Faktor der Elektronen. Für die z-Komponente ergibt sich hieraus das magnetische Moment
\begin{align}
\mu_{S_z} = - g_s \cdot \mu_B \cdot m_s
\label{muSz}
\end{align}
mit dem Bohrschen Magneton $\mu_B = \frac{\hbar e}{2 m}$. Auf dieses magnetische Moment $\mu_{S_z}$ wirkt in einem inhomogenen Magnetfeld die Kraft
\begin{align}
F_z = - m \mu_B \frac{\partial B}{\partial z}.
\label{Kraft}
\end{align}
Anhand von \ref{Kraft} wird auch die Notwendigkeit eines inhomogenen Magnetfelds offensichtlich, da andernfalls die Kraft verschwinden würde.

\newpage
\section{Versuchsaufbau}

\begin{figure}[h!]
	\centering{\includegraphics[width=15cm]{Bilder/Aufbau.png}}
	\caption{Versuchsaufbau des Stern-Gerlach Versuchs [1]}
\end{figure}
\noindent Der Versuch aus einem Kaliumofen, einem Elektromagneten, einen Langmuir-Taylor-Detektor (LT-Detektor), einer Drehschieber- und einer Turbopumpe. Das bei circa 190$^\circ$C im Ofen verdampfte Kalium wird mit Blenden kollimiert. Die Teilchenstromdichte $\rho(\nu)$ und die Geschwindigkeit $\nu$ hängen von der Temperatur ab, die sich mithilfe eines Kupfer-Nickel-Thermometers messen lässt. Im thermischen Gleichgewicht ergibt sich eine Maxwell'sche Geschwindigkeitsverteilung
\begin{align}
\rho (\nu) d \nu = \frac{4}{\sqrt{\pi} \alpha^3} \nu^2 \exp \left( - \frac{\nu^2}{\alpha^2} \right) d \nu,
\end{align}
wobei $\alpha = \sqrt{\frac{2 k T}{m}}$ die wahrscheinlichste Geschwindigkeit angibt. Daraus ergibt sich der Teilchenstrom aus $I(\nu) = \rho (\nu) \nu$ zu 
\begin{align}
I (\nu) = \frac{4}{\sqrt{\pi} \alpha^3} \nu^3 \exp \left( - \frac{\nu^2}{\alpha^2} \right) d \nu.
\end{align}
Das Maximum dieser Verteilung liegt bei $\nu_m = \sqrt{\frac{3 \alpha}{2}}$. Die Kaliumatome passieren einen Elektromagneten, indem sie die Kraft $F_z$ erfahren und dadurch parabelfärmige Flugbahnen beschreiben. Die Ablenkung ist proportional zu $\frac{1}{\nu^2}$. Damit die Ablenkung möglichst groß wird, muss der Feldgradient in der Größenordnung des Strahldurchmessers liegen. Es wird daher ein zylinderförmiges Polschuhprofil verwendet. Das magnetische Feld entspricht dabei dem von zwei gegenläufig stromdurchflossenen parallelen Drähten im Abstand von 2a. In y-Richtung ist der Feldgradient unabhängig von y, falls der Atomstrahl etwa eine Entfernung von 1,3a von den Drähten in z-Richtung besitzt. Für den Feldgradienten gilt unter den Voraussetzungen
\begin{align}
\frac{\partial B}{\partial z} = 0,968 \frac{B}{a}.
\end{align}

\begin{figure}[h!]
	\centering{\includegraphics[width=7cm]{Bilder/Polschuh.png}}
	\caption{Versuchsaufbau des Stern-Gerlach Versuchs [1]}
\end{figure}

\noindent Der Atomstrahl wird nach der Ablenkung mit dem LT-Detektor nachgewiesen: an einem erhitzten Wolfram-Draht werden die Kaliumatome ionisiert und anschließend reemittiert. Zum Nachweis muss das Ionisierungspotential kleiner sein als die Austrittsarbeit des Wolframs. \newline
Durch den LT-Detektor wird die wird die Intensitätsverteilung $I(z)$ entlang der z-Achse gemessen. Ohne Magnetfeld wird ein Maximum in der Nullposition zu erwarten sein, während bei eingeschaltetem Magnetfeld durch den Elektronenspin von $m_s$ = $\pm \frac{1}{2}$ eine Aufspaltung erwartet wird. Der Abstand zwischen einem Maximum und der Nullposition beträgt

\begin{align}
s = \mu_{S_z} \cdot \frac{l \cdot L \left(1 - \frac{L}{2l}\right)}{6 k_B T} \cdot \frac{\partial B}{\partial z}.
\label{s}
\end{align}

\noindent $L$ bezeichnet hierbei die Polschuhlänge und $l$ den Abstand von der Eintrittsblende zum Magneten bis zum Wolframdraht.



\newpage
\section{Auswertung}

Um das Bohrsche Magneton bestimmen zu können, werden zunächst bei verschiedenen Magnetfeldstärken $B$ die Aufspaltung der $K$-Atome untersucht. Zum Vergleich wird ebenfalls die Intensitätsverteilung bei ausgeschaltetem Magnetfeld aufgenommen. In den nachfolgenden Abbildungen sind letztere sowie die bei der maximal verwendeten Feldstärke von $B$ = \SI{0.660}{T} dargestellt.

\begin{figure}[h!]
	\centering{\includegraphics[width=9.5cm]{Bilder/OhneB.pdf}}
	\caption{Intensitätsverteilung bei ausgeschaltetem Magnetfeld}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=9.5cm]{Bilder/MitB.pdf}}
	\caption{Intensitätsverteilung bei eingeschaltetem Magnetfeld}
\end{figure}

\noindent Vergleicht man die beiden graphischen Darstellungen lässt sich deutlich eine Aufspaltung des Intensitätsmaximums bei eingeschaltetem Magnetfeld $B$ beobachten, welche zu erwarten war. Anschließend werden die Maxima des zugehörigen Feldgradienten gesucht. Die Resultate sind in Tab. \ref{Ergebnisse} zusammengefasst. 

\begin{table}[h!]
	\centering
		\captionabove{Feldgradienten und die dazugehörigen Maxima der Energieaufpaltung.}
		\begin{tabular}{l l l}
			\toprule[1.5pt] 
			Feldgradient & linkes Maximum & rechtes Maximum  \\
			$\frac{\partial B}{\partial z}$ / $\frac{T}{m}$ & $z_l$ / mm & $z_r$ / mm \\
			\midrule 
			0.220	& 5.350 & 6.200 \\
			0.303	& 5.200 & 6.250 \\
			0.380	& 5.150 & 6.350 \\
			0.455	& 5.100 & 6.400 \\
			0.526	& 4.950 & 6.550 \\
			0.595	& 4.900 & 6.559 \\
			0.660	& 4.850 & 6.700 \\
			\bottomrule[1.5pt] 
		\end{tabular}
	\label{Ergebnisse}
\end{table}

\noindent Um nun das Bohrsche Magneton $\mu_B$ zu bestimmen, werden als erster Schritt die Intensitäten bzgl. des linken und rechten Maximums in einen linearen Bezug zu den Feldgradienten $\frac{\partial B}{\partial z}$ gebracht. Die Korrelationen sind nachfolgend für das linke sowie das rechte Maximum aufgetragen.

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/LinkesMaximum.png}}
	\caption{Zusammenhang zwischen dem Magnetfeldgradienten und dem linken Maximum der Aufspaltung}
\end{figure}
\newpage
\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/RechtesMaximum.png}}
	\caption{Zusammenhang zwischen dem Magnetfeldgradienten und dem rechten Maximum der Aufspaltung}
\end{figure}

\noindent Die linearen Funktionen, gemäß der Relation $f(x) = ax +b$, lauten für die Verteilung der $z_l$ bzw. $z_r$:
\begin{align}
f_{z_l} &= (-2,90 \pm 0,26) \cdot 10^{-6} \frac{\text{1}}{{T}} \cdot x + (557,5 \pm 47,0) \cdot 10^{-6} \frac{\text{1}}{\text{m}} \\
f_{z_r} &= (2,91 \pm 0,29) \cdot 10^{-6} \frac{\text{1}}{{T}} \cdot x + (592,5 \pm 53,4) \cdot 10^{-6} \frac{\text{1}}{\text{m}}.
\end{align}

\noindent Die beiden Steigungen $m_{z_l}$ und $m_{z_r}$ können nun genutzt werden, um $\mu_B$ zu bestimmen. Dieses wird jeweils im Bezug auf das linke/rechte Intensitätsmaximum bestimmt. Dazu können die beiden Relationen \ref{muSz} sowie \ref{s} verwendet werden. Erstere wird dabei in die zweite eingesetzt und die Formel lässt sich anschließend nach dem Bohrschen Magneton $\mu_B$ auflösen. Die darin enthaltenen Größen lauten:
\begin{align*}
T_{\text{ofen}} = \SI{458,5}{K} \quad l = \SI{0,0455}{m} \quad L = \SI{0,07}{m}.
\end{align*}
Weiter gilt für Elektronen $g_S$ = 2 sowie $m_S$ = $\pm \frac{1}{2}$. Letztlich folgt für das Bohrsche Magneton der jeweiligen Seite:

\begin{align}
\mu_{B,l} &= (3,75 \pm 0,33) \cdot 10^{-24} \frac{\text{J}}{\text{T}} \\
\mu_{B,r} &= (3,80 \pm 0,40) \cdot 10^{-24} \frac{\text{J}}{\text{T}}
\end{align}

\newpage
\section{Diskussion}

Es zeigt sich anhand der Ergebnisse zunächst, dass es sich bei dem vorgestellten Verfahren zur Ermittlung des Bohrschen Magnetons $\mu_B$ um eine sinnvolle Methode handelt. Es konnte aus beiden Maxima der Intensitätsaufspaltung ein nahezu gleiches Ergebnis errechnet werden. \\
Beim Vergleich mit dem Literaturwert, $\mu_B$ = $9,27 \cdot 10^{-24}$ $\frac{\text{J}}{\text{T}}$ [2], ist eine Abweichung von knapp 60 \% zu beobachten. Trotz dieser hohen relativen Abweichung, konnte das Verfahren zur Messung von $\mu_B$ bestätigt werden, da e sich hierbei um Größen in einem sehr kleinen Bereich handelt. Der Erfolg liegt darin, dass der in diesem Versuch ermittelte Wert in derselben Größenordnung wie der Vergleichswert liegen. \\
Herausforderungen bei dem Messprozess sind zum einen der große Einfluss der systematischen Ungenauigkeiten. Ersichtlich wird dies bei Betrachtung der Regressionsgeraden, bei denen die statistischen Fehlerabschätzungen keine Erklärung für die hohe Abweichung zulässt. Ebenfalls könnten Unregelmäßigkeiten des inhomogenen Magnetfeldes die Messung erschwert haben. Darüber hinaus konnte der Messprozess nur in Vorwärtsrichtung (bzgl. des Verstellens des Pontiometers) stattfinden. Dadurch wurden einige Positionen nicht exakt eingestellt.






\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Fortgeschrittenenpraktikum: Skript zum Versuch Nummer 25, "Der Stern-Gerlach-Versuch"
\item Lexikon der Physik, Bohrsches Magneton: Literaturwert, http://www.spektrum.de/
lexikon/physik/bohrsches-magneton/1827
\end{enumerate}
\newpage
\section{Anhang}

Nachfolgend befinden sich die graphischen Darstellungen der Messwerte für die Intensitätsverteilungen der $K$-Atome bei ausgeschaltetem sowie eingeschaltetem Magnetfeld $B$. Die Plots sind gemäß der Magnetfeldstärke sortiert.

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/ohneB.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B03A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B03A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B04A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B05A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B06A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B07A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B08A.png}}
\end{figure}

\begin{figure}[h!]
	\centering{\includegraphics[width=11cm]{Bilder/B09A.png}}
\end{figure}
\end{document}

