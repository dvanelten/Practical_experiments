\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{subfigure} 
\usepackage{siunitx}
\usepackage{amssymb}
\pagestyle{fancy}
\rhead{\textbf{Versuch 351}  \\ Protokoll vom \today}
\lhead{\textbf{Konstantin Pfrang, Dennis van Elten}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}
\begin{document}
\title{Versuch 351 - Fourier-Analyse und Synthese}
\author{Konstantin Pfrang \and Dennis van Elten}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents
\newpage

\section{Ziel}
In der Physik sind periodische Vorgänge in Raum und Zeit von großer Bedeutung. Das Fourier-Theorem besagt, dass sich diese periodischen Funktionen im allgemeinen durch Sinus- und Kosinusreihen eintwickeln lassen. In diesem Versuch soll diese Aussage durch die Überlagerung von sinusförmigen Spannungssignalen verdeutlicht werden.
\section{Theorie} 
Nach dem bereits erwähnten Fourier-Theorem lassen sich stetige periodische Funktionen mit der Periodendauer $T$ durch die Reihe
\begin{equation}
f(t)=f(t+T)=\frac{a_0}{2}+\sum^\infty_{n=1}{\left(a_n cos\left(\frac{2\pi n}{T}t\right)+b_n sin\left(\frac{2\pi n}{T}t\right)\right)}
\end{equation} 
darstellen. Dabei konvergiert die Reihe gleichmäßig gegen die exakte Darstellung der Funktion $f(t)$. Liegt an der Stelle $t_0$ eine Unstetigkeit der Funktion vor, so treten an dieser Stelle Überschwingungen in der Fourier-Reihe auf, die auch mit steigender Ordnung $n$ nicht verschwinden. Diese Tatsache wird als Gibb'sches Phänomen bezeichnet. \\
Für die Koeffizienten $a_n$ und $b_n$ mit $n\in \mathbb{N}^+$  gilt:
\begin{eqnarray}
a_n&=&\frac{2}{T}\int^T_0 f(t) cos\left(\frac{2\pi n}{T} t\right)dt\\
b_n&=&\frac{2}{T}\int^T_0 f(t) sin\left(\frac{2\pi n}{T} t\right)dt
\end{eqnarray}
Damit geben sie die Amplituden der $n$-ten Oberschwingung mit der Frequenz $\nu_n=\frac{n}{T}$ an. Allgemein treten nur ganzzahlige Vielfache der Grundfrequenz auf. Ist $f(t)$ eine gerade Funktion, also $f(t)=f(-t)$, so sind alle $b_n=0$. Ist sie eine ungerade Funktion mit $f(t)=-f(-t)$, so gilt für alle $n$: $a_n=0$.\\
Durch die Fouriertransformation erhält man das Frequenzspektrum $g(\nu)$ der periodischen Funktion f(t):
\begin{equation}
g(\nu )=\int^{\infty}_{-\infty}{f(t)e^{i\nu t}} dt 
\end{equation}
$g(\nu)$ besteht aus einer konvergierenden Reihe von $\delta$-Funtkionen. Im Gegensatz dazu besäßen aperiodische Funktionen statt diesem diskretem Spektrum ein kontinuierliches Frequenzspektrum.\\
Die Fouriertransformation ist umkehrbar, die Umkehrung besitzt die Form
\begin{equation}
f(t)=\frac{1}{2\pi}\int^\infty_{-\infty}{g(\nu)e^{i\nu t} d\nu}
\end{equation}
Um exakte Ergebnisse zu erhalten, müsste über einen unendlich langen Zeitraum integriert werden, was in der Praxis nicht möglich ist. Daher treten Abweichungen von den theoretischen Ergebnissen auf. Durch die begrenzte Integration ist die Periodizität der Funktion $f(t)$ aufgehoben und für $g(\nu)$ ergeben sich überall stetige und differenzierbare Funktionen, also ein Linienspektrum mit endlich breiten Linien.
\section{Durchführung}
\subsection{Fourier-Analyse}
Zur Durchführung der Fourier-Analyse wird der in Abb. 1 dargestellt schematische Aufbau verwendet. Mit dessen Hilfe wird eine Sägezahn-, eine Rechteck- und eine Dreieckspannung, die mit dem Funktionsgenerators erzeugt wird, untersucht. Der Rechner führt dabei die Fourier-Transformation durch und zeigt das Spektrum auf dem Bildschirm an, an welchem die Amplituden eines jeden Peaks ausgemessen und mit den berechneten theoretischen Fourierkoeffizienten verglichen wird.\\
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Schematischer Aufbau für die Fourier-Analyse [1]}
\label{fig:Abbildung}
\end{figure}
Bei der Messung muss beachtet werden, dass die Funktion $f(t)$ durch die Digitalisierung der Daten nicht kontinuierlich  aufgezeichnet wird, sondern nur in endlichen Zeitabständen aufgenommen werden. Daher spielt die Abtastfrequenz $\nu_A$ eine wichtige Rolle. Es gilt nach dem Abtasttheorem, dass der Fehler bei der Fourier-Transformation vernachlässigt werden kann, falls die $\nu_A$ größer ist, als die doppelte der höchsten im Spektrum von $f(t)$ vorkommenden Frequenz ist:
\begin{equation}
\nu_A>2\nu_{max}
\end{equation}
\subsection{Fourier-Synthese}
Zur weiteren Überprüfung des Fourier-Theorems, werden mithilfe eine Oberwellengenerators eine Sägezahn-, eine Rechteck- und eine Dreieckspannung erzeugt und mit einem Oszilloskop dargestellt. Dazu werden die Koeffizienten $a_n$ und $b_n$ für ersten neun Oberschwingungen berechnet, die Amplituden der Schwingungen an die Theoriewerte angepasst und anschließend die Spannungsformen durch Überlagerung der Oberwellen erzeugt.\\
Um die Phasenbeziehung der einzelnen Oberschwingungen einstellen zu können, wird jeweils die $n$-te Oberschwingung mit der Grundschwingung auf dem Oszilloskop im XY-Betrieb dargestellt. Dort erkennt man geschlossene Kurven, die sogenannten Lissajous-Figuren, die mithilfe des Phasenreglers soweit verändert werden, bis sie sich zu einer gewundene Linie wie in Abb. 2 ergeben. Für Oberwellen mit geraden $n$ ist nun die Phase zur Grundschwingung $\frac{\pi}{2}$ oder $\frac{3\pi}{2}$, für gerades $n$ beträgt sie 0 oder $\pi$. Durch den $90\symbol{23}$-Schalter wird nun die Oberwelle in die jeweils gewollte Phase bzw. dazu um $\pi$ versetzt gebracht. Da die Lissajous-Figuren es nicht ermöglichen, zwischen diesen beiden Phasen zu unterscheiden, muss bei der späteren Überlagerung der Schwingungen mit dem $180\symbol{23}$-Schalter gegebenfalls korrigiert werden.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild2.png}}
\caption{Beispiel einer Lissajous-Figur nach angepasster Phasenbeziehung [1]}
\label{fig:Abbildung}
\end{figure}

\section{Auswertung}

\subsection{Fourier-Analyse}
Zur Bestimmung der Fourier-Koeffizienten der jeweiligen Spannungstypen wurde die Wechselspannung des Funktionsgenerators verwendet. Dieser wurde bei einer konstanten Frequenz von 10 kHz bedient.

Die Abweichung $\delta$ in den Tabellen 1 bis 3 gibt Auskunft über das Amplitudenverhältnis $A_n/A_1$ des theoretisch ermittelten und des im Experiment gemessenen Wertes und berechnet sich aus
\begin{equation}
\delta = \left(\frac{A_1}{A_n}\right)_{th} \cdot \left(\left(\frac{A_n}{A_1}\right) - \left(\frac{A_n}{A_1}\right)_{th}\right).
\end{equation}
Dabei gibt der Index $n$ den n-ten Fourierkoeffizienten an. Diese werden mithilfe der Gleichungen [Gl.(2.2)] und  [Gl.(2.3)] bestimmt.

\subsubsection{Sägezahnspannung}

Die Funktion, die die Sägespannung beschreibt, lautet 
\begin{equation}
f(t) = \frac{2A}{T}t	, \quad -\frac{T}{2} \leq t \leq \frac{T}{2}.
\end{equation}
Es handelt sich hierbei um eine ungerade Funktion. Es ergeben sich für die Koeffizienten:
\begin{align}
&a_n = 0 \\
&b_n = -\frac{1 \cdot A (-1)^n}{n \pi} = \begin{cases} \frac{-A}{n \pi} , \quad n \quad gerade \\ \frac{A}{n \pi} , \quad n \quad ungerade \end{cases} 
\end{align}
In Tabelle 1 sind die Messwerte aufgetragen.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c|c} 
$n$ & $A_n$[$V$] & $A_n/A_1$ & ($A_n/A_1)_{th}$ & $|\delta|$[$\%$] \\
\hline \hline
1		& 2,040	& 1,00 & 1,00 	& 0,00\\
2		& 1,100	& 0,54 & 0,50 	& 7,84\\
3		& 0,736	& 0,36 & 0,33 	& 8,24\\
4		& 0,488	& 0,24 & 0,25 	& 4,31\\
5		& 0,444	& 0,22 & 0,20 	& 8,82\\
6		& 0,348	& 0,17 & 0,17 	& 2,35\\
7		& 0,300	& 0,15 & 0,14 	& 2,94\\
8		& 0,278	& 0,14 & 0,13 	& 9,02\\
9		& 0,220	& 0,11 & 0,11 	& 2,94\\
\end{tabular}
\caption{Fourier-Analyse der Sägezahnspannung}
\label{fig:Tab1}
\end{table}

\subsubsection{Dreieckspannung}

Um die Funktion der Dreieckspannung wird durch
\begin{equation}
f(t) = \begin{cases} A-\frac{4A}{T}t	, \quad -\frac{T}{2} \leq t \leq 0 \\ A+\frac{4A}{T}t , \quad 0 \leq t \leq \frac{T}{2} \end{cases}
\end{equation}
beschrieben.
Des Weiteren ergeben sich folgende Koeffizienten:
\begin{align}
&a_n = \frac{4A}{n^2 \pi^2} (1-(-1)^n) = \begin{cases} \frac{8A}{n^2 \pi^2} , \quad n \quad gerade \\ 0 , \quad \quad n \quad ungerade \end{cases} \\
&b_n = 0
\end{align}
In Tabelle 2 sind die Messwerte aufgetragen.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c|c} 
$n$ & $A_n$[$V$] & $A_n/A_1$ & ($A_n/A_1)_{th}$ & $|\delta|$[$\%$] \\
\hline \hline
1		& 2,840	& 1,000 & 1,000 	& 0,00\\
3		& 0,314	& 0,111 & 0,111 	& 0,39\\
5		& 0,112	& 0,039 & 0,040 	& 1,41\\
7		& 0,050	& 0,018 & 0,020 	& 11,97\\
9		& 0,031	& 0,011 & 0,012 	& 11,59\\
\end{tabular}
\caption{Fourier-Analyse der Dreieckspannung}
\label{fig:Tab1}
\end{table}

\subsubsection{Rechteckspannung}

Für die Funktion der Rechteckspannung, einem weiteren Signaltyp, gilt 
\begin{equation}
f(t) = \begin{cases} -A	, \quad -\frac{T}{2} \leq t \leq 0 \\ A ,  \quad \quad 0 \leq t \leq \frac{T}{2} \end{cases}
\end{equation}.
Die Koeffizienten berechnen sich wie zuvor nach den Gleichungen [Gl.(2.2)] und [GL.(2.3)]:
\begin{align}
&a_n = 0 \\
&b_n = \frac{2A}{n \pi} (1-(-1)^n) = \begin{cases} 0 , \quad n \quad gerade \\ \frac{4A}{n \pi} , \quad n \quad ungerade \end{cases} 
\end{align}
Die ermittelten Werte sind in der nachstehenden Tabelle aufgeführt.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c|c} 
$n$ & $A_n$[$V$] & $A_n/A_1$ & ($A_n/A_1)_{th}$ & $|\delta|$[$\%$] \\
\hline \hline
1		& 4,48	& 1,000 & 1,000 	& 0,00\\
3		& 1,49	& 0,332 & 0,333 	& 0,21\\
5		& 0,89	& 0,198 & 0,200 	& 0,89\\
7		& 0,63	& 0,140 & 0,143 	& 1,97\\
9		& 0,48	& 0,107 & 0,111 	& 3,47\\
\end{tabular}
\caption{Fourier-Analyse der Rechteckspannung}
\label{fig:Tab1}
\end{table}

\newpage
\subsection{Fourier-Synthese}

Die verwendete Amplitude der Grundschwingung für alle synthetisierten Signale betrug $0,615 V$. Die Amplituden der Oberschwingungen wurden nach den errechneten Fourierkoeffizienten eingestellt. Dabei konnten die Amplituden der Oberwellen am Sinuswellengenerator konnten ziemlich exakt eingestellt werden. Die Abbildungen 3-5 stellen das Bild, bestehend aus der Summe der Oberwellen, auf dem Schirm des Oszilloskops dar.

\begin{table}[htbp]
\centering\begin{tabular}{c|c} 
$n$ & ($A_n/A_1)$ \\
\hline \hline
1		& 0,615	\\
2		& 0,308	\\
3		& 0,205	\\
4		& 0,154	\\
5		& 0,123	\\
6		& 0,103	\\
7		& 0,088	\\
8		& 0,077	\\
9		& 0,068	\\
\end{tabular}
\caption{Fourierkoeffizienten der Sägezahnspannung}
\label{fig:Tab1}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=11cm]{Bilder/Bild3.png}}
\caption{Synthetisierte Sägezahnspannung}
\label{fig:Abbildung}
\end{figure}

\begin{table}[htbp]
\centering\begin{tabular}{c|c} 
$n$ & ($A_n/A_1)$ \\
\hline \hline
1		& 0,615	\\
3		& 0,068	\\
5		& 0,025	\\
7		& 0,013	\\
9		& 0,008	\\
\end{tabular}
\caption{Fourierkoeffizienten der Dreieckspannung}
\label{fig:Tab1}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=11cm]{Bilder/Bild4.png}}
\caption{Synthetisierte Dreieckspannung}
\label{fig:Abbildung}
\end{figure}

\begin{table}[htbp]
\centering\begin{tabular}{c|c} 
$n$ & ($A_n/A_1)$ \\
\hline \hline
1		& 0,615	\\
3		& 0,205	\\
5		& 0,123	\\
7		& 0,088	\\
9		& 0,068	\\
\end{tabular}
\caption{Fourierkoeffizienten der Rechteckspannung}
\label{fig:Tab1}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=11cm]{Bilder/Bild5.png}}
\caption{Synthetisierte Rechteckspannung}
\label{fig:Abbildung}
\end{figure}

\newpage
\section{Diskussion}
Die Abweichungen bei der Fourier-Analyse zwischen den experimentellen und theoretischen Werten liegen im Bereich von maximal $12 \%$. Insgesamt sind das endliche Integrationsintervall bei der Transformation und die Ungenauigkeiten beim Ablesen des Frequenzspektrums maßgeblich für die Abweichungen verantworlich.
\newline
Bei der Fourier-Synthese sind die gewünschten Signale entstanden. Für eine exaktere Synthese wären allerdings unendlich viele Oberschwingungen nötig gewesen, so dass dies nur eine Annäherung ist. Gleichzeitig ist es aber so, dass mit zunehmendem n der Einfluss immer geringer wird. An den Unstetigkeitsstellen der Sägezahn- und Rechteckspannung lässt sich deutlich das Gibbsche Phänomen erkennen. An diesen Stellen lassen sich die Ober- und Unterschwingungen deutlich erkennen. Die Dreieckspannung besitzt keine unstetigen Stellen, daher ist diese am glattesten.
\newline
Insgesamt konnte das Fourier-Theorem durch die Ergebnisse bestätigt werden.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 351, Fourier-Analyse und Synthese
\newline
http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V351.pdf
\end{enumerate}
\end{document}