\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}


\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\thispagestyle{empty}
\title{\Large{Versuch 203}\\
\huge{Verdampfungswärme und Dampfdruckkurve}}
\author{Konstantin Pfrang \and Dennis van Elten}
\maketitle
\thispagestyle{empty}

\newpage
\thispagestyle{empty}
\tableofcontents

\newpage
\setcounter{page}{1}
\section{Ziel}
Ziel dieses Versuches ist, den Vorgang der Phasenumwandlung von flüssig zu gasförmig am Beispiel von Wasser zu untersuchen. Unter Phase versteht man hierbei ein abgeschlossenes System, in dem ein Stoff in einem physikalisch homogenen Zustand vorliegt.\\
Zur Untersuchung wird die Dampfdruckkurve für niedrige Druckbereiche bis 1 bar und für hohe Druckbereiche bis zu 15 bar ermittelt und daraus die Verdampfungswärme bestimmt.

\section{Theoretische Grundlagen}

\subsection{Das Zustandsdiagramm}
Das in Abb. 1 gezeigte Diagramm wird Zustandsdiagramm genannt und beschreibt die Umwandlung zwischen fest, flüssig und gasförmig. Dabei ist der Druck $p$ gegen die Temperatur $T$ aufgetragen. 
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild11.png}}
\caption{Qualitatives Zustandsdiagramm des Wassers [1]}
\label{fig:Abb1}
\end{figure}
Hier am Beispiel des Wassers können die drei Flächen, die durch die Kurven getrennt werden je einem der drei Aggregatzustände zugeordnet werden. Dabei besitzt das System zwei Freiheitsgrade. Im Gegensatz dazu besitzt es auf den eingezeichneten Kurven nur einen Freiheitsgrad, da T und p nicht mehr frei wählbar sind.\\
Als Dampfdruckkurve wird die vom Tripelpunkt (TP.) zum kritischen Punkt (K.P.) verlaufende Kurve bezeichnet. Der Tripelpunkt ist der Punkt, an dem alle drei Aggregatzustände gleichzeitig existieren können und ab dem kritischen Punkt ist es nicht mehr möglich, zwischen Aggregatzuständen zu unterscheiden.
 Auf ihr liegt die Situation vor, dass zwei Phasen koexistieren. Die Dampfdruckkurve wird maßgeblich durch die Verdampfungswärme festgelegt.


\subsection{Die mikroskopischen Vorgänge bei der Verdampfung und Kondensation}
Wird eine Flüssigkeit in ein evakuiertes Gefäß, das heißt ohne Druck, gebracht, so beobachtet man auch oberhalb der Flüssigkeit einen Druckanstieg, der sich damit erklärt, dass ein Teil der Flüssigkeitsmenge verdampft, also von der Flüssigen in die Gasphase übergegangen ist. Das Verdampfen bedeutet, dass die Teilchen, deren kinetische Energie hoch genug ist, die Flüssigkeitsoberfläche verlassen. Dabei müssen die intermolekularen Kräfte überwunden werden weshalb Energie von außen oder der Flüssigkeit benötigt wird.
Die für diese Phasenänderung bei gleichbleibender Temperatur benötigte Energie wird als Verdampfungswärme bezeichnet. Bezieht man diese auf einen Mol einer Flüssigkeit spricht man von der molaren Verdampfungswärme L [$\frac{J}{mol}$]. Die selbe Verdampfungswärme wird beim umgekehrten Prozess, der Kondensation wieder frei.\\
Die sich in Dampfphase befindlichen Moleküle erzeugen durch Stöße auf sich selbst, die Flüssigkeitsfläche und untereinander einen Druck $p$. Ein Teil der Moleküle, die auf die Flüssigkeitsfläche treffen, gehen wieder in den flüssigen Zustand über und nach einer ausreichend langen Zeit wird ein Gleichgewicht erreicht, in dem gleich viele Moleküle in Gasphase übergehen, wie wieder zurück in die Flüssige.
In diesem Zustand bleibt die Anzahl der verdampften Moleküle gleich, so dass sich ein konstanter Druck, auch Sättigungsdruck genannt, einstellt. Bei zunehmender Temperatur erhöht sich die kinetische Energie der Moleküle, damit gehen mehr von ihnen in die Gasphase über und der Druck steigt an. Der Sättigungsdruck hängt somit von der Temperatur ab, jedoch nicht von dem Volumen des Gasraumes, da bei veränderten Volumen so viele Moleküle verdampfen bzw. kondensieren, bis der bis der Druck im Gleichgewicht wieder eingestellt ist. Aus dieser Überlegung folgt, dass sich das Verhalten des Dampfes hier nicht durch die allgemeine Gasgleichung [Gl. (2.1)]
\begin{mycapequ}[htbp]
  \begin{equation}
    pV=N_Ak_BT=RT
  \end{equation}
\end{mycapequ}
beschreiben lässt. Hierbei ist $N_A$ die Avogadro-Konstante und $k_B$ die Boltzmann-Konstante. Außerdem ist die allgemeine Gaskonstante R definiert durch $R=N_A*k_B$

Um das Verhalten der Dampfdruckkurve dennoch mathematisch beschreiben zu können, wird nun in Kapitel 2.3 eine Differentialgleichung dieser hergeleitet.

\subsection{Herleitung einer Differentialgleichung der Dampfdruckkurve}
Eine Differentialgleichung für die Dampfdruckkurve lässt sich bestimmen, indem ein reversibler Kreisprozess, in dem ein Mol eines Stoffes wie in Abb. 2 gezeigt zunächst isotherm - isobar verdampft und anschließend genau so wieder kondensiert, betrachtet wird.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Darstellung des Kreisprozesses im pV-Diagramm [1]}
\label{fig:Abb2}
\end{figure}\\
Aus dem Kreisprozess lässt sich für die während des gesamten Vorganges zugeführte Wärme $dQ_{Ges}$ die Gleichung
\begin{equation}
dQ_{Ges}=C_F dT - c_D dT + dL
\end{equation}
herleiten. Dabei ist $dL$ die Änderung der Wärmemenge, $C_D$ die Molwärme des Dampfes und $C_F$ die Molwärme der Flüssigkeit. 
Der erste Hauptsatz der Thermodynamik besagt, dass diese Wärme gleich der geleisteten Arbeit $W_{Ges}$ entspricht, die sich durch Gl. (2.3) ergibt.
\begin{equation}
W_{Ges}= dp\cdot (V_D-V_F)
\end{equation}
Also erhält man die Gleichung
\begin{equation}
C_F dT - c_D dT + dL=dp\cdot (V_D-V_F)
\end{equation}
Außerdem ergibt nach dem zweiten Hauptsatz der Thermodynamik, dass beim reversiblen Kreisprozess die Summe der reduzierten Wärmemenge gleich Null ist.
\begin{equation}
\sum_i{\frac{Q_i}{T_i}}=0
\end{equation}
Vernachlässigt man Differentialausdrücke 2. Ordnung folgt aus Gleichung (2.4) und (2.5) die Clausius-Clapeyronsche Gleichung:
\begin{equation}
dp\cdot (V_D-V_F)=\frac{L}{T} dT
\end{equation}
Allgemein ist die Integration der Clausius-Clapeyronschen Gleichung schwer, da $V_D$, $V_F$ und $L$ komplizierte Funktionen der Temperatur sein können. In gewissen Temperaturbereichen ist es, wie der nächsten Abschnitt zeigen wird, jedoch durch Näherungen möglich, die die Integration zu vereinfachen.

\subsection{Integration der Clausius-Clapeyroschen Gleichung}
Die bereits angesprochene Vereinfachung wird ermöglicht, wenn die Temperatur $T$ sich weit unter der kritischen Temperatur $T_{Kr}$ (siehe Abb. 1) befindet. Dadurch können folgende vereinfachende Näherungen gemacht werden:\\
 
\begin{compactenum}[(i)]
	\item $V_F$ ist vernachlässigbar im Vergleich zu $V_D$
	\item {Für $V_D$ gilt die ideale Gasgleichung 
	\begin{equation}
	V_D(p,T)=\frac{R\cdot T}{p}
	\end{equation}}
	\item {Die Verdampfungswärme ist unabhängig von Druck und Temperatur}
\end{compactenum}

unter diesen Annahmen, ist es möglich, die Clausius-Clapeyronsche Gleichung zu integrieren und man erhält die Gleichung für den Zusammenhang zwischen dem Druck $p$ und der Temperatur $T$
\begin{equation}
ln({p})=-\frac{L}{R\cdot T}+const
\end{equation}
\begin{equation}
\Leftrightarrow \qquad p=p_0 \cdot exp{\left(-\frac{L}{R\cdot T}\right)}
\end{equation}

\section{Durchführung}
\subsection{Bestimmung der Dampfdruckkurve von Wasser für niedrigen Druck}
Die Dampfdruckkurve wird mithilfe der in Abb. 3 skizzierten Apparatur ermittelt.
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild2.png}}
\caption{Messapparatur zur Aufnahme der Dampfdruckkurve in niedrigen Druckbereichen bis 1 Bar [1]}
\label{fig:Abb1}
\end{figure}
Zunächst wird die kalte Apparatur mit der Wasserstrahlpumpe auf den niedrigst möglichen Druck evakuiert, wobei Absperrhahn und Drosselventil geöffnet und Belüftungsventil geschlossen sein muss. Die Woulffsche Flasche verhindert, dass beim Abdrehen des Leitungswassers etwas davon unter Umständen in die Messapparatur gelangt. Deshalb sollte immer erst das Drosselventil geschlossen werden.

Anschließend wird das Drosselventil und der Absperrhahn geschlossen und das Wasser wird durch die Heizhaube erhitzt. Nach einer kurzen Zeit fängt das Wasser an zu sieden, ansonsten wird durch leichtes öffnen des Drosselventils der Druck weiter verringert. Aufsteigende Dämpfe werden durch den Rückflusskühler kondensiert, damit diese nicht das Manometer oder die Pumpe erreichen. Während der Messung sollte deshalb ständig die Kühlschlange von Wasser durchflossen werden. Bei ansteigender Temperatur wird der Durchfluss immer weiter reduziert.
Letztlich werden laufend der Dampfdruck mit dem Manometer und die dazugehörige Temperatur mit an dem Thermometer, das in den Dampfraum eintaucht gemessen.
\newpage
\subsection{Bestimmung der Dampfdruckkurve von Wasser für hohen Druck}
In höheren Druckbereichen als 1 Bar wird die in Abb. 4 dargestellte Apparatur verwendet. Sie setzt sich zusammen aus einem durchbohrten Stahlbolzen, in dessen Innenraum das zu untersuchende Wasser ist, einem U-Rohr und einem Drucksensor, durch den der Druck gemessen wird. Eine Erhitzung des Drucksensors wird vermieden, indem in die darunter angebrachte Kupferschale etwas kaltes Wassers gefüllt wird.

Erhitzt wird der Zylinder mit der Flüssigkeit durch eine Heizwicklung um den Stahlzylinder. Die Temperatur des Wassers kann durch ein elektronisches Thermometer, dass durch eine Bohrung in den Hohlraum ragt, gemessen werde.
Während der Messung wird der Sättigungsdampfdruck mit der jeweils zugehörigen Temperatur gemessen.

\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild3.png}}
\caption{Messapparatur zur Aufnahme der Dampfdruckkurve in hohen Drücken über 1 Bar [1]}
\label{fig:Abb1}
\end{figure}


\section{Auswertung}

\subsection{Dampfdruckkurve von Wasser bei niedrigem Druck (0-1 bar)}
\newline
Aus den in der nachstehenden Tabelle 1 aufgelisteten Messwerten wird das Diagramm (Abb.5) erstellt. In diesem wird der logarithmische Druck p gegen den Kehrwert der Temperatur T aufgetragen.


\begin{table}
\centering
\begin{tabular}{c	|c	|c	|c	|c}
T[°C] & T[K] & p[mbar] & 1/T & ln(p) \\
\hline
22		& 295		&  55		& 0,003390	& 4,0073 \\
29		& 302		&  70		& 0,003311	& 4,2485 \\
35		& 308		&  85		& 0,003247	& 4,4427 \\
38,5	& 311,5	& 100		& 0,003210	& 4,6052 \\
43		& 316		& 115		& 0,003165	& 4,7449 \\
46,5	& 319,5	& 130		& 0,003130	& 4,8675 \\
50		& 323		& 145		& 0,003096	& 4,9767 \\
52,5	& 325,5	& 160		& 0,003072	& 5,0752 \\
56		& 329		& 175		& 0,003040	& 5,1648 \\
58,5	& 331,5	& 190		& 0,003017	& 5,2470 \\
60,5	& 333,5	& 205		& 0,002999	& 5,3230 \\
62		& 335		& 220		& 0,002985	& 5,3936 \\
63		& 336		& 235		& 0,002976	& 5,4596 \\ 
65		& 338		& 250		& 0,002959	& 5,5215 \\
66,5	& 339,5	& 265		& 0,002946	& 5,5797 \\
67		& 340		& 280		& 0,002941	& 5,6348 \\
68		& 341		& 295		& 0,002933	& 5,6870 \\
70		& 343		& 310		& 0,002915	& 5,7366 \\
71		& 344		& 325		& 0,002907	& 5,7838 \\
72		& 345		& 340		& 0,002899	& 5,8289 \\ 
73		& 346		& 355		& 0,002890	& 5,8721 \\
74		& 347		& 370		& 0,002882	& 5,9135 \\ 
75		& 348		& 385		& 0,002874	& 5,9532 \\
76		& 349		& 400		& 0,002865	& 5,9915 \\ 
77		& 350		& 415		& 0,002857	& 6,0283 \\
77,8	& 350,8	& 430		& 0,002851	& 6,0638 \\
78,5	& 351,5	& 445		& 0,002845	& 6,0981 \\
79		& 352		& 460		& 0,002841	& 6,1312 \\
80		& 353		& 475		& 0,002833	& 6,1633 \\
80,8	& 353,8	& 490		& 0,002826	& 6,1944 \\
81,5	& 354,5	& 505		& 0,002821	& 6,2246 \\
82,3	& 355,3	& 520		& 0,002815	& 6,2538 \\
83		& 356		& 535		& 0,002809	& 6,2823 \\
83,6	& 356,6	& 550		& 0,002804	& 6,3099 \\
84,3	& 357,3	& 565		& 0,002799	& 6,3368 \\
85		& 358		& 580		& 0,002793	& 6,3630 \\
85,5	& 358,5	& 595		& 0,002789	& 6,3886 \\
86,4	& 359,4	& 610		& 0,002782	& 6,4135\\
87		& 360		& 625		& 0,002778	& 6,4378 \\
87,5	& 360,5	& 640		& 0,002774	& 6,4615 \\
88,1	& 361,1	& 655		& 0,002769	& 6,4846 \\
88,9	& 361,9	& 670		& 0,002763	& 6,5073 \\ 
89,5	& 362,5	& 685		& 0,002759	& 6,5294 \\
90		& 363		& 700		& 0,002755	& 6,5511 \\ 
90,5	& 363,5	& 715		& 0,002751	& 6,5723
\label{fig: Tab1}
\end{tabular}
\end{table}


\caption{Messung für den Druck p kleiner 1 bar}
\begin{table}
\centering
\begin{tabular}{c	|c	|c	|c	|c}
T[°C] & T[K] & p[bar] & 1/T & ln(p) \\
\hline
91,1	& 364,1	& 730		& 0,002746	& 6,5930 \\
91,5	& 364,5	& 745		& 0,002743	& 6,6134 \\
92,1	& 365,1	& 760		& 0,002739	& 6,6333 \\
92,7	& 365,7	& 775		& 0,002734	& 6,6529 \\
93,3	& 366,3	& 790		& 0,002730	& 6,6720 \\
93,7	& 366,7	& 805		& 0,002727	& 6,6908 \\
94,2	& 367,2	& 820		& 0,002723	& 6,7093 \\
94,7	& 367,7	& 835		& 0,002720	& 6,7274 \\
95,1	& 368,1	& 850		& 0,002717	& 6,7452 \\
95,6	& 368,6	& 865		& 0,002713	& 6,7627 \\
96,3	& 369,3	& 880		& 0,002708	& 6,7799 \\ 
96,5	& 369,5	& 895		& 0,002706	& 6,7968 \\
97		& 370		& 910		& 0,002703	& 6,8134 \\
97,5	& 370,5	& 925		& 0,002699	& 6,8298 \\
97,9	& 370,9	& 940		& 0,002696	& 6,8459 \\
98,5	& 371,5	& 955		& 0,002692	& 6,8617 \\
99		& 372		& 970		& 0,002688	& 6,8773 \\
99,3	& 372,3	& 985		& 0,002686	& 6,8926 \\
99,7	& 372,7	& 1000	& 0,002683	& 6,9078 
\end{tabular}
\label{fig: Tab1}
\caption{Messung für den Druck p kleiner 1 bar}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=15cm]{Bilder/Bild20.png}}
\caption{ln(p) aufgetragen gegen 1/T im Bereich p kleiner 1 bar}
\label{fig:Abb1}
\end{figure}

Mit der Gleichung (2.8) lässt sich nun die Verdampfungswärme $L$ bestimmen. Mithilfe der integrierten Clausius-Clapeyronschen Gleichung ergiben sich die beiden Gleichungen:
\begin{equation}
a &= \frac{ln({p})}{\frac{1}{T}} = ln({p}) T
\end{equation}
\begin{equation}
ln({p}) &= -\frac{L}{RT} \Leftrightarrow ln({p}) T = - \frac{L}{R}.
\end{equation}
Aus den beiden vorherigen Gleichungen [Gl.(4.1)] und [Gl.(4.2)] ergibt sich für die Verdampfungswärme $L$:
\begin{equation}
a &= -\frac{L}{R} \Leftrightarrow L = (-R)a.
\end{equation}
Dabei ist $R$ die universelle Gaskonstante $R = 8,314472 J/mol \cdot K$ und $a$ entspricht der Steigung der in Abbildung 5 eingezeichneten Ausgleichsgeraden. Diese ist linear und besitzt die Steigung $a = (-$4409,51$ \pm$54,34$)$ und den y-Achsenabschnitt $b = ($18,68$ \pm$0,16$)$. Somit ergibt sich für die Verdampfungswärme $L$
\begin{equation}
L=a (-R) = -(-4409,51 \pm 54,34) \cdot 8,314472 \frac{J}{mol} = (36662,87 \pm{451,81}) \frac{J}{mol}.
\end{equation}
Die äußere Verdampfungswärme $L_a$ für eine Temperatur von T=373K lässt sich nun über die Allgemeine Gasgleichung abschätzen:
\begin{equation}
L_a= RT = 8,314472 \cdot 373 \frac{J}{mol} = 3101,31 \frac{J}{mol}.
\end{equation}
Aus den zuvor berechneten Größen ist es möglich $L_i$ zu berechnen. Diese gibt die notwendige Arbeit wieder, die nötig ist um die bei der Verdampfung existenten Anziehungskräfte, die im Molekül herrschen, zu überwinden. Diese wird mit Gleichung
\begin{equation}
L_i = L - L_a = (36662,87 \pm{451,81})\frac{J}{mol} - 3101,31 \frac{J}{mol} = (33561,56 \pm{451,81})\frac{J}{mol}
\end{equation}
berechnet.
Um letztendlich $L_i$ pro Molekül zu bestimmen muss dessen Wert mit dem Kehrwert von $N_A$ = 6,0221415 \cdot $10^{23}$$\frac{1}{mol}$, der Avogadro-Konstante, multipliziert werden:
\begin{equation}
L_i = (33561,56 \pm{451,81}) \cdot \frac{1}{N_A} J = (5,573 \pm{0,075}) {\cdot 10^{-20}} J = (0,3479 \pm{0,0047}) eV.
\end{equation}
Die Umrechung von J in eV erfolgt dabei über den Faktor 1 eV = 1,602·10$^{-19}$ J.
\newline
\newline

\subsection{Dampfdruckkurve von Wasser bei hohem Druck (1-15 bar)}
\newline
In Tabelle 2 sind dabei die während der Messung erhaltenen Daten sowie die im späteren Schritt berechnete Verdampfungswärme $L$ aufgeführt.

\begin{table}
\centering
\begin{tabular}{c	|c	|c	|c	|c}
T[°C] & T[K] & p[bar] & L(T) [J/mol] & L(T) [kJ/mol] \\
\hline
22.1	& 295.1	& 1.8		& 10070.26	& 10.07 \\
27.1	& 300.1	& 1.83	& 8051.16		& 8.05 	\\
47.5	& 320.5	& 1.95	& 3861.71		& 3.86	\\
62.6	& 335.6	& 2.1		& 3973.94		& 3.97	\\
79.5	& 352.5	& 2.4		& 7179.58		& 7.18	\\
91.3	& 364.3	& 2.7		& 11013.67	& 11.01	\\
100.4	& 373.4	& 3			& 14450.66	& 14.45	\\
107.4	& 380.4	& 3.3		& 17142.83	& 17.14	\\
112.8	& 385.8	& 3.6		& 19148.42	& 19.15	\\
118.1	& 391.1	& 3.9		& 21001.77	& 21		\\
122.2	& 395.2	& 4.2		& 22334.98	& 22.33	\\
126.2	& 399.2	& 4.5		& 23540.12	& 23.54	\\
130 	& 403		& 4.8		& 24592.67	& 24.59	\\
133.4	& 406.4	& 5.1		& 25456.76	& 25.46	\\
136.6	& 409.6	& 5.4		& 26203.43	& 26.2	\\
139.7	& 412.7	& 5.7		& 26866.54	& 26.87	\\
142.4	& 415.4	& 6			& 27397.34	& 27.4	\\
145 	& 418		& 6.3		& 27868.94	& 27.87	\\
147.5	& 420.5	& 6.6		& 28287.36	& 28.29	\\
149.9	& 422.9	& 6.9		& 28658.2		& 28.66	\\
152.2	& 425.2	& 7.2		& 28986.61	& 28.99	\\
154.4	& 427.4	& 7.5		& 29277.29	& 29.28	\\
156.5	& 429.5	& 7.8		& 29534.43	& 29.53	\\
158.4	& 431.4	& 8.1		& 29750.85	& 29.75	\\	
160.2	& 433.2	& 8.4		& 29942.35	& 29.94	\\
162 	& 435		& 8.7		& 30121.31	& 30.12	\\
163.7	& 436.7	& 9			& 30279.38	& 30.28	\\
165.5	& 438.5	& 9.3		& 30435.71	& 30.44	\\
167.1	& 440.1	& 9.6		& 30565.61	& 30.57	\\
168.7	& 441.7	& 9.9		& 30687.4		& 30.69	\\
170.3	& 443.3	& 10.2	& 30801.45	& 30.8	\\
171.8	& 444.8	& 10.5	& 30901.69	& 30.9	\\
173.4	& 446.4	& 10.8	& 31001.83	& 31		\\
175 	& 448		& 11.1	& 31095.29	& 31.1	\\
176.4	& 449.4	& 11.4	& 31171.88	& 31.17	\\
178		& 451		& 11.7	& 31253.75	& 31.25	\\
179.3	& 452.3	& 12		& 31316.03	& 31.32	\\
\end{tabular}
\label{fig: Tab2}
\end{table}

\begin{table}
\centering
\begin{tabular}{c	|c	|c	|c	|c}
T[°C] & T[K] & p[bar] & L(T) [J/mol] & L(T) [kJ/mol] \\
\hline
180.5	& 453.5	& 12.3	& 31370.31	& 31.37	\\
181.7	& 454.7	& 12.6	& 31421.63	& 31.42	\\
182.9	& 455.9	& 12.9	& 31470.11	& 31.47	\\
184		& 457		& 13.2	& 31512.15	& 31.51	\\
185.2	& 458.2	& 13.5	& 31555.51	& 31.56	\\
186.4	& 459.4	& 13.8	& 31596.35	& 31.6	\\
187.6	& 460.6	& 14.1	& 31634.79	& 31.63	\\
188.7	& 461.7	& 14.4	& 31667.99	& 31.67	\\
189.9	& 462.9	& 14.7	& 31702.09	& 31.7	\\
190.9	& 463.9	& 15		& 31728.88	& 31.73	
\end{tabular}
\label{fig: Tab2}
\caption{Messung des Drucks p im Bereich 1 bis 15 bar}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=15cm]{Bilder/Bild21.png}}
\caption{Messwerte für den Druck p im Bereich 1 bis 15 bar}
\label{fig:Abb1}
\end{figure}

Die in Abbildung 6 aufgetragene Ausgleichsfunktion p(T) ist ein Polynom 3. Grades und hat die Form: $cT^3+dT^2+eT+f$. Dieses Polynom wurde mit GNUPLOT erzeugt. Die Koeffizienten besitzen folgende Werte:
\newline
\newline
c = $(4,619 \pm{0,912})$ $\cdot 10^{-6}$$\frac{bar}{K^3}$
\newline
d = $(-4,546 \pm{0,106})$ $\cdot 10^{-3}$$\frac{bar}{K^2}$
\newline
e = $(1,50 \pm}{0,04})$ $\frac{bar}{K}$
\newline
f = $(-163,775 \pm{5,087})$ $bar$.
\newline
\newline
Mithilfe der Näherung für die Bestimmung von $V_D$ gibt die Gleichung
\begin{equation}
(p + \frac{a}{V^2})V = RT.
\end{equation}
Dabei ist $a = 0,9 \frac{J m^3}{mol^2}$.
Damit gilt der Zusammenhang:
\begin{equation}
L = ({V_D}-{V_F}) \cdot T \cdot \frac{dp}{dT}.
\end{equation}
Durch weitere Umformungen ergibt sich schließlich die Gleichung für die Verdampfungswärme L in Abhängigkeit von der Temperatur T:
\begin{equation}
L(T) = \left[ \frac{RT}{2}+\sqrt{(\frac{RT}{2})^2-a(cT^3+dT^2+eT+f)}\right] \frac{3cT^3+2dT^2+eT}{cT^3+dT^2+eT+f}.
\end{equation}

Abbildung 7 veranschaulicht dabei die für die Messdaten errechnete Verdampfungswärme L(T). Dabei wurde die Trendlinie als Polynom 5. Grades angenähert.

\begin{figure}[htbp]
\centering{\includegraphics[width=13cm]{Bilder/Bild22.png}}
\caption{L(T) im Druckbereich zwischen 1<p<15 bar}
\label{fig:Abb1}
\end{figure}

\newpage
\section{Diskussion der Ergebnisse}
Die Verdampfungswärme L von Wasser beträgt im Temperaturintervall [$0\symbol{23}C$ ,$100\symbol{23}C$] in etwa zwischen 41.000 $\frac{J}{mol}$ und 45.000 $\frac{J}{mol}$. 
Der im Experiment ermittelte Wert von 36662,87 $\frac{J}{mol}$ für die Verdampfungswärme liegt dabei knapp unter dem Literaturwerten. Dies kann zum einen daran liegen, dass der Druck nach der Evakuierung mit der Wasserstrahlpumpe schnell anstieg. Eine mögliche Undichtigkeit konnte nicht festgestellt werden, so dass eventuell ein systematischer Fehler aufgetreten ist.

\section{Literaturverzeichnis}
\begin{enumerate}
	\item 	Physikalisches Anfängerpraktikum TU Dortmund, Skript zum Versuch Nr. 203, 			Verdampfungswärme und Dampfdruckkurve.\\
	$http://129.217.224.2/HOMEPAGE/PHYSIKER/BACHELOR/AP/SKRIPT/V203.pdf$
			

\end{enumerate}







\end{document}