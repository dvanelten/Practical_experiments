\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel, varioref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{paralist}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage[pdfborder={0 0 0}]{hyperref} 
\pagestyle{fancy}
\rhead{\textbf{Versuch 701}  \\ Protokoll vom 3. Juni 2014}
\lhead{\textbf{Dennis van Elten, Konstantin Pfrang}  \\ Anfänger-Praktikum WS13/14}

\DeclareCaptionType{mycapequ}[][List of equations]
\captionsetup[mycapequ]{labelformat=empty}
\addto\captionsngerman{
\renewcommand{\figurename}{Abb.}
\renewcommand{\tablename}{Tab.}
}
\numberwithin{equation}{section}	
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

\begin{document}
\title{\begin{Large}\textmd{ Versuch 701} \end{Large}\\  Reichweite von $\alpha$-Strahlung}
\author{Konstantin Pfrang \and Dennis van Elten}
\date{3. Juni 2014}
\setcounter{page}{-1} 
\maketitle
\thispagestyle{empty} 
\newpage
\thispagestyle{empty} 
\tableofcontents

\newpage


\section{Einleitung}
Während dieses Versuches soll die Reichweite von $\alpha$-Strahlung in Luft bestimmt werden. Da die Anzahl der Stöße nicht für alle Teilchen der selben Energie gleich ist, wird die mittlere Reichweite $R_m$ als die Entfernung, bei der die Hälfte der vorhandenen $\alpha$-Teilchen noch ankommt, definiert.\\
Darüber hinaus soll zusätzlich die Statistik des radioaktiven Zerfalls überprüft werden.

\section{Theoretische Grundlagen}
Es ist möglich, die Energie von  $\alpha$-Strahlung zu bestimmen, indem ihre Reichweite untersucht wird. Der Energieverlust der $\alpha$-Teilchen kann dabei durch elastische Stöße erfolgen, die sogenannte Rutherford Streuung, die jedoch im Versuch eine geringere Rolle spielt. Außerdem kann der Energieverlust durch Anregung oder Dissoziation von Molekülen zustandekommen. Der Energieverlust $-\frac{dE_\alpha}{dx}$ ist dabei abhängig von der Energie der  $\alpha$-Strahlung und der Dichte der Materie. Bei kleinen Geschwindigkeiten der Strahlung ist dabei die Wahrscheinlichkeit der Wechselwirkung größer.\\
Ist die Energie groß genug, so kann der Energieverlust durch die Bethe-Bloch-Gleichung beschrieben werden.
\begin{align}
-\frac{dE_\alpha}{dx}=\frac{z^4e^4}{4\pi\epsilon_0m_e}\frac{nZ}{v^2}\ln\left(\frac{2m_ev^2}{I}\right)
\end{align}
Hierbei ist $z$ die Ladung und $v$ die Geschwindigkeit der $\alpha$-Strahlung, $Z$ die Ordnungszahl, $n$ die Teilchendichte und $I$ die Ionisierungsenergie des durchlaufenen Targetgases.\\
Die Reichweite $R$ der Strahlung bis zur absoluten Abbremsung wird über den Zusammenhang
\begin{align}
R=\int_0^{E_\alpha}\frac{dE_\alpha}{-dE_\alpha/dx}
\end{align}
berechnet. Da bei sehr kleinen Energien auch Ladungsaustauschprozesse stattfinden, verliert die Bethe-Bloch-Gleichung dort ihre Gültigkeit. Deshalb werden zur Bestimmung der mittleren Reichweite empirisch gewonnene Kurven benutzt, aus denen für die Reichweite der $\alpha$-Strahlung in Luft mit Energien $E_\alpha\leq 2{,}5 \, \mathrm{MeV}$ die Beziehung 
\begin{align}
R_m=3{,}1 \cdot E_\alpha^{3/2}
\end{align}
 folgt. Hierbei ist $R_m$ in mm und $E_\alpha$ in MeV anzugeben.\\
Bei konstanter Temperatur und konstantem Volumen ist die Reichweite der Teilchen proportional zum Druck $p$. Deshalb ist es möglich, eine Messung der Reichweite durch Variation des Druckes durchzuführen. Für einen festen Abstand $x_0$ zwischen Detektor und $\alpha$-Strahler ergibt sich die 'effektive Länge' x durch die Beziehung
\begin{align}
x=x_0\frac{p}{p_0}
\end{align}
mit $p_0 = 1013 \hspace{2} \mathrm{mbar}$.
\section{Durchführung}
Während des Experiments wird der in Abbildung 1 dargestellte experimenteller Aufbau verwendet. In dem evakuiertem Glaszylinder befindet sich ein $\alpha$-Präparat auf einer verschiebbaren Halterung und ein Detektor. 
\begin{figure}[htbp]
\centering{\includegraphics{Bilder/Bild1.png}}
\caption{Verwendete Aufbau zur Bestimmung der Reichweite von $\alpha$-Strahlung [1]}
\label{fig:Abbildung}
\end{figure}
Als Strahlungsquelle wird ein Am-Präparat mit einer Halbwertzeit von $T_\frac{1}{2}=458\mathrm{a}$ verwendet:
\begin{align}
^{241}_{95}Am\longrightarrow^{237}_{93}Np+^{4}_{2}He^{++}
\end{align}
Als Detektor dient ein Halbleiter-Sperrschichtzähler, dessen Aufbau dem einer Diode ähnelt, die in Sperrrichtung betrieben wird. Wenn ein Ion einfällt, erzeugt dieses in der Verarmungszone Elektronen-Loch Paare, die zu einem Stromimpuls führen, der durch einen Vorverstärker verstärkt und einen Vielkanalanalysator (Multichannel Analyzer MCA) analysiert wird. Die Energie der $\alpha$-Teilchen ist proportional zur Pulshöhe und kann durch ein Histogramm aufgenommen werden.
\subsection{Messprogramm}
Zunächst werden die Diskreminatorschwellen eingestellt, indem bei Atmosphärendruck und maximalem Quellen-Detektor Abstand die Schwelle so eingestellt wird, dass keine Pulse mehr gezählt werden und anschließend der Abstand soweit verringert wird, bis der MCA anfängt zu zählen.\\
\subsubsection{Bestimmung der Reichweite von $\alpha$-Strahlung}
Der Glaszylinder wird mit der Pumpe auf einen Druck von $p\approx0\,\mathrm{mbar}$ evakuiert und anschließend das Ventil geschlossen und die Pumpe ausgestellt. Anschließend kann der Druck innerhalb des Zylinders durch Öffnen und Schließen des Ventils variiert werden. Die Zählrate der $\alpha$-Strahlung wird nun in Abhängigkeit vom Druck $p$ in Abständen von ca. $50\,$mbar gemessen. Bei jeder Einstellung wird diese über einen Zeitraum von $120\,$ s aufgenommen.
\subsubsection{Statistik des radioaktiven Zerfalls}
Hierzu wird die Zählrate für ca. $10\,$s  bei evakuiertem Gefäß mindestens 100 mal gemessen und anschließend in einem Histogramm aufgetragen.

\newpage
\section{Auswertung}
\subsection{Energie der $\alpha$-Teilchen als Funktion des Drucks}
In Tab.1 sind die bei einem Quelle/Präparat-Abstand  von $2,8\,$ cm aufgenommenen Messdaten aufgelistet. 

\begin{table}[htbp]
\centering\footnotesize\begin{tabular}{c|c|c}
Druck p \mathrm{[mBar]}& $\mathrm{E_{max}} \mathrm{(MCA)}$& Gesamtzählrate \mathrm{[Zerfälle/120 s]} \\
\hline
0		&	348&	68522\\
50	&	344&	67870\\
100	&	335&	67624\\
150	&	335&	67497\\
200	&	331&	67127\\
250	&	312&	66894\\
300	&	296&	66810\\
350	&	304&	66576\\
400	&	291&	66098\\
450	&	280&	64799\\
500	&	264&	64541\\
550	&	238&	63135\\
600	&	224&	62087\\
650	&	207&	61021\\
700	&	199&	58992\\
750	&	158&	50285\\
800	&	116&	37441\\
850	&	115&	15492\\
900	&	115&	4176\\
950	&	115&	1825\\
1000& 115&	1259
\end{tabular}
\caption{Messdaten zur Bestimmung der Reichweite von $\alpha$-Strahlung bei einem Präparat/Quelle-Abstand von d = 0,028 m}
\end{table}

In der nachstehenden Abbildung (Abb.2) sind die Energien (hier: Kanalnummer des MCA) der $\alpha$-Teilchen gegen den Druck $\mathrm{p}$ aufgetragen.

\begin{figure}[htbp]
\centering{\includegraphics[width=12cm, height=6cm]{Bilder/Bild2.png}}
\caption{Energien der $\alpha$-Teilchen bei einem Abstand d = 2,8 cm}
\label{fig:Abbildung}
\end{figure}

Die dabei von Gnuplot erzeugte Regressionsgerade besitzt die Form:

\begin{equation}
\mathrm{f(p) = (-0,28 \pm 0,01) \frac{1}{mBar} \cdot p + (378 \pm 8)}.
\end{equation}

Da diese Funktion jedoch nur den Zusammenhang zwischen Kanalzahl des Vielkanalanalysators und dem Druck beschreibt, muss [Gl.(4.1)] in eine andere Form gebracht werden. Dazu wird angenommen, dass das Energiemaximum $\mathrm{E_{max} = 4\, MeV}$ bei $\mathrm{p = 0\, mBar}$ beträgt und linear abnimmt. Die korrigierte Gleichung lautet:

\begin{equation}
\mathrm{f(p) = 4\, MeV - 0,0027 \frac{MeV}{mBar} \cdot p}.
\end{equation}

\newpage
Analog wird die Energie der $\alpha$-Teilchen als Funktion des Drucks für einen zweiten Abstand bestimmt. Tabelle 2 beinhaltet die Messdaten für einen Quelle/Präparat-Abstand $\mathrm{d\, = 4,0\, cm}$.

\begin{table}[htbp]
\centering\footnotesize\begin{tabular}{c|c|c}
Druck p \mathrm{[mBar]}& $\mathrm{E_{max}} \mathrm{(MCA)}$& Gesamtzählrate \mathrm{[Zerfälle/120 s]} \\
\hline
0		&	346&	43753\\
100	&	335&	42770\\
200	&	312&	42470\\
300	&	272&	41223\\
400	&	248&	39932\\
450	&	228&	39648\\
500	&	208&	37999\\
550	&	175&	35397\\
600	&	114&	28641\\
650	&	114&	11627\\
700	&	114&	2450\\
750	&	114&	785\\
800	&	114&	715\\
850	&	114&	734\\
900	&	114&	751\\
950	&	114&	701\\
1000& 114&	769
\end{tabular}
\caption{Messdaten zur Bestimmung der Reichweite von $\alpha$-Strahlung bei einem Präparat/Quelle-Abstand von d = 0,04 m}
\end{table}
\newpage
Aus den gemessenen Daten gewinnt man das Diagramm aus Abb.3.

\begin{figure}[htbp]
\centering{\includegraphics[width=12cm, height=6cm]{Bilder/Bild3.png}}
\caption{Energien der $\alpha$-Teilchen bei einem Abstand d = 4,0 cm}
\label{fig:Abbildung}
\end{figure}

Die errechnete Regressionsgerade lautet in diesem Fall

\begin{equation}
\mathrm{f(p) = (-0,28 \pm 0,03) \frac{1}{mBar} \cdot p + (344 \pm 16)},
\end{equation}

beziehungsweise angepasst

\begin{equation}
\mathrm{f(p) = 4\, MeV - 0,0027 \frac{MeV}{mBar} \cdot p}.
\end{equation}

\newpage
\subsection{Zählrate als Funktion der effektiven Länge}

Um die mittlere Weglänge zu bestimmen wird das folgende Diagramm (Abb.4) erstellt. Der Bereich, in dem der Abfall der Zählrate sehr groß ist, wird durch eine Regressionsgerade angenähert. Um nun die gesuchte mittlere Weglänge zu erhalten wird mithilfe der Geraden an der Stelle 34261 der korrespondierende Wert $x_{eff}$ errechnet. Dieser Wert auf der y-Achse wird gewählt, da dieser der Hälfte des Maximums entspricht.

\begin{figure}[htbp]
\centering{\includegraphics[width=13cm, height=8cm]{Bilder/Bild4.png}}
\caption{Zählraten der $\alpha$-Teilchen bei einem Abstand d = 2,8 cm}
\label{fig:Abbildung}
\end{figure}

Die Regressionsgerade lautet
\begin{equation}
\mathrm{f(x) = (-8,8 \pm 1,1) \cdot 10^6 \frac{1}{m \cdot s} \cdot x + (227152 \pm 22620)s^{-1}}.
\end{equation}
Daraus ergibt sich mit der ausgewählten Zählrate für die mittlere Weglänge $x_{eff} =  \mathrm{(0,022 \pm 0,004) m}$.

Für die zweite Messreihe für einen Quelle/Präparat-Abstand von $\mathrm{d = 0,04 \,m}$ wird identisch vorgegangen. 

Die mittlere Weglänge wird über den Wert 21877 für die Zählrate und der Regressionsfunktion

\begin{equation}
\mathrm{f(x) = (-4,4 \pm 0,5) \cdot 10^6 \frac{1}{m \cdot s} \cdot x + (127055 \pm 13490)s^{-1}}
\end{equation}
bestimmt. Sie lautet: $x_{eff} =  \mathrm{(0,024 \pm 0,004) m}$.

\begin{figure}[htbp]
\centering{\includegraphics[width=13cm, height=8cm]{Bilder/Bild5.png}}
\caption{Zählraten der $\alpha$-Teilchen bei einem Abstand d = 4,0 cm}
\label{fig:Abbildung}
\end{figure}

Insgesamt ergibt sich eine gemittelte mittlere Weglänge von
\begin{equation}
x_{eff} = (0,023 \pm 0,007) \mathrm{m}.
\end{equation}


\newpage
\subsection{Energie der $\alpha$-Teilchen als Funktion der effektiven Länge}
In diesem Abschnitt wird die Energie der $\alpha$-Teilchen als Funktion der effektiven Länge gezeichnet. Durch eine Ausgleichsrechnung wird ein lineares Polynom ermittelt, welches mind. einmal differenzierbar ist. Durch Ableiten des Polynoms ergibt sich der Energieverlust $\mathrm{-\frac{dE}{dx}}$. In Abb.6 ist die Energie gegen die effektive Länge für die alpha-Teilchen für einen Abstand $2,8\,$ cm aufgetragen.

\begin{figure}[htbp]
\centering{\includegraphics[width=15cm, height=9cm]{Bilder/Bild6.png}}
\caption{Energien der $\alpha$-Teilchen bei einem Abstand d = 2,8 cm}
\label{fig:Abbildung}
\end{figure}

Die Funktion der Regressionsgeraden lautet
\begin{equation}
\mathrm{E(x) = (-97,7 \pm 0,1) \frac{MeV}{m} \cdot x + 4 MeV}.
\end{equation}

Durch einmaliges Ableiten nach der Variablen $\mathrm{x}$ ergibt sich daraus der gesuchte Energieverlust
\begin{equation}
\mathrm{-\frac{dE}{dx} = (97,7 \mp 0,1) \frac{MeV}{m}}.
\end{equation}

Mit dieser Formel lässt sich aus der ermittelten effektiven Weglänge $x_{eff} = \mathrm{0,022 {m}}$ für diesen Abstand die zugehörige Energie bestimmen. Diese lautet
\begin{equation}
\mathrm{E(0,022 \, m) = (-97,7 \pm 0,1) \frac{MeV}{m}\cdot 0,022 \, m + 4 \, MeV = (1,85 \pm 0,34) MeV}. 
\end{equation}

Für den zweiten Abstand $\mathrm{d = 4,0\, cm}$ errechnet sich der Energieverlust auf dieselbe Weise. In dem folgenden Diagramm sind die Messwerte für diesen Abstand aufgetragen. Die Funktion der Regressionsgerade ergibt sich zu
\begin{equation}
\mathrm{E(x) = (-97,6 \pm 0,1) \frac{MeV}{m} \cdot x + 4 MeV}.
\end{equation}

Daraus ergibt sich folgender Energieverlust:

\begin{equation}
\mathrm{-\frac{dE}{dx} = (97,6 \mp 0,1) \frac{MeV}{m}}.
\end{equation}

Auch hier ist es möglich den Energiewert zur effektiven Weglänge $x_{eff} = \mathrm{0,024 {m}}$ für den Abstand $\mathrm{d = 0,04 \, m}$ = zubestimmen. Die Energie beträgt
\begin{equation}
\mathrm{E(0,024 \, m) = (-97,6 \pm 0,1) \frac{MeV}{m}\cdot 0,024 \, m + 4 \, MeV = (1,66 \pm 0,28) MeV}. 
\end{equation}

Für die aus den beiden Abständen gemittelte effektive Weglänge $x_{eff} = \mathrm{(0,023 \pm 0,007) \, m}$ beträgt die Energie $\mathrm{E = (1,75 \pm 0,32) \, MeV}$ 

\begin{figure}[htbp]
\centering{\includegraphics[width=15cm, height=9cm]{Bilder/Bild7.png}}
\caption{Energien der $\alpha$-Teilchen bei einem Abstand d = 4,0 cm}
\label{fig:Abbildung}
\end{figure}

\clearpage
\subsection{Statistik des radioaktiven Zerfalls}
Um die Statistik des $\alpha$-Zerfalls zu untersuchen, werden die gesammelten Daten, die in Tab.3 aufgelistet sind, mit Matlab als Histogramm geplottet.

\begin{table}[htbp]
\centering\begin{tabular}{c|c|c|c}
\multicolumn{4}{c}{\textbf{Zerfälle $\mathrm{[0.1\, s^{-1}]} $}}\\
\hline
3482	&3517	&3347	&3435 \\
3526	&3383	&3202	&3466 \\
3273	&3321	&3355	&3314 \\
3548	&3416	&3499	&3426 \\
3324	&3571	&3478	&3358 \\
3482	&3273	&3486	&3568 \\
3477	&3206	&3284	&3536 \\
3460	&3616	&3350	&3235 \\
3560	&3395	&3430	&3453 \\
3422	&3482	&3479	&3274 \\
3343	&3448	&3414	&3508 \\
3459	&3413	&3339	&3568 \\
3480	&3440	&3293	&3464 \\
3274	&3453	&3271	&3376 \\
3456	&3380	&3402	&3373 \\
3441	&3408	&3638	&3398 \\
3540	&3367	&3261	&3344 \\
3419	&3485	&3416	&3545 \\
3260	&3293	&3451	&3296 \\
3371	&3291	&3675	&3360 \\
3500	&3285	&3559	&3383 \\
3416	&3361	&3543	&3419 \\ 
3354	&3389	&3329	&3415 \\
3260	&3437	&3361	&3455 \\
3567	&3487	&3456	&3524 
\end{tabular}
\caption{Messdaten zur Statistik des $\alpha$-Zerfalls}
\end{table}

\begin{figure}[htbp]
\centering{\includegraphics[width=15cm, height=9cm]{Bilder/Bild8.png}}
\caption{Histogramm der Zerfallsraten mit Gauß-Verteilung}
\label{fig:Abbildung}
\end{figure}

\begin{figure}[htbp]
\centering{\includegraphics[width=15cm, height=9cm]{Bilder/Bild9.png}}
\caption{Histogramm der Zerfallsraten mit Poisson-Verteilung}
\label{fig:Abbildung}
\end{figure}

Der Mittelwert dieser Datenreihe lautet $\mathrm{\overline{x} = 3416,22 \frac{1}{10\,s}}$ und die Varianz $\mathrm{\sigma^2 = 9875,75 \frac{1}{10\,s}}$.

\newpage
\section{Diskussion}
Die Reichweite von $\alpha$-Strahlung beträgt bei Normaldruck in etwa 10 cm, bei niedrigem Luftdruck allerdings deutlich weniger. Ebenso ist die Reichweite energieabhängig, so dass nicht genau gesagt werden kann ob die experimentell ermittelten Werte perfekt passen. Allerdings beträgt die Abweichung zwischen den beiden effektiven Weglängen nur knapp $9\, \%$. Da beide Messreihen unter selben Bedingungen durchgeführt wurden lässt sich die Abweichung wohl auf Fehler beim Ablesen des Barometers oder auf statistische Schwankungen des Zerfallsprozesses zurückführen. Anhand der letzten beiden Abb. 8 und 9 ist eine statistische Verteilung des Zerfalls bei $\alpha$-Teilchen ersichtlich. Allerdings ist aufgrund der Anzahl an Messungen die Annäherung an eine Poisson- bzw. Gauß-Verteilung nicht sehr hoch. Laut Messdaten handelt es sich bei dem $\alpha$-Zerfall um eine Gauß-Verteilung, obwohl dieser eigentlich Poisson-verteilt ist.

\section{Literaturverzeichnis}
\begin{enumerate}[{[}1{]}]
\item TU Dortmund, Physikalisches Anfängerpraktikum: Skript zum Versuch Nummer 701, Reichweite von $\alpha$-Strahlung
\end{enumerate}
\end{document}


